import liste.*;
import tests.*;

import org.junit.Before;

/**
 * Die Test-Klasse ListTest.
 *
 * @author  Rainer Helfrich
 * @version 3.10.2020
 */
public class ListTest extends Tester
{
    /**
     *  Setzt das Testgerüst fuer den Test.
     *
     * Wird vor jeder Testfall-Methode aufgerufen.
     */
    @Before
    public void setUp()
    {
        theTestList = new List<Integer>();
    }
}
