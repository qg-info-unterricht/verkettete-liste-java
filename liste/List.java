package liste;

/**
 * Klasse List 
 * 
 * @author Rainer Helfrich
 * @author Frank Schiebel
 * @version Oktober 2021
 */
public class List<T>
{
    
    /**
     * Der erste Knoten der Liste
     */
    private Node<T> first;
    

    /**
     * Konstruktor
     * 
     */
    public List() {
        
    }
    
    /**
     * Gibt die Anzahl der Elemente der Liste zurück
     */
    public int length() {
        
        return 0;
        
    }

    /**
     * Gibt den n-ten Wert (0-basierte Zählweise) der Liste zurück.
     * @param n Der Index des gewünschten Elements
     * @return Den n-ten Wert
     */
    public T getValueAtN(int n) {
        // getValueAtFIXME: n-tes Element aufsuchen und zurückgeben.
        return null;    
    }
    
    /**
     * Hängt einen neuen Wert hinten an die Liste an.
     * @param val Der anzuhängende Wert
     */
    public void append(T val) {
        
    }

    /**
     * Fügt einen neuen Wert an einer gewünschten Stelle in der Liste ein.
     * @param index Die Stelle, an der der neue Wert stehen soll (0 <= index <= laenge())
     * @param val Der einzufügende Wert
     */
    public void insertAt(int index, T val) {
        
    }

    /**
     * Gibt zurück, ob ein Wert sich in der Liste befindet
     * @param val Der zu suchende Wert
     * @return true, wenn der Wert enthalten ist; false sonst
     */
    public boolean hasValue(T val) {
        return true;
    }

    /**
     * Entfernt das Element, das am gegebenen Index steht, aus der Liste.
     * @param index Die Stelle, von der der Wert entfernt werden soll.
     */
    public void removeAt(int index) {
        
    }

    /**
     * Gibt zurück, ob die Liste leer ist.
     * @return true, wenn die Liste keine Elemente enthält; false sonst
     */
    public boolean isEmpty() {
        return false;
    }
}
