package tests;

import static org.junit.Assert.*;

import liste.*;

/**
 * Die Test-Klasse SubTest5.
 *
 * @author Rainer Helfrich
 * @version 03.10.2020
 */
public class SubTest5 {


    public static void test1Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(86);
        Tester.verifyListContents(theTestList, new int[]{86});

        assertTrue("Fehler: Element 86 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(86));
        theTestList.append(145);
        Tester.verifyListContents(theTestList, new int[]{86, 145});

        assertTrue("Fehler: Element 86 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(86));
        assertFalse("Fehler: Element 169 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(169));
        theTestList.append(788);
        Tester.verifyListContents(theTestList, new int[]{86, 145, 788});

        assertFalse("Fehler: Element 477 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(477));
        assertTrue("Fehler: Element 788 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(788));
        theTestList.insertAt(1, 991);
        Tester.verifyListContents(theTestList, new int[]{86, 991, 145, 788});

        assertTrue("Fehler: Element 788 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(788));
        theTestList.insertAt(4, 931);
        Tester.verifyListContents(theTestList, new int[]{86, 991, 145, 788, 931});

        assertFalse("Fehler: Element 611 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(611));
        theTestList.insertAt(0, 201);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 991, 145, 788, 931});

        theTestList.insertAt(2, 153);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 991, 145, 788, 931});

        assertTrue("Fehler: Element 931 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(931));
        assertFalse("Fehler: Element 695 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(695));
        theTestList.append(69);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 991, 145, 788, 931, 69});

        theTestList.insertAt(7, 713);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 991, 145, 788, 931, 713, 69});

        theTestList.append(297);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 991, 145, 788, 931, 713, 69, 297});

        theTestList.insertAt(9, 147);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 991, 145, 788, 931, 713, 69, 147, 297});

        theTestList.append(963);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 991, 145, 788, 931, 713, 69, 147, 297, 963});

        theTestList.append(334);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 991, 145, 788, 931, 713, 69, 147, 297, 963, 334});

        theTestList.insertAt(3, 972);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 713, 69, 147, 297, 963, 334});

        theTestList.append(842);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 713, 69, 147, 297, 963, 334, 842});

        theTestList.append(267);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 713, 69, 147, 297, 963, 334, 842, 267});

        assertFalse("Fehler: Element 682 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(682));
        theTestList.insertAt(12, 275);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 713, 69, 147, 297, 275, 963, 334, 842, 267});

        theTestList.append(758);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 713, 69, 147, 297, 275, 963, 334, 842, 267, 758});

        theTestList.append(341);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 713, 69, 147, 297, 275, 963, 334, 842, 267, 758, 341});

        theTestList.append(233);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 713, 69, 147, 297, 275, 963, 334, 842, 267, 758, 341, 233});

        theTestList.append(819);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 713, 69, 147, 297, 275, 963, 334, 842, 267, 758, 341, 233, 819});

        theTestList.insertAt(14, 702);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 713, 69, 147, 297, 275, 963, 702, 334, 842, 267, 758, 341, 233, 819});

        theTestList.append(853);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 713, 69, 147, 297, 275, 963, 702, 334, 842, 267, 758, 341, 233, 819, 853});

        assertTrue("Fehler: Element 334 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(334));
        assertFalse("Fehler: Element 401 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(401));
        theTestList.insertAt(8, 859);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 275, 963, 702, 334, 842, 267, 758, 341, 233, 819, 853});

        assertFalse("Fehler: Element 623 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(623));
        assertTrue("Fehler: Element 275 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(275));
        assertFalse("Fehler: Element 645 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(645));
        theTestList.append(804);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 275, 963, 702, 334, 842, 267, 758, 341, 233, 819, 853, 804});

        assertTrue("Fehler: Element 233 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(233));
        theTestList.insertAt(23, 59);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 275, 963, 702, 334, 842, 267, 758, 341, 233, 819, 59, 853, 804});

        theTestList.insertAt(23, 285);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 275, 963, 702, 334, 842, 267, 758, 341, 233, 819, 285, 59, 853, 804});

        theTestList.insertAt(18, 283);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804});

        theTestList.append(305);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305});

        theTestList.append(675);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675});

        theTestList.append(547);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547});

        assertFalse("Fehler: Element 589 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(589));
        theTestList.append(972);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972});

        assertFalse("Fehler: Element 453 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(453));
        theTestList.append(991);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 991});

        theTestList.insertAt(13, 197);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 197, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 991});

        theTestList.insertAt(33, 360);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 197, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 360, 991});

        assertFalse("Fehler: Element 744 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(744));
        theTestList.append(177);
        Tester.verifyListContents(theTestList, new int[]{201, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 197, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 360, 991, 177});

        assertFalse("Fehler: Element 497 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(497));
        theTestList.insertAt(1, 31);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 197, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 360, 991, 177});

        theTestList.insertAt(34, 934);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 197, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 934, 360, 991, 177});

        theTestList.append(770);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 197, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 934, 360, 991, 177, 770});

        theTestList.append(339);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 197, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 934, 360, 991, 177, 770, 339});

        theTestList.append(513);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 197, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 934, 360, 991, 177, 770, 339, 513});

        assertFalse("Fehler: Element 486 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(486));
        theTestList.append(937);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 197, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 934, 360, 991, 177, 770, 339, 513, 937});

        theTestList.append(348);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 197, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 934, 360, 991, 177, 770, 339, 513, 937, 348});

        theTestList.insertAt(34, 254);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 197, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348});

        assertFalse("Fehler: Element 402 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(402));
        theTestList.insertAt(14, 211);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 211, 197, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348});

        theTestList.append(302);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 153, 972, 991, 145, 788, 931, 859, 713, 69, 147, 297, 211, 197, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 302});

        theTestList.insertAt(9, 951);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 153, 972, 991, 145, 788, 931, 951, 859, 713, 69, 147, 297, 211, 197, 275, 963, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 302});

        theTestList.insertAt(19, 655);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 153, 972, 991, 145, 788, 931, 951, 859, 713, 69, 147, 297, 211, 197, 275, 963, 655, 702, 334, 842, 283, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 302});

        assertFalse("Fehler: Element 557 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(557));
        theTestList.insertAt(24, 522);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 153, 972, 991, 145, 788, 931, 951, 859, 713, 69, 147, 297, 211, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 302});

        theTestList.insertAt(7, 738);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 302});

        theTestList.append(972);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 302, 972});

        theTestList.insertAt(3, 36);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 36, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 302, 972});

        theTestList.insertAt(50, 922);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 36, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972});

        theTestList.append(834);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 36, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834});

        theTestList.append(269);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 36, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269});

        assertFalse("Fehler: Element 784 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(784));
        theTestList.append(894);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 36, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894});

        assertFalse("Fehler: Element 442 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(442));
        theTestList.append(226);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 36, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226});

        assertFalse("Fehler: Element 883 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(883));
        theTestList.append(495);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 36, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226, 495});

        theTestList.insertAt(18, 808);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 86, 36, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 808, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226, 495});

        theTestList.insertAt(2, 811);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 811, 86, 36, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 808, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226, 495});

        theTestList.append(462);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 811, 86, 36, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 808, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226, 495, 462});

        theTestList.insertAt(30, 809);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 811, 86, 36, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 808, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 809, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226, 495, 462});

        assertFalse("Fehler: Element 475 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(475));
        theTestList.append(52);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 811, 86, 36, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 808, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 809, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226, 495, 462, 52});

        assertTrue("Fehler: Element 972 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(972));
        theTestList.insertAt(46, 982);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 811, 86, 36, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 808, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 809, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 982, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226, 495, 462, 52});

        theTestList.insertAt(5, 117);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 811, 86, 36, 117, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 808, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 809, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 982, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226, 495, 462, 52});

        theTestList.append(347);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 811, 86, 36, 117, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 808, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 809, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 982, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226, 495, 462, 52, 347});

        theTestList.append(867);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 811, 86, 36, 117, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 808, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 809, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 982, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226, 495, 462, 52, 347, 867});

        theTestList.insertAt(20, 778);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 811, 86, 36, 117, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 778, 808, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 809, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 982, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226, 495, 462, 52, 347, 867});

        theTestList.append(822);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 811, 86, 36, 117, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 778, 808, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 809, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 982, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226, 495, 462, 52, 347, 867, 822});

        theTestList.insertAt(65, 295);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 811, 86, 36, 117, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 778, 808, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 809, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 982, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226, 495, 462, 295, 52, 347, 867, 822});

        theTestList.append(849);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 811, 86, 36, 117, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 778, 808, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 809, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 982, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226, 495, 462, 295, 52, 347, 867, 822, 849});

        theTestList.append(447);
        Tester.verifyListContents(theTestList, new int[]{201, 31, 811, 86, 36, 117, 153, 972, 991, 145, 738, 788, 931, 951, 859, 713, 69, 147, 297, 211, 778, 808, 197, 275, 963, 655, 702, 334, 842, 283, 522, 267, 809, 758, 341, 233, 819, 285, 59, 853, 804, 305, 675, 547, 972, 254, 934, 360, 982, 991, 177, 770, 339, 513, 937, 348, 922, 302, 972, 834, 269, 894, 226, 495, 462, 295, 52, 347, 867, 822, 849, 447});

    }

    public static void test2Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(688);
        Tester.verifyListContents(theTestList, new int[]{688});

        theTestList.insertAt(1, 958);
        Tester.verifyListContents(theTestList, new int[]{688, 958});

        theTestList.append(672);
        Tester.verifyListContents(theTestList, new int[]{688, 958, 672});

        assertTrue("Fehler: Element 672 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(672));
        theTestList.append(816);
        Tester.verifyListContents(theTestList, new int[]{688, 958, 672, 816});

        theTestList.append(797);
        Tester.verifyListContents(theTestList, new int[]{688, 958, 672, 816, 797});

        assertTrue("Fehler: Element 797 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(797));
        theTestList.insertAt(0, 316);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 958, 672, 816, 797});

        theTestList.append(469);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 958, 672, 816, 797, 469});

        assertFalse("Fehler: Element 549 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(549));
        theTestList.insertAt(3, 408);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 958, 408, 672, 816, 797, 469});

        theTestList.append(271);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 958, 408, 672, 816, 797, 469, 271});

        theTestList.append(679);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 958, 408, 672, 816, 797, 469, 271, 679});

        theTestList.insertAt(8, 18);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 958, 408, 672, 816, 797, 469, 18, 271, 679});

        theTestList.insertAt(9, 899);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 958, 408, 672, 816, 797, 469, 18, 899, 271, 679});

        theTestList.insertAt(12, 761);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 958, 408, 672, 816, 797, 469, 18, 899, 271, 679, 761});

        theTestList.insertAt(8, 183);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 958, 408, 672, 816, 797, 469, 183, 18, 899, 271, 679, 761});

        theTestList.insertAt(14, 112);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 958, 408, 672, 816, 797, 469, 183, 18, 899, 271, 679, 761, 112});

        theTestList.insertAt(12, 239);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 958, 408, 672, 816, 797, 469, 183, 18, 899, 271, 239, 679, 761, 112});

        theTestList.insertAt(2, 952);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 672, 816, 797, 469, 183, 18, 899, 271, 239, 679, 761, 112});

        assertTrue("Fehler: Element 316 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(316));
        assertFalse("Fehler: Element 136 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(136));
        assertFalse("Fehler: Element 802 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(802));
        theTestList.insertAt(16, 235);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 672, 816, 797, 469, 183, 18, 899, 271, 239, 679, 761, 235, 112});

        theTestList.append(411);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 672, 816, 797, 469, 183, 18, 899, 271, 239, 679, 761, 235, 112, 411});

        theTestList.insertAt(5, 583);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 797, 469, 183, 18, 899, 271, 239, 679, 761, 235, 112, 411});

        theTestList.insertAt(12, 187);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 797, 469, 183, 18, 187, 899, 271, 239, 679, 761, 235, 112, 411});

        theTestList.append(717);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 797, 469, 183, 18, 187, 899, 271, 239, 679, 761, 235, 112, 411, 717});

        theTestList.insertAt(18, 82);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 797, 469, 183, 18, 187, 899, 271, 239, 679, 761, 82, 235, 112, 411, 717});

        theTestList.append(177);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 797, 469, 183, 18, 187, 899, 271, 239, 679, 761, 82, 235, 112, 411, 717, 177});

        theTestList.append(814);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 797, 469, 183, 18, 187, 899, 271, 239, 679, 761, 82, 235, 112, 411, 717, 177, 814});

        assertFalse("Fehler: Element 364 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(364));
        assertFalse("Fehler: Element 714 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(714));
        theTestList.append(487);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 797, 469, 183, 18, 187, 899, 271, 239, 679, 761, 82, 235, 112, 411, 717, 177, 814, 487});

        theTestList.insertAt(17, 688);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 797, 469, 183, 18, 187, 899, 271, 239, 679, 688, 761, 82, 235, 112, 411, 717, 177, 814, 487});

        theTestList.append(205);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 797, 469, 183, 18, 187, 899, 271, 239, 679, 688, 761, 82, 235, 112, 411, 717, 177, 814, 487, 205});

        theTestList.insertAt(11, 999);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 797, 469, 183, 999, 18, 187, 899, 271, 239, 679, 688, 761, 82, 235, 112, 411, 717, 177, 814, 487, 205});

        assertFalse("Fehler: Element 101 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(101));
        theTestList.append(194);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 797, 469, 183, 999, 18, 187, 899, 271, 239, 679, 688, 761, 82, 235, 112, 411, 717, 177, 814, 487, 205, 194});

        theTestList.insertAt(19, 690);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 797, 469, 183, 999, 18, 187, 899, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 205, 194});

        theTestList.insertAt(8, 735);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 735, 797, 469, 183, 999, 18, 187, 899, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 205, 194});

        assertFalse("Fehler: Element 68 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(68));
        theTestList.append(812);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 735, 797, 469, 183, 999, 18, 187, 899, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 205, 194, 812});

        assertFalse("Fehler: Element 957 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(957));
        theTestList.insertAt(16, 204);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 735, 797, 469, 183, 999, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 205, 194, 812});

        theTestList.append(563);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 735, 797, 469, 183, 999, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 205, 194, 812, 563});

        theTestList.insertAt(13, 597);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 735, 797, 469, 183, 999, 597, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 205, 194, 812, 563});

        assertFalse("Fehler: Element 242 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(242));
        theTestList.append(323);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 735, 797, 469, 183, 999, 597, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 205, 194, 812, 563, 323});

        theTestList.insertAt(32, 892);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 735, 797, 469, 183, 999, 597, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 205, 194, 812, 563, 323});

        assertFalse("Fehler: Element 366 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(366));
        theTestList.append(77);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 735, 797, 469, 183, 999, 597, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 205, 194, 812, 563, 323, 77});

        theTestList.append(237);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 735, 797, 469, 183, 999, 597, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 205, 194, 812, 563, 323, 77, 237});

        theTestList.append(781);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 735, 797, 469, 183, 999, 597, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 205, 194, 812, 563, 323, 77, 237, 781});

        theTestList.insertAt(14, 286);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 735, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 205, 194, 812, 563, 323, 77, 237, 781});

        theTestList.insertAt(9, 290);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 408, 583, 672, 816, 735, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 205, 194, 812, 563, 323, 77, 237, 781});

        theTestList.insertAt(4, 279);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 205, 194, 812, 563, 323, 77, 237, 781});

        theTestList.append(18);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 205, 194, 812, 563, 323, 77, 237, 781, 18});

        assertTrue("Fehler: Element 408 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(408));
        theTestList.insertAt(36, 228);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 228, 205, 194, 812, 563, 323, 77, 237, 781, 18});

        theTestList.append(290);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 228, 205, 194, 812, 563, 323, 77, 237, 781, 18, 290});

        theTestList.append(576);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 228, 205, 194, 812, 563, 323, 77, 237, 781, 18, 290, 576});

        theTestList.append(604);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 228, 205, 194, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604});

        theTestList.insertAt(39, 920);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604});

        assertTrue("Fehler: Element 604 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(604));
        theTestList.append(958);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958});

        assertFalse("Fehler: Element 842 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(842));
        theTestList.insertAt(10, 320);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958});

        theTestList.append(252);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252});

        theTestList.append(646);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646});

        theTestList.insertAt(33, 813);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646});

        theTestList.append(767);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767});

        assertFalse("Fehler: Element 389 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(389));
        theTestList.append(161);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161});

        assertTrue("Fehler: Element 767 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(767));
        assertTrue("Fehler: Element 194 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(194));
        theTestList.insertAt(29, 971);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 971, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161});

        theTestList.append(459);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 971, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161, 459});

        assertTrue("Fehler: Element 583 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(583));
        theTestList.append(35);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 971, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161, 459, 35});

        assertFalse("Fehler: Element 501 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(501));
        theTestList.append(191);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 971, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161, 459, 35, 191});

        theTestList.append(606);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 971, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161, 459, 35, 191, 606});

        theTestList.append(556);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 971, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161, 459, 35, 191, 606, 556});

        theTestList.append(812);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 971, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161, 459, 35, 191, 606, 556, 812});

        theTestList.append(993);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 971, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161, 459, 35, 191, 606, 556, 812, 993});

        assertTrue("Fehler: Element 812 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(812));
        assertFalse("Fehler: Element 507 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(507));
        theTestList.insertAt(4, 475);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 475, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 971, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161, 459, 35, 191, 606, 556, 812, 993});

        theTestList.append(619);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 475, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 239, 679, 688, 690, 761, 82, 971, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161, 459, 35, 191, 606, 556, 812, 993, 619});

        assertFalse("Fehler: Element 123 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(123));
        theTestList.insertAt(24, 382);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 475, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 382, 239, 679, 688, 690, 761, 82, 971, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161, 459, 35, 191, 606, 556, 812, 993, 619});

        theTestList.append(749);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 475, 279, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 382, 239, 679, 688, 690, 761, 82, 971, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161, 459, 35, 191, 606, 556, 812, 993, 619, 749});

        assertTrue("Fehler: Element 320 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(320));
        theTestList.insertAt(6, 380);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 475, 279, 380, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 382, 239, 679, 688, 690, 761, 82, 971, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161, 459, 35, 191, 606, 556, 812, 993, 619, 749});

        theTestList.insertAt(48, 639);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 475, 279, 380, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 382, 239, 679, 688, 690, 761, 82, 971, 235, 112, 411, 717, 813, 177, 814, 487, 892, 228, 205, 194, 920, 812, 563, 639, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161, 459, 35, 191, 606, 556, 812, 993, 619, 749});

        assertTrue("Fehler: Element 77 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(77));
        assertFalse("Fehler: Element 392 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(392));
        assertFalse("Fehler: Element 817 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(817));
        theTestList.insertAt(39, 49);
        Tester.verifyListContents(theTestList, new int[]{316, 688, 952, 958, 475, 279, 380, 408, 583, 672, 816, 735, 320, 290, 797, 469, 183, 999, 597, 286, 18, 187, 899, 204, 271, 382, 239, 679, 688, 690, 761, 82, 971, 235, 112, 411, 717, 813, 177, 49, 814, 487, 892, 228, 205, 194, 920, 812, 563, 639, 323, 77, 237, 781, 18, 290, 576, 604, 958, 252, 646, 767, 161, 459, 35, 191, 606, 556, 812, 993, 619, 749});

    }

    public static void test3Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(328);
        Tester.verifyListContents(theTestList, new int[]{328});

        theTestList.insertAt(1, 224);
        Tester.verifyListContents(theTestList, new int[]{328, 224});

        theTestList.append(431);
        Tester.verifyListContents(theTestList, new int[]{328, 224, 431});

        theTestList.append(936);
        Tester.verifyListContents(theTestList, new int[]{328, 224, 431, 936});

        theTestList.insertAt(1, 399);
        Tester.verifyListContents(theTestList, new int[]{328, 399, 224, 431, 936});

        theTestList.append(424);
        Tester.verifyListContents(theTestList, new int[]{328, 399, 224, 431, 936, 424});

        theTestList.insertAt(3, 844);
        Tester.verifyListContents(theTestList, new int[]{328, 399, 224, 844, 431, 936, 424});

        theTestList.append(486);
        Tester.verifyListContents(theTestList, new int[]{328, 399, 224, 844, 431, 936, 424, 486});

        theTestList.append(60);
        Tester.verifyListContents(theTestList, new int[]{328, 399, 224, 844, 431, 936, 424, 486, 60});

        theTestList.append(581);
        Tester.verifyListContents(theTestList, new int[]{328, 399, 224, 844, 431, 936, 424, 486, 60, 581});

        theTestList.append(664);
        Tester.verifyListContents(theTestList, new int[]{328, 399, 224, 844, 431, 936, 424, 486, 60, 581, 664});

        theTestList.insertAt(4, 702);
        Tester.verifyListContents(theTestList, new int[]{328, 399, 224, 844, 702, 431, 936, 424, 486, 60, 581, 664});

        theTestList.insertAt(8, 488);
        Tester.verifyListContents(theTestList, new int[]{328, 399, 224, 844, 702, 431, 936, 424, 488, 486, 60, 581, 664});

        theTestList.append(845);
        Tester.verifyListContents(theTestList, new int[]{328, 399, 224, 844, 702, 431, 936, 424, 488, 486, 60, 581, 664, 845});

        theTestList.append(725);
        Tester.verifyListContents(theTestList, new int[]{328, 399, 224, 844, 702, 431, 936, 424, 488, 486, 60, 581, 664, 845, 725});

        theTestList.append(830);
        Tester.verifyListContents(theTestList, new int[]{328, 399, 224, 844, 702, 431, 936, 424, 488, 486, 60, 581, 664, 845, 725, 830});

        theTestList.insertAt(5, 728);
        Tester.verifyListContents(theTestList, new int[]{328, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 725, 830});

        assertTrue("Fehler: Element 936 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(936));
        theTestList.insertAt(0, 798);
        Tester.verifyListContents(theTestList, new int[]{798, 328, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 725, 830});

        assertTrue("Fehler: Element 725 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(725));
        theTestList.insertAt(0, 786);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 328, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 725, 830});

        theTestList.append(816);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 328, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 725, 830, 816});

        assertTrue("Fehler: Element 424 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(424));
        theTestList.insertAt(3, 157);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 328, 157, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 725, 830, 816});

        assertFalse("Fehler: Element 528 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(528));
        assertFalse("Fehler: Element 540 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(540));
        theTestList.insertAt(20, 869);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 328, 157, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 725, 830, 869, 816});

        theTestList.append(954);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 328, 157, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 725, 830, 869, 816, 954});

        theTestList.insertAt(18, 153);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 328, 157, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954});

        theTestList.append(633);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 328, 157, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954, 633});

        assertTrue("Fehler: Element 830 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(830));
        theTestList.insertAt(4, 946);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 328, 157, 946, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954, 633});

        theTestList.insertAt(3, 736);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954, 633});

        assertTrue("Fehler: Element 60 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(60));
        theTestList.append(665);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954, 633, 665});

        theTestList.append(851);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954, 633, 665, 851});

        assertFalse("Fehler: Element 305 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(305));
        theTestList.append(312);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954, 633, 665, 851, 312});

        theTestList.append(595);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954, 633, 665, 851, 312, 595});

        theTestList.insertAt(2, 213);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954, 633, 665, 851, 312, 595});

        theTestList.append(247);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954, 633, 665, 851, 312, 595, 247});

        theTestList.insertAt(29, 294);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954, 633, 665, 294, 851, 312, 595, 247});

        theTestList.append(536);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954, 633, 665, 294, 851, 312, 595, 247, 536});

        theTestList.append(322);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954, 633, 665, 294, 851, 312, 595, 247, 536, 322});

        theTestList.append(524);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954, 633, 665, 294, 851, 312, 595, 247, 536, 322, 524});

        theTestList.append(852);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 936, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954, 633, 665, 294, 851, 312, 595, 247, 536, 322, 524, 852});

        assertTrue("Fehler: Element 399 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(399));
        theTestList.insertAt(14, 181);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 936, 181, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 869, 816, 954, 633, 665, 294, 851, 312, 595, 247, 536, 322, 524, 852});

        theTestList.insertAt(25, 585);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 936, 181, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 294, 851, 312, 595, 247, 536, 322, 524, 852});

        assertTrue("Fehler: Element 852 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(852));
        theTestList.append(121);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 936, 181, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 294, 851, 312, 595, 247, 536, 322, 524, 852, 121});

        assertFalse("Fehler: Element 343 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(343));
        theTestList.insertAt(12, 431);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 702, 728, 431, 431, 936, 181, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 294, 851, 312, 595, 247, 536, 322, 524, 852, 121});

        theTestList.insertAt(10, 493);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 294, 851, 312, 595, 247, 536, 322, 524, 852, 121});

        theTestList.append(539);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 294, 851, 312, 595, 247, 536, 322, 524, 852, 121, 539});

        assertFalse("Fehler: Element 532 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(532));
        theTestList.insertAt(33, 563);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 294, 851, 312, 595, 247, 536, 322, 524, 852, 121, 539});

        theTestList.append(980);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 294, 851, 312, 595, 247, 536, 322, 524, 852, 121, 539, 980});

        assertTrue("Fehler: Element 328 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(328));
        theTestList.insertAt(43, 856);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 121, 539, 980});

        theTestList.append(167);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 121, 539, 980, 167});

        theTestList.insertAt(34, 700);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 121, 539, 980, 167});

        theTestList.insertAt(48, 977);
        Tester.verifyListContents(theTestList, new int[]{786, 798, 213, 328, 736, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 121, 539, 980, 977, 167});

        assertTrue("Fehler: Element 845 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(845));
        theTestList.insertAt(1, 592);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 488, 486, 60, 581, 664, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 121, 539, 980, 977, 167});

        assertTrue("Fehler: Element 830 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(830));
        assertFalse("Fehler: Element 781 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(781));
        theTestList.insertAt(24, 684);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 488, 486, 60, 581, 664, 684, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 121, 539, 980, 977, 167});

        theTestList.insertAt(19, 401);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 401, 488, 486, 60, 581, 664, 684, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 121, 539, 980, 977, 167});

        theTestList.insertAt(24, 737);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 121, 539, 980, 977, 167});

        assertFalse("Fehler: Element 987 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(987));
        theTestList.insertAt(49, 675);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167});

        theTestList.append(347);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347});

        theTestList.insertAt(6, 897);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 897, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347});

        theTestList.append(517);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 897, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517});

        assertTrue("Fehler: Element 665 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(665));
        theTestList.insertAt(28, 793);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 897, 157, 946, 399, 224, 844, 493, 702, 728, 431, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 793, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517});

        theTestList.insertAt(16, 616);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 897, 157, 946, 399, 224, 844, 493, 702, 728, 431, 616, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 793, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517});

        assertTrue("Fehler: Element 224 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(224));
        theTestList.insertAt(13, 279);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 897, 157, 946, 399, 224, 844, 493, 279, 702, 728, 431, 616, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 793, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517});

        theTestList.append(785);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 897, 157, 946, 399, 224, 844, 493, 279, 702, 728, 431, 616, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 793, 845, 153, 725, 830, 585, 869, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517, 785});

        theTestList.insertAt(37, 104);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 897, 157, 946, 399, 224, 844, 493, 279, 702, 728, 431, 616, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 793, 845, 153, 725, 830, 585, 869, 104, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517, 785});

        theTestList.append(398);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 897, 157, 946, 399, 224, 844, 493, 279, 702, 728, 431, 616, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 793, 845, 153, 725, 830, 585, 869, 104, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517, 785, 398});

        assertTrue("Fehler: Element 424 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(424));
        assertFalse("Fehler: Element 602 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(602));
        assertFalse("Fehler: Element 222 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(222));
        theTestList.append(173);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 897, 157, 946, 399, 224, 844, 493, 279, 702, 728, 431, 616, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 793, 845, 153, 725, 830, 585, 869, 104, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517, 785, 398, 173});

        theTestList.insertAt(34, 107);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 897, 157, 946, 399, 224, 844, 493, 279, 702, 728, 431, 616, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 793, 845, 153, 725, 107, 830, 585, 869, 104, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517, 785, 398, 173});

        assertTrue("Fehler: Element 213 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(213));
        theTestList.append(699);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 897, 157, 946, 399, 224, 844, 493, 279, 702, 728, 431, 616, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 793, 845, 153, 725, 107, 830, 585, 869, 104, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517, 785, 398, 173, 699});

        theTestList.insertAt(51, 209);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 897, 157, 946, 399, 224, 844, 493, 279, 702, 728, 431, 616, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 793, 845, 153, 725, 107, 830, 585, 869, 104, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 209, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517, 785, 398, 173, 699});

        theTestList.insertAt(6, 902);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 902, 897, 157, 946, 399, 224, 844, 493, 279, 702, 728, 431, 616, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 793, 845, 153, 725, 107, 830, 585, 869, 104, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 209, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517, 785, 398, 173, 699});

        assertTrue("Fehler: Element 539 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(539));
        assertTrue("Fehler: Element 60 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(60));
        assertTrue("Fehler: Element 946 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(946));
        theTestList.append(119);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 902, 897, 157, 946, 399, 224, 844, 493, 279, 702, 728, 431, 616, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 793, 845, 153, 725, 107, 830, 585, 869, 104, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 209, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517, 785, 398, 173, 699, 119});

        theTestList.insertAt(15, 870);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 902, 897, 157, 946, 399, 224, 844, 493, 279, 870, 702, 728, 431, 616, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 793, 845, 153, 725, 107, 830, 585, 869, 104, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 209, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517, 785, 398, 173, 699, 119});

        assertFalse("Fehler: Element 52 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(52));
        theTestList.append(992);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 902, 897, 157, 946, 399, 224, 844, 493, 279, 870, 702, 728, 431, 616, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 793, 845, 153, 725, 107, 830, 585, 869, 104, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 209, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517, 785, 398, 173, 699, 119, 992});

        theTestList.append(514);
        Tester.verifyListContents(theTestList, new int[]{786, 592, 798, 213, 328, 736, 902, 897, 157, 946, 399, 224, 844, 493, 279, 870, 702, 728, 431, 616, 431, 936, 181, 424, 401, 488, 486, 60, 581, 737, 664, 684, 793, 845, 153, 725, 107, 830, 585, 869, 104, 816, 954, 633, 665, 563, 700, 294, 851, 312, 595, 247, 536, 209, 322, 524, 852, 856, 675, 121, 539, 980, 977, 167, 347, 517, 785, 398, 173, 699, 119, 992, 514});

    }

    public static void test4Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 25);
        Tester.verifyListContents(theTestList, new int[]{25});

        theTestList.append(550);
        Tester.verifyListContents(theTestList, new int[]{25, 550});

        theTestList.append(359);
        Tester.verifyListContents(theTestList, new int[]{25, 550, 359});

        assertFalse("Fehler: Element 571 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(571));
        assertTrue("Fehler: Element 25 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(25));
        theTestList.insertAt(0, 671);
        Tester.verifyListContents(theTestList, new int[]{671, 25, 550, 359});

        theTestList.append(425);
        Tester.verifyListContents(theTestList, new int[]{671, 25, 550, 359, 425});

        theTestList.insertAt(1, 880);
        Tester.verifyListContents(theTestList, new int[]{671, 880, 25, 550, 359, 425});

        assertFalse("Fehler: Element 955 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(955));
        theTestList.insertAt(3, 478);
        Tester.verifyListContents(theTestList, new int[]{671, 880, 25, 478, 550, 359, 425});

        theTestList.insertAt(4, 284);
        Tester.verifyListContents(theTestList, new int[]{671, 880, 25, 478, 284, 550, 359, 425});

        theTestList.insertAt(1, 776);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 880, 25, 478, 284, 550, 359, 425});

        theTestList.insertAt(2, 928);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 359, 425});

        assertFalse("Fehler: Element 174 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(174));
        theTestList.append(395);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 359, 425, 395});

        assertFalse("Fehler: Element 830 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(830));
        assertTrue("Fehler: Element 284 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(284));
        theTestList.append(551);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 359, 425, 395, 551});

        theTestList.append(874);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 359, 425, 395, 551, 874});

        theTestList.append(819);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 359, 425, 395, 551, 874, 819});

        theTestList.insertAt(8, 406);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 406, 359, 425, 395, 551, 874, 819});

        theTestList.insertAt(12, 989);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 406, 359, 425, 395, 989, 551, 874, 819});

        theTestList.append(235);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 406, 359, 425, 395, 989, 551, 874, 819, 235});

        theTestList.insertAt(10, 340);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 406, 359, 340, 425, 395, 989, 551, 874, 819, 235});

        theTestList.insertAt(9, 422);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 406, 422, 359, 340, 425, 395, 989, 551, 874, 819, 235});

        assertFalse("Fehler: Element 529 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(529));
        theTestList.append(140);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 406, 422, 359, 340, 425, 395, 989, 551, 874, 819, 235, 140});

        theTestList.append(186);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 406, 422, 359, 340, 425, 395, 989, 551, 874, 819, 235, 140, 186});

        assertFalse("Fehler: Element 514 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(514));
        theTestList.append(522);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 406, 422, 359, 340, 425, 395, 989, 551, 874, 819, 235, 140, 186, 522});

        assertFalse("Fehler: Element 540 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(540));
        theTestList.append(921);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 406, 422, 359, 340, 425, 395, 989, 551, 874, 819, 235, 140, 186, 522, 921});

        theTestList.insertAt(10, 396);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 406, 422, 396, 359, 340, 425, 395, 989, 551, 874, 819, 235, 140, 186, 522, 921});

        assertFalse("Fehler: Element 938 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(938));
        assertTrue("Fehler: Element 396 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(396));
        theTestList.insertAt(19, 10);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 406, 422, 396, 359, 340, 425, 395, 989, 551, 874, 819, 10, 235, 140, 186, 522, 921});

        theTestList.insertAt(11, 539);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 550, 406, 422, 396, 539, 359, 340, 425, 395, 989, 551, 874, 819, 10, 235, 140, 186, 522, 921});

        theTestList.insertAt(7, 318);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 318, 550, 406, 422, 396, 539, 359, 340, 425, 395, 989, 551, 874, 819, 10, 235, 140, 186, 522, 921});

        assertTrue("Fehler: Element 318 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(318));
        theTestList.insertAt(12, 308);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 318, 550, 406, 422, 396, 308, 539, 359, 340, 425, 395, 989, 551, 874, 819, 10, 235, 140, 186, 522, 921});

        theTestList.append(411);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 318, 550, 406, 422, 396, 308, 539, 359, 340, 425, 395, 989, 551, 874, 819, 10, 235, 140, 186, 522, 921, 411});

        theTestList.insertAt(7, 599);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 284, 599, 318, 550, 406, 422, 396, 308, 539, 359, 340, 425, 395, 989, 551, 874, 819, 10, 235, 140, 186, 522, 921, 411});

        theTestList.insertAt(6, 660);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 318, 550, 406, 422, 396, 308, 539, 359, 340, 425, 395, 989, 551, 874, 819, 10, 235, 140, 186, 522, 921, 411});

        theTestList.insertAt(17, 568);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 425, 395, 989, 551, 874, 819, 10, 235, 140, 186, 522, 921, 411});

        assertFalse("Fehler: Element 499 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(499));
        assertFalse("Fehler: Element 997 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(997));
        theTestList.insertAt(9, 49);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 425, 395, 989, 551, 874, 819, 10, 235, 140, 186, 522, 921, 411});

        theTestList.append(507);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 425, 395, 989, 551, 874, 819, 10, 235, 140, 186, 522, 921, 411, 507});

        assertTrue("Fehler: Element 235 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(235));
        assertFalse("Fehler: Element 89 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(89));
        theTestList.insertAt(34, 393);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 425, 395, 989, 551, 874, 819, 10, 235, 140, 186, 522, 921, 411, 507, 393});

        theTestList.append(836);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 425, 395, 989, 551, 874, 819, 10, 235, 140, 186, 522, 921, 411, 507, 393, 836});

        assertTrue("Fehler: Element 989 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(989));
        assertTrue("Fehler: Element 989 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(989));
        theTestList.append(23);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 425, 395, 989, 551, 874, 819, 10, 235, 140, 186, 522, 921, 411, 507, 393, 836, 23});

        assertTrue("Fehler: Element 422 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(422));
        theTestList.insertAt(31, 621);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 425, 395, 989, 551, 874, 819, 10, 235, 140, 186, 522, 621, 921, 411, 507, 393, 836, 23});

        assertFalse("Fehler: Element 771 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(771));
        theTestList.append(925);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 425, 395, 989, 551, 874, 819, 10, 235, 140, 186, 522, 621, 921, 411, 507, 393, 836, 23, 925});

        theTestList.insertAt(24, 67);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 425, 395, 989, 551, 67, 874, 819, 10, 235, 140, 186, 522, 621, 921, 411, 507, 393, 836, 23, 925});

        theTestList.insertAt(38, 832);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 425, 395, 989, 551, 67, 874, 819, 10, 235, 140, 186, 522, 621, 921, 411, 507, 393, 836, 832, 23, 925});

        assertFalse("Fehler: Element 435 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(435));
        theTestList.append(162);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 425, 395, 989, 551, 67, 874, 819, 10, 235, 140, 186, 522, 621, 921, 411, 507, 393, 836, 832, 23, 925, 162});

        theTestList.insertAt(36, 183);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 425, 395, 989, 551, 67, 874, 819, 10, 235, 140, 186, 522, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162});

        theTestList.insertAt(31, 980);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 425, 395, 989, 551, 67, 874, 819, 10, 235, 140, 186, 980, 522, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162});

        theTestList.append(754);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 425, 395, 989, 551, 67, 874, 819, 10, 235, 140, 186, 980, 522, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754});

        theTestList.append(439);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 425, 395, 989, 551, 67, 874, 819, 10, 235, 140, 186, 980, 522, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439});

        theTestList.insertAt(20, 281);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 281, 425, 395, 989, 551, 67, 874, 819, 10, 235, 140, 186, 980, 522, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439});

        theTestList.append(597);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 281, 425, 395, 989, 551, 67, 874, 819, 10, 235, 140, 186, 980, 522, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597});

        theTestList.insertAt(31, 485);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 281, 425, 395, 989, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 522, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597});

        theTestList.append(351);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 281, 425, 395, 989, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 522, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351});

        assertTrue("Fehler: Element 874 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(874));
        theTestList.insertAt(35, 779);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 281, 425, 395, 989, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 522, 779, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351});

        theTestList.append(405);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 281, 425, 395, 989, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 522, 779, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405});

        theTestList.insertAt(24, 576);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 281, 425, 395, 989, 576, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 522, 779, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405});

        assertTrue("Fehler: Element 406 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(406));
        theTestList.append(575);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 281, 425, 395, 989, 576, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 522, 779, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405, 575});

        assertFalse("Fehler: Element 512 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(512));
        assertFalse("Fehler: Element 190 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(190));
        assertTrue("Fehler: Element 340 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(340));
        theTestList.insertAt(23, 857);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 281, 425, 395, 857, 989, 576, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 522, 779, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405, 575});

        theTestList.append(697);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 281, 425, 395, 857, 989, 576, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 522, 779, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405, 575, 697});

        assertFalse("Fehler: Element 57 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(57));
        theTestList.append(354);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 308, 539, 359, 568, 340, 281, 425, 395, 857, 989, 576, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 522, 779, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405, 575, 697, 354});

        assertTrue("Fehler: Element 597 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(597));
        assertTrue("Fehler: Element 162 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(162));
        theTestList.insertAt(15, 990);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 990, 308, 539, 359, 568, 340, 281, 425, 395, 857, 989, 576, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 522, 779, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405, 575, 697, 354});

        assertFalse("Fehler: Element 139 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(139));
        theTestList.append(207);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 990, 308, 539, 359, 568, 340, 281, 425, 395, 857, 989, 576, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 522, 779, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405, 575, 697, 354, 207});

        assertFalse("Fehler: Element 191 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(191));
        theTestList.insertAt(58, 190);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 990, 308, 539, 359, 568, 340, 281, 425, 395, 857, 989, 576, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 522, 779, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405, 575, 697, 354, 190, 207});

        theTestList.insertAt(37, 358);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 990, 308, 539, 359, 568, 340, 281, 425, 395, 857, 989, 576, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 358, 522, 779, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405, 575, 697, 354, 190, 207});

        assertTrue("Fehler: Element 576 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(576));
        theTestList.insertAt(3, 716);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 716, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 990, 308, 539, 359, 568, 340, 281, 425, 395, 857, 989, 576, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 358, 522, 779, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405, 575, 697, 354, 190, 207});

        theTestList.append(434);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 716, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 990, 308, 539, 359, 568, 340, 281, 425, 395, 857, 989, 576, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 358, 522, 779, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405, 575, 697, 354, 190, 207, 434});

        theTestList.append(845);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 716, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 990, 308, 539, 359, 568, 340, 281, 425, 395, 857, 989, 576, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 358, 522, 779, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405, 575, 697, 354, 190, 207, 434, 845});

        theTestList.append(249);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 716, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 990, 308, 539, 359, 568, 340, 281, 425, 395, 857, 989, 576, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 358, 522, 779, 621, 921, 411, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405, 575, 697, 354, 190, 207, 434, 845, 249});

        theTestList.insertAt(44, 874);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 716, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 990, 308, 539, 359, 568, 340, 281, 425, 395, 857, 989, 576, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 358, 522, 779, 621, 921, 411, 874, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405, 575, 697, 354, 190, 207, 434, 845, 249});

        theTestList.append(557);
        Tester.verifyListContents(theTestList, new int[]{671, 776, 928, 716, 880, 25, 478, 660, 284, 599, 49, 318, 550, 406, 422, 396, 990, 308, 539, 359, 568, 340, 281, 425, 395, 857, 989, 576, 551, 67, 874, 819, 10, 235, 140, 485, 186, 980, 358, 522, 779, 621, 921, 411, 874, 507, 183, 393, 836, 832, 23, 925, 162, 754, 439, 597, 351, 405, 575, 697, 354, 190, 207, 434, 845, 249, 557});

        assertTrue("Fehler: Element 67 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(67));
    }

    public static void test5Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(442);
        Tester.verifyListContents(theTestList, new int[]{442});

        theTestList.insertAt(0, 475);
        Tester.verifyListContents(theTestList, new int[]{475, 442});

        theTestList.append(474);
        Tester.verifyListContents(theTestList, new int[]{475, 442, 474});

        theTestList.append(8);
        Tester.verifyListContents(theTestList, new int[]{475, 442, 474, 8});

        theTestList.append(921);
        Tester.verifyListContents(theTestList, new int[]{475, 442, 474, 8, 921});

        theTestList.append(786);
        Tester.verifyListContents(theTestList, new int[]{475, 442, 474, 8, 921, 786});

        assertFalse("Fehler: Element 885 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(885));
        theTestList.append(570);
        Tester.verifyListContents(theTestList, new int[]{475, 442, 474, 8, 921, 786, 570});

        theTestList.append(263);
        Tester.verifyListContents(theTestList, new int[]{475, 442, 474, 8, 921, 786, 570, 263});

        theTestList.insertAt(1, 865);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 474, 8, 921, 786, 570, 263});

        theTestList.append(315);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 474, 8, 921, 786, 570, 263, 315});

        theTestList.insertAt(3, 128);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 128, 474, 8, 921, 786, 570, 263, 315});

        theTestList.insertAt(3, 892);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 8, 921, 786, 570, 263, 315});

        theTestList.append(502);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 8, 921, 786, 570, 263, 315, 502});

        theTestList.append(626);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 8, 921, 786, 570, 263, 315, 502, 626});

        assertTrue("Fehler: Element 475 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(475));
        assertFalse("Fehler: Element 312 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(312));
        assertTrue("Fehler: Element 892 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(892));
        assertTrue("Fehler: Element 442 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(442));
        assertTrue("Fehler: Element 8 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(8));
        theTestList.insertAt(6, 68);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 263, 315, 502, 626});

        theTestList.append(493);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 263, 315, 502, 626, 493});

        theTestList.append(232);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 263, 315, 502, 626, 493, 232});

        assertTrue("Fehler: Element 263 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(263));
        theTestList.insertAt(11, 923);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 923, 263, 315, 502, 626, 493, 232});

        theTestList.insertAt(18, 467);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 923, 263, 315, 502, 626, 493, 232, 467});

        assertTrue("Fehler: Element 570 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(570));
        assertTrue("Fehler: Element 315 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(315));
        theTestList.append(240);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 923, 263, 315, 502, 626, 493, 232, 467, 240});

        theTestList.insertAt(12, 218);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 923, 218, 263, 315, 502, 626, 493, 232, 467, 240});

        theTestList.append(262);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 923, 218, 263, 315, 502, 626, 493, 232, 467, 240, 262});

        assertFalse("Fehler: Element 879 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(879));
        assertTrue("Fehler: Element 442 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(442));
        assertFalse("Fehler: Element 488 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(488));
        theTestList.insertAt(18, 54);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 923, 218, 263, 315, 502, 626, 493, 54, 232, 467, 240, 262});

        theTestList.append(922);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 923, 218, 263, 315, 502, 626, 493, 54, 232, 467, 240, 262, 922});

        theTestList.insertAt(14, 895);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 923, 218, 263, 895, 315, 502, 626, 493, 54, 232, 467, 240, 262, 922});

        theTestList.append(302);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 923, 218, 263, 895, 315, 502, 626, 493, 54, 232, 467, 240, 262, 922, 302});

        theTestList.insertAt(20, 651);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 923, 218, 263, 895, 315, 502, 626, 493, 54, 651, 232, 467, 240, 262, 922, 302});

        theTestList.append(292);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 923, 218, 263, 895, 315, 502, 626, 493, 54, 651, 232, 467, 240, 262, 922, 302, 292});

        theTestList.append(539);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 923, 218, 263, 895, 315, 502, 626, 493, 54, 651, 232, 467, 240, 262, 922, 302, 292, 539});

        theTestList.insertAt(17, 102);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 474, 68, 8, 921, 786, 570, 923, 218, 263, 895, 315, 502, 102, 626, 493, 54, 651, 232, 467, 240, 262, 922, 302, 292, 539});

        theTestList.insertAt(5, 943);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 923, 218, 263, 895, 315, 502, 102, 626, 493, 54, 651, 232, 467, 240, 262, 922, 302, 292, 539});

        assertFalse("Fehler: Element 72 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(72));
        assertTrue("Fehler: Element 626 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(626));
        assertTrue("Fehler: Element 539 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(539));
        theTestList.append(603);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 923, 218, 263, 895, 315, 502, 102, 626, 493, 54, 651, 232, 467, 240, 262, 922, 302, 292, 539, 603});

        theTestList.append(886);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 923, 218, 263, 895, 315, 502, 102, 626, 493, 54, 651, 232, 467, 240, 262, 922, 302, 292, 539, 603, 886});

        theTestList.append(658);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 923, 218, 263, 895, 315, 502, 102, 626, 493, 54, 651, 232, 467, 240, 262, 922, 302, 292, 539, 603, 886, 658});

        theTestList.append(72);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 923, 218, 263, 895, 315, 502, 102, 626, 493, 54, 651, 232, 467, 240, 262, 922, 302, 292, 539, 603, 886, 658, 72});

        theTestList.insertAt(18, 127);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 923, 218, 263, 895, 315, 502, 127, 102, 626, 493, 54, 651, 232, 467, 240, 262, 922, 302, 292, 539, 603, 886, 658, 72});

        theTestList.append(808);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 923, 218, 263, 895, 315, 502, 127, 102, 626, 493, 54, 651, 232, 467, 240, 262, 922, 302, 292, 539, 603, 886, 658, 72, 808});

        theTestList.insertAt(19, 704);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 651, 232, 467, 240, 262, 922, 302, 292, 539, 603, 886, 658, 72, 808});

        theTestList.insertAt(12, 905);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 651, 232, 467, 240, 262, 922, 302, 292, 539, 603, 886, 658, 72, 808});

        theTestList.insertAt(34, 241);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 651, 232, 467, 240, 262, 922, 302, 292, 539, 241, 603, 886, 658, 72, 808});

        assertFalse("Fehler: Element 801 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(801));
        theTestList.append(286);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 651, 232, 467, 240, 262, 922, 302, 292, 539, 241, 603, 886, 658, 72, 808, 286});

        theTestList.insertAt(25, 802);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 232, 467, 240, 262, 922, 302, 292, 539, 241, 603, 886, 658, 72, 808, 286});

        theTestList.append(20);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 232, 467, 240, 262, 922, 302, 292, 539, 241, 603, 886, 658, 72, 808, 286, 20});

        theTestList.append(374);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 232, 467, 240, 262, 922, 302, 292, 539, 241, 603, 886, 658, 72, 808, 286, 20, 374});

        theTestList.append(579);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 232, 467, 240, 262, 922, 302, 292, 539, 241, 603, 886, 658, 72, 808, 286, 20, 374, 579});

        theTestList.append(917);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 232, 467, 240, 262, 922, 302, 292, 539, 241, 603, 886, 658, 72, 808, 286, 20, 374, 579, 917});

        assertTrue("Fehler: Element 54 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(54));
        assertFalse("Fehler: Element 32 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(32));
        assertTrue("Fehler: Element 241 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(241));
        assertTrue("Fehler: Element 72 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(72));
        theTestList.append(580);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 232, 467, 240, 262, 922, 302, 292, 539, 241, 603, 886, 658, 72, 808, 286, 20, 374, 579, 917, 580});

        theTestList.append(600);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 232, 467, 240, 262, 922, 302, 292, 539, 241, 603, 886, 658, 72, 808, 286, 20, 374, 579, 917, 580, 600});

        theTestList.insertAt(42, 588);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 232, 467, 240, 262, 922, 302, 292, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600});

        theTestList.insertAt(13, 179);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 232, 467, 240, 262, 922, 302, 292, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600});

        assertFalse("Fehler: Element 277 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(277));
        assertFalse("Fehler: Element 654 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(654));
        theTestList.append(521);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 232, 467, 240, 262, 922, 302, 292, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521});

        assertTrue("Fehler: Element 603 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(603));
        theTestList.append(22);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 232, 467, 240, 262, 922, 302, 292, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22});

        theTestList.insertAt(34, 368);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 232, 467, 240, 262, 922, 302, 368, 292, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22});

        theTestList.append(799);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 232, 467, 240, 262, 922, 302, 368, 292, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22, 799});

        assertFalse("Fehler: Element 827 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(827));
        theTestList.insertAt(28, 690);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 690, 232, 467, 240, 262, 922, 302, 368, 292, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22, 799});

        assertTrue("Fehler: Element 128 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(128));
        assertFalse("Fehler: Element 590 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(590));
        theTestList.append(881);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 690, 232, 467, 240, 262, 922, 302, 368, 292, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22, 799, 881});

        theTestList.insertAt(18, 821);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 821, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 690, 232, 467, 240, 262, 922, 302, 368, 292, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22, 799, 881});

        theTestList.append(369);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 821, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 690, 232, 467, 240, 262, 922, 302, 368, 292, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22, 799, 881, 369});

        theTestList.append(158);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 821, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 690, 232, 467, 240, 262, 922, 302, 368, 292, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22, 799, 881, 369, 158});

        theTestList.append(228);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 821, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 690, 232, 467, 240, 262, 922, 302, 368, 292, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22, 799, 881, 369, 158, 228});

        theTestList.insertAt(56, 538);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 821, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 690, 232, 467, 240, 262, 922, 302, 368, 292, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22, 799, 538, 881, 369, 158, 228});

        assertTrue("Fehler: Element 179 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(179));
        assertTrue("Fehler: Element 580 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(580));
        theTestList.insertAt(38, 412);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 821, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 690, 232, 467, 240, 262, 922, 302, 368, 292, 412, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22, 799, 538, 881, 369, 158, 228});

        theTestList.append(181);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 821, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 690, 232, 467, 240, 262, 922, 302, 368, 292, 412, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22, 799, 538, 881, 369, 158, 228, 181});

        assertTrue("Fehler: Element 865 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(865));
        assertTrue("Fehler: Element 579 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(579));
        theTestList.append(854);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 821, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 690, 232, 467, 240, 262, 922, 302, 368, 292, 412, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22, 799, 538, 881, 369, 158, 228, 181, 854});

        assertTrue("Fehler: Element 475 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(475));
        theTestList.append(56);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 821, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 690, 232, 467, 240, 262, 922, 302, 368, 292, 412, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22, 799, 538, 881, 369, 158, 228, 181, 854, 56});

        theTestList.append(582);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 821, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 690, 232, 467, 240, 262, 922, 302, 368, 292, 412, 539, 241, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22, 799, 538, 881, 369, 158, 228, 181, 854, 56, 582});

        theTestList.insertAt(41, 677);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 821, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 690, 232, 467, 240, 262, 922, 302, 368, 292, 412, 539, 241, 677, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22, 799, 538, 881, 369, 158, 228, 181, 854, 56, 582});

        assertTrue("Fehler: Element 228 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(228));
        theTestList.append(352);
        Tester.verifyListContents(theTestList, new int[]{475, 865, 442, 892, 128, 943, 474, 68, 8, 921, 786, 570, 905, 179, 923, 218, 263, 895, 821, 315, 502, 127, 704, 102, 626, 493, 54, 802, 651, 690, 232, 467, 240, 262, 922, 302, 368, 292, 412, 539, 241, 677, 603, 886, 658, 72, 808, 286, 588, 20, 374, 579, 917, 580, 600, 521, 22, 799, 538, 881, 369, 158, 228, 181, 854, 56, 582, 352});

    }

    public static void test6Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(744);
        Tester.verifyListContents(theTestList, new int[]{744});

        theTestList.insertAt(0, 168);
        Tester.verifyListContents(theTestList, new int[]{168, 744});

        theTestList.append(793);
        Tester.verifyListContents(theTestList, new int[]{168, 744, 793});

        theTestList.append(957);
        Tester.verifyListContents(theTestList, new int[]{168, 744, 793, 957});

        theTestList.insertAt(2, 275);
        Tester.verifyListContents(theTestList, new int[]{168, 744, 275, 793, 957});

        theTestList.append(529);
        Tester.verifyListContents(theTestList, new int[]{168, 744, 275, 793, 957, 529});

        theTestList.append(969);
        Tester.verifyListContents(theTestList, new int[]{168, 744, 275, 793, 957, 529, 969});

        theTestList.append(625);
        Tester.verifyListContents(theTestList, new int[]{168, 744, 275, 793, 957, 529, 969, 625});

        theTestList.insertAt(1, 570);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 625});

        theTestList.append(434);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 625, 434});

        assertFalse("Fehler: Element 82 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(82));
        theTestList.insertAt(9, 634);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 625, 634, 434});

        theTestList.insertAt(9, 56);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 625, 56, 634, 434});

        theTestList.insertAt(11, 646);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 625, 56, 634, 646, 434});

        theTestList.append(274);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 625, 56, 634, 646, 434, 274});

        theTestList.insertAt(8, 435);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 435, 625, 56, 634, 646, 434, 274});

        theTestList.append(396);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 435, 625, 56, 634, 646, 434, 274, 396});

        assertTrue("Fehler: Element 625 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(625));
        theTestList.append(32);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 435, 625, 56, 634, 646, 434, 274, 396, 32});

        theTestList.append(793);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 435, 625, 56, 634, 646, 434, 274, 396, 32, 793});

        theTestList.insertAt(18, 413);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 435, 625, 56, 634, 646, 434, 274, 396, 32, 793, 413});

        theTestList.append(948);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 435, 625, 56, 634, 646, 434, 274, 396, 32, 793, 413, 948});

        assertTrue("Fehler: Element 32 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(32));
        theTestList.insertAt(18, 514);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 435, 625, 56, 634, 646, 434, 274, 396, 32, 793, 514, 413, 948});

        theTestList.append(85);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 435, 625, 56, 634, 646, 434, 274, 396, 32, 793, 514, 413, 948, 85});

        theTestList.insertAt(15, 596);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 435, 625, 56, 634, 646, 434, 274, 596, 396, 32, 793, 514, 413, 948, 85});

        assertFalse("Fehler: Element 142 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(142));
        assertTrue("Fehler: Element 396 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(396));
        assertTrue("Fehler: Element 793 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(793));
        assertFalse("Fehler: Element 517 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(517));
        assertTrue("Fehler: Element 435 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(435));
        theTestList.append(638);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 435, 625, 56, 634, 646, 434, 274, 596, 396, 32, 793, 514, 413, 948, 85, 638});

        assertTrue("Fehler: Element 168 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(168));
        theTestList.insertAt(24, 842);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 275, 793, 957, 529, 969, 435, 625, 56, 634, 646, 434, 274, 596, 396, 32, 793, 514, 413, 948, 85, 638, 842});

        theTestList.insertAt(3, 882);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 793, 957, 529, 969, 435, 625, 56, 634, 646, 434, 274, 596, 396, 32, 793, 514, 413, 948, 85, 638, 842});

        assertTrue("Fehler: Element 434 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(434));
        assertTrue("Fehler: Element 744 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(744));
        theTestList.insertAt(26, 577);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 793, 957, 529, 969, 435, 625, 56, 634, 646, 434, 274, 596, 396, 32, 793, 514, 413, 948, 85, 638, 842, 577});

        theTestList.insertAt(7, 174);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 793, 957, 174, 529, 969, 435, 625, 56, 634, 646, 434, 274, 596, 396, 32, 793, 514, 413, 948, 85, 638, 842, 577});

        theTestList.append(521);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 793, 957, 174, 529, 969, 435, 625, 56, 634, 646, 434, 274, 596, 396, 32, 793, 514, 413, 948, 85, 638, 842, 577, 521});

        theTestList.append(605);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 793, 957, 174, 529, 969, 435, 625, 56, 634, 646, 434, 274, 596, 396, 32, 793, 514, 413, 948, 85, 638, 842, 577, 521, 605});

        theTestList.insertAt(23, 758);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 793, 957, 174, 529, 969, 435, 625, 56, 634, 646, 434, 274, 596, 396, 32, 793, 514, 413, 758, 948, 85, 638, 842, 577, 521, 605});

        theTestList.insertAt(16, 768);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 793, 957, 174, 529, 969, 435, 625, 56, 634, 646, 434, 768, 274, 596, 396, 32, 793, 514, 413, 758, 948, 85, 638, 842, 577, 521, 605});

        theTestList.insertAt(24, 652);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 793, 957, 174, 529, 969, 435, 625, 56, 634, 646, 434, 768, 274, 596, 396, 32, 793, 514, 413, 652, 758, 948, 85, 638, 842, 577, 521, 605});

        assertFalse("Fehler: Element 363 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(363));
        assertFalse("Fehler: Element 706 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(706));
        assertTrue("Fehler: Element 275 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(275));
        assertTrue("Fehler: Element 744 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(744));
        theTestList.insertAt(7, 802);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 793, 957, 802, 174, 529, 969, 435, 625, 56, 634, 646, 434, 768, 274, 596, 396, 32, 793, 514, 413, 652, 758, 948, 85, 638, 842, 577, 521, 605});

        theTestList.append(841);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 793, 957, 802, 174, 529, 969, 435, 625, 56, 634, 646, 434, 768, 274, 596, 396, 32, 793, 514, 413, 652, 758, 948, 85, 638, 842, 577, 521, 605, 841});

        assertTrue("Fehler: Element 85 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(85));
        theTestList.insertAt(18, 653);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 793, 957, 802, 174, 529, 969, 435, 625, 56, 634, 646, 434, 768, 653, 274, 596, 396, 32, 793, 514, 413, 652, 758, 948, 85, 638, 842, 577, 521, 605, 841});

        theTestList.insertAt(28, 758);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 793, 957, 802, 174, 529, 969, 435, 625, 56, 634, 646, 434, 768, 653, 274, 596, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841});

        theTestList.append(620);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 793, 957, 802, 174, 529, 969, 435, 625, 56, 634, 646, 434, 768, 653, 274, 596, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620});

        assertFalse("Fehler: Element 348 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(348));
        assertFalse("Fehler: Element 437 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(437));
        theTestList.append(829);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 793, 957, 802, 174, 529, 969, 435, 625, 56, 634, 646, 434, 768, 653, 274, 596, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829});

        assertFalse("Fehler: Element 820 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(820));
        assertTrue("Fehler: Element 842 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(842));
        theTestList.insertAt(5, 427);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 435, 625, 56, 634, 646, 434, 768, 653, 274, 596, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829});

        theTestList.insertAt(12, 21);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 625, 56, 634, 646, 434, 768, 653, 274, 596, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829});

        assertTrue("Fehler: Element 634 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(634));
        theTestList.append(967);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 625, 56, 634, 646, 434, 768, 653, 274, 596, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829, 967});

        theTestList.append(50);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 625, 56, 634, 646, 434, 768, 653, 274, 596, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829, 967, 50});

        theTestList.append(705);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 625, 56, 634, 646, 434, 768, 653, 274, 596, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829, 967, 50, 705});

        theTestList.insertAt(23, 125);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 625, 56, 634, 646, 434, 768, 653, 274, 596, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829, 967, 50, 705});

        theTestList.insertAt(45, 670);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 625, 56, 634, 646, 434, 768, 653, 274, 596, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829, 967, 50, 705, 670});

        theTestList.insertAt(17, 19);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 625, 56, 634, 19, 646, 434, 768, 653, 274, 596, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829, 967, 50, 705, 670});

        theTestList.append(787);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 625, 56, 634, 19, 646, 434, 768, 653, 274, 596, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829, 967, 50, 705, 670, 787});

        assertTrue("Fehler: Element 435 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(435));
        assertTrue("Fehler: Element 32 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(32));
        assertFalse("Fehler: Element 835 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(835));
        theTestList.append(686);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 625, 56, 634, 19, 646, 434, 768, 653, 274, 596, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829, 967, 50, 705, 670, 787, 686});

        theTestList.append(795);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 625, 56, 634, 19, 646, 434, 768, 653, 274, 596, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829, 967, 50, 705, 670, 787, 686, 795});

        theTestList.append(733);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 625, 56, 634, 19, 646, 434, 768, 653, 274, 596, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829, 967, 50, 705, 670, 787, 686, 795, 733});

        theTestList.insertAt(14, 209);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 209, 625, 56, 634, 19, 646, 434, 768, 653, 274, 596, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829, 967, 50, 705, 670, 787, 686, 795, 733});

        assertTrue("Fehler: Element 829 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(829));
        assertFalse("Fehler: Element 619 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(619));
        theTestList.insertAt(25, 834);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 209, 625, 56, 634, 19, 646, 434, 768, 653, 274, 596, 834, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829, 967, 50, 705, 670, 787, 686, 795, 733});

        assertFalse("Fehler: Element 294 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(294));
        assertTrue("Fehler: Element 32 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(32));
        theTestList.insertAt(45, 584);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 209, 625, 56, 634, 19, 646, 434, 768, 653, 274, 596, 834, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829, 584, 967, 50, 705, 670, 787, 686, 795, 733});

        assertFalse("Fehler: Element 502 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(502));
        theTestList.insertAt(47, 991);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 209, 625, 56, 634, 19, 646, 434, 768, 653, 274, 596, 834, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829, 584, 967, 991, 50, 705, 670, 787, 686, 795, 733});

        assertFalse("Fehler: Element 696 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(696));
        assertTrue("Fehler: Element 56 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(56));
        theTestList.insertAt(2, 269);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 269, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 209, 625, 56, 634, 19, 646, 434, 768, 653, 274, 596, 834, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 521, 605, 841, 620, 829, 584, 967, 991, 50, 705, 670, 787, 686, 795, 733});

        assertFalse("Fehler: Element 388 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(388));
        assertFalse("Fehler: Element 144 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(144));
        assertFalse("Fehler: Element 106 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(106));
        theTestList.insertAt(41, 139);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 269, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 209, 625, 56, 634, 19, 646, 434, 768, 653, 274, 596, 834, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 139, 521, 605, 841, 620, 829, 584, 967, 991, 50, 705, 670, 787, 686, 795, 733});

        assertTrue("Fehler: Element 396 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(396));
        assertTrue("Fehler: Element 274 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(274));
        assertTrue("Fehler: Element 758 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(758));
        theTestList.insertAt(18, 572);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 269, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 209, 625, 56, 572, 634, 19, 646, 434, 768, 653, 274, 596, 834, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 139, 521, 605, 841, 620, 829, 584, 967, 991, 50, 705, 670, 787, 686, 795, 733});

        assertTrue("Fehler: Element 758 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(758));
        theTestList.append(709);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 269, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 209, 625, 56, 572, 634, 19, 646, 434, 768, 653, 274, 596, 834, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 139, 521, 605, 841, 620, 829, 584, 967, 991, 50, 705, 670, 787, 686, 795, 733, 709});

        theTestList.append(932);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 269, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 209, 625, 56, 572, 634, 19, 646, 434, 768, 653, 274, 596, 834, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 139, 521, 605, 841, 620, 829, 584, 967, 991, 50, 705, 670, 787, 686, 795, 733, 709, 932});

        theTestList.insertAt(16, 26);
        Tester.verifyListContents(theTestList, new int[]{168, 570, 269, 744, 882, 275, 427, 793, 957, 802, 174, 529, 969, 21, 435, 209, 26, 625, 56, 572, 634, 19, 646, 434, 768, 653, 274, 596, 834, 125, 396, 32, 793, 514, 413, 652, 758, 758, 948, 85, 638, 842, 577, 139, 521, 605, 841, 620, 829, 584, 967, 991, 50, 705, 670, 787, 686, 795, 733, 709, 932});

        assertFalse("Fehler: Element 911 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(911));
    }

    public static void test7Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 482);
        Tester.verifyListContents(theTestList, new int[]{482});

        theTestList.insertAt(0, 248);
        Tester.verifyListContents(theTestList, new int[]{248, 482});

        theTestList.insertAt(1, 151);
        Tester.verifyListContents(theTestList, new int[]{248, 151, 482});

        assertTrue("Fehler: Element 482 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(482));
        theTestList.insertAt(1, 526);
        Tester.verifyListContents(theTestList, new int[]{248, 526, 151, 482});

        theTestList.insertAt(1, 669);
        Tester.verifyListContents(theTestList, new int[]{248, 669, 526, 151, 482});

        theTestList.insertAt(0, 553);
        Tester.verifyListContents(theTestList, new int[]{553, 248, 669, 526, 151, 482});

        assertTrue("Fehler: Element 151 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(151));
        assertFalse("Fehler: Element 253 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(253));
        assertTrue("Fehler: Element 526 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(526));
        theTestList.insertAt(4, 528);
        Tester.verifyListContents(theTestList, new int[]{553, 248, 669, 526, 528, 151, 482});

        theTestList.insertAt(7, 694);
        Tester.verifyListContents(theTestList, new int[]{553, 248, 669, 526, 528, 151, 482, 694});

        theTestList.insertAt(4, 651);
        Tester.verifyListContents(theTestList, new int[]{553, 248, 669, 526, 651, 528, 151, 482, 694});

        theTestList.insertAt(5, 181);
        Tester.verifyListContents(theTestList, new int[]{553, 248, 669, 526, 651, 181, 528, 151, 482, 694});

        theTestList.append(825);
        Tester.verifyListContents(theTestList, new int[]{553, 248, 669, 526, 651, 181, 528, 151, 482, 694, 825});

        theTestList.insertAt(11, 388);
        Tester.verifyListContents(theTestList, new int[]{553, 248, 669, 526, 651, 181, 528, 151, 482, 694, 825, 388});

        assertTrue("Fehler: Element 151 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(151));
        assertFalse("Fehler: Element 584 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(584));
        theTestList.insertAt(9, 79);
        Tester.verifyListContents(theTestList, new int[]{553, 248, 669, 526, 651, 181, 528, 151, 482, 79, 694, 825, 388});

        assertFalse("Fehler: Element 41 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(41));
        theTestList.append(152);
        Tester.verifyListContents(theTestList, new int[]{553, 248, 669, 526, 651, 181, 528, 151, 482, 79, 694, 825, 388, 152});

        theTestList.insertAt(11, 360);
        Tester.verifyListContents(theTestList, new int[]{553, 248, 669, 526, 651, 181, 528, 151, 482, 79, 694, 360, 825, 388, 152});

        theTestList.insertAt(5, 350);
        Tester.verifyListContents(theTestList, new int[]{553, 248, 669, 526, 651, 350, 181, 528, 151, 482, 79, 694, 360, 825, 388, 152});

        theTestList.insertAt(4, 975);
        Tester.verifyListContents(theTestList, new int[]{553, 248, 669, 526, 975, 651, 350, 181, 528, 151, 482, 79, 694, 360, 825, 388, 152});

        assertFalse("Fehler: Element 963 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(963));
        theTestList.append(625);
        Tester.verifyListContents(theTestList, new int[]{553, 248, 669, 526, 975, 651, 350, 181, 528, 151, 482, 79, 694, 360, 825, 388, 152, 625});

        assertTrue("Fehler: Element 350 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(350));
        theTestList.insertAt(0, 38);
        Tester.verifyListContents(theTestList, new int[]{38, 553, 248, 669, 526, 975, 651, 350, 181, 528, 151, 482, 79, 694, 360, 825, 388, 152, 625});

        theTestList.append(548);
        Tester.verifyListContents(theTestList, new int[]{38, 553, 248, 669, 526, 975, 651, 350, 181, 528, 151, 482, 79, 694, 360, 825, 388, 152, 625, 548});

        theTestList.insertAt(3, 24);
        Tester.verifyListContents(theTestList, new int[]{38, 553, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 79, 694, 360, 825, 388, 152, 625, 548});

        assertFalse("Fehler: Element 626 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(626));
        assertFalse("Fehler: Element 929 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(929));
        theTestList.insertAt(16, 605);
        Tester.verifyListContents(theTestList, new int[]{38, 553, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 79, 694, 360, 605, 825, 388, 152, 625, 548});

        theTestList.insertAt(13, 447);
        Tester.verifyListContents(theTestList, new int[]{38, 553, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 605, 825, 388, 152, 625, 548});

        theTestList.insertAt(17, 75);
        Tester.verifyListContents(theTestList, new int[]{38, 553, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 625, 548});

        theTestList.append(868);
        Tester.verifyListContents(theTestList, new int[]{38, 553, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 625, 548, 868});

        theTestList.append(797);
        Tester.verifyListContents(theTestList, new int[]{38, 553, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 625, 548, 868, 797});

        theTestList.insertAt(22, 155);
        Tester.verifyListContents(theTestList, new int[]{38, 553, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 155, 625, 548, 868, 797});

        theTestList.insertAt(0, 50);
        Tester.verifyListContents(theTestList, new int[]{50, 38, 553, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 155, 625, 548, 868, 797});

        theTestList.insertAt(3, 92);
        Tester.verifyListContents(theTestList, new int[]{50, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 155, 625, 548, 868, 797});

        assertTrue("Fehler: Element 528 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(528));
        assertFalse("Fehler: Element 839 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(839));
        theTestList.append(128);
        Tester.verifyListContents(theTestList, new int[]{50, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 155, 625, 548, 868, 797, 128});

        theTestList.append(185);
        Tester.verifyListContents(theTestList, new int[]{50, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 155, 625, 548, 868, 797, 128, 185});

        assertTrue("Fehler: Element 975 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(975));
        theTestList.insertAt(1, 247);
        Tester.verifyListContents(theTestList, new int[]{50, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 155, 625, 548, 868, 797, 128, 185});

        theTestList.append(118);
        Tester.verifyListContents(theTestList, new int[]{50, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 155, 625, 548, 868, 797, 128, 185, 118});

        theTestList.insertAt(1, 612);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 155, 625, 548, 868, 797, 128, 185, 118});

        theTestList.append(436);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 155, 625, 548, 868, 797, 128, 185, 118, 436});

        theTestList.insertAt(26, 402);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 797, 128, 185, 118, 436});

        theTestList.append(437);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 797, 128, 185, 118, 436, 437});

        theTestList.append(462);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 797, 128, 185, 118, 436, 437, 462});

        assertTrue("Fehler: Element 388 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(388));
        assertFalse("Fehler: Element 772 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(772));
        theTestList.append(274);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 797, 128, 185, 118, 436, 437, 462, 274});

        theTestList.append(722);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 797, 128, 185, 118, 436, 437, 462, 274, 722});

        theTestList.append(875);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 797, 128, 185, 118, 436, 437, 462, 274, 722, 875});

        assertFalse("Fehler: Element 757 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(757));
        assertFalse("Fehler: Element 326 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(326));
        theTestList.insertAt(37, 66);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 797, 128, 185, 118, 436, 437, 66, 462, 274, 722, 875});

        assertTrue("Fehler: Element 825 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(825));
        theTestList.insertAt(37, 200);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 797, 128, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875});

        assertTrue("Fehler: Element 274 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(274));
        assertFalse("Fehler: Element 817 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(817));
        theTestList.append(903);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 797, 128, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 903});

        theTestList.insertAt(33, 590);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 797, 128, 590, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 903});

        assertTrue("Fehler: Element 436 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(436));
        assertFalse("Fehler: Element 208 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(208));
        theTestList.insertAt(44, 36);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 797, 128, 590, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903});

        assertTrue("Fehler: Element 360 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(360));
        assertTrue("Fehler: Element 360 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(360));
        theTestList.append(166);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 797, 128, 590, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166});

        assertTrue("Fehler: Element 360 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(360));
        theTestList.insertAt(6, 228);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 228, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 797, 128, 590, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166});

        theTestList.insertAt(32, 852);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 228, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166});

        assertFalse("Fehler: Element 985 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(985));
        assertTrue("Fehler: Element 548 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(548));
        theTestList.append(340);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 228, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340});

        theTestList.append(287);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 228, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287});

        theTestList.insertAt(36, 817);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 228, 248, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 817, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287});

        theTestList.insertAt(8, 715);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 228, 248, 715, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 817, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287});

        theTestList.insertAt(6, 275);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 275, 228, 248, 715, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 817, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287});

        theTestList.append(354);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 275, 228, 248, 715, 24, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 817, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287, 354});

        theTestList.insertAt(11, 210);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 275, 228, 248, 715, 24, 210, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 817, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287, 354});

        theTestList.insertAt(25, 234);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 275, 228, 248, 715, 24, 210, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 234, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 817, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287, 354});

        assertTrue("Fehler: Element 388 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(388));
        theTestList.insertAt(57, 435);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 275, 228, 248, 715, 24, 210, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 234, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 817, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287, 354, 435});

        theTestList.append(750);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 275, 228, 248, 715, 24, 210, 669, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 234, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 817, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287, 354, 435, 750});

        theTestList.insertAt(13, 706);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 275, 228, 248, 715, 24, 210, 669, 706, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 234, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 817, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287, 354, 435, 750});

        assertTrue("Fehler: Element 526 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(526));
        theTestList.insertAt(42, 338);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 275, 228, 248, 715, 24, 210, 669, 706, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 234, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 817, 338, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287, 354, 435, 750});

        assertFalse("Fehler: Element 0 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(0));
        theTestList.append(900);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 275, 228, 248, 715, 24, 210, 669, 706, 526, 975, 651, 350, 181, 528, 151, 482, 447, 79, 694, 360, 234, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 817, 338, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287, 354, 435, 750, 900});

        assertTrue("Fehler: Element 903 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(903));
        assertFalse("Fehler: Element 190 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(190));
        theTestList.insertAt(23, 730);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 275, 228, 248, 715, 24, 210, 669, 706, 526, 975, 651, 350, 181, 528, 151, 482, 447, 730, 79, 694, 360, 234, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 817, 338, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287, 354, 435, 750, 900});

        assertTrue("Fehler: Element 210 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(210));
        theTestList.append(637);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 275, 228, 248, 715, 24, 210, 669, 706, 526, 975, 651, 350, 181, 528, 151, 482, 447, 730, 79, 694, 360, 234, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 817, 338, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287, 354, 435, 750, 900, 637});

        theTestList.append(964);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 275, 228, 248, 715, 24, 210, 669, 706, 526, 975, 651, 350, 181, 528, 151, 482, 447, 730, 79, 694, 360, 234, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 817, 338, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287, 354, 435, 750, 900, 637, 964});

        theTestList.insertAt(23, 697);
        Tester.verifyListContents(theTestList, new int[]{50, 612, 247, 38, 553, 92, 275, 228, 248, 715, 24, 210, 669, 706, 526, 975, 651, 350, 181, 528, 151, 482, 447, 697, 730, 79, 694, 360, 234, 75, 605, 825, 388, 152, 402, 155, 625, 548, 868, 852, 797, 128, 590, 817, 338, 185, 118, 436, 437, 200, 66, 462, 274, 722, 875, 36, 903, 166, 340, 287, 354, 435, 750, 900, 637, 964});

    }

    public static void test8Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 126);
        Tester.verifyListContents(theTestList, new int[]{126});

        theTestList.append(503);
        Tester.verifyListContents(theTestList, new int[]{126, 503});

        assertTrue("Fehler: Element 503 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(503));
        theTestList.insertAt(1, 997);
        Tester.verifyListContents(theTestList, new int[]{126, 997, 503});

        theTestList.insertAt(0, 376);
        Tester.verifyListContents(theTestList, new int[]{376, 126, 997, 503});

        assertFalse("Fehler: Element 766 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(766));
        assertFalse("Fehler: Element 10 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(10));
        theTestList.append(687);
        Tester.verifyListContents(theTestList, new int[]{376, 126, 997, 503, 687});

        assertFalse("Fehler: Element 212 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(212));
        theTestList.insertAt(3, 613);
        Tester.verifyListContents(theTestList, new int[]{376, 126, 997, 613, 503, 687});

        assertFalse("Fehler: Element 820 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(820));
        theTestList.append(118);
        Tester.verifyListContents(theTestList, new int[]{376, 126, 997, 613, 503, 687, 118});

        theTestList.append(869);
        Tester.verifyListContents(theTestList, new int[]{376, 126, 997, 613, 503, 687, 118, 869});

        theTestList.insertAt(2, 281);
        Tester.verifyListContents(theTestList, new int[]{376, 126, 281, 997, 613, 503, 687, 118, 869});

        assertFalse("Fehler: Element 945 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(945));
        theTestList.append(388);
        Tester.verifyListContents(theTestList, new int[]{376, 126, 281, 997, 613, 503, 687, 118, 869, 388});

        theTestList.insertAt(2, 323);
        Tester.verifyListContents(theTestList, new int[]{376, 126, 323, 281, 997, 613, 503, 687, 118, 869, 388});

        assertFalse("Fehler: Element 835 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(835));
        theTestList.insertAt(2, 125);
        Tester.verifyListContents(theTestList, new int[]{376, 126, 125, 323, 281, 997, 613, 503, 687, 118, 869, 388});

        assertFalse("Fehler: Element 591 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(591));
        theTestList.append(296);
        Tester.verifyListContents(theTestList, new int[]{376, 126, 125, 323, 281, 997, 613, 503, 687, 118, 869, 388, 296});

        assertTrue("Fehler: Element 281 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(281));
        theTestList.insertAt(1, 590);
        Tester.verifyListContents(theTestList, new int[]{376, 590, 126, 125, 323, 281, 997, 613, 503, 687, 118, 869, 388, 296});

        assertTrue("Fehler: Element 281 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(281));
        theTestList.append(490);
        Tester.verifyListContents(theTestList, new int[]{376, 590, 126, 125, 323, 281, 997, 613, 503, 687, 118, 869, 388, 296, 490});

        theTestList.insertAt(6, 941);
        Tester.verifyListContents(theTestList, new int[]{376, 590, 126, 125, 323, 281, 941, 997, 613, 503, 687, 118, 869, 388, 296, 490});

        theTestList.insertAt(7, 380);
        Tester.verifyListContents(theTestList, new int[]{376, 590, 126, 125, 323, 281, 941, 380, 997, 613, 503, 687, 118, 869, 388, 296, 490});

        theTestList.insertAt(14, 491);
        Tester.verifyListContents(theTestList, new int[]{376, 590, 126, 125, 323, 281, 941, 380, 997, 613, 503, 687, 118, 869, 491, 388, 296, 490});

        theTestList.insertAt(8, 416);
        Tester.verifyListContents(theTestList, new int[]{376, 590, 126, 125, 323, 281, 941, 380, 416, 997, 613, 503, 687, 118, 869, 491, 388, 296, 490});

        theTestList.append(630);
        Tester.verifyListContents(theTestList, new int[]{376, 590, 126, 125, 323, 281, 941, 380, 416, 997, 613, 503, 687, 118, 869, 491, 388, 296, 490, 630});

        theTestList.insertAt(12, 368);
        Tester.verifyListContents(theTestList, new int[]{376, 590, 126, 125, 323, 281, 941, 380, 416, 997, 613, 503, 368, 687, 118, 869, 491, 388, 296, 490, 630});

        theTestList.insertAt(15, 341);
        Tester.verifyListContents(theTestList, new int[]{376, 590, 126, 125, 323, 281, 941, 380, 416, 997, 613, 503, 368, 687, 118, 341, 869, 491, 388, 296, 490, 630});

        assertTrue("Fehler: Element 416 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(416));
        theTestList.insertAt(0, 628);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 416, 997, 613, 503, 368, 687, 118, 341, 869, 491, 388, 296, 490, 630});

        theTestList.append(470);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 416, 997, 613, 503, 368, 687, 118, 341, 869, 491, 388, 296, 490, 630, 470});

        theTestList.insertAt(10, 966);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 416, 966, 997, 613, 503, 368, 687, 118, 341, 869, 491, 388, 296, 490, 630, 470});

        theTestList.insertAt(9, 778);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 416, 966, 997, 613, 503, 368, 687, 118, 341, 869, 491, 388, 296, 490, 630, 470});

        assertFalse("Fehler: Element 781 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(781));
        assertTrue("Fehler: Element 388 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(388));
        assertFalse("Fehler: Element 95 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(95));
        theTestList.insertAt(11, 81);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 416, 81, 966, 997, 613, 503, 368, 687, 118, 341, 869, 491, 388, 296, 490, 630, 470});

        assertTrue("Fehler: Element 503 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(503));
        theTestList.insertAt(10, 81);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 81, 416, 81, 966, 997, 613, 503, 368, 687, 118, 341, 869, 491, 388, 296, 490, 630, 470});

        theTestList.append(137);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 81, 416, 81, 966, 997, 613, 503, 368, 687, 118, 341, 869, 491, 388, 296, 490, 630, 470, 137});

        assertTrue("Fehler: Element 137 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(137));
        assertTrue("Fehler: Element 778 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(778));
        theTestList.insertAt(25, 77);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 81, 416, 81, 966, 997, 613, 503, 368, 687, 118, 341, 869, 491, 388, 296, 77, 490, 630, 470, 137});

        assertFalse("Fehler: Element 910 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(910));
        assertTrue("Fehler: Element 126 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(126));
        assertTrue("Fehler: Element 323 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(323));
        theTestList.insertAt(18, 711);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 81, 416, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 490, 630, 470, 137});

        theTestList.append(307);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 81, 416, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 490, 630, 470, 137, 307});

        theTestList.insertAt(30, 362);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 81, 416, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 490, 630, 470, 362, 137, 307});

        assertTrue("Fehler: Element 997 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(997));
        theTestList.append(914);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 81, 416, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 490, 630, 470, 362, 137, 307, 914});

        assertTrue("Fehler: Element 118 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(118));
        theTestList.append(189);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 81, 416, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 490, 630, 470, 362, 137, 307, 914, 189});

        theTestList.insertAt(10, 719);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 490, 630, 470, 362, 137, 307, 914, 189});

        assertTrue("Fehler: Element 997 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(997));
        theTestList.insertAt(13, 63);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 490, 630, 470, 362, 137, 307, 914, 189});

        theTestList.insertAt(35, 390);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 490, 630, 470, 362, 137, 307, 390, 914, 189});

        assertTrue("Fehler: Element 380 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(380));
        theTestList.append(851);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 490, 630, 470, 362, 137, 307, 390, 914, 189, 851});

        assertFalse("Fehler: Element 508 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(508));
        theTestList.append(100);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 490, 630, 470, 362, 137, 307, 390, 914, 189, 851, 100});

        assertTrue("Fehler: Element 941 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(941));
        assertTrue("Fehler: Element 491 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(491));
        theTestList.insertAt(14, 274);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 490, 630, 470, 362, 137, 307, 390, 914, 189, 851, 100});

        assertTrue("Fehler: Element 687 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(687));
        assertTrue("Fehler: Element 590 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(590));
        theTestList.insertAt(30, 91);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 490, 630, 470, 362, 137, 307, 390, 914, 189, 851, 100});

        assertTrue("Fehler: Element 189 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(189));
        assertTrue("Fehler: Element 719 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(719));
        theTestList.append(213);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 490, 630, 470, 362, 137, 307, 390, 914, 189, 851, 100, 213});

        assertFalse("Fehler: Element 847 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(847));
        assertTrue("Fehler: Element 941 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(941));
        theTestList.append(401);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 490, 630, 470, 362, 137, 307, 390, 914, 189, 851, 100, 213, 401});

        theTestList.append(870);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 490, 630, 470, 362, 137, 307, 390, 914, 189, 851, 100, 213, 401, 870});

        theTestList.insertAt(38, 201);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 490, 630, 470, 362, 137, 307, 390, 201, 914, 189, 851, 100, 213, 401, 870});

        theTestList.append(651);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 490, 630, 470, 362, 137, 307, 390, 201, 914, 189, 851, 100, 213, 401, 870, 651});

        theTestList.append(508);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 490, 630, 470, 362, 137, 307, 390, 201, 914, 189, 851, 100, 213, 401, 870, 651, 508});

        theTestList.append(372);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 490, 630, 470, 362, 137, 307, 390, 201, 914, 189, 851, 100, 213, 401, 870, 651, 508, 372});

        assertTrue("Fehler: Element 63 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(63));
        theTestList.append(805);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 490, 630, 470, 362, 137, 307, 390, 201, 914, 189, 851, 100, 213, 401, 870, 651, 508, 372, 805});

        theTestList.insertAt(31, 26);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 26, 490, 630, 470, 362, 137, 307, 390, 201, 914, 189, 851, 100, 213, 401, 870, 651, 508, 372, 805});

        theTestList.append(170);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 26, 490, 630, 470, 362, 137, 307, 390, 201, 914, 189, 851, 100, 213, 401, 870, 651, 508, 372, 805, 170});

        assertFalse("Fehler: Element 263 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(263));
        theTestList.append(490);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 26, 490, 630, 470, 362, 137, 307, 390, 201, 914, 189, 851, 100, 213, 401, 870, 651, 508, 372, 805, 170, 490});

        theTestList.append(496);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 26, 490, 630, 470, 362, 137, 307, 390, 201, 914, 189, 851, 100, 213, 401, 870, 651, 508, 372, 805, 170, 490, 496});

        theTestList.insertAt(34, 331);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 26, 490, 630, 331, 470, 362, 137, 307, 390, 201, 914, 189, 851, 100, 213, 401, 870, 651, 508, 372, 805, 170, 490, 496});

        theTestList.insertAt(38, 318);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 26, 490, 630, 331, 470, 362, 137, 318, 307, 390, 201, 914, 189, 851, 100, 213, 401, 870, 651, 508, 372, 805, 170, 490, 496});

        assertFalse("Fehler: Element 102 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(102));
        theTestList.append(212);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 26, 490, 630, 331, 470, 362, 137, 318, 307, 390, 201, 914, 189, 851, 100, 213, 401, 870, 651, 508, 372, 805, 170, 490, 496, 212});

        theTestList.append(626);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 26, 490, 630, 331, 470, 362, 137, 318, 307, 390, 201, 914, 189, 851, 100, 213, 401, 870, 651, 508, 372, 805, 170, 490, 496, 212, 626});

        assertFalse("Fehler: Element 927 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(927));
        theTestList.insertAt(34, 666);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 26, 490, 630, 666, 331, 470, 362, 137, 318, 307, 390, 201, 914, 189, 851, 100, 213, 401, 870, 651, 508, 372, 805, 170, 490, 496, 212, 626});

        assertFalse("Fehler: Element 367 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(367));
        theTestList.insertAt(54, 123);
        Tester.verifyListContents(theTestList, new int[]{628, 376, 590, 126, 125, 323, 281, 941, 380, 778, 719, 81, 416, 63, 274, 81, 966, 997, 613, 503, 368, 711, 687, 118, 341, 869, 491, 388, 296, 77, 91, 26, 490, 630, 666, 331, 470, 362, 137, 318, 307, 390, 201, 914, 189, 851, 100, 213, 401, 870, 651, 508, 372, 805, 123, 170, 490, 496, 212, 626});

        assertTrue("Fehler: Element 613 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(613));
        assertTrue("Fehler: Element 274 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(274));
    }

    public static void test9Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 766);
        Tester.verifyListContents(theTestList, new int[]{766});

        theTestList.insertAt(1, 357);
        Tester.verifyListContents(theTestList, new int[]{766, 357});

        assertFalse("Fehler: Element 702 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(702));
        theTestList.insertAt(1, 250);
        Tester.verifyListContents(theTestList, new int[]{766, 250, 357});

        theTestList.append(965);
        Tester.verifyListContents(theTestList, new int[]{766, 250, 357, 965});

        theTestList.append(151);
        Tester.verifyListContents(theTestList, new int[]{766, 250, 357, 965, 151});

        theTestList.insertAt(0, 918);
        Tester.verifyListContents(theTestList, new int[]{918, 766, 250, 357, 965, 151});

        theTestList.append(175);
        Tester.verifyListContents(theTestList, new int[]{918, 766, 250, 357, 965, 151, 175});

        assertTrue("Fehler: Element 250 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(250));
        theTestList.append(545);
        Tester.verifyListContents(theTestList, new int[]{918, 766, 250, 357, 965, 151, 175, 545});

        assertTrue("Fehler: Element 151 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(151));
        theTestList.append(894);
        Tester.verifyListContents(theTestList, new int[]{918, 766, 250, 357, 965, 151, 175, 545, 894});

        theTestList.insertAt(3, 20);
        Tester.verifyListContents(theTestList, new int[]{918, 766, 250, 20, 357, 965, 151, 175, 545, 894});

        theTestList.insertAt(4, 365);
        Tester.verifyListContents(theTestList, new int[]{918, 766, 250, 20, 365, 357, 965, 151, 175, 545, 894});

        theTestList.insertAt(4, 101);
        Tester.verifyListContents(theTestList, new int[]{918, 766, 250, 20, 101, 365, 357, 965, 151, 175, 545, 894});

        assertTrue("Fehler: Element 766 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(766));
        theTestList.insertAt(0, 430);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 365, 357, 965, 151, 175, 545, 894});

        theTestList.append(655);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 365, 357, 965, 151, 175, 545, 894, 655});

        theTestList.append(105);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 365, 357, 965, 151, 175, 545, 894, 655, 105});

        assertTrue("Fehler: Element 766 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(766));
        theTestList.insertAt(12, 798);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 365, 357, 965, 151, 175, 545, 798, 894, 655, 105});

        assertTrue("Fehler: Element 101 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(101));
        theTestList.insertAt(12, 317);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 365, 357, 965, 151, 175, 545, 317, 798, 894, 655, 105});

        theTestList.insertAt(14, 16);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 365, 357, 965, 151, 175, 545, 317, 798, 16, 894, 655, 105});

        assertTrue("Fehler: Element 175 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(175));
        assertFalse("Fehler: Element 752 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(752));
        theTestList.append(103);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 365, 357, 965, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103});

        theTestList.append(99);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 365, 357, 965, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99});

        theTestList.append(51);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 365, 357, 965, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51});

        assertFalse("Fehler: Element 619 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(619));
        theTestList.append(806);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 365, 357, 965, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806});

        theTestList.append(96);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 365, 357, 965, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96});

        assertTrue("Fehler: Element 655 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(655));
        assertFalse("Fehler: Element 60 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(60));
        theTestList.append(52);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 365, 357, 965, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52});

        theTestList.insertAt(6, 436);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 436, 365, 357, 965, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52});

        assertTrue("Fehler: Element 365 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(365));
        theTestList.insertAt(10, 457);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52});

        theTestList.append(991);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 991});

        theTestList.append(570);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 991, 570});

        theTestList.append(810);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 991, 570, 810});

        theTestList.append(761);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 991, 570, 810, 761});

        theTestList.append(618);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 991, 570, 810, 761, 618});

        assertFalse("Fehler: Element 989 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(989));
        theTestList.insertAt(26, 315);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 250, 20, 101, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 761, 618});

        theTestList.insertAt(3, 309);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 20, 101, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 761, 618});

        assertTrue("Fehler: Element 545 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(545));
        assertTrue("Fehler: Element 20 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(20));
        assertTrue("Fehler: Element 894 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(894));
        theTestList.append(597);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 20, 101, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 761, 618, 597});

        assertFalse("Fehler: Element 328 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(328));
        assertTrue("Fehler: Element 52 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(52));
        assertFalse("Fehler: Element 294 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(294));
        theTestList.append(283);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 20, 101, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 761, 618, 597, 283});

        theTestList.append(809);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 20, 101, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 761, 618, 597, 283, 809});

        assertTrue("Fehler: Element 283 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(283));
        theTestList.append(487);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 20, 101, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 761, 618, 597, 283, 809, 487});

        theTestList.insertAt(7, 951);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 20, 101, 951, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 761, 618, 597, 283, 809, 487});

        assertTrue("Fehler: Element 545 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(545));
        assertFalse("Fehler: Element 816 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(816));
        theTestList.append(78);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 20, 101, 951, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 761, 618, 597, 283, 809, 487, 78});

        theTestList.insertAt(6, 10);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 20, 10, 101, 951, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 761, 618, 597, 283, 809, 487, 78});

        theTestList.append(47);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 20, 10, 101, 951, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 761, 618, 597, 283, 809, 487, 78, 47});

        theTestList.append(62);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 20, 10, 101, 951, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 761, 618, 597, 283, 809, 487, 78, 47, 62});

        assertTrue("Fehler: Element 357 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(357));
        assertTrue("Fehler: Element 78 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(78));
        theTestList.append(508);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 20, 10, 101, 951, 436, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 761, 618, 597, 283, 809, 487, 78, 47, 62, 508});

        theTestList.insertAt(10, 913);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 761, 618, 597, 283, 809, 487, 78, 47, 62, 508});

        theTestList.insertAt(5, 415);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 761, 618, 597, 283, 809, 487, 78, 47, 62, 508});

        theTestList.append(199);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 761, 618, 597, 283, 809, 487, 78, 47, 62, 508, 199});

        theTestList.insertAt(35, 505);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 505, 761, 618, 597, 283, 809, 487, 78, 47, 62, 508, 199});

        theTestList.append(98);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 96, 52, 315, 991, 570, 810, 505, 761, 618, 597, 283, 809, 487, 78, 47, 62, 508, 199, 98});

        assertFalse("Fehler: Element 212 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(212));
        assertTrue("Fehler: Element 52 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(52));
        theTestList.insertAt(29, 759);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 759, 96, 52, 315, 991, 570, 810, 505, 761, 618, 597, 283, 809, 487, 78, 47, 62, 508, 199, 98});

        theTestList.insertAt(48, 366);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 759, 96, 52, 315, 991, 570, 810, 505, 761, 618, 597, 283, 809, 487, 78, 47, 62, 508, 199, 366, 98});

        theTestList.insertAt(44, 105);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 759, 96, 52, 315, 991, 570, 810, 505, 761, 618, 597, 283, 809, 487, 78, 105, 47, 62, 508, 199, 366, 98});

        theTestList.insertAt(39, 63);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 759, 96, 52, 315, 991, 570, 810, 505, 761, 618, 63, 597, 283, 809, 487, 78, 105, 47, 62, 508, 199, 366, 98});

        assertTrue("Fehler: Element 798 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(798));
        theTestList.append(391);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 759, 96, 52, 315, 991, 570, 810, 505, 761, 618, 63, 597, 283, 809, 487, 78, 105, 47, 62, 508, 199, 366, 98, 391});

        assertFalse("Fehler: Element 204 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(204));
        assertFalse("Fehler: Element 738 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(738));
        assertTrue("Fehler: Element 913 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(913));
        theTestList.insertAt(17, 613);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 613, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 759, 96, 52, 315, 991, 570, 810, 505, 761, 618, 63, 597, 283, 809, 487, 78, 105, 47, 62, 508, 199, 366, 98, 391});

        theTestList.append(450);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 613, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 759, 96, 52, 315, 991, 570, 810, 505, 761, 618, 63, 597, 283, 809, 487, 78, 105, 47, 62, 508, 199, 366, 98, 391, 450});

        assertFalse("Fehler: Element 115 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(115));
        assertTrue("Fehler: Element 759 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(759));
        assertTrue("Fehler: Element 457 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(457));
        theTestList.append(39);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 613, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 759, 96, 52, 315, 991, 570, 810, 505, 761, 618, 63, 597, 283, 809, 487, 78, 105, 47, 62, 508, 199, 366, 98, 391, 450, 39});

        assertFalse("Fehler: Element 507 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(507));
        assertTrue("Fehler: Element 51 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(51));
        theTestList.append(305);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 613, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 759, 96, 52, 315, 991, 570, 810, 505, 761, 618, 63, 597, 283, 809, 487, 78, 105, 47, 62, 508, 199, 366, 98, 391, 450, 39, 305});

        theTestList.append(650);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 613, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 759, 96, 52, 315, 991, 570, 810, 505, 761, 618, 63, 597, 283, 809, 487, 78, 105, 47, 62, 508, 199, 366, 98, 391, 450, 39, 305, 650});

        assertFalse("Fehler: Element 602 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(602));
        assertTrue("Fehler: Element 415 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(415));
        assertFalse("Fehler: Element 145 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(145));
        theTestList.append(191);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 613, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 759, 96, 52, 315, 991, 570, 810, 505, 761, 618, 63, 597, 283, 809, 487, 78, 105, 47, 62, 508, 199, 366, 98, 391, 450, 39, 305, 650, 191});

        theTestList.append(761);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 613, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 759, 96, 52, 315, 991, 570, 810, 505, 761, 618, 63, 597, 283, 809, 487, 78, 105, 47, 62, 508, 199, 366, 98, 391, 450, 39, 305, 650, 191, 761});

        assertTrue("Fehler: Element 761 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(761));
        theTestList.append(45);
        Tester.verifyListContents(theTestList, new int[]{430, 918, 766, 309, 250, 415, 20, 10, 101, 951, 436, 913, 365, 357, 965, 457, 151, 613, 175, 545, 317, 798, 16, 894, 655, 105, 103, 99, 51, 806, 759, 96, 52, 315, 991, 570, 810, 505, 761, 618, 63, 597, 283, 809, 487, 78, 105, 47, 62, 508, 199, 366, 98, 391, 450, 39, 305, 650, 191, 761, 45});

    }

    public static void test10Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 901);
        Tester.verifyListContents(theTestList, new int[]{901});

        theTestList.append(308);
        Tester.verifyListContents(theTestList, new int[]{901, 308});

        theTestList.insertAt(1, 511);
        Tester.verifyListContents(theTestList, new int[]{901, 511, 308});

        assertFalse("Fehler: Element 954 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(954));
        theTestList.insertAt(0, 633);
        Tester.verifyListContents(theTestList, new int[]{633, 901, 511, 308});

        theTestList.insertAt(2, 758);
        Tester.verifyListContents(theTestList, new int[]{633, 901, 758, 511, 308});

        theTestList.insertAt(1, 787);
        Tester.verifyListContents(theTestList, new int[]{633, 787, 901, 758, 511, 308});

        assertFalse("Fehler: Element 835 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(835));
        assertFalse("Fehler: Element 853 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(853));
        theTestList.append(302);
        Tester.verifyListContents(theTestList, new int[]{633, 787, 901, 758, 511, 308, 302});

        theTestList.insertAt(2, 137);
        Tester.verifyListContents(theTestList, new int[]{633, 787, 137, 901, 758, 511, 308, 302});

        assertTrue("Fehler: Element 137 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(137));
        theTestList.append(688);
        Tester.verifyListContents(theTestList, new int[]{633, 787, 137, 901, 758, 511, 308, 302, 688});

        theTestList.append(946);
        Tester.verifyListContents(theTestList, new int[]{633, 787, 137, 901, 758, 511, 308, 302, 688, 946});

        theTestList.append(222);
        Tester.verifyListContents(theTestList, new int[]{633, 787, 137, 901, 758, 511, 308, 302, 688, 946, 222});

        theTestList.insertAt(4, 688);
        Tester.verifyListContents(theTestList, new int[]{633, 787, 137, 901, 688, 758, 511, 308, 302, 688, 946, 222});

        assertFalse("Fehler: Element 529 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(529));
        assertTrue("Fehler: Element 511 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(511));
        theTestList.append(168);
        Tester.verifyListContents(theTestList, new int[]{633, 787, 137, 901, 688, 758, 511, 308, 302, 688, 946, 222, 168});

        theTestList.append(341);
        Tester.verifyListContents(theTestList, new int[]{633, 787, 137, 901, 688, 758, 511, 308, 302, 688, 946, 222, 168, 341});

        theTestList.append(188);
        Tester.verifyListContents(theTestList, new int[]{633, 787, 137, 901, 688, 758, 511, 308, 302, 688, 946, 222, 168, 341, 188});

        theTestList.append(334);
        Tester.verifyListContents(theTestList, new int[]{633, 787, 137, 901, 688, 758, 511, 308, 302, 688, 946, 222, 168, 341, 188, 334});

        theTestList.append(857);
        Tester.verifyListContents(theTestList, new int[]{633, 787, 137, 901, 688, 758, 511, 308, 302, 688, 946, 222, 168, 341, 188, 334, 857});

        assertFalse("Fehler: Element 970 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(970));
        theTestList.append(288);
        Tester.verifyListContents(theTestList, new int[]{633, 787, 137, 901, 688, 758, 511, 308, 302, 688, 946, 222, 168, 341, 188, 334, 857, 288});

        theTestList.insertAt(0, 42);
        Tester.verifyListContents(theTestList, new int[]{42, 633, 787, 137, 901, 688, 758, 511, 308, 302, 688, 946, 222, 168, 341, 188, 334, 857, 288});

        assertFalse("Fehler: Element 737 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(737));
        theTestList.insertAt(19, 220);
        Tester.verifyListContents(theTestList, new int[]{42, 633, 787, 137, 901, 688, 758, 511, 308, 302, 688, 946, 222, 168, 341, 188, 334, 857, 288, 220});

        theTestList.insertAt(1, 853);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 758, 511, 308, 302, 688, 946, 222, 168, 341, 188, 334, 857, 288, 220});

        theTestList.append(814);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 758, 511, 308, 302, 688, 946, 222, 168, 341, 188, 334, 857, 288, 220, 814});

        theTestList.insertAt(7, 305);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 511, 308, 302, 688, 946, 222, 168, 341, 188, 334, 857, 288, 220, 814});

        theTestList.append(767);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 511, 308, 302, 688, 946, 222, 168, 341, 188, 334, 857, 288, 220, 814, 767});

        theTestList.insertAt(9, 229);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 688, 946, 222, 168, 341, 188, 334, 857, 288, 220, 814, 767});

        theTestList.insertAt(19, 35);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 688, 946, 222, 168, 341, 188, 35, 334, 857, 288, 220, 814, 767});

        theTestList.append(512);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 688, 946, 222, 168, 341, 188, 35, 334, 857, 288, 220, 814, 767, 512});

        theTestList.append(676);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 688, 946, 222, 168, 341, 188, 35, 334, 857, 288, 220, 814, 767, 512, 676});

        assertFalse("Fehler: Element 916 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(916));
        theTestList.append(466);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 688, 946, 222, 168, 341, 188, 35, 334, 857, 288, 220, 814, 767, 512, 676, 466});

        theTestList.insertAt(13, 101);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 222, 168, 341, 188, 35, 334, 857, 288, 220, 814, 767, 512, 676, 466});

        assertTrue("Fehler: Element 302 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(302));
        theTestList.insertAt(16, 381);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 188, 35, 334, 857, 288, 220, 814, 767, 512, 676, 466});

        theTestList.insertAt(31, 434);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 188, 35, 334, 857, 288, 220, 814, 767, 512, 676, 466, 434});

        theTestList.append(496);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 188, 35, 334, 857, 288, 220, 814, 767, 512, 676, 466, 434, 496});

        theTestList.append(846);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 188, 35, 334, 857, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846});

        theTestList.append(66);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 188, 35, 334, 857, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 66});

        assertTrue("Fehler: Element 857 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(857));
        assertTrue("Fehler: Element 633 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(633));
        theTestList.insertAt(34, 945);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 188, 35, 334, 857, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66});

        theTestList.append(156);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 188, 35, 334, 857, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156});

        theTestList.append(536);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 188, 35, 334, 857, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536});

        theTestList.append(852);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 188, 35, 334, 857, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852});

        theTestList.insertAt(24, 65);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 188, 35, 334, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852});

        assertFalse("Fehler: Element 922 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(922));
        theTestList.insertAt(3, 39);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 39, 787, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 188, 35, 334, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852});

        theTestList.insertAt(5, 853);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 39, 787, 853, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 188, 35, 334, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852});

        assertTrue("Fehler: Element 220 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(220));
        theTestList.insertAt(22, 657);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 39, 787, 853, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 657, 188, 35, 334, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852});

        assertTrue("Fehler: Element 302 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(302));
        theTestList.append(821);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 39, 787, 853, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 657, 188, 35, 334, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821});

        theTestList.insertAt(22, 446);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 39, 787, 853, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 446, 657, 188, 35, 334, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821});

        assertFalse("Fehler: Element 463 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(463));
        assertFalse("Fehler: Element 807 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(807));
        theTestList.insertAt(27, 630);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 39, 787, 853, 137, 901, 688, 305, 758, 229, 511, 308, 302, 101, 688, 946, 381, 222, 168, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821});

        theTestList.insertAt(13, 557);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 39, 787, 853, 137, 901, 688, 305, 758, 229, 511, 557, 308, 302, 101, 688, 946, 381, 222, 168, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821});

        theTestList.insertAt(22, 740);
        Tester.verifyListContents(theTestList, new int[]{42, 853, 633, 39, 787, 853, 137, 901, 688, 305, 758, 229, 511, 557, 308, 302, 101, 688, 946, 381, 222, 168, 740, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821});

        theTestList.insertAt(1, 926);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 853, 137, 901, 688, 305, 758, 229, 511, 557, 308, 302, 101, 688, 946, 381, 222, 168, 740, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821});

        theTestList.insertAt(6, 524);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 853, 137, 901, 688, 305, 758, 229, 511, 557, 308, 302, 101, 688, 946, 381, 222, 168, 740, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821});

        theTestList.insertAt(14, 943);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 308, 302, 101, 688, 946, 381, 222, 168, 740, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821});

        theTestList.append(459);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 308, 302, 101, 688, 946, 381, 222, 168, 740, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821, 459});

        theTestList.append(799);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 308, 302, 101, 688, 946, 381, 222, 168, 740, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821, 459, 799});

        theTestList.insertAt(17, 315);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 315, 308, 302, 101, 688, 946, 381, 222, 168, 740, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821, 459, 799});

        theTestList.append(469);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 315, 308, 302, 101, 688, 946, 381, 222, 168, 740, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821, 459, 799, 469});

        assertTrue("Fehler: Element 853 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(853));
        assertFalse("Fehler: Element 728 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(728));
        theTestList.insertAt(19, 36);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821, 459, 799, 469});

        theTestList.insertAt(17, 37);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 37, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821, 459, 799, 469});

        theTestList.insertAt(55, 474);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 37, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821, 459, 474, 799, 469});

        assertFalse("Fehler: Element 947 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(947));
        assertTrue("Fehler: Element 688 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(688));
        theTestList.append(999);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 37, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821, 459, 474, 799, 469, 999});

        theTestList.insertAt(29, 549);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 37, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 549, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821, 459, 474, 799, 469, 999});

        theTestList.insertAt(60, 556);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 37, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 549, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821, 459, 474, 799, 469, 999, 556});

        assertTrue("Fehler: Element 536 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(536));
        theTestList.append(77);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 37, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 549, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821, 459, 474, 799, 469, 999, 556, 77});

        theTestList.insertAt(7, 760);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 760, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 37, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 549, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821, 459, 474, 799, 469, 999, 556, 77});

        theTestList.append(470);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 760, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 37, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 549, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821, 459, 474, 799, 469, 999, 556, 77, 470});

        theTestList.append(97);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 760, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 37, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 549, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 156, 536, 852, 821, 459, 474, 799, 469, 999, 556, 77, 470, 97});

        theTestList.insertAt(52, 387);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 760, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 37, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 549, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 387, 156, 536, 852, 821, 459, 474, 799, 469, 999, 556, 77, 470, 97});

        theTestList.append(102);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 760, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 37, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 549, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 387, 156, 536, 852, 821, 459, 474, 799, 469, 999, 556, 77, 470, 97, 102});

        assertFalse("Fehler: Element 958 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(958));
        theTestList.insertAt(55, 532);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 760, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 37, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 549, 341, 446, 657, 188, 35, 334, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 387, 156, 536, 532, 852, 821, 459, 474, 799, 469, 999, 556, 77, 470, 97, 102});

        theTestList.insertAt(37, 76);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 760, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 37, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 549, 341, 446, 657, 188, 35, 334, 76, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 387, 156, 536, 532, 852, 821, 459, 474, 799, 469, 999, 556, 77, 470, 97, 102});

        theTestList.append(289);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 760, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 37, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 549, 341, 446, 657, 188, 35, 334, 76, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 387, 156, 536, 532, 852, 821, 459, 474, 799, 469, 999, 556, 77, 470, 97, 102, 289});

        assertTrue("Fehler: Element 37 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(37));
        assertTrue("Fehler: Element 229 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(229));
        theTestList.append(395);
        Tester.verifyListContents(theTestList, new int[]{42, 926, 853, 633, 39, 787, 524, 760, 853, 137, 901, 688, 305, 758, 229, 943, 511, 557, 37, 315, 308, 36, 302, 101, 688, 946, 381, 222, 168, 740, 549, 341, 446, 657, 188, 35, 334, 76, 630, 857, 65, 288, 220, 814, 767, 512, 676, 466, 434, 496, 846, 945, 66, 387, 156, 536, 532, 852, 821, 459, 474, 799, 469, 999, 556, 77, 470, 97, 102, 289, 395});

        assertFalse("Fehler: Element 586 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(586));
        assertFalse("Fehler: Element 68 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(68));
        assertFalse("Fehler: Element 310 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(310));
        assertTrue("Fehler: Element 799 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(799));
    }
}
