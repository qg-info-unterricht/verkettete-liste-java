package tests;


import static org.junit.Assert.*;

import liste.*;

/**
 * Die Test-Klasse SubTest6.
 *
 * @author Rainer Helfrich
 * @version 03.10.2020
 */
public class SubTest6 {


    public static void test1Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(651);
        Tester.verifyListContents(theTestList, new int[]{651});

        theTestList.insertAt(1, 576);
        Tester.verifyListContents(theTestList, new int[]{651, 576});

        assertTrue("Fehler: Element 576 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(576));
        assertTrue("Fehler: Element 651 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(651));
        theTestList.append(763);
        Tester.verifyListContents(theTestList, new int[]{651, 576, 763});

        assertFalse("Fehler: Element 586 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(586));
        theTestList.append(920);
        Tester.verifyListContents(theTestList, new int[]{651, 576, 763, 920});

        theTestList.insertAt(0, 418);
        Tester.verifyListContents(theTestList, new int[]{418, 651, 576, 763, 920});

        theTestList.append(781);
        Tester.verifyListContents(theTestList, new int[]{418, 651, 576, 763, 920, 781});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{418, 576, 763, 920, 781});

        theTestList.append(304);
        Tester.verifyListContents(theTestList, new int[]{418, 576, 763, 920, 781, 304});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{418, 763, 920, 781, 304});

        assertTrue("Fehler: Element 304 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(304));
        assertFalse("Fehler: Element 468 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(468));
        assertTrue("Fehler: Element 781 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(781));
        theTestList.insertAt(0, 223);
        Tester.verifyListContents(theTestList, new int[]{223, 418, 763, 920, 781, 304});

        assertFalse("Fehler: Element 24 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(24));
        theTestList.append(939);
        Tester.verifyListContents(theTestList, new int[]{223, 418, 763, 920, 781, 304, 939});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{223, 418, 763, 920, 781, 939});

        theTestList.insertAt(4, 685);
        Tester.verifyListContents(theTestList, new int[]{223, 418, 763, 920, 685, 781, 939});

        theTestList.append(475);
        Tester.verifyListContents(theTestList, new int[]{223, 418, 763, 920, 685, 781, 939, 475});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{223, 418, 763, 920, 685, 781, 939});

        assertTrue("Fehler: Element 418 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(418));
        theTestList.insertAt(7, 598);
        Tester.verifyListContents(theTestList, new int[]{223, 418, 763, 920, 685, 781, 939, 598});

        assertFalse("Fehler: Element 843 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(843));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{418, 763, 920, 685, 781, 939, 598});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{418, 763, 685, 781, 939, 598});

        theTestList.append(402);
        Tester.verifyListContents(theTestList, new int[]{418, 763, 685, 781, 939, 598, 402});

        theTestList.insertAt(1, 36);
        Tester.verifyListContents(theTestList, new int[]{418, 36, 763, 685, 781, 939, 598, 402});

        theTestList.insertAt(0, 13);
        Tester.verifyListContents(theTestList, new int[]{13, 418, 36, 763, 685, 781, 939, 598, 402});

        
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(862);
        Tester.verifyListContents(theTestList, new int[]{862});

        theTestList.insertAt(0, 814);
        Tester.verifyListContents(theTestList, new int[]{814, 862});

        assertTrue("Fehler: Element 862 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(862));
        theTestList.insertAt(0, 615);
        Tester.verifyListContents(theTestList, new int[]{615, 814, 862});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{615, 862});

        theTestList.insertAt(2, 963);
        Tester.verifyListContents(theTestList, new int[]{615, 862, 963});

        theTestList.append(419);
        Tester.verifyListContents(theTestList, new int[]{615, 862, 963, 419});

        assertTrue("Fehler: Element 862 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(862));
        theTestList.insertAt(4, 371);
        Tester.verifyListContents(theTestList, new int[]{615, 862, 963, 419, 371});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{615, 963, 419, 371});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{615, 963, 371});

        assertFalse("Fehler: Element 609 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(609));
        theTestList.append(641);
        Tester.verifyListContents(theTestList, new int[]{615, 963, 371, 641});

        assertTrue("Fehler: Element 615 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(615));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{615, 963, 371});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{615, 371});

        assertTrue("Fehler: Element 615 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(615));
        theTestList.append(952);
        Tester.verifyListContents(theTestList, new int[]{615, 371, 952});

        assertFalse("Fehler: Element 187 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(187));
        assertTrue("Fehler: Element 952 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(952));
        theTestList.append(133);
        Tester.verifyListContents(theTestList, new int[]{615, 371, 952, 133});

        assertTrue("Fehler: Element 371 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(371));
        theTestList.append(958);
        Tester.verifyListContents(theTestList, new int[]{615, 371, 952, 133, 958});

        theTestList.append(20);
        Tester.verifyListContents(theTestList, new int[]{615, 371, 952, 133, 958, 20});

        assertFalse("Fehler: Element 422 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(422));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{371, 952, 133, 958, 20});

        assertTrue("Fehler: Element 958 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(958));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(772);
        Tester.verifyListContents(theTestList, new int[]{772});

        assertFalse("Fehler: Element 224 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(224));
        theTestList.append(968);
        Tester.verifyListContents(theTestList, new int[]{772, 968});

        theTestList.insertAt(1, 105);
        Tester.verifyListContents(theTestList, new int[]{772, 105, 968});

        theTestList.insertAt(0, 503);
        Tester.verifyListContents(theTestList, new int[]{503, 772, 105, 968});

        assertTrue("Fehler: Element 968 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(968));
        assertFalse("Fehler: Element 803 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(803));
        assertTrue("Fehler: Element 968 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(968));
        theTestList.append(74);
        Tester.verifyListContents(theTestList, new int[]{503, 772, 105, 968, 74});

        theTestList.insertAt(3, 682);
        Tester.verifyListContents(theTestList, new int[]{503, 772, 105, 682, 968, 74});

        assertTrue("Fehler: Element 772 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(772));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 682, 968, 74});

        assertTrue("Fehler: Element 105 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(105));
        theTestList.insertAt(2, 614);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 614, 682, 968, 74});

        theTestList.insertAt(0, 525);
        Tester.verifyListContents(theTestList, new int[]{525, 503, 105, 614, 682, 968, 74});

        assertTrue("Fehler: Element 503 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(503));
        assertTrue("Fehler: Element 968 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(968));
        theTestList.append(878);
        Tester.verifyListContents(theTestList, new int[]{525, 503, 105, 614, 682, 968, 74, 878});

        theTestList.insertAt(8, 185);
        Tester.verifyListContents(theTestList, new int[]{525, 503, 105, 614, 682, 968, 74, 878, 185});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{525, 503, 105, 682, 968, 74, 878, 185});

        assertTrue("Fehler: Element 878 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(878));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 682, 968, 74, 878, 185});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 682, 968, 74, 185});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 968, 74, 185});

        assertFalse("Fehler: Element 313 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(313));
        assertTrue("Fehler: Element 968 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(968));
        theTestList.append(97);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 968, 74, 185, 97});

        theTestList.insertAt(3, 177);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 968, 177, 74, 185, 97});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 968, 177, 74, 97});

        theTestList.append(816);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 968, 177, 74, 97, 816});

        assertTrue("Fehler: Element 105 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(105));
        theTestList.insertAt(5, 180);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 968, 177, 74, 180, 97, 816});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 968, 177, 74, 180, 816});

        assertFalse("Fehler: Element 475 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(475));
        theTestList.insertAt(4, 40);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 968, 177, 40, 74, 180, 816});

        assertFalse("Fehler: Element 849 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(849));
        theTestList.append(910);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 968, 177, 40, 74, 180, 816, 910});

        theTestList.append(679);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 968, 177, 40, 74, 180, 816, 910, 679});

        assertFalse("Fehler: Element 295 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(295));
        theTestList.insertAt(4, 739);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 968, 177, 739, 40, 74, 180, 816, 910, 679});

        theTestList.append(773);
        Tester.verifyListContents(theTestList, new int[]{503, 105, 968, 177, 739, 40, 74, 180, 816, 910, 679, 773});

    }

    public static void test2Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 810);
        Tester.verifyListContents(theTestList, new int[]{810});

        theTestList.insertAt(0, 594);
        Tester.verifyListContents(theTestList, new int[]{594, 810});

        theTestList.append(914);
        Tester.verifyListContents(theTestList, new int[]{594, 810, 914});

        assertTrue("Fehler: Element 810 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(810));
        assertFalse("Fehler: Element 84 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(84));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{810, 914});

        theTestList.insertAt(2, 791);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 791});

        theTestList.append(92);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 791, 92});

        theTestList.append(154);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 791, 92, 154});

        theTestList.append(79);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 791, 92, 154, 79});

        theTestList.append(266);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 791, 92, 154, 79, 266});

        theTestList.append(535);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 791, 92, 154, 79, 266, 535});

        theTestList.insertAt(6, 522);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 791, 92, 154, 79, 522, 266, 535});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 791, 92, 154, 79, 266, 535});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 92, 154, 79, 266, 535});

        theTestList.append(496);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 92, 154, 79, 266, 535, 496});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 92, 154, 79, 266, 496});

        theTestList.append(192);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 92, 154, 79, 266, 496, 192});

        theTestList.append(723);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 92, 154, 79, 266, 496, 192, 723});

        theTestList.append(299);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 92, 154, 79, 266, 496, 192, 723, 299});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 154, 79, 266, 496, 192, 723, 299});

        assertTrue("Fehler: Element 299 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(299));
        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 154, 79, 266, 496, 192, 299});

        assertFalse("Fehler: Element 407 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(407));
        theTestList.insertAt(5, 143);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 154, 79, 266, 143, 496, 192, 299});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 154, 79, 266, 143, 496, 192});

        theTestList.insertAt(4, 126);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 154, 79, 126, 266, 143, 496, 192});

        theTestList.insertAt(7, 374);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 154, 79, 126, 266, 143, 374, 496, 192});

        theTestList.insertAt(2, 47);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 47, 154, 79, 126, 266, 143, 374, 496, 192});

        theTestList.insertAt(8, 686);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 47, 154, 79, 126, 266, 143, 686, 374, 496, 192});

        theTestList.insertAt(3, 607);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 47, 607, 154, 79, 126, 266, 143, 686, 374, 496, 192});

        theTestList.insertAt(3, 599);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 47, 599, 607, 154, 79, 126, 266, 143, 686, 374, 496, 192});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 599, 607, 154, 79, 126, 266, 143, 686, 374, 496, 192});

        assertTrue("Fehler: Element 686 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(686));
        theTestList.insertAt(3, 216);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 599, 216, 607, 154, 79, 126, 266, 143, 686, 374, 496, 192});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 599, 216, 607, 154, 126, 266, 143, 686, 374, 496, 192});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 599, 216, 607, 154, 126, 266, 143, 374, 496, 192});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 599, 216, 607, 154, 126, 143, 374, 496, 192});

        theTestList.insertAt(10, 865);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 599, 216, 607, 154, 126, 143, 374, 496, 865, 192});

        assertTrue("Fehler: Element 607 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(607));
        assertTrue("Fehler: Element 914 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(914));
        theTestList.insertAt(8, 444);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 599, 216, 607, 154, 126, 143, 444, 374, 496, 865, 192});

        theTestList.append(137);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 599, 216, 607, 154, 126, 143, 444, 374, 496, 865, 192, 137});

        theTestList.append(349);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 599, 216, 607, 154, 126, 143, 444, 374, 496, 865, 192, 137, 349});

        theTestList.removeAt(12);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 599, 216, 607, 154, 126, 143, 444, 374, 496, 865, 137, 349});

        assertTrue("Fehler: Element 914 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(914));
        assertFalse("Fehler: Element 156 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(156));
        assertTrue("Fehler: Element 349 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(349));
        theTestList.append(318);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 599, 216, 607, 154, 126, 143, 444, 374, 496, 865, 137, 349, 318});

        theTestList.append(700);
        Tester.verifyListContents(theTestList, new int[]{810, 914, 599, 216, 607, 154, 126, 143, 444, 374, 496, 865, 137, 349, 318, 700});

        assertTrue("Fehler: Element 444 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(444));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 154, 126, 143, 444, 374, 496, 865, 137, 349, 318, 700});

        assertTrue("Fehler: Element 216 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(216));
        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 154, 126, 143, 444, 496, 865, 137, 349, 318, 700});

        assertFalse("Fehler: Element 597 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(597));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 126, 143, 444, 496, 865, 137, 349, 318, 700});

        theTestList.insertAt(10, 414);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 126, 143, 444, 496, 865, 137, 414, 349, 318, 700});

        assertFalse("Fehler: Element 188 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(188));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 143, 444, 496, 865, 137, 414, 349, 318, 700});

        assertFalse("Fehler: Element 175 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(175));
        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 143, 444, 496, 865, 137, 414, 349, 700});

        theTestList.insertAt(9, 515);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 143, 444, 496, 865, 137, 515, 414, 349, 700});

        theTestList.insertAt(7, 768);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 143, 444, 496, 768, 865, 137, 515, 414, 349, 700});

        theTestList.insertAt(7, 82);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 143, 444, 496, 82, 768, 865, 137, 515, 414, 349, 700});

        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 143, 444, 496, 82, 768, 865, 515, 414, 349, 700});

        theTestList.insertAt(4, 443);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 443, 143, 444, 496, 82, 768, 865, 515, 414, 349, 700});

        theTestList.insertAt(8, 713);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 443, 143, 444, 496, 713, 82, 768, 865, 515, 414, 349, 700});

        theTestList.insertAt(3, 311);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 311, 607, 443, 143, 444, 496, 713, 82, 768, 865, 515, 414, 349, 700});

        assertFalse("Fehler: Element 553 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(553));
        theTestList.append(503);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 311, 607, 443, 143, 444, 496, 713, 82, 768, 865, 515, 414, 349, 700, 503});

        theTestList.append(665);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 311, 607, 443, 143, 444, 496, 713, 82, 768, 865, 515, 414, 349, 700, 503, 665});

        theTestList.insertAt(14, 923);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 311, 607, 443, 143, 444, 496, 713, 82, 768, 865, 515, 923, 414, 349, 700, 503, 665});

        assertFalse("Fehler: Element 274 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(274));
        assertTrue("Fehler: Element 713 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(713));
        assertFalse("Fehler: Element 138 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(138));
        theTestList.append(518);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 311, 607, 443, 143, 444, 496, 713, 82, 768, 865, 515, 923, 414, 349, 700, 503, 665, 518});

        theTestList.removeAt(17);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 311, 607, 443, 143, 444, 496, 713, 82, 768, 865, 515, 923, 414, 349, 503, 665, 518});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 443, 143, 444, 496, 713, 82, 768, 865, 515, 923, 414, 349, 503, 665, 518});

        theTestList.append(758);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 443, 143, 444, 496, 713, 82, 768, 865, 515, 923, 414, 349, 503, 665, 518, 758});

        theTestList.removeAt(14);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 443, 143, 444, 496, 713, 82, 768, 865, 515, 923, 349, 503, 665, 518, 758});

        assertFalse("Fehler: Element 662 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(662));
        theTestList.append(578);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 443, 143, 444, 496, 713, 82, 768, 865, 515, 923, 349, 503, 665, 518, 758, 578});

        theTestList.insertAt(18, 349);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 443, 143, 444, 496, 713, 82, 768, 865, 515, 923, 349, 503, 665, 518, 349, 758, 578});

        theTestList.insertAt(11, 961);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 443, 143, 444, 496, 713, 82, 768, 961, 865, 515, 923, 349, 503, 665, 518, 349, 758, 578});

        theTestList.append(166);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 443, 143, 444, 496, 713, 82, 768, 961, 865, 515, 923, 349, 503, 665, 518, 349, 758, 578, 166});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 143, 444, 496, 713, 82, 768, 961, 865, 515, 923, 349, 503, 665, 518, 349, 758, 578, 166});

        assertFalse("Fehler: Element 776 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(776));
        theTestList.insertAt(8, 665);
        Tester.verifyListContents(theTestList, new int[]{914, 599, 216, 607, 143, 444, 496, 713, 665, 82, 768, 961, 865, 515, 923, 349, 503, 665, 518, 349, 758, 578, 166});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 749);
        Tester.verifyListContents(theTestList, new int[]{749});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 588);
        Tester.verifyListContents(theTestList, new int[]{588});

        assertFalse("Fehler: Element 942 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(942));
        assertTrue("Fehler: Element 588 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(588));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(264);
        Tester.verifyListContents(theTestList, new int[]{264});

        theTestList.insertAt(1, 842);
        Tester.verifyListContents(theTestList, new int[]{264, 842});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(709);
        Tester.verifyListContents(theTestList, new int[]{709});

        assertTrue("Fehler: Element 709 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(709));
    }

    public static void test3Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 962);
        Tester.verifyListContents(theTestList, new int[]{962});

        theTestList.insertAt(0, 257);
        Tester.verifyListContents(theTestList, new int[]{257, 962});

        theTestList.insertAt(0, 662);
        Tester.verifyListContents(theTestList, new int[]{662, 257, 962});

        theTestList.insertAt(2, 798);
        Tester.verifyListContents(theTestList, new int[]{662, 257, 798, 962});

        assertFalse("Fehler: Element 327 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(327));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{662, 257, 798});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 70);
        Tester.verifyListContents(theTestList, new int[]{70});

        assertTrue("Fehler: Element 70 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(70));
        assertTrue("Fehler: Element 70 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(70));
        assertFalse("Fehler: Element 181 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(181));
        assertFalse("Fehler: Element 741 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(741));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(890);
        Tester.verifyListContents(theTestList, new int[]{890});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(465);
        Tester.verifyListContents(theTestList, new int[]{465});

        theTestList.append(118);
        Tester.verifyListContents(theTestList, new int[]{465, 118});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{118});

        theTestList.insertAt(1, 665);
        Tester.verifyListContents(theTestList, new int[]{118, 665});

        assertTrue("Fehler: Element 665 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(665));
        theTestList.insertAt(2, 90);
        Tester.verifyListContents(theTestList, new int[]{118, 665, 90});

        theTestList.append(998);
        Tester.verifyListContents(theTestList, new int[]{118, 665, 90, 998});

        theTestList.append(60);
        Tester.verifyListContents(theTestList, new int[]{118, 665, 90, 998, 60});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{118, 665, 998, 60});

        theTestList.append(365);
        Tester.verifyListContents(theTestList, new int[]{118, 665, 998, 60, 365});

        theTestList.insertAt(1, 45);
        Tester.verifyListContents(theTestList, new int[]{118, 45, 665, 998, 60, 365});

        theTestList.append(275);
        Tester.verifyListContents(theTestList, new int[]{118, 45, 665, 998, 60, 365, 275});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{118, 665, 998, 60, 365, 275});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(69);
        Tester.verifyListContents(theTestList, new int[]{69});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(612);
        Tester.verifyListContents(theTestList, new int[]{612});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(417);
        Tester.verifyListContents(theTestList, new int[]{417});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(595);
        Tester.verifyListContents(theTestList, new int[]{595});

        theTestList.insertAt(0, 939);
        Tester.verifyListContents(theTestList, new int[]{939, 595});

        assertTrue("Fehler: Element 939 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(939));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 141);
        Tester.verifyListContents(theTestList, new int[]{141});

        theTestList.append(570);
        Tester.verifyListContents(theTestList, new int[]{141, 570});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{570});

        assertFalse("Fehler: Element 263 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(263));
        assertFalse("Fehler: Element 448 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(448));
        assertFalse("Fehler: Element 95 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(95));
        theTestList.insertAt(1, 684);
        Tester.verifyListContents(theTestList, new int[]{570, 684});

        theTestList.append(264);
        Tester.verifyListContents(theTestList, new int[]{570, 684, 264});

        theTestList.insertAt(1, 478);
        Tester.verifyListContents(theTestList, new int[]{570, 478, 684, 264});

        theTestList.insertAt(1, 300);
        Tester.verifyListContents(theTestList, new int[]{570, 300, 478, 684, 264});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{300, 478, 684, 264});

        assertTrue("Fehler: Element 300 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(300));
        assertTrue("Fehler: Element 684 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(684));
        assertTrue("Fehler: Element 478 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(478));
        theTestList.append(308);
        Tester.verifyListContents(theTestList, new int[]{300, 478, 684, 264, 308});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 264, 308});

        assertTrue("Fehler: Element 684 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(684));
        theTestList.insertAt(4, 743);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 264, 308, 743});

        assertFalse("Fehler: Element 825 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(825));
        theTestList.insertAt(2, 721);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 721, 264, 308, 743});

        theTestList.insertAt(2, 688);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 688, 721, 264, 308, 743});

        theTestList.insertAt(4, 214);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 688, 721, 214, 264, 308, 743});

        theTestList.insertAt(3, 144);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 688, 144, 721, 214, 264, 308, 743});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 688, 144, 721, 264, 308, 743});

        theTestList.insertAt(7, 845);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 688, 144, 721, 264, 308, 845, 743});

        theTestList.append(371);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 688, 144, 721, 264, 308, 845, 743, 371});

        theTestList.append(606);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 688, 144, 721, 264, 308, 845, 743, 371, 606});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 688, 144, 721, 264, 308, 845, 371, 606});

        assertFalse("Fehler: Element 196 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(196));
        assertTrue("Fehler: Element 845 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(845));
        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 688, 144, 721, 264, 308, 845, 606});

        assertTrue("Fehler: Element 721 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(721));
        theTestList.append(817);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 688, 144, 721, 264, 308, 845, 606, 817});

        theTestList.insertAt(7, 575);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 688, 144, 721, 264, 308, 575, 845, 606, 817});

        assertFalse("Fehler: Element 364 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(364));
        assertTrue("Fehler: Element 845 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(845));
        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 688, 144, 721, 264, 308, 575, 845, 606});

        theTestList.insertAt(9, 704);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 688, 144, 721, 264, 308, 575, 845, 704, 606});

        theTestList.insertAt(9, 587);
        Tester.verifyListContents(theTestList, new int[]{478, 684, 688, 144, 721, 264, 308, 575, 845, 587, 704, 606});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{478, 688, 144, 721, 264, 308, 575, 845, 587, 704, 606});

        assertFalse("Fehler: Element 910 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(910));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{478, 688, 144, 264, 308, 575, 845, 587, 704, 606});

        theTestList.insertAt(10, 871);
        Tester.verifyListContents(theTestList, new int[]{478, 688, 144, 264, 308, 575, 845, 587, 704, 606, 871});

        assertTrue("Fehler: Element 704 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(704));
        theTestList.insertAt(1, 600);
        Tester.verifyListContents(theTestList, new int[]{478, 600, 688, 144, 264, 308, 575, 845, 587, 704, 606, 871});

        theTestList.insertAt(1, 910);
        Tester.verifyListContents(theTestList, new int[]{478, 910, 600, 688, 144, 264, 308, 575, 845, 587, 704, 606, 871});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{478, 910, 600, 688, 144, 264, 308, 575, 587, 704, 606, 871});

        theTestList.insertAt(2, 326);
        Tester.verifyListContents(theTestList, new int[]{478, 910, 326, 600, 688, 144, 264, 308, 575, 587, 704, 606, 871});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{478, 910, 326, 688, 144, 264, 308, 575, 587, 704, 606, 871});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{478, 910, 326, 688, 264, 308, 575, 587, 704, 606, 871});

        theTestList.append(866);
        Tester.verifyListContents(theTestList, new int[]{478, 910, 326, 688, 264, 308, 575, 587, 704, 606, 871, 866});

        theTestList.append(406);
        Tester.verifyListContents(theTestList, new int[]{478, 910, 326, 688, 264, 308, 575, 587, 704, 606, 871, 866, 406});

        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{478, 910, 326, 688, 264, 308, 575, 587, 704, 606, 866, 406});

        theTestList.append(954);
        Tester.verifyListContents(theTestList, new int[]{478, 910, 326, 688, 264, 308, 575, 587, 704, 606, 866, 406, 954});

        theTestList.insertAt(2, 916);
        Tester.verifyListContents(theTestList, new int[]{478, 910, 916, 326, 688, 264, 308, 575, 587, 704, 606, 866, 406, 954});

        theTestList.append(483);
        Tester.verifyListContents(theTestList, new int[]{478, 910, 916, 326, 688, 264, 308, 575, 587, 704, 606, 866, 406, 954, 483});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{478, 910, 916, 326, 688, 264, 575, 587, 704, 606, 866, 406, 954, 483});

        assertFalse("Fehler: Element 477 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(477));
        theTestList.append(280);
        Tester.verifyListContents(theTestList, new int[]{478, 910, 916, 326, 688, 264, 575, 587, 704, 606, 866, 406, 954, 483, 280});

        assertFalse("Fehler: Element 307 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(307));
        theTestList.append(424);
        Tester.verifyListContents(theTestList, new int[]{478, 910, 916, 326, 688, 264, 575, 587, 704, 606, 866, 406, 954, 483, 280, 424});

    }

    public static void test4Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 931);
        Tester.verifyListContents(theTestList, new int[]{931});

        assertFalse("Fehler: Element 999 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(999));
        theTestList.append(829);
        Tester.verifyListContents(theTestList, new int[]{931, 829});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{829});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 737);
        Tester.verifyListContents(theTestList, new int[]{737});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 412);
        Tester.verifyListContents(theTestList, new int[]{412});

        theTestList.append(455);
        Tester.verifyListContents(theTestList, new int[]{412, 455});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{412});

        theTestList.insertAt(1, 884);
        Tester.verifyListContents(theTestList, new int[]{412, 884});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{884});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 995);
        Tester.verifyListContents(theTestList, new int[]{995});

        theTestList.insertAt(1, 304);
        Tester.verifyListContents(theTestList, new int[]{995, 304});

        theTestList.append(220);
        Tester.verifyListContents(theTestList, new int[]{995, 304, 220});

        theTestList.append(348);
        Tester.verifyListContents(theTestList, new int[]{995, 304, 220, 348});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{995, 304, 348});

        assertFalse("Fehler: Element 706 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(706));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{304, 348});

        theTestList.append(944);
        Tester.verifyListContents(theTestList, new int[]{304, 348, 944});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{304, 944});

        theTestList.insertAt(1, 372);
        Tester.verifyListContents(theTestList, new int[]{304, 372, 944});

        assertTrue("Fehler: Element 944 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(944));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{304, 944});

        theTestList.append(225);
        Tester.verifyListContents(theTestList, new int[]{304, 944, 225});

        theTestList.insertAt(1, 496);
        Tester.verifyListContents(theTestList, new int[]{304, 496, 944, 225});

        assertTrue("Fehler: Element 225 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(225));
        theTestList.append(693);
        Tester.verifyListContents(theTestList, new int[]{304, 496, 944, 225, 693});

        theTestList.insertAt(4, 543);
        Tester.verifyListContents(theTestList, new int[]{304, 496, 944, 225, 543, 693});

        theTestList.append(718);
        Tester.verifyListContents(theTestList, new int[]{304, 496, 944, 225, 543, 693, 718});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{304, 944, 225, 543, 693, 718});

        theTestList.append(896);
        Tester.verifyListContents(theTestList, new int[]{304, 944, 225, 543, 693, 718, 896});

        assertTrue("Fehler: Element 225 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(225));
        theTestList.insertAt(0, 159);
        Tester.verifyListContents(theTestList, new int[]{159, 304, 944, 225, 543, 693, 718, 896});

        theTestList.insertAt(4, 199);
        Tester.verifyListContents(theTestList, new int[]{159, 304, 944, 225, 199, 543, 693, 718, 896});

        assertTrue("Fehler: Element 304 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(304));
        assertTrue("Fehler: Element 896 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(896));
        theTestList.insertAt(0, 317);
        Tester.verifyListContents(theTestList, new int[]{317, 159, 304, 944, 225, 199, 543, 693, 718, 896});

        theTestList.append(858);
        Tester.verifyListContents(theTestList, new int[]{317, 159, 304, 944, 225, 199, 543, 693, 718, 896, 858});

        theTestList.append(581);
        Tester.verifyListContents(theTestList, new int[]{317, 159, 304, 944, 225, 199, 543, 693, 718, 896, 858, 581});

        theTestList.insertAt(0, 97);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 159, 304, 944, 225, 199, 543, 693, 718, 896, 858, 581});

        theTestList.insertAt(4, 667);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 159, 304, 667, 944, 225, 199, 543, 693, 718, 896, 858, 581});

        theTestList.removeAt(12);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 159, 304, 667, 944, 225, 199, 543, 693, 718, 896, 581});

        theTestList.append(267);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 159, 304, 667, 944, 225, 199, 543, 693, 718, 896, 581, 267});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 159, 304, 667, 944, 225, 543, 693, 718, 896, 581, 267});

        theTestList.append(368);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 159, 304, 667, 944, 225, 543, 693, 718, 896, 581, 267, 368});

        theTestList.append(245);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 159, 304, 667, 944, 225, 543, 693, 718, 896, 581, 267, 368, 245});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 159, 304, 667, 944, 225, 543, 693, 896, 581, 267, 368, 245});

        theTestList.append(371);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 159, 304, 667, 944, 225, 543, 693, 896, 581, 267, 368, 245, 371});

        assertFalse("Fehler: Element 828 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(828));
        theTestList.insertAt(2, 919);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 919, 159, 304, 667, 944, 225, 543, 693, 896, 581, 267, 368, 245, 371});

        theTestList.removeAt(14);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 919, 159, 304, 667, 944, 225, 543, 693, 896, 581, 267, 368, 371});

        theTestList.insertAt(3, 272);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 919, 272, 159, 304, 667, 944, 225, 543, 693, 896, 581, 267, 368, 371});

        theTestList.append(718);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 919, 272, 159, 304, 667, 944, 225, 543, 693, 896, 581, 267, 368, 371, 718});

        theTestList.append(568);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 919, 272, 159, 304, 667, 944, 225, 543, 693, 896, 581, 267, 368, 371, 718, 568});

        assertFalse("Fehler: Element 287 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(287));
        theTestList.insertAt(3, 874);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 919, 874, 272, 159, 304, 667, 944, 225, 543, 693, 896, 581, 267, 368, 371, 718, 568});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 919, 272, 159, 304, 667, 944, 225, 543, 693, 896, 581, 267, 368, 371, 718, 568});

        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 919, 272, 159, 304, 667, 944, 225, 543, 693, 581, 267, 368, 371, 718, 568});

        theTestList.append(810);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 919, 272, 159, 304, 667, 944, 225, 543, 693, 581, 267, 368, 371, 718, 568, 810});

        assertTrue("Fehler: Element 267 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(267));
        assertTrue("Fehler: Element 368 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(368));
        theTestList.append(65);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 919, 272, 159, 304, 667, 944, 225, 543, 693, 581, 267, 368, 371, 718, 568, 810, 65});

        theTestList.append(234);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 919, 272, 159, 304, 667, 944, 225, 543, 693, 581, 267, 368, 371, 718, 568, 810, 65, 234});

        theTestList.append(285);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 919, 272, 159, 304, 667, 944, 225, 543, 693, 581, 267, 368, 371, 718, 568, 810, 65, 234, 285});

        theTestList.insertAt(17, 501);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 919, 272, 159, 304, 667, 944, 225, 543, 693, 581, 267, 368, 371, 718, 568, 501, 810, 65, 234, 285});

        theTestList.insertAt(7, 818);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 919, 272, 159, 304, 667, 818, 944, 225, 543, 693, 581, 267, 368, 371, 718, 568, 501, 810, 65, 234, 285});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 272, 159, 304, 667, 818, 944, 225, 543, 693, 581, 267, 368, 371, 718, 568, 501, 810, 65, 234, 285});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 272, 159, 304, 667, 818, 944, 543, 693, 581, 267, 368, 371, 718, 568, 501, 810, 65, 234, 285});

        assertTrue("Fehler: Element 718 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(718));
        theTestList.append(277);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 272, 159, 304, 667, 818, 944, 543, 693, 581, 267, 368, 371, 718, 568, 501, 810, 65, 234, 285, 277});

        theTestList.append(319);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 272, 159, 304, 667, 818, 944, 543, 693, 581, 267, 368, 371, 718, 568, 501, 810, 65, 234, 285, 277, 319});

        theTestList.append(190);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 272, 159, 304, 667, 818, 944, 543, 693, 581, 267, 368, 371, 718, 568, 501, 810, 65, 234, 285, 277, 319, 190});

        theTestList.insertAt(2, 631);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 631, 272, 159, 304, 667, 818, 944, 543, 693, 581, 267, 368, 371, 718, 568, 501, 810, 65, 234, 285, 277, 319, 190});

        theTestList.insertAt(13, 516);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 631, 272, 159, 304, 667, 818, 944, 543, 693, 581, 267, 516, 368, 371, 718, 568, 501, 810, 65, 234, 285, 277, 319, 190});

        assertTrue("Fehler: Element 371 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(371));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 272, 159, 304, 667, 818, 944, 543, 693, 581, 267, 516, 368, 371, 718, 568, 501, 810, 65, 234, 285, 277, 319, 190});

        theTestList.removeAt(21);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 272, 159, 304, 667, 818, 944, 543, 693, 581, 267, 516, 368, 371, 718, 568, 501, 810, 65, 234, 277, 319, 190});

        theTestList.append(446);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 272, 159, 304, 667, 818, 944, 543, 693, 581, 267, 516, 368, 371, 718, 568, 501, 810, 65, 234, 277, 319, 190, 446});

        theTestList.append(922);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 272, 159, 304, 667, 818, 944, 543, 693, 581, 267, 516, 368, 371, 718, 568, 501, 810, 65, 234, 277, 319, 190, 446, 922});

        theTestList.append(442);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 272, 159, 304, 667, 818, 944, 543, 693, 581, 267, 516, 368, 371, 718, 568, 501, 810, 65, 234, 277, 319, 190, 446, 922, 442});

        assertTrue("Fehler: Element 818 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(818));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 272, 159, 667, 818, 944, 543, 693, 581, 267, 516, 368, 371, 718, 568, 501, 810, 65, 234, 277, 319, 190, 446, 922, 442});

        theTestList.removeAt(13);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 272, 159, 667, 818, 944, 543, 693, 581, 267, 516, 368, 718, 568, 501, 810, 65, 234, 277, 319, 190, 446, 922, 442});

        theTestList.removeAt(13);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 272, 159, 667, 818, 944, 543, 693, 581, 267, 516, 368, 568, 501, 810, 65, 234, 277, 319, 190, 446, 922, 442});

        theTestList.append(351);
        Tester.verifyListContents(theTestList, new int[]{97, 317, 272, 159, 667, 818, 944, 543, 693, 581, 267, 516, 368, 568, 501, 810, 65, 234, 277, 319, 190, 446, 922, 442, 351});

        assertTrue("Fehler: Element 234 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(234));
        assertTrue("Fehler: Element 922 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(922));
        assertTrue("Fehler: Element 351 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(351));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 157);
        Tester.verifyListContents(theTestList, new int[]{157});

        theTestList.append(344);
        Tester.verifyListContents(theTestList, new int[]{157, 344});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{157});

        assertTrue("Fehler: Element 157 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(157));
        assertTrue("Fehler: Element 157 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(157));
        assertTrue("Fehler: Element 157 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(157));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(897);
        Tester.verifyListContents(theTestList, new int[]{897});

        assertTrue("Fehler: Element 897 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(897));
    }

    public static void test5Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(186);
        Tester.verifyListContents(theTestList, new int[]{186});

        theTestList.append(758);
        Tester.verifyListContents(theTestList, new int[]{186, 758});

        assertFalse("Fehler: Element 673 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(673));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{186});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(894);
        Tester.verifyListContents(theTestList, new int[]{894});

        theTestList.insertAt(0, 873);
        Tester.verifyListContents(theTestList, new int[]{873, 894});

        theTestList.append(332);
        Tester.verifyListContents(theTestList, new int[]{873, 894, 332});

        assertTrue("Fehler: Element 873 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(873));
        assertFalse("Fehler: Element 519 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(519));
        theTestList.insertAt(2, 79);
        Tester.verifyListContents(theTestList, new int[]{873, 894, 79, 332});

        theTestList.append(482);
        Tester.verifyListContents(theTestList, new int[]{873, 894, 79, 332, 482});

        assertTrue("Fehler: Element 873 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(873));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(534);
        Tester.verifyListContents(theTestList, new int[]{534});

        theTestList.append(923);
        Tester.verifyListContents(theTestList, new int[]{534, 923});

        theTestList.insertAt(0, 834);
        Tester.verifyListContents(theTestList, new int[]{834, 534, 923});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{834, 923});

        assertTrue("Fehler: Element 923 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(923));
        theTestList.append(45);
        Tester.verifyListContents(theTestList, new int[]{834, 923, 45});

        theTestList.append(275);
        Tester.verifyListContents(theTestList, new int[]{834, 923, 45, 275});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 993);
        Tester.verifyListContents(theTestList, new int[]{993});

        theTestList.insertAt(1, 568);
        Tester.verifyListContents(theTestList, new int[]{993, 568});

        theTestList.insertAt(1, 966);
        Tester.verifyListContents(theTestList, new int[]{993, 966, 568});

        theTestList.insertAt(1, 131);
        Tester.verifyListContents(theTestList, new int[]{993, 131, 966, 568});

        theTestList.append(863);
        Tester.verifyListContents(theTestList, new int[]{993, 131, 966, 568, 863});

        assertFalse("Fehler: Element 774 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(774));
        theTestList.append(123);
        Tester.verifyListContents(theTestList, new int[]{993, 131, 966, 568, 863, 123});

        theTestList.insertAt(2, 480);
        Tester.verifyListContents(theTestList, new int[]{993, 131, 480, 966, 568, 863, 123});

        assertFalse("Fehler: Element 873 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(873));
        assertFalse("Fehler: Element 528 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(528));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{993, 131, 480, 568, 863, 123});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{993, 131, 480, 863, 123});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{993, 131, 480, 863});

        theTestList.append(416);
        Tester.verifyListContents(theTestList, new int[]{993, 131, 480, 863, 416});

        theTestList.append(257);
        Tester.verifyListContents(theTestList, new int[]{993, 131, 480, 863, 416, 257});

        theTestList.append(823);
        Tester.verifyListContents(theTestList, new int[]{993, 131, 480, 863, 416, 257, 823});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(806);
        Tester.verifyListContents(theTestList, new int[]{806});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(610);
        Tester.verifyListContents(theTestList, new int[]{610});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(288);
        Tester.verifyListContents(theTestList, new int[]{288});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 98);
        Tester.verifyListContents(theTestList, new int[]{98});

        theTestList.insertAt(0, 255);
        Tester.verifyListContents(theTestList, new int[]{255, 98});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{255});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(742);
        Tester.verifyListContents(theTestList, new int[]{742});

        theTestList.insertAt(1, 403);
        Tester.verifyListContents(theTestList, new int[]{742, 403});

        theTestList.insertAt(0, 236);
        Tester.verifyListContents(theTestList, new int[]{236, 742, 403});

        assertTrue("Fehler: Element 236 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(236));
        theTestList.insertAt(3, 278);
        Tester.verifyListContents(theTestList, new int[]{236, 742, 403, 278});

        assertTrue("Fehler: Element 278 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(278));
        theTestList.append(21);
        Tester.verifyListContents(theTestList, new int[]{236, 742, 403, 278, 21});

        theTestList.append(154);
        Tester.verifyListContents(theTestList, new int[]{236, 742, 403, 278, 21, 154});

        theTestList.insertAt(3, 719);
        Tester.verifyListContents(theTestList, new int[]{236, 742, 403, 719, 278, 21, 154});

        theTestList.insertAt(7, 599);
        Tester.verifyListContents(theTestList, new int[]{236, 742, 403, 719, 278, 21, 154, 599});

        assertTrue("Fehler: Element 21 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(21));
        assertFalse("Fehler: Element 93 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(93));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{236, 742, 719, 278, 21, 154, 599});

        theTestList.insertAt(2, 605);
        Tester.verifyListContents(theTestList, new int[]{236, 742, 605, 719, 278, 21, 154, 599});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 278, 21, 154, 599});

        theTestList.append(75);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 278, 21, 154, 599, 75});

        assertFalse("Fehler: Element 155 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(155));
        assertTrue("Fehler: Element 719 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(719));
        theTestList.append(528);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 278, 21, 154, 599, 75, 528});

        theTestList.append(138);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 278, 21, 154, 599, 75, 528, 138});

        theTestList.append(340);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 278, 21, 154, 599, 75, 528, 138, 340});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 278, 21, 154, 599, 75, 138, 340});

        theTestList.append(744);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 278, 21, 154, 599, 75, 138, 340, 744});

        assertFalse("Fehler: Element 449 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(449));
        theTestList.append(719);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 278, 21, 154, 599, 75, 138, 340, 744, 719});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 21, 154, 599, 75, 138, 340, 744, 719});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 21, 154, 599, 75, 138, 340, 719});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 21, 154, 599, 75, 138, 719});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 21, 599, 75, 138, 719});

        theTestList.insertAt(7, 445);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 21, 599, 75, 138, 445, 719});

        theTestList.insertAt(5, 845);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 21, 599, 845, 75, 138, 445, 719});

        theTestList.append(333);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 21, 599, 845, 75, 138, 445, 719, 333});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 21, 599, 845, 75, 138, 719, 333});

        theTestList.insertAt(2, 536);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 536, 719, 21, 599, 845, 75, 138, 719, 333});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 21, 599, 845, 75, 138, 719, 333});

        assertTrue("Fehler: Element 599 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(599));
        theTestList.append(733);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 21, 599, 845, 75, 138, 719, 333, 733});

        theTestList.append(861);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 21, 599, 845, 75, 138, 719, 333, 733, 861});

        theTestList.insertAt(6, 878);
        Tester.verifyListContents(theTestList, new int[]{236, 605, 719, 21, 599, 845, 878, 75, 138, 719, 333, 733, 861});

        theTestList.insertAt(1, 889);
        Tester.verifyListContents(theTestList, new int[]{236, 889, 605, 719, 21, 599, 845, 878, 75, 138, 719, 333, 733, 861});

        theTestList.append(939);
        Tester.verifyListContents(theTestList, new int[]{236, 889, 605, 719, 21, 599, 845, 878, 75, 138, 719, 333, 733, 861, 939});

        assertFalse("Fehler: Element 667 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(667));
        theTestList.insertAt(2, 741);
        Tester.verifyListContents(theTestList, new int[]{236, 889, 741, 605, 719, 21, 599, 845, 878, 75, 138, 719, 333, 733, 861, 939});

        theTestList.append(664);
        Tester.verifyListContents(theTestList, new int[]{236, 889, 741, 605, 719, 21, 599, 845, 878, 75, 138, 719, 333, 733, 861, 939, 664});

        theTestList.append(344);
        Tester.verifyListContents(theTestList, new int[]{236, 889, 741, 605, 719, 21, 599, 845, 878, 75, 138, 719, 333, 733, 861, 939, 664, 344});

        theTestList.insertAt(18, 976);
        Tester.verifyListContents(theTestList, new int[]{236, 889, 741, 605, 719, 21, 599, 845, 878, 75, 138, 719, 333, 733, 861, 939, 664, 344, 976});

        theTestList.append(896);
        Tester.verifyListContents(theTestList, new int[]{236, 889, 741, 605, 719, 21, 599, 845, 878, 75, 138, 719, 333, 733, 861, 939, 664, 344, 976, 896});

        theTestList.insertAt(4, 794);
        Tester.verifyListContents(theTestList, new int[]{236, 889, 741, 605, 794, 719, 21, 599, 845, 878, 75, 138, 719, 333, 733, 861, 939, 664, 344, 976, 896});

        theTestList.insertAt(8, 209);
        Tester.verifyListContents(theTestList, new int[]{236, 889, 741, 605, 794, 719, 21, 599, 209, 845, 878, 75, 138, 719, 333, 733, 861, 939, 664, 344, 976, 896});

        theTestList.removeAt(15);
        Tester.verifyListContents(theTestList, new int[]{236, 889, 741, 605, 794, 719, 21, 599, 209, 845, 878, 75, 138, 719, 333, 861, 939, 664, 344, 976, 896});

        theTestList.insertAt(2, 35);
        Tester.verifyListContents(theTestList, new int[]{236, 889, 35, 741, 605, 794, 719, 21, 599, 209, 845, 878, 75, 138, 719, 333, 861, 939, 664, 344, 976, 896});

    }

    public static void test6Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 760);
        Tester.verifyListContents(theTestList, new int[]{760});

        theTestList.append(924);
        Tester.verifyListContents(theTestList, new int[]{760, 924});

        theTestList.append(276);
        Tester.verifyListContents(theTestList, new int[]{760, 924, 276});

        theTestList.insertAt(1, 920);
        Tester.verifyListContents(theTestList, new int[]{760, 920, 924, 276});

        theTestList.insertAt(1, 574);
        Tester.verifyListContents(theTestList, new int[]{760, 574, 920, 924, 276});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{574, 920, 924, 276});

        theTestList.append(528);
        Tester.verifyListContents(theTestList, new int[]{574, 920, 924, 276, 528});

        theTestList.append(900);
        Tester.verifyListContents(theTestList, new int[]{574, 920, 924, 276, 528, 900});

        theTestList.append(211);
        Tester.verifyListContents(theTestList, new int[]{574, 920, 924, 276, 528, 900, 211});

        theTestList.append(189);
        Tester.verifyListContents(theTestList, new int[]{574, 920, 924, 276, 528, 900, 211, 189});

        theTestList.append(328);
        Tester.verifyListContents(theTestList, new int[]{574, 920, 924, 276, 528, 900, 211, 189, 328});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{574, 924, 276, 528, 900, 211, 189, 328});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{574, 924, 276, 900, 211, 189, 328});

        theTestList.insertAt(1, 457);
        Tester.verifyListContents(theTestList, new int[]{574, 457, 924, 276, 900, 211, 189, 328});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{457, 924, 276, 900, 211, 189, 328});

        theTestList.insertAt(7, 459);
        Tester.verifyListContents(theTestList, new int[]{457, 924, 276, 900, 211, 189, 328, 459});

        theTestList.insertAt(1, 777);
        Tester.verifyListContents(theTestList, new int[]{457, 777, 924, 276, 900, 211, 189, 328, 459});

        assertFalse("Fehler: Element 59 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(59));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{457, 777, 924, 900, 211, 189, 328, 459});

        assertTrue("Fehler: Element 189 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(189));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{457, 777, 900, 211, 189, 328, 459});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{457, 900, 211, 189, 328, 459});

        theTestList.insertAt(1, 319);
        Tester.verifyListContents(theTestList, new int[]{457, 319, 900, 211, 189, 328, 459});

        theTestList.append(477);
        Tester.verifyListContents(theTestList, new int[]{457, 319, 900, 211, 189, 328, 459, 477});

        theTestList.insertAt(5, 955);
        Tester.verifyListContents(theTestList, new int[]{457, 319, 900, 211, 189, 955, 328, 459, 477});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{319, 900, 211, 189, 955, 328, 459, 477});

        assertTrue("Fehler: Element 319 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(319));
        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{319, 900, 211, 189, 955, 328, 477});

        assertTrue("Fehler: Element 955 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(955));
        assertFalse("Fehler: Element 839 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(839));
        assertFalse("Fehler: Element 985 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(985));
        theTestList.insertAt(2, 771);
        Tester.verifyListContents(theTestList, new int[]{319, 900, 771, 211, 189, 955, 328, 477});

        assertFalse("Fehler: Element 848 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(848));
        theTestList.insertAt(2, 421);
        Tester.verifyListContents(theTestList, new int[]{319, 900, 421, 771, 211, 189, 955, 328, 477});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{319, 421, 771, 211, 189, 955, 328, 477});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{319, 771, 211, 189, 955, 328, 477});

        assertTrue("Fehler: Element 328 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(328));
        theTestList.append(852);
        Tester.verifyListContents(theTestList, new int[]{319, 771, 211, 189, 955, 328, 477, 852});

        theTestList.append(131);
        Tester.verifyListContents(theTestList, new int[]{319, 771, 211, 189, 955, 328, 477, 852, 131});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{319, 771, 211, 189, 955, 328, 477, 131});

        theTestList.append(506);
        Tester.verifyListContents(theTestList, new int[]{319, 771, 211, 189, 955, 328, 477, 131, 506});

        theTestList.insertAt(0, 59);
        Tester.verifyListContents(theTestList, new int[]{59, 319, 771, 211, 189, 955, 328, 477, 131, 506});

        theTestList.append(824);
        Tester.verifyListContents(theTestList, new int[]{59, 319, 771, 211, 189, 955, 328, 477, 131, 506, 824});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 16);
        Tester.verifyListContents(theTestList, new int[]{16});

        assertTrue("Fehler: Element 16 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(16));
        assertTrue("Fehler: Element 16 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(16));
        theTestList.insertAt(1, 714);
        Tester.verifyListContents(theTestList, new int[]{16, 714});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{16});

        assertTrue("Fehler: Element 16 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(16));
        assertFalse("Fehler: Element 300 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(300));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(470);
        Tester.verifyListContents(theTestList, new int[]{470});

        assertFalse("Fehler: Element 472 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(472));
        assertFalse("Fehler: Element 603 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(603));
        theTestList.append(394);
        Tester.verifyListContents(theTestList, new int[]{470, 394});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{470});

// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(904);
        Tester.verifyListContents(theTestList, new int[]{904});

        theTestList.append(504);
        Tester.verifyListContents(theTestList, new int[]{904, 504});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{904});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(774);
        Tester.verifyListContents(theTestList, new int[]{774});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 476);
        Tester.verifyListContents(theTestList, new int[]{476});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 132);
        Tester.verifyListContents(theTestList, new int[]{132});

        theTestList.append(507);
        Tester.verifyListContents(theTestList, new int[]{132, 507});

        assertTrue("Fehler: Element 507 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(507));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 885);
        Tester.verifyListContents(theTestList, new int[]{885});

        theTestList.append(526);
        Tester.verifyListContents(theTestList, new int[]{885, 526});

        assertTrue("Fehler: Element 885 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(885));
        assertFalse("Fehler: Element 436 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(436));
        theTestList.insertAt(2, 174);
        Tester.verifyListContents(theTestList, new int[]{885, 526, 174});

        assertFalse("Fehler: Element 772 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(772));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{526, 174});

        theTestList.insertAt(2, 17);
        Tester.verifyListContents(theTestList, new int[]{526, 174, 17});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{174, 17});

        theTestList.insertAt(1, 828);
        Tester.verifyListContents(theTestList, new int[]{174, 828, 17});

        theTestList.append(697);
        Tester.verifyListContents(theTestList, new int[]{174, 828, 17, 697});

        theTestList.append(920);
        Tester.verifyListContents(theTestList, new int[]{174, 828, 17, 697, 920});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{174, 828, 17, 920});

        theTestList.insertAt(1, 518);
        Tester.verifyListContents(theTestList, new int[]{174, 518, 828, 17, 920});

        theTestList.insertAt(3, 685);
        Tester.verifyListContents(theTestList, new int[]{174, 518, 828, 685, 17, 920});

        theTestList.append(396);
        Tester.verifyListContents(theTestList, new int[]{174, 518, 828, 685, 17, 920, 396});

        theTestList.append(763);
        Tester.verifyListContents(theTestList, new int[]{174, 518, 828, 685, 17, 920, 396, 763});

        assertFalse("Fehler: Element 950 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(950));
        theTestList.insertAt(3, 548);
        Tester.verifyListContents(theTestList, new int[]{174, 518, 828, 548, 685, 17, 920, 396, 763});

        assertFalse("Fehler: Element 275 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(275));
        theTestList.append(604);
        Tester.verifyListContents(theTestList, new int[]{174, 518, 828, 548, 685, 17, 920, 396, 763, 604});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{174, 518, 828, 548, 685, 920, 396, 763, 604});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{174, 518, 828, 685, 920, 396, 763, 604});

        theTestList.insertAt(2, 345);
        Tester.verifyListContents(theTestList, new int[]{174, 518, 345, 828, 685, 920, 396, 763, 604});

        theTestList.append(751);
        Tester.verifyListContents(theTestList, new int[]{174, 518, 345, 828, 685, 920, 396, 763, 604, 751});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{174, 518, 345, 828, 920, 396, 763, 604, 751});

        assertTrue("Fehler: Element 604 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(604));
        assertFalse("Fehler: Element 953 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(953));
        assertFalse("Fehler: Element 812 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(812));
        theTestList.append(136);
        Tester.verifyListContents(theTestList, new int[]{174, 518, 345, 828, 920, 396, 763, 604, 751, 136});

    }

    public static void test7Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(342);
        Tester.verifyListContents(theTestList, new int[]{342});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 771);
        Tester.verifyListContents(theTestList, new int[]{771});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 553);
        Tester.verifyListContents(theTestList, new int[]{553});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(585);
        Tester.verifyListContents(theTestList, new int[]{585});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 455);
        Tester.verifyListContents(theTestList, new int[]{455});

        assertFalse("Fehler: Element 286 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(286));
        theTestList.append(286);
        Tester.verifyListContents(theTestList, new int[]{455, 286});

        theTestList.insertAt(1, 58);
        Tester.verifyListContents(theTestList, new int[]{455, 58, 286});

        theTestList.insertAt(0, 438);
        Tester.verifyListContents(theTestList, new int[]{438, 455, 58, 286});

        theTestList.insertAt(4, 496);
        Tester.verifyListContents(theTestList, new int[]{438, 455, 58, 286, 496});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{438, 58, 286, 496});

        assertTrue("Fehler: Element 496 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(496));
        theTestList.insertAt(1, 162);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 58, 286, 496});

        theTestList.insertAt(2, 195);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 58, 286, 496});

        assertTrue("Fehler: Element 496 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(496));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 286, 496});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 496});

        theTestList.append(911);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 496, 911});

        theTestList.append(986);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 496, 911, 986});

        theTestList.insertAt(3, 102);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 102, 496, 911, 986});

        assertFalse("Fehler: Element 979 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(979));
        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 102, 496, 911});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 102, 911});

        theTestList.append(858);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 102, 911, 858});

        theTestList.insertAt(4, 413);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 102, 413, 911, 858});

        theTestList.append(102);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 102, 413, 911, 858, 102});

        theTestList.insertAt(5, 998);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 102, 413, 998, 911, 858, 102});

        theTestList.insertAt(5, 323);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 102, 413, 323, 998, 911, 858, 102});

        assertTrue("Fehler: Element 413 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(413));
        theTestList.insertAt(8, 721);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 102, 413, 323, 998, 911, 721, 858, 102});

        theTestList.append(272);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 102, 413, 323, 998, 911, 721, 858, 102, 272});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 102, 413, 323, 998, 911, 721, 102, 272});

        theTestList.append(868);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 102, 413, 323, 998, 911, 721, 102, 272, 868});

        assertFalse("Fehler: Element 20 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(20));
        theTestList.insertAt(7, 8);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 102, 413, 323, 998, 8, 911, 721, 102, 272, 868});

        theTestList.insertAt(10, 757);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 195, 102, 413, 323, 998, 8, 911, 721, 757, 102, 272, 868});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{438, 162, 102, 413, 323, 998, 8, 911, 721, 757, 102, 272, 868});

        theTestList.insertAt(1, 375);
        Tester.verifyListContents(theTestList, new int[]{438, 375, 162, 102, 413, 323, 998, 8, 911, 721, 757, 102, 272, 868});

        theTestList.insertAt(9, 216);
        Tester.verifyListContents(theTestList, new int[]{438, 375, 162, 102, 413, 323, 998, 8, 911, 216, 721, 757, 102, 272, 868});

        theTestList.append(692);
        Tester.verifyListContents(theTestList, new int[]{438, 375, 162, 102, 413, 323, 998, 8, 911, 216, 721, 757, 102, 272, 868, 692});

        theTestList.append(875);
        Tester.verifyListContents(theTestList, new int[]{438, 375, 162, 102, 413, 323, 998, 8, 911, 216, 721, 757, 102, 272, 868, 692, 875});

        theTestList.insertAt(7, 132);
        Tester.verifyListContents(theTestList, new int[]{438, 375, 162, 102, 413, 323, 998, 132, 8, 911, 216, 721, 757, 102, 272, 868, 692, 875});

        assertFalse("Fehler: Element 297 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(297));
        assertTrue("Fehler: Element 911 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(911));
        theTestList.insertAt(18, 702);
        Tester.verifyListContents(theTestList, new int[]{438, 375, 162, 102, 413, 323, 998, 132, 8, 911, 216, 721, 757, 102, 272, 868, 692, 875, 702});

        assertTrue("Fehler: Element 692 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(692));
        theTestList.insertAt(19, 707);
        Tester.verifyListContents(theTestList, new int[]{438, 375, 162, 102, 413, 323, 998, 132, 8, 911, 216, 721, 757, 102, 272, 868, 692, 875, 702, 707});

        theTestList.append(57);
        Tester.verifyListContents(theTestList, new int[]{438, 375, 162, 102, 413, 323, 998, 132, 8, 911, 216, 721, 757, 102, 272, 868, 692, 875, 702, 707, 57});

        theTestList.append(896);
        Tester.verifyListContents(theTestList, new int[]{438, 375, 162, 102, 413, 323, 998, 132, 8, 911, 216, 721, 757, 102, 272, 868, 692, 875, 702, 707, 57, 896});

        assertFalse("Fehler: Element 198 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(198));
        assertFalse("Fehler: Element 770 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(770));
        theTestList.insertAt(18, 643);
        Tester.verifyListContents(theTestList, new int[]{438, 375, 162, 102, 413, 323, 998, 132, 8, 911, 216, 721, 757, 102, 272, 868, 692, 875, 643, 702, 707, 57, 896});

        assertTrue("Fehler: Element 102 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(102));
        theTestList.append(397);
        Tester.verifyListContents(theTestList, new int[]{438, 375, 162, 102, 413, 323, 998, 132, 8, 911, 216, 721, 757, 102, 272, 868, 692, 875, 643, 702, 707, 57, 896, 397});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{438, 375, 102, 413, 323, 998, 132, 8, 911, 216, 721, 757, 102, 272, 868, 692, 875, 643, 702, 707, 57, 896, 397});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{438, 375, 102, 413, 323, 998, 132, 8, 911, 721, 757, 102, 272, 868, 692, 875, 643, 702, 707, 57, 896, 397});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{375, 102, 413, 323, 998, 132, 8, 911, 721, 757, 102, 272, 868, 692, 875, 643, 702, 707, 57, 896, 397});

        assertTrue("Fehler: Element 643 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(643));
        theTestList.append(85);
        Tester.verifyListContents(theTestList, new int[]{375, 102, 413, 323, 998, 132, 8, 911, 721, 757, 102, 272, 868, 692, 875, 643, 702, 707, 57, 896, 397, 85});

        theTestList.insertAt(5, 794);
        Tester.verifyListContents(theTestList, new int[]{375, 102, 413, 323, 998, 794, 132, 8, 911, 721, 757, 102, 272, 868, 692, 875, 643, 702, 707, 57, 896, 397, 85});

        theTestList.insertAt(2, 647);
        Tester.verifyListContents(theTestList, new int[]{375, 102, 647, 413, 323, 998, 794, 132, 8, 911, 721, 757, 102, 272, 868, 692, 875, 643, 702, 707, 57, 896, 397, 85});

        theTestList.removeAt(12);
        Tester.verifyListContents(theTestList, new int[]{375, 102, 647, 413, 323, 998, 794, 132, 8, 911, 721, 757, 272, 868, 692, 875, 643, 702, 707, 57, 896, 397, 85});

        theTestList.insertAt(1, 70);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 794, 132, 8, 911, 721, 757, 272, 868, 692, 875, 643, 702, 707, 57, 896, 397, 85});

        assertTrue("Fehler: Element 102 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(102));
        theTestList.append(397);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 794, 132, 8, 911, 721, 757, 272, 868, 692, 875, 643, 702, 707, 57, 896, 397, 85, 397});

        theTestList.removeAt(15);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 794, 132, 8, 911, 721, 757, 272, 868, 875, 643, 702, 707, 57, 896, 397, 85, 397});

        theTestList.append(491);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 794, 132, 8, 911, 721, 757, 272, 868, 875, 643, 702, 707, 57, 896, 397, 85, 397, 491});

        theTestList.append(418);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 794, 132, 8, 911, 721, 757, 272, 868, 875, 643, 702, 707, 57, 896, 397, 85, 397, 491, 418});

        theTestList.removeAt(19);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 794, 132, 8, 911, 721, 757, 272, 868, 875, 643, 702, 707, 896, 397, 85, 397, 491, 418});

        assertFalse("Fehler: Element 177 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(177));
        assertFalse("Fehler: Element 45 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(45));
        assertFalse("Fehler: Element 310 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(310));
        theTestList.removeAt(21);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 794, 132, 8, 911, 721, 757, 272, 868, 875, 643, 702, 707, 896, 397, 397, 491, 418});

        assertFalse("Fehler: Element 73 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(73));
        theTestList.append(30);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 794, 132, 8, 911, 721, 757, 272, 868, 875, 643, 702, 707, 896, 397, 397, 491, 418, 30});

        theTestList.insertAt(7, 226);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 226, 794, 132, 8, 911, 721, 757, 272, 868, 875, 643, 702, 707, 896, 397, 397, 491, 418, 30});

        assertFalse("Fehler: Element 155 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(155));
        assertFalse("Fehler: Element 395 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(395));
        theTestList.append(913);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 226, 794, 132, 8, 911, 721, 757, 272, 868, 875, 643, 702, 707, 896, 397, 397, 491, 418, 30, 913});

        theTestList.append(885);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 226, 794, 132, 8, 911, 721, 757, 272, 868, 875, 643, 702, 707, 896, 397, 397, 491, 418, 30, 913, 885});

        theTestList.insertAt(18, 647);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 226, 794, 132, 8, 911, 721, 757, 272, 868, 875, 643, 647, 702, 707, 896, 397, 397, 491, 418, 30, 913, 885});

        theTestList.removeAt(23);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 226, 794, 132, 8, 911, 721, 757, 272, 868, 875, 643, 647, 702, 707, 896, 397, 491, 418, 30, 913, 885});

        theTestList.insertAt(11, 448);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 226, 794, 132, 8, 448, 911, 721, 757, 272, 868, 875, 643, 647, 702, 707, 896, 397, 491, 418, 30, 913, 885});

        theTestList.append(2);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 226, 794, 132, 8, 448, 911, 721, 757, 272, 868, 875, 643, 647, 702, 707, 896, 397, 491, 418, 30, 913, 885, 2});

        theTestList.removeAt(25);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 226, 794, 132, 8, 448, 911, 721, 757, 272, 868, 875, 643, 647, 702, 707, 896, 397, 491, 30, 913, 885, 2});

        theTestList.insertAt(12, 390);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 102, 647, 413, 323, 998, 226, 794, 132, 8, 448, 390, 911, 721, 757, 272, 868, 875, 643, 647, 702, 707, 896, 397, 491, 30, 913, 885, 2});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 647, 413, 323, 998, 226, 794, 132, 8, 448, 390, 911, 721, 757, 272, 868, 875, 643, 647, 702, 707, 896, 397, 491, 30, 913, 885, 2});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 647, 413, 323, 998, 226, 794, 132, 448, 390, 911, 721, 757, 272, 868, 875, 643, 647, 702, 707, 896, 397, 491, 30, 913, 885, 2});

        theTestList.insertAt(19, 133);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 647, 413, 323, 998, 226, 794, 132, 448, 390, 911, 721, 757, 272, 868, 875, 643, 647, 133, 702, 707, 896, 397, 491, 30, 913, 885, 2});

        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 647, 413, 323, 998, 226, 794, 132, 448, 390, 721, 757, 272, 868, 875, 643, 647, 133, 702, 707, 896, 397, 491, 30, 913, 885, 2});

        theTestList.insertAt(15, 611);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 647, 413, 323, 998, 226, 794, 132, 448, 390, 721, 757, 272, 868, 611, 875, 643, 647, 133, 702, 707, 896, 397, 491, 30, 913, 885, 2});

        assertFalse("Fehler: Element 902 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(902));
        theTestList.insertAt(2, 327);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 327, 647, 413, 323, 998, 226, 794, 132, 448, 390, 721, 757, 272, 868, 611, 875, 643, 647, 133, 702, 707, 896, 397, 491, 30, 913, 885, 2});

        assertFalse("Fehler: Element 366 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(366));
        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{375, 70, 327, 647, 413, 323, 998, 794, 132, 448, 390, 721, 757, 272, 868, 611, 875, 643, 647, 133, 702, 707, 896, 397, 491, 30, 913, 885, 2});

        assertFalse("Fehler: Element 319 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(319));
    }

    public static void test8Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 139);
        Tester.verifyListContents(theTestList, new int[]{139});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(364);
        Tester.verifyListContents(theTestList, new int[]{364});

        assertFalse("Fehler: Element 166 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(166));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 280);
        Tester.verifyListContents(theTestList, new int[]{280});

        theTestList.insertAt(1, 996);
        Tester.verifyListContents(theTestList, new int[]{280, 996});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{996});

        assertTrue("Fehler: Element 996 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(996));
        theTestList.insertAt(0, 894);
        Tester.verifyListContents(theTestList, new int[]{894, 996});

        assertTrue("Fehler: Element 996 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(996));
        theTestList.insertAt(2, 557);
        Tester.verifyListContents(theTestList, new int[]{894, 996, 557});

        assertFalse("Fehler: Element 980 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(980));
        theTestList.insertAt(0, 472);
        Tester.verifyListContents(theTestList, new int[]{472, 894, 996, 557});

        theTestList.insertAt(1, 546);
        Tester.verifyListContents(theTestList, new int[]{472, 546, 894, 996, 557});

        assertFalse("Fehler: Element 788 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(788));
        assertTrue("Fehler: Element 557 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(557));
        theTestList.insertAt(0, 31);
        Tester.verifyListContents(theTestList, new int[]{31, 472, 546, 894, 996, 557});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{31, 472, 546, 894, 996});

        assertTrue("Fehler: Element 894 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(894));
        assertFalse("Fehler: Element 978 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(978));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{31, 546, 894, 996});

        assertTrue("Fehler: Element 996 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(996));
        theTestList.insertAt(2, 518);
        Tester.verifyListContents(theTestList, new int[]{31, 546, 518, 894, 996});

        theTestList.insertAt(1, 555);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 546, 518, 894, 996});

        theTestList.append(276);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 546, 518, 894, 996, 276});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 546, 518, 996, 276});

        theTestList.insertAt(2, 388);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 388, 546, 518, 996, 276});

        theTestList.append(897);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 388, 546, 518, 996, 276, 897});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 388, 546, 518, 276, 897});

        theTestList.append(626);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 388, 546, 518, 276, 897, 626});

        assertTrue("Fehler: Element 897 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(897));
        assertTrue("Fehler: Element 388 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(388));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 388, 546, 276, 897, 626});

        assertFalse("Fehler: Element 92 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(92));
        theTestList.append(45);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 388, 546, 276, 897, 626, 45});

        theTestList.append(307);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 388, 546, 276, 897, 626, 45, 307});

        theTestList.append(479);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 388, 546, 276, 897, 626, 45, 307, 479});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 388, 546, 897, 626, 45, 307, 479});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 388, 546, 897, 45, 307, 479});

        theTestList.append(712);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 388, 546, 897, 45, 307, 479, 712});

        theTestList.insertAt(8, 766);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 388, 546, 897, 45, 307, 479, 766, 712});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 388, 546, 897, 45, 479, 766, 712});

        assertTrue("Fehler: Element 555 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(555));
        theTestList.insertAt(3, 878);
        Tester.verifyListContents(theTestList, new int[]{31, 555, 388, 878, 546, 897, 45, 479, 766, 712});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(419);
        Tester.verifyListContents(theTestList, new int[]{419});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 966);
        Tester.verifyListContents(theTestList, new int[]{966});

        theTestList.insertAt(1, 116);
        Tester.verifyListContents(theTestList, new int[]{966, 116});

        theTestList.insertAt(0, 856);
        Tester.verifyListContents(theTestList, new int[]{856, 966, 116});

        theTestList.append(942);
        Tester.verifyListContents(theTestList, new int[]{856, 966, 116, 942});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{966, 116, 942});

        assertTrue("Fehler: Element 966 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(966));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{966, 116});

        theTestList.append(247);
        Tester.verifyListContents(theTestList, new int[]{966, 116, 247});

        assertFalse("Fehler: Element 803 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(803));
        theTestList.insertAt(1, 747);
        Tester.verifyListContents(theTestList, new int[]{966, 747, 116, 247});

        theTestList.append(338);
        Tester.verifyListContents(theTestList, new int[]{966, 747, 116, 247, 338});

        theTestList.append(251);
        Tester.verifyListContents(theTestList, new int[]{966, 747, 116, 247, 338, 251});

        assertFalse("Fehler: Element 67 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(67));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{966, 747, 247, 338, 251});

        theTestList.insertAt(1, 686);
        Tester.verifyListContents(theTestList, new int[]{966, 686, 747, 247, 338, 251});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{966, 686, 747, 247, 338});

        assertTrue("Fehler: Element 966 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(966));
        theTestList.append(740);
        Tester.verifyListContents(theTestList, new int[]{966, 686, 747, 247, 338, 740});

        theTestList.insertAt(2, 423);
        Tester.verifyListContents(theTestList, new int[]{966, 686, 423, 747, 247, 338, 740});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{966, 686, 423, 247, 338, 740});

        assertFalse("Fehler: Element 272 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(272));
        assertTrue("Fehler: Element 740 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(740));
        theTestList.append(308);
        Tester.verifyListContents(theTestList, new int[]{966, 686, 423, 247, 338, 740, 308});

        assertTrue("Fehler: Element 338 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(338));
        assertFalse("Fehler: Element 607 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(607));
        assertTrue("Fehler: Element 247 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(247));
        assertFalse("Fehler: Element 109 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(109));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 125);
        Tester.verifyListContents(theTestList, new int[]{125});

        theTestList.insertAt(0, 887);
        Tester.verifyListContents(theTestList, new int[]{887, 125});

        assertTrue("Fehler: Element 887 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(887));
        assertTrue("Fehler: Element 887 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(887));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{125});

        assertTrue("Fehler: Element 125 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(125));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(259);
        Tester.verifyListContents(theTestList, new int[]{259});

        theTestList.append(750);
        Tester.verifyListContents(theTestList, new int[]{259, 750});

        assertFalse("Fehler: Element 268 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(268));
        assertTrue("Fehler: Element 259 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(259));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{259});

        theTestList.insertAt(1, 456);
        Tester.verifyListContents(theTestList, new int[]{259, 456});

        theTestList.insertAt(0, 415);
        Tester.verifyListContents(theTestList, new int[]{415, 259, 456});

        assertFalse("Fehler: Element 797 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(797));
        assertTrue("Fehler: Element 456 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(456));
        theTestList.insertAt(0, 729);
        Tester.verifyListContents(theTestList, new int[]{729, 415, 259, 456});

        theTestList.insertAt(1, 985);
        Tester.verifyListContents(theTestList, new int[]{729, 985, 415, 259, 456});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{729, 985, 415, 456});

        theTestList.insertAt(1, 233);
        Tester.verifyListContents(theTestList, new int[]{729, 233, 985, 415, 456});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{729, 233, 415, 456});

        theTestList.insertAt(4, 825);
        Tester.verifyListContents(theTestList, new int[]{729, 233, 415, 456, 825});

        theTestList.insertAt(4, 651);
        Tester.verifyListContents(theTestList, new int[]{729, 233, 415, 456, 651, 825});

        theTestList.insertAt(1, 733);
        Tester.verifyListContents(theTestList, new int[]{729, 733, 233, 415, 456, 651, 825});

    }

    public static void test9Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(235);
        Tester.verifyListContents(theTestList, new int[]{235});

        assertTrue("Fehler: Element 235 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(235));
// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(84);
        Tester.verifyListContents(theTestList, new int[]{84});

        theTestList.append(913);
        Tester.verifyListContents(theTestList, new int[]{84, 913});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{84});

        theTestList.append(60);
        Tester.verifyListContents(theTestList, new int[]{84, 60});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{84});

        theTestList.append(949);
        Tester.verifyListContents(theTestList, new int[]{84, 949});

        assertFalse("Fehler: Element 745 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(745));
        theTestList.insertAt(2, 633);
        Tester.verifyListContents(theTestList, new int[]{84, 949, 633});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(619);
        Tester.verifyListContents(theTestList, new int[]{619});

        assertTrue("Fehler: Element 619 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(619));
        theTestList.insertAt(0, 589);
        Tester.verifyListContents(theTestList, new int[]{589, 619});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{589});

        assertTrue("Fehler: Element 589 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(589));
        theTestList.insertAt(1, 407);
        Tester.verifyListContents(theTestList, new int[]{589, 407});

        theTestList.append(959);
        Tester.verifyListContents(theTestList, new int[]{589, 407, 959});

        theTestList.insertAt(3, 823);
        Tester.verifyListContents(theTestList, new int[]{589, 407, 959, 823});

        assertTrue("Fehler: Element 823 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(823));
        assertFalse("Fehler: Element 975 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(975));
        theTestList.append(945);
        Tester.verifyListContents(theTestList, new int[]{589, 407, 959, 823, 945});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(529);
        Tester.verifyListContents(theTestList, new int[]{529});

        theTestList.append(258);
        Tester.verifyListContents(theTestList, new int[]{529, 258});

        assertTrue("Fehler: Element 258 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(258));
        assertFalse("Fehler: Element 384 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(384));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{529});

        assertTrue("Fehler: Element 529 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(529));
        theTestList.insertAt(1, 770);
        Tester.verifyListContents(theTestList, new int[]{529, 770});

        assertFalse("Fehler: Element 16 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(16));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{529});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 381);
        Tester.verifyListContents(theTestList, new int[]{381});

        theTestList.insertAt(1, 342);
        Tester.verifyListContents(theTestList, new int[]{381, 342});

        theTestList.append(167);
        Tester.verifyListContents(theTestList, new int[]{381, 342, 167});

        theTestList.insertAt(2, 5);
        Tester.verifyListContents(theTestList, new int[]{381, 342, 5, 167});

        assertFalse("Fehler: Element 252 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(252));
        assertTrue("Fehler: Element 381 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(381));
        theTestList.insertAt(2, 484);
        Tester.verifyListContents(theTestList, new int[]{381, 342, 484, 5, 167});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{381, 342, 484, 5});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{381, 484, 5});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{484, 5});

        theTestList.insertAt(2, 724);
        Tester.verifyListContents(theTestList, new int[]{484, 5, 724});

        theTestList.insertAt(1, 719);
        Tester.verifyListContents(theTestList, new int[]{484, 719, 5, 724});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{484, 719, 5});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{484, 719});

        theTestList.insertAt(1, 32);
        Tester.verifyListContents(theTestList, new int[]{484, 32, 719});

        assertTrue("Fehler: Element 719 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(719));
        assertTrue("Fehler: Element 32 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(32));
        theTestList.append(482);
        Tester.verifyListContents(theTestList, new int[]{484, 32, 719, 482});

        theTestList.append(262);
        Tester.verifyListContents(theTestList, new int[]{484, 32, 719, 482, 262});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(952);
        Tester.verifyListContents(theTestList, new int[]{952});

        theTestList.append(369);
        Tester.verifyListContents(theTestList, new int[]{952, 369});

        theTestList.append(182);
        Tester.verifyListContents(theTestList, new int[]{952, 369, 182});

        theTestList.append(516);
        Tester.verifyListContents(theTestList, new int[]{952, 369, 182, 516});

        assertFalse("Fehler: Element 386 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(386));
        theTestList.append(992);
        Tester.verifyListContents(theTestList, new int[]{952, 369, 182, 516, 992});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 689);
        Tester.verifyListContents(theTestList, new int[]{689});

        theTestList.append(449);
        Tester.verifyListContents(theTestList, new int[]{689, 449});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{449});

        theTestList.insertAt(1, 757);
        Tester.verifyListContents(theTestList, new int[]{449, 757});

        assertFalse("Fehler: Element 884 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(884));
        assertTrue("Fehler: Element 449 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(449));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{757});

        theTestList.append(521);
        Tester.verifyListContents(theTestList, new int[]{757, 521});

        theTestList.insertAt(2, 517);
        Tester.verifyListContents(theTestList, new int[]{757, 521, 517});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{757, 517});

        theTestList.insertAt(1, 179);
        Tester.verifyListContents(theTestList, new int[]{757, 179, 517});

        assertTrue("Fehler: Element 517 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(517));
        assertTrue("Fehler: Element 757 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(757));
        theTestList.insertAt(1, 129);
        Tester.verifyListContents(theTestList, new int[]{757, 129, 179, 517});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{757, 129, 179});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{757, 179});

        theTestList.append(628);
        Tester.verifyListContents(theTestList, new int[]{757, 179, 628});

        theTestList.append(552);
        Tester.verifyListContents(theTestList, new int[]{757, 179, 628, 552});

        assertTrue("Fehler: Element 628 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(628));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(955);
        Tester.verifyListContents(theTestList, new int[]{955});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(454);
        Tester.verifyListContents(theTestList, new int[]{454});

        theTestList.append(116);
        Tester.verifyListContents(theTestList, new int[]{454, 116});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{454});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 652);
        Tester.verifyListContents(theTestList, new int[]{652});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 158);
        Tester.verifyListContents(theTestList, new int[]{158});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(938);
        Tester.verifyListContents(theTestList, new int[]{938});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 713);
        Tester.verifyListContents(theTestList, new int[]{713});

        theTestList.append(189);
        Tester.verifyListContents(theTestList, new int[]{713, 189});

        assertTrue("Fehler: Element 189 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(189));
        theTestList.insertAt(2, 699);
        Tester.verifyListContents(theTestList, new int[]{713, 189, 699});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{713, 189});

        theTestList.insertAt(2, 880);
        Tester.verifyListContents(theTestList, new int[]{713, 189, 880});

    }

    public static void test10Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(819);
        Tester.verifyListContents(theTestList, new int[]{819});

        theTestList.append(743);
        Tester.verifyListContents(theTestList, new int[]{819, 743});

        theTestList.insertAt(0, 432);
        Tester.verifyListContents(theTestList, new int[]{432, 819, 743});

        assertTrue("Fehler: Element 432 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(432));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{432, 819});

        theTestList.insertAt(2, 91);
        Tester.verifyListContents(theTestList, new int[]{432, 819, 91});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{819, 91});

        assertFalse("Fehler: Element 612 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(612));
        theTestList.insertAt(2, 346);
        Tester.verifyListContents(theTestList, new int[]{819, 91, 346});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{91, 346});

        theTestList.append(697);
        Tester.verifyListContents(theTestList, new int[]{91, 346, 697});

        assertTrue("Fehler: Element 346 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(346));
        assertTrue("Fehler: Element 91 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(91));
        theTestList.insertAt(0, 249);
        Tester.verifyListContents(theTestList, new int[]{249, 91, 346, 697});

        theTestList.insertAt(3, 230);
        Tester.verifyListContents(theTestList, new int[]{249, 91, 346, 230, 697});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{249, 91, 346, 230});

        assertTrue("Fehler: Element 346 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(346));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{91, 346, 230});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{346, 230});

        theTestList.insertAt(1, 15);
        Tester.verifyListContents(theTestList, new int[]{346, 15, 230});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{346, 15});

        theTestList.append(230);
        Tester.verifyListContents(theTestList, new int[]{346, 15, 230});

        theTestList.insertAt(2, 405);
        Tester.verifyListContents(theTestList, new int[]{346, 15, 405, 230});

        theTestList.append(497);
        Tester.verifyListContents(theTestList, new int[]{346, 15, 405, 230, 497});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{346, 15, 405, 497});

        theTestList.append(919);
        Tester.verifyListContents(theTestList, new int[]{346, 15, 405, 497, 919});

        theTestList.append(108);
        Tester.verifyListContents(theTestList, new int[]{346, 15, 405, 497, 919, 108});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{15, 405, 497, 919, 108});

        theTestList.insertAt(1, 197);
        Tester.verifyListContents(theTestList, new int[]{15, 197, 405, 497, 919, 108});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{15, 197, 405, 497, 108});

        theTestList.insertAt(3, 538);
        Tester.verifyListContents(theTestList, new int[]{15, 197, 405, 538, 497, 108});

        theTestList.insertAt(1, 875);
        Tester.verifyListContents(theTestList, new int[]{15, 875, 197, 405, 538, 497, 108});

        assertFalse("Fehler: Element 486 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(486));
        theTestList.append(896);
        Tester.verifyListContents(theTestList, new int[]{15, 875, 197, 405, 538, 497, 108, 896});

        assertTrue("Fehler: Element 15 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(15));
        theTestList.append(786);
        Tester.verifyListContents(theTestList, new int[]{15, 875, 197, 405, 538, 497, 108, 896, 786});

        assertFalse("Fehler: Element 816 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(816));
        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{15, 875, 197, 405, 538, 108, 896, 786});

        assertFalse("Fehler: Element 622 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(622));
        assertTrue("Fehler: Element 15 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(15));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{15, 875, 405, 538, 108, 896, 786});

        theTestList.append(512);
        Tester.verifyListContents(theTestList, new int[]{15, 875, 405, 538, 108, 896, 786, 512});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{15, 875, 538, 108, 896, 786, 512});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(181);
        Tester.verifyListContents(theTestList, new int[]{181});

        assertFalse("Fehler: Element 506 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(506));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 42);
        Tester.verifyListContents(theTestList, new int[]{42});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 438);
        Tester.verifyListContents(theTestList, new int[]{438});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 749);
        Tester.verifyListContents(theTestList, new int[]{749});

        theTestList.append(970);
        Tester.verifyListContents(theTestList, new int[]{749, 970});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{749});

        theTestList.insertAt(1, 948);
        Tester.verifyListContents(theTestList, new int[]{749, 948});

        theTestList.append(149);
        Tester.verifyListContents(theTestList, new int[]{749, 948, 149});

        theTestList.insertAt(1, 167);
        Tester.verifyListContents(theTestList, new int[]{749, 167, 948, 149});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(785);
        Tester.verifyListContents(theTestList, new int[]{785});

// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(126);
        Tester.verifyListContents(theTestList, new int[]{126});

        assertFalse("Fehler: Element 519 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(519));
        theTestList.append(864);
        Tester.verifyListContents(theTestList, new int[]{126, 864});

        theTestList.append(168);
        Tester.verifyListContents(theTestList, new int[]{126, 864, 168});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{126, 168});

        theTestList.append(194);
        Tester.verifyListContents(theTestList, new int[]{126, 168, 194});

        assertTrue("Fehler: Element 194 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(194));
        theTestList.insertAt(3, 113);
        Tester.verifyListContents(theTestList, new int[]{126, 168, 194, 113});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{126, 194, 113});

        theTestList.append(110);
        Tester.verifyListContents(theTestList, new int[]{126, 194, 113, 110});

        assertFalse("Fehler: Element 131 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(131));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{194, 113, 110});

        theTestList.append(927);
        Tester.verifyListContents(theTestList, new int[]{194, 113, 110, 927});

        theTestList.insertAt(3, 730);
        Tester.verifyListContents(theTestList, new int[]{194, 113, 110, 730, 927});

        assertFalse("Fehler: Element 103 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(103));
        theTestList.append(355);
        Tester.verifyListContents(theTestList, new int[]{194, 113, 110, 730, 927, 355});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{113, 110, 730, 927, 355});

        assertTrue("Fehler: Element 730 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(730));
        theTestList.append(365);
        Tester.verifyListContents(theTestList, new int[]{113, 110, 730, 927, 355, 365});

        theTestList.append(29);
        Tester.verifyListContents(theTestList, new int[]{113, 110, 730, 927, 355, 365, 29});

        theTestList.append(869);
        Tester.verifyListContents(theTestList, new int[]{113, 110, 730, 927, 355, 365, 29, 869});

        theTestList.append(246);
        Tester.verifyListContents(theTestList, new int[]{113, 110, 730, 927, 355, 365, 29, 869, 246});

        theTestList.insertAt(4, 508);
        Tester.verifyListContents(theTestList, new int[]{113, 110, 730, 927, 508, 355, 365, 29, 869, 246});

        theTestList.insertAt(0, 415);
        Tester.verifyListContents(theTestList, new int[]{415, 113, 110, 730, 927, 508, 355, 365, 29, 869, 246});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{415, 113, 730, 927, 508, 355, 365, 29, 869, 246});

        theTestList.insertAt(8, 506);
        Tester.verifyListContents(theTestList, new int[]{415, 113, 730, 927, 508, 355, 365, 29, 506, 869, 246});

        theTestList.append(862);
        Tester.verifyListContents(theTestList, new int[]{415, 113, 730, 927, 508, 355, 365, 29, 506, 869, 246, 862});

        theTestList.insertAt(11, 299);
        Tester.verifyListContents(theTestList, new int[]{415, 113, 730, 927, 508, 355, 365, 29, 506, 869, 246, 299, 862});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{415, 113, 730, 927, 355, 365, 29, 506, 869, 246, 299, 862});

        assertFalse("Fehler: Element 559 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(559));
        theTestList.insertAt(4, 59);
        Tester.verifyListContents(theTestList, new int[]{415, 113, 730, 927, 59, 355, 365, 29, 506, 869, 246, 299, 862});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{415, 113, 730, 927, 59, 355, 365, 29, 506, 246, 299, 862});

        assertFalse("Fehler: Element 419 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(419));
        theTestList.insertAt(1, 531);
        Tester.verifyListContents(theTestList, new int[]{415, 531, 113, 730, 927, 59, 355, 365, 29, 506, 246, 299, 862});

        theTestList.insertAt(5, 499);
        Tester.verifyListContents(theTestList, new int[]{415, 531, 113, 730, 927, 499, 59, 355, 365, 29, 506, 246, 299, 862});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{415, 531, 113, 730, 927, 59, 355, 365, 29, 506, 246, 299, 862});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{415, 531, 730, 927, 59, 355, 365, 29, 506, 246, 299, 862});

        assertTrue("Fehler: Element 927 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(927));
        assertFalse("Fehler: Element 221 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(221));
        assertTrue("Fehler: Element 29 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(29));
    }
}
