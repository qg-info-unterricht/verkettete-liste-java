package tests;

import static org.junit.Assert.*;

import liste.*;

/**
 * Die Test-Klasse SubTest7.
 *
 * @author Rainer Helfrich
 * @version 03.10.2020
 */
public class SubTest7 {


    public static void test1Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(92);
        Tester.verifyListContents(theTestList, new int[]{92});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 0);
        Tester.verifyListContents(theTestList, new int[]{0});

        theTestList.insertAt(1, 922);
        Tester.verifyListContents(theTestList, new int[]{0, 922});

        theTestList.insertAt(2, 956);
        Tester.verifyListContents(theTestList, new int[]{0, 922, 956});

        theTestList.insertAt(0, 607);
        Tester.verifyListContents(theTestList, new int[]{607, 0, 922, 956});

        theTestList.insertAt(2, 114);
        Tester.verifyListContents(theTestList, new int[]{607, 0, 114, 922, 956});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{607, 0, 114, 956});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{607, 114, 956});

        assertFalse("Fehler: Element 371 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(371));
        theTestList.append(215);
        Tester.verifyListContents(theTestList, new int[]{607, 114, 956, 215});

        theTestList.append(718);
        Tester.verifyListContents(theTestList, new int[]{607, 114, 956, 215, 718});

        theTestList.append(276);
        Tester.verifyListContents(theTestList, new int[]{607, 114, 956, 215, 718, 276});

        theTestList.insertAt(4, 788);
        Tester.verifyListContents(theTestList, new int[]{607, 114, 956, 215, 788, 718, 276});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{607, 114, 956, 788, 718, 276});

        assertTrue("Fehler: Element 788 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(788));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{607, 114, 956, 788, 276});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{607, 956, 788, 276});

        theTestList.append(832);
        Tester.verifyListContents(theTestList, new int[]{607, 956, 788, 276, 832});

        theTestList.append(102);
        Tester.verifyListContents(theTestList, new int[]{607, 956, 788, 276, 832, 102});

        theTestList.append(7);
        Tester.verifyListContents(theTestList, new int[]{607, 956, 788, 276, 832, 102, 7});

        assertFalse("Fehler: Element 240 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(240));
        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{607, 956, 788, 276, 832, 102});

        theTestList.insertAt(0, 393);
        Tester.verifyListContents(theTestList, new int[]{393, 607, 956, 788, 276, 832, 102});

        theTestList.insertAt(1, 691);
        Tester.verifyListContents(theTestList, new int[]{393, 691, 607, 956, 788, 276, 832, 102});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{393, 691, 607, 788, 276, 832, 102});

        assertFalse("Fehler: Element 886 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(886));
        theTestList.append(344);
        Tester.verifyListContents(theTestList, new int[]{393, 691, 607, 788, 276, 832, 102, 344});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{393, 691, 607, 788, 832, 102, 344});

        theTestList.insertAt(5, 109);
        Tester.verifyListContents(theTestList, new int[]{393, 691, 607, 788, 832, 109, 102, 344});

        assertFalse("Fehler: Element 190 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(190));
        theTestList.append(970);
        Tester.verifyListContents(theTestList, new int[]{393, 691, 607, 788, 832, 109, 102, 344, 970});

        assertFalse("Fehler: Element 766 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(766));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{691, 607, 788, 832, 109, 102, 344, 970});

        theTestList.insertAt(6, 497);
        Tester.verifyListContents(theTestList, new int[]{691, 607, 788, 832, 109, 102, 497, 344, 970});

        assertFalse("Fehler: Element 230 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(230));
        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{691, 607, 788, 832, 109, 102, 497, 344});

        assertTrue("Fehler: Element 691 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(691));
        theTestList.insertAt(7, 665);
        Tester.verifyListContents(theTestList, new int[]{691, 607, 788, 832, 109, 102, 497, 665, 344});

        assertFalse("Fehler: Element 212 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(212));
        theTestList.insertAt(4, 659);
        Tester.verifyListContents(theTestList, new int[]{691, 607, 788, 832, 659, 109, 102, 497, 665, 344});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{691, 607, 788, 832, 659, 109, 497, 665, 344});

        theTestList.insertAt(5, 493);
        Tester.verifyListContents(theTestList, new int[]{691, 607, 788, 832, 659, 493, 109, 497, 665, 344});

        assertTrue("Fehler: Element 344 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(344));
        assertTrue("Fehler: Element 691 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(691));
        assertFalse("Fehler: Element 539 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(539));
        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{691, 607, 788, 832, 659, 493, 109, 497, 344});

        theTestList.insertAt(3, 819);
        Tester.verifyListContents(theTestList, new int[]{691, 607, 788, 819, 832, 659, 493, 109, 497, 344});

        assertFalse("Fehler: Element 672 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(672));
        theTestList.insertAt(10, 387);
        Tester.verifyListContents(theTestList, new int[]{691, 607, 788, 819, 832, 659, 493, 109, 497, 344, 387});

        theTestList.insertAt(3, 805);
        Tester.verifyListContents(theTestList, new int[]{691, 607, 788, 805, 819, 832, 659, 493, 109, 497, 344, 387});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{607, 788, 805, 819, 832, 659, 493, 109, 497, 344, 387});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{607, 788, 805, 819, 832, 659, 493, 109, 344, 387});

        assertTrue("Fehler: Element 819 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(819));
        theTestList.insertAt(10, 722);
        Tester.verifyListContents(theTestList, new int[]{607, 788, 805, 819, 832, 659, 493, 109, 344, 387, 722});

        assertFalse("Fehler: Element 39 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(39));
        assertFalse("Fehler: Element 739 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(739));
        theTestList.insertAt(2, 757);
        Tester.verifyListContents(theTestList, new int[]{607, 788, 757, 805, 819, 832, 659, 493, 109, 344, 387, 722});

        theTestList.insertAt(10, 857);
        Tester.verifyListContents(theTestList, new int[]{607, 788, 757, 805, 819, 832, 659, 493, 109, 344, 857, 387, 722});

        theTestList.append(96);
        Tester.verifyListContents(theTestList, new int[]{607, 788, 757, 805, 819, 832, 659, 493, 109, 344, 857, 387, 722, 96});

        theTestList.insertAt(10, 37);
        Tester.verifyListContents(theTestList, new int[]{607, 788, 757, 805, 819, 832, 659, 493, 109, 344, 37, 857, 387, 722, 96});

        theTestList.append(426);
        Tester.verifyListContents(theTestList, new int[]{607, 788, 757, 805, 819, 832, 659, 493, 109, 344, 37, 857, 387, 722, 96, 426});

        theTestList.insertAt(13, 503);
        Tester.verifyListContents(theTestList, new int[]{607, 788, 757, 805, 819, 832, 659, 493, 109, 344, 37, 857, 387, 503, 722, 96, 426});

        theTestList.removeAt(12);
        Tester.verifyListContents(theTestList, new int[]{607, 788, 757, 805, 819, 832, 659, 493, 109, 344, 37, 857, 503, 722, 96, 426});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 825);
        Tester.verifyListContents(theTestList, new int[]{825});

        assertTrue("Fehler: Element 825 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(825));
        theTestList.append(627);
        Tester.verifyListContents(theTestList, new int[]{825, 627});

        theTestList.insertAt(0, 945);
        Tester.verifyListContents(theTestList, new int[]{945, 825, 627});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{825, 627});

        theTestList.append(213);
        Tester.verifyListContents(theTestList, new int[]{825, 627, 213});

        theTestList.append(566);
        Tester.verifyListContents(theTestList, new int[]{825, 627, 213, 566});

        assertFalse("Fehler: Element 97 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(97));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{627, 213, 566});

        theTestList.append(280);
        Tester.verifyListContents(theTestList, new int[]{627, 213, 566, 280});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{627, 566, 280});

        assertTrue("Fehler: Element 627 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(627));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{566, 280});

        theTestList.append(117);
        Tester.verifyListContents(theTestList, new int[]{566, 280, 117});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{280, 117});

        theTestList.append(755);
        Tester.verifyListContents(theTestList, new int[]{280, 117, 755});

        theTestList.insertAt(1, 32);
        Tester.verifyListContents(theTestList, new int[]{280, 32, 117, 755});

        theTestList.append(695);
        Tester.verifyListContents(theTestList, new int[]{280, 32, 117, 755, 695});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{280, 32, 117, 695});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{280, 117, 695});

        theTestList.insertAt(1, 53);
        Tester.verifyListContents(theTestList, new int[]{280, 53, 117, 695});

        theTestList.insertAt(4, 846);
        Tester.verifyListContents(theTestList, new int[]{280, 53, 117, 695, 846});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{53, 117, 695, 846});

        theTestList.append(242);
        Tester.verifyListContents(theTestList, new int[]{53, 117, 695, 846, 242});

        theTestList.append(515);
        Tester.verifyListContents(theTestList, new int[]{53, 117, 695, 846, 242, 515});

        theTestList.insertAt(6, 471);
        Tester.verifyListContents(theTestList, new int[]{53, 117, 695, 846, 242, 515, 471});

        theTestList.append(73);
        Tester.verifyListContents(theTestList, new int[]{53, 117, 695, 846, 242, 515, 471, 73});

        assertTrue("Fehler: Element 846 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(846));
        assertFalse("Fehler: Element 65 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(65));
        theTestList.insertAt(7, 273);
        Tester.verifyListContents(theTestList, new int[]{53, 117, 695, 846, 242, 515, 471, 273, 73});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{53, 117, 695, 846, 242, 515, 471, 73});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{53, 117, 695, 846, 242, 515, 73});

        theTestList.append(99);
        Tester.verifyListContents(theTestList, new int[]{53, 117, 695, 846, 242, 515, 73, 99});

        theTestList.insertAt(2, 644);
        Tester.verifyListContents(theTestList, new int[]{53, 117, 644, 695, 846, 242, 515, 73, 99});

    }

    public static void test2Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 265);
        Tester.verifyListContents(theTestList, new int[]{265});

        assertFalse("Fehler: Element 478 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(478));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(357);
        Tester.verifyListContents(theTestList, new int[]{357});

        theTestList.append(214);
        Tester.verifyListContents(theTestList, new int[]{357, 214});

        theTestList.insertAt(2, 277);
        Tester.verifyListContents(theTestList, new int[]{357, 214, 277});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{357, 214});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{357});

        assertFalse("Fehler: Element 291 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(291));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 771);
        Tester.verifyListContents(theTestList, new int[]{771});

        theTestList.append(619);
        Tester.verifyListContents(theTestList, new int[]{771, 619});

        theTestList.insertAt(2, 245);
        Tester.verifyListContents(theTestList, new int[]{771, 619, 245});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(689);
        Tester.verifyListContents(theTestList, new int[]{689});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(977);
        Tester.verifyListContents(theTestList, new int[]{977});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(318);
        Tester.verifyListContents(theTestList, new int[]{318});

        theTestList.append(413);
        Tester.verifyListContents(theTestList, new int[]{318, 413});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{413});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(149);
        Tester.verifyListContents(theTestList, new int[]{149});

        theTestList.append(114);
        Tester.verifyListContents(theTestList, new int[]{149, 114});

        theTestList.insertAt(0, 476);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 114});

        theTestList.append(323);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 114, 323});

        assertTrue("Fehler: Element 114 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(114));
        assertTrue("Fehler: Element 114 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(114));
        theTestList.append(940);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 114, 323, 940});

        assertFalse("Fehler: Element 641 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(641));
        theTestList.insertAt(4, 59);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 114, 323, 59, 940});

        assertTrue("Fehler: Element 149 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(149));
        theTestList.append(488);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 114, 323, 59, 940, 488});

        assertTrue("Fehler: Element 149 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(149));
        assertFalse("Fehler: Element 390 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(390));
        theTestList.insertAt(3, 614);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 114, 614, 323, 59, 940, 488});

        theTestList.insertAt(2, 557);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 557, 114, 614, 323, 59, 940, 488});

        theTestList.append(908);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 557, 114, 614, 323, 59, 940, 488, 908});

        theTestList.append(155);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 557, 114, 614, 323, 59, 940, 488, 908, 155});

        theTestList.append(596);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 557, 114, 614, 323, 59, 940, 488, 908, 155, 596});

        theTestList.insertAt(5, 416);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 557, 114, 614, 416, 323, 59, 940, 488, 908, 155, 596});

        theTestList.insertAt(2, 832);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 114, 614, 416, 323, 59, 940, 488, 908, 155, 596});

        theTestList.insertAt(10, 600);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 114, 614, 416, 323, 59, 940, 600, 488, 908, 155, 596});

        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 114, 614, 416, 323, 59, 940, 600, 908, 155, 596});

        theTestList.insertAt(9, 466);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 114, 614, 416, 323, 59, 466, 940, 600, 908, 155, 596});

        theTestList.append(670);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 114, 614, 416, 323, 59, 466, 940, 600, 908, 155, 596, 670});

        theTestList.append(762);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 114, 614, 416, 323, 59, 466, 940, 600, 908, 155, 596, 670, 762});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 114, 614, 416, 59, 466, 940, 600, 908, 155, 596, 670, 762});

        theTestList.insertAt(16, 813);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 114, 614, 416, 59, 466, 940, 600, 908, 155, 596, 670, 762, 813});

        theTestList.append(759);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 114, 614, 416, 59, 466, 940, 600, 908, 155, 596, 670, 762, 813, 759});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 114, 416, 59, 466, 940, 600, 908, 155, 596, 670, 762, 813, 759});

        assertFalse("Fehler: Element 783 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(783));
        theTestList.insertAt(15, 961);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 114, 416, 59, 466, 940, 600, 908, 155, 596, 670, 762, 961, 813, 759});

        theTestList.append(732);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 114, 416, 59, 466, 940, 600, 908, 155, 596, 670, 762, 961, 813, 759, 732});

        theTestList.insertAt(4, 414);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 414, 114, 416, 59, 466, 940, 600, 908, 155, 596, 670, 762, 961, 813, 759, 732});

        assertTrue("Fehler: Element 832 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(832));
        theTestList.insertAt(14, 879);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 414, 114, 416, 59, 466, 940, 600, 908, 155, 596, 879, 670, 762, 961, 813, 759, 732});

        theTestList.insertAt(8, 971);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 414, 114, 416, 59, 971, 466, 940, 600, 908, 155, 596, 879, 670, 762, 961, 813, 759, 732});

        theTestList.insertAt(9, 743);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 414, 114, 416, 59, 971, 743, 466, 940, 600, 908, 155, 596, 879, 670, 762, 961, 813, 759, 732});

        theTestList.insertAt(5, 836);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 414, 836, 114, 416, 59, 971, 743, 466, 940, 600, 908, 155, 596, 879, 670, 762, 961, 813, 759, 732});

        theTestList.removeAt(16);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 414, 836, 114, 416, 59, 971, 743, 466, 940, 600, 908, 155, 879, 670, 762, 961, 813, 759, 732});

        assertTrue("Fehler: Element 961 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(961));
        theTestList.removeAt(22);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 414, 836, 114, 416, 59, 971, 743, 466, 940, 600, 908, 155, 879, 670, 762, 961, 813, 759});

        theTestList.insertAt(17, 173);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 414, 836, 114, 416, 59, 971, 743, 466, 940, 600, 908, 155, 879, 173, 670, 762, 961, 813, 759});

        theTestList.removeAt(15);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 414, 836, 114, 416, 59, 971, 743, 466, 940, 600, 908, 879, 173, 670, 762, 961, 813, 759});

        theTestList.insertAt(5, 97);
        Tester.verifyListContents(theTestList, new int[]{476, 149, 832, 557, 414, 97, 836, 114, 416, 59, 971, 743, 466, 940, 600, 908, 879, 173, 670, 762, 961, 813, 759});

        assertTrue("Fehler: Element 59 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(59));
        theTestList.insertAt(1, 301);
        Tester.verifyListContents(theTestList, new int[]{476, 301, 149, 832, 557, 414, 97, 836, 114, 416, 59, 971, 743, 466, 940, 600, 908, 879, 173, 670, 762, 961, 813, 759});

        assertTrue("Fehler: Element 114 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(114));
        theTestList.insertAt(21, 884);
        Tester.verifyListContents(theTestList, new int[]{476, 301, 149, 832, 557, 414, 97, 836, 114, 416, 59, 971, 743, 466, 940, 600, 908, 879, 173, 670, 762, 884, 961, 813, 759});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{476, 301, 832, 557, 414, 97, 836, 114, 416, 59, 971, 743, 466, 940, 600, 908, 879, 173, 670, 762, 884, 961, 813, 759});

        theTestList.append(974);
        Tester.verifyListContents(theTestList, new int[]{476, 301, 832, 557, 414, 97, 836, 114, 416, 59, 971, 743, 466, 940, 600, 908, 879, 173, 670, 762, 884, 961, 813, 759, 974});

        theTestList.removeAt(13);
        Tester.verifyListContents(theTestList, new int[]{476, 301, 832, 557, 414, 97, 836, 114, 416, 59, 971, 743, 466, 600, 908, 879, 173, 670, 762, 884, 961, 813, 759, 974});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(812);
        Tester.verifyListContents(theTestList, new int[]{812});

        theTestList.append(57);
        Tester.verifyListContents(theTestList, new int[]{812, 57});

        theTestList.append(944);
        Tester.verifyListContents(theTestList, new int[]{812, 57, 944});

        theTestList.append(400);
        Tester.verifyListContents(theTestList, new int[]{812, 57, 944, 400});

        assertTrue("Fehler: Element 57 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(57));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{57, 944, 400});

        assertFalse("Fehler: Element 854 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(854));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{944, 400});

        theTestList.insertAt(1, 930);
        Tester.verifyListContents(theTestList, new int[]{944, 930, 400});

        theTestList.append(626);
        Tester.verifyListContents(theTestList, new int[]{944, 930, 400, 626});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{930, 400, 626});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{930, 626});

        theTestList.append(857);
        Tester.verifyListContents(theTestList, new int[]{930, 626, 857});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{930, 626});

        theTestList.append(321);
        Tester.verifyListContents(theTestList, new int[]{930, 626, 321});

        assertTrue("Fehler: Element 930 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(930));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{930, 321});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{930});

        theTestList.insertAt(1, 583);
        Tester.verifyListContents(theTestList, new int[]{930, 583});

        theTestList.append(118);
        Tester.verifyListContents(theTestList, new int[]{930, 583, 118});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{930, 118});

        assertFalse("Fehler: Element 152 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(152));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{118});

        theTestList.insertAt(1, 746);
        Tester.verifyListContents(theTestList, new int[]{118, 746});

        assertFalse("Fehler: Element 978 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(978));
    }

    public static void test3Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(486);
        Tester.verifyListContents(theTestList, new int[]{486});

        theTestList.append(44);
        Tester.verifyListContents(theTestList, new int[]{486, 44});

        assertTrue("Fehler: Element 486 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(486));
        assertTrue("Fehler: Element 44 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(44));
        theTestList.insertAt(1, 973);
        Tester.verifyListContents(theTestList, new int[]{486, 973, 44});

        theTestList.append(17);
        Tester.verifyListContents(theTestList, new int[]{486, 973, 44, 17});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{973, 44, 17});

        theTestList.append(126);
        Tester.verifyListContents(theTestList, new int[]{973, 44, 17, 126});

        theTestList.insertAt(4, 515);
        Tester.verifyListContents(theTestList, new int[]{973, 44, 17, 126, 515});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{973, 44, 17, 515});

        theTestList.append(239);
        Tester.verifyListContents(theTestList, new int[]{973, 44, 17, 515, 239});

        theTestList.insertAt(1, 845);
        Tester.verifyListContents(theTestList, new int[]{973, 845, 44, 17, 515, 239});

        theTestList.insertAt(3, 462);
        Tester.verifyListContents(theTestList, new int[]{973, 845, 44, 462, 17, 515, 239});

        theTestList.insertAt(3, 247);
        Tester.verifyListContents(theTestList, new int[]{973, 845, 44, 247, 462, 17, 515, 239});

        assertFalse("Fehler: Element 759 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(759));
        theTestList.insertAt(3, 118);
        Tester.verifyListContents(theTestList, new int[]{973, 845, 44, 118, 247, 462, 17, 515, 239});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{973, 845, 44, 118, 247, 462, 17, 239});

        theTestList.insertAt(4, 201);
        Tester.verifyListContents(theTestList, new int[]{973, 845, 44, 118, 201, 247, 462, 17, 239});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{845, 44, 118, 201, 247, 462, 17, 239});

        theTestList.append(51);
        Tester.verifyListContents(theTestList, new int[]{845, 44, 118, 201, 247, 462, 17, 239, 51});

        assertFalse("Fehler: Element 161 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(161));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{845, 44, 201, 247, 462, 17, 239, 51});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{845, 44, 201, 247, 462, 239, 51});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{44, 201, 247, 462, 239, 51});

        theTestList.append(694);
        Tester.verifyListContents(theTestList, new int[]{44, 201, 247, 462, 239, 51, 694});

        theTestList.append(700);
        Tester.verifyListContents(theTestList, new int[]{44, 201, 247, 462, 239, 51, 694, 700});

        assertFalse("Fehler: Element 497 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(497));
        assertFalse("Fehler: Element 475 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(475));
        theTestList.insertAt(5, 264);
        Tester.verifyListContents(theTestList, new int[]{44, 201, 247, 462, 239, 264, 51, 694, 700});

        theTestList.insertAt(1, 353);
        Tester.verifyListContents(theTestList, new int[]{44, 353, 201, 247, 462, 239, 264, 51, 694, 700});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{44, 353, 201, 247, 462, 239, 264, 694, 700});

        theTestList.insertAt(1, 974);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 247, 462, 239, 264, 694, 700});

        theTestList.insertAt(5, 874);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 247, 874, 462, 239, 264, 694, 700});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 247, 874, 239, 264, 694, 700});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 247, 239, 264, 694, 700});

        theTestList.insertAt(6, 26);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 247, 239, 26, 264, 694, 700});

        assertTrue("Fehler: Element 264 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(264));
        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 247, 239, 26, 264, 700});

        assertFalse("Fehler: Element 993 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(993));
        theTestList.append(882);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 247, 239, 26, 264, 700, 882});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 247, 239, 26, 264, 882});

        assertFalse("Fehler: Element 680 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(680));
        theTestList.insertAt(2, 340);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 340, 353, 201, 247, 239, 26, 264, 882});

        assertFalse("Fehler: Element 896 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(896));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 247, 239, 26, 264, 882});

        theTestList.insertAt(4, 389);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 389, 247, 239, 26, 264, 882});

        theTestList.append(299);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 389, 247, 239, 26, 264, 882, 299});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 389, 247, 239, 264, 882, 299});

        theTestList.insertAt(3, 195);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 195, 201, 389, 247, 239, 264, 882, 299});

        assertTrue("Fehler: Element 195 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(195));
        theTestList.append(613);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 195, 201, 389, 247, 239, 264, 882, 299, 613});

        theTestList.insertAt(5, 611);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 195, 201, 611, 389, 247, 239, 264, 882, 299, 613});

        assertTrue("Fehler: Element 264 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(264));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 611, 389, 247, 239, 264, 882, 299, 613});

        theTestList.append(242);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 611, 389, 247, 239, 264, 882, 299, 613, 242});

        assertFalse("Fehler: Element 75 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(75));
        assertTrue("Fehler: Element 389 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(389));
        theTestList.insertAt(13, 140);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 611, 389, 247, 239, 264, 882, 299, 613, 242, 140});

        assertFalse("Fehler: Element 388 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(388));
        theTestList.append(288);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 611, 389, 247, 239, 264, 882, 299, 613, 242, 140, 288});

        assertFalse("Fehler: Element 759 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(759));
        theTestList.removeAt(14);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 611, 389, 247, 239, 264, 882, 299, 613, 242, 140});

        theTestList.append(352);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 611, 389, 247, 239, 264, 882, 299, 613, 242, 140, 352});

        theTestList.append(363);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 611, 389, 247, 239, 264, 882, 299, 613, 242, 140, 352, 363});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 389, 247, 239, 264, 882, 299, 613, 242, 140, 352, 363});

        assertTrue("Fehler: Element 264 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(264));
        theTestList.append(218);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 389, 247, 239, 264, 882, 299, 613, 242, 140, 352, 363, 218});

        theTestList.append(644);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 389, 247, 239, 264, 882, 299, 613, 242, 140, 352, 363, 218, 644});

        theTestList.insertAt(16, 756);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 389, 247, 239, 264, 882, 299, 613, 242, 140, 352, 363, 218, 756, 644});

        theTestList.append(900);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 389, 247, 239, 264, 882, 299, 613, 242, 140, 352, 363, 218, 756, 644, 900});

        theTestList.insertAt(15, 183);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 389, 247, 239, 264, 882, 299, 613, 242, 140, 352, 363, 183, 218, 756, 644, 900});

        assertTrue("Fehler: Element 644 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(644));
        theTestList.removeAt(15);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 389, 247, 239, 264, 882, 299, 613, 242, 140, 352, 363, 218, 756, 644, 900});

        theTestList.insertAt(14, 348);
        Tester.verifyListContents(theTestList, new int[]{44, 974, 353, 201, 389, 247, 239, 264, 882, 299, 613, 242, 140, 352, 348, 363, 218, 756, 644, 900});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(43);
        Tester.verifyListContents(theTestList, new int[]{43});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 504);
        Tester.verifyListContents(theTestList, new int[]{504});

        theTestList.insertAt(1, 667);
        Tester.verifyListContents(theTestList, new int[]{504, 667});

        assertFalse("Fehler: Element 187 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(187));
        theTestList.append(452);
        Tester.verifyListContents(theTestList, new int[]{504, 667, 452});

        theTestList.append(647);
        Tester.verifyListContents(theTestList, new int[]{504, 667, 452, 647});

        theTestList.insertAt(3, 463);
        Tester.verifyListContents(theTestList, new int[]{504, 667, 452, 463, 647});

        theTestList.insertAt(3, 826);
        Tester.verifyListContents(theTestList, new int[]{504, 667, 452, 826, 463, 647});

        theTestList.insertAt(1, 532);
        Tester.verifyListContents(theTestList, new int[]{504, 532, 667, 452, 826, 463, 647});

        theTestList.insertAt(5, 90);
        Tester.verifyListContents(theTestList, new int[]{504, 532, 667, 452, 826, 90, 463, 647});

        assertTrue("Fehler: Element 647 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(647));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{504, 532, 452, 826, 90, 463, 647});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{504, 532, 452, 826, 90, 647});

        assertFalse("Fehler: Element 265 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(265));
        theTestList.insertAt(0, 669);
        Tester.verifyListContents(theTestList, new int[]{669, 504, 532, 452, 826, 90, 647});

        assertFalse("Fehler: Element 687 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(687));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 454);
        Tester.verifyListContents(theTestList, new int[]{454});

        theTestList.insertAt(0, 620);
        Tester.verifyListContents(theTestList, new int[]{620, 454});

        theTestList.append(450);
        Tester.verifyListContents(theTestList, new int[]{620, 454, 450});

        theTestList.append(955);
        Tester.verifyListContents(theTestList, new int[]{620, 454, 450, 955});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{620, 450, 955});

        theTestList.append(292);
        Tester.verifyListContents(theTestList, new int[]{620, 450, 955, 292});

    }

    public static void test4Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 753);
        Tester.verifyListContents(theTestList, new int[]{753});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 61);
        Tester.verifyListContents(theTestList, new int[]{61});

        assertFalse("Fehler: Element 938 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(938));
        theTestList.append(705);
        Tester.verifyListContents(theTestList, new int[]{61, 705});

        theTestList.insertAt(2, 124);
        Tester.verifyListContents(theTestList, new int[]{61, 705, 124});

        assertFalse("Fehler: Element 306 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(306));
        theTestList.append(920);
        Tester.verifyListContents(theTestList, new int[]{61, 705, 124, 920});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{705, 124, 920});

        theTestList.insertAt(0, 569);
        Tester.verifyListContents(theTestList, new int[]{569, 705, 124, 920});

        theTestList.append(963);
        Tester.verifyListContents(theTestList, new int[]{569, 705, 124, 920, 963});

        theTestList.append(6);
        Tester.verifyListContents(theTestList, new int[]{569, 705, 124, 920, 963, 6});

        theTestList.append(749);
        Tester.verifyListContents(theTestList, new int[]{569, 705, 124, 920, 963, 6, 749});

        theTestList.append(883);
        Tester.verifyListContents(theTestList, new int[]{569, 705, 124, 920, 963, 6, 749, 883});

        theTestList.insertAt(0, 875);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 124, 920, 963, 6, 749, 883});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 124, 920, 6, 749, 883});

        theTestList.insertAt(5, 554);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 124, 920, 554, 6, 749, 883});

        assertFalse("Fehler: Element 165 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(165));
        assertFalse("Fehler: Element 444 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(444));
        theTestList.insertAt(6, 313);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 124, 920, 554, 313, 6, 749, 883});

        theTestList.insertAt(3, 949);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 949, 124, 920, 554, 313, 6, 749, 883});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 949, 124, 920, 554, 313, 6, 883});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 949, 124, 920, 554, 313, 883});

        theTestList.insertAt(3, 449);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 449, 949, 124, 920, 554, 313, 883});

        assertFalse("Fehler: Element 486 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(486));
        assertTrue("Fehler: Element 705 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(705));
        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 449, 949, 124, 554, 313, 883});

        theTestList.insertAt(9, 788);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 449, 949, 124, 554, 313, 883, 788});

        assertFalse("Fehler: Element 335 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(335));
        theTestList.append(170);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 449, 949, 124, 554, 313, 883, 788, 170});

        theTestList.insertAt(4, 10);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 449, 10, 949, 124, 554, 313, 883, 788, 170});

        theTestList.append(431);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 449, 10, 949, 124, 554, 313, 883, 788, 170, 431});

        assertFalse("Fehler: Element 161 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(161));
        assertFalse("Fehler: Element 542 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(542));
        assertFalse("Fehler: Element 726 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(726));
        assertTrue("Fehler: Element 875 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(875));
        assertTrue("Fehler: Element 569 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(569));
        theTestList.insertAt(7, 949);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 449, 10, 949, 124, 949, 554, 313, 883, 788, 170, 431});

        theTestList.removeAt(12);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 449, 10, 949, 124, 949, 554, 313, 883, 788, 431});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{875, 569, 705, 449, 10, 949, 124, 949, 313, 883, 788, 431});

        assertTrue("Fehler: Element 875 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(875));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 949, 313, 883, 788, 431});

        theTestList.insertAt(9, 600);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 949, 313, 883, 600, 788, 431});

        assertFalse("Fehler: Element 93 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(93));
        assertFalse("Fehler: Element 368 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(368));
        theTestList.append(212);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 949, 313, 883, 600, 788, 431, 212});

        theTestList.removeAt(12);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 949, 313, 883, 600, 788, 431});

        theTestList.append(575);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 949, 313, 883, 600, 788, 431, 575});

        theTestList.append(157);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 949, 313, 883, 600, 788, 431, 575, 157});

        theTestList.append(838);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 949, 313, 883, 600, 788, 431, 575, 157, 838});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 313, 883, 600, 788, 431, 575, 157, 838});

        theTestList.removeAt(13);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 313, 883, 600, 788, 431, 575, 157});

        theTestList.insertAt(12, 980);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 313, 883, 600, 788, 431, 575, 980, 157});

        theTestList.append(677);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 313, 883, 600, 788, 431, 575, 980, 157, 677});

        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 313, 883, 600, 788, 575, 980, 157, 677});

        theTestList.insertAt(9, 693);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 313, 883, 600, 693, 788, 575, 980, 157, 677});

        theTestList.append(37);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 313, 883, 600, 693, 788, 575, 980, 157, 677, 37});

        theTestList.append(212);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 313, 883, 600, 693, 788, 575, 980, 157, 677, 37, 212});

        theTestList.append(838);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 313, 883, 600, 693, 788, 575, 980, 157, 677, 37, 212, 838});

        theTestList.insertAt(9, 427);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 313, 883, 600, 427, 693, 788, 575, 980, 157, 677, 37, 212, 838});

        theTestList.append(971);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 313, 883, 600, 427, 693, 788, 575, 980, 157, 677, 37, 212, 838, 971});

        theTestList.insertAt(13, 708);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 313, 883, 600, 427, 693, 788, 575, 708, 980, 157, 677, 37, 212, 838, 971});

        theTestList.removeAt(16);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 313, 883, 600, 427, 693, 788, 575, 708, 980, 157, 37, 212, 838, 971});

        theTestList.insertAt(16, 67);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 313, 883, 600, 427, 693, 788, 575, 708, 980, 157, 67, 37, 212, 838, 971});

        theTestList.insertAt(15, 133);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 313, 883, 600, 427, 693, 788, 575, 708, 980, 133, 157, 67, 37, 212, 838, 971});

        theTestList.insertAt(6, 525);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 949, 124, 525, 313, 883, 600, 427, 693, 788, 575, 708, 980, 133, 157, 67, 37, 212, 838, 971});

        theTestList.insertAt(4, 977);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 977, 949, 124, 525, 313, 883, 600, 427, 693, 788, 575, 708, 980, 133, 157, 67, 37, 212, 838, 971});

        assertTrue("Fehler: Element 600 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(600));
        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{875, 705, 449, 10, 977, 124, 525, 313, 883, 600, 427, 693, 788, 575, 708, 980, 133, 157, 67, 37, 212, 838, 971});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(834);
        Tester.verifyListContents(theTestList, new int[]{834});

        assertTrue("Fehler: Element 834 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(834));
        theTestList.insertAt(0, 657);
        Tester.verifyListContents(theTestList, new int[]{657, 834});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{657});

        assertTrue("Fehler: Element 657 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(657));
        theTestList.insertAt(1, 884);
        Tester.verifyListContents(theTestList, new int[]{657, 884});

        theTestList.append(45);
        Tester.verifyListContents(theTestList, new int[]{657, 884, 45});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{884, 45});

        theTestList.append(644);
        Tester.verifyListContents(theTestList, new int[]{884, 45, 644});

        assertTrue("Fehler: Element 884 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(884));
        theTestList.append(413);
        Tester.verifyListContents(theTestList, new int[]{884, 45, 644, 413});

        assertTrue("Fehler: Element 644 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(644));
        theTestList.append(686);
        Tester.verifyListContents(theTestList, new int[]{884, 45, 644, 413, 686});

        assertFalse("Fehler: Element 804 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(804));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{884, 45, 644, 413});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{884, 644, 413});

        theTestList.append(881);
        Tester.verifyListContents(theTestList, new int[]{884, 644, 413, 881});

        theTestList.insertAt(4, 685);
        Tester.verifyListContents(theTestList, new int[]{884, 644, 413, 881, 685});

        assertFalse("Fehler: Element 87 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(87));
        assertTrue("Fehler: Element 884 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(884));
        assertTrue("Fehler: Element 413 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(413));
        assertTrue("Fehler: Element 413 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(413));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{884, 644, 413, 881});

        theTestList.append(897);
        Tester.verifyListContents(theTestList, new int[]{884, 644, 413, 881, 897});

        theTestList.append(300);
        Tester.verifyListContents(theTestList, new int[]{884, 644, 413, 881, 897, 300});

        theTestList.insertAt(1, 396);
        Tester.verifyListContents(theTestList, new int[]{884, 396, 644, 413, 881, 897, 300});

        assertTrue("Fehler: Element 300 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(300));
        assertTrue("Fehler: Element 413 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(413));
        assertTrue("Fehler: Element 884 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(884));
        theTestList.insertAt(6, 568);
        Tester.verifyListContents(theTestList, new int[]{884, 396, 644, 413, 881, 897, 568, 300});

    }

    public static void test5Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 173);
        Tester.verifyListContents(theTestList, new int[]{173});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 843);
        Tester.verifyListContents(theTestList, new int[]{843});

        assertFalse("Fehler: Element 602 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(602));
        assertTrue("Fehler: Element 843 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(843));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(914);
        Tester.verifyListContents(theTestList, new int[]{914});

        theTestList.insertAt(0, 814);
        Tester.verifyListContents(theTestList, new int[]{814, 914});

        theTestList.insertAt(1, 74);
        Tester.verifyListContents(theTestList, new int[]{814, 74, 914});

        assertFalse("Fehler: Element 787 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(787));
        theTestList.append(846);
        Tester.verifyListContents(theTestList, new int[]{814, 74, 914, 846});

        theTestList.append(95);
        Tester.verifyListContents(theTestList, new int[]{814, 74, 914, 846, 95});

        theTestList.insertAt(0, 562);
        Tester.verifyListContents(theTestList, new int[]{562, 814, 74, 914, 846, 95});

        theTestList.insertAt(6, 84);
        Tester.verifyListContents(theTestList, new int[]{562, 814, 74, 914, 846, 95, 84});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{562, 814, 74, 914, 95, 84});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{562, 74, 914, 95, 84});

        assertFalse("Fehler: Element 182 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(182));
        assertFalse("Fehler: Element 495 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(495));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{562, 74, 914, 84});

        theTestList.insertAt(1, 556);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 74, 914, 84});

        theTestList.insertAt(1, 813);
        Tester.verifyListContents(theTestList, new int[]{562, 813, 556, 74, 914, 84});

        theTestList.append(640);
        Tester.verifyListContents(theTestList, new int[]{562, 813, 556, 74, 914, 84, 640});

        assertFalse("Fehler: Element 601 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(601));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 74, 914, 84, 640});

        theTestList.insertAt(1, 365);
        Tester.verifyListContents(theTestList, new int[]{562, 365, 556, 74, 914, 84, 640});

        theTestList.append(126);
        Tester.verifyListContents(theTestList, new int[]{562, 365, 556, 74, 914, 84, 640, 126});

        theTestList.insertAt(7, 955);
        Tester.verifyListContents(theTestList, new int[]{562, 365, 556, 74, 914, 84, 640, 955, 126});

        theTestList.append(309);
        Tester.verifyListContents(theTestList, new int[]{562, 365, 556, 74, 914, 84, 640, 955, 126, 309});

        assertFalse("Fehler: Element 638 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(638));
        assertFalse("Fehler: Element 765 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(765));
        theTestList.append(551);
        Tester.verifyListContents(theTestList, new int[]{562, 365, 556, 74, 914, 84, 640, 955, 126, 309, 551});

        theTestList.append(347);
        Tester.verifyListContents(theTestList, new int[]{562, 365, 556, 74, 914, 84, 640, 955, 126, 309, 551, 347});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{562, 365, 556, 74, 914, 84, 640, 955, 309, 551, 347});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{562, 365, 556, 74, 914, 84, 640, 955, 551, 347});

        theTestList.append(995);
        Tester.verifyListContents(theTestList, new int[]{562, 365, 556, 74, 914, 84, 640, 955, 551, 347, 995});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 74, 914, 84, 640, 955, 551, 347, 995});

        assertTrue("Fehler: Element 640 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(640));
        assertFalse("Fehler: Element 864 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(864));
        theTestList.insertAt(7, 115);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 74, 914, 84, 640, 955, 115, 551, 347, 995});

        theTestList.append(568);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 74, 914, 84, 640, 955, 115, 551, 347, 995, 568});

        theTestList.insertAt(2, 54);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 54, 74, 914, 84, 640, 955, 115, 551, 347, 995, 568});

        theTestList.insertAt(5, 939);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 54, 74, 914, 939, 84, 640, 955, 115, 551, 347, 995, 568});

        assertTrue("Fehler: Element 84 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(84));
        theTestList.insertAt(8, 790);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 54, 74, 914, 939, 84, 640, 790, 955, 115, 551, 347, 995, 568});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 54, 74, 939, 84, 640, 790, 955, 115, 551, 347, 995, 568});

        assertTrue("Fehler: Element 84 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(84));
        theTestList.append(589);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 54, 74, 939, 84, 640, 790, 955, 115, 551, 347, 995, 568, 589});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 54, 939, 84, 640, 790, 955, 115, 551, 347, 995, 568, 589});

        theTestList.append(83);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 54, 939, 84, 640, 790, 955, 115, 551, 347, 995, 568, 589, 83});

        theTestList.insertAt(9, 267);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 54, 939, 84, 640, 790, 955, 115, 267, 551, 347, 995, 568, 589, 83});

        theTestList.append(131);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 54, 939, 84, 640, 790, 955, 115, 267, 551, 347, 995, 568, 589, 83, 131});

        theTestList.insertAt(6, 602);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 54, 939, 84, 640, 602, 790, 955, 115, 267, 551, 347, 995, 568, 589, 83, 131});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 54, 939, 84, 640, 602, 790, 115, 267, 551, 347, 995, 568, 589, 83, 131});

        theTestList.insertAt(6, 397);
        Tester.verifyListContents(theTestList, new int[]{562, 556, 54, 939, 84, 640, 397, 602, 790, 115, 267, 551, 347, 995, 568, 589, 83, 131});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 781);
        Tester.verifyListContents(theTestList, new int[]{781});

        theTestList.insertAt(0, 964);
        Tester.verifyListContents(theTestList, new int[]{964, 781});

        theTestList.insertAt(0, 380);
        Tester.verifyListContents(theTestList, new int[]{380, 964, 781});

        theTestList.insertAt(0, 991);
        Tester.verifyListContents(theTestList, new int[]{991, 380, 964, 781});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 92);
        Tester.verifyListContents(theTestList, new int[]{92});

        theTestList.append(854);
        Tester.verifyListContents(theTestList, new int[]{92, 854});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{854});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(24);
        Tester.verifyListContents(theTestList, new int[]{24});

        assertFalse("Fehler: Element 994 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(994));
        assertFalse("Fehler: Element 769 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(769));
// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 85);
        Tester.verifyListContents(theTestList, new int[]{85});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 739);
        Tester.verifyListContents(theTestList, new int[]{739});

        assertTrue("Fehler: Element 739 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(739));
        assertTrue("Fehler: Element 739 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(739));
        theTestList.insertAt(1, 609);
        Tester.verifyListContents(theTestList, new int[]{739, 609});

        theTestList.append(646);
        Tester.verifyListContents(theTestList, new int[]{739, 609, 646});

        assertFalse("Fehler: Element 697 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(697));
        theTestList.append(611);
        Tester.verifyListContents(theTestList, new int[]{739, 609, 646, 611});

        theTestList.append(159);
        Tester.verifyListContents(theTestList, new int[]{739, 609, 646, 611, 159});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{739, 646, 611, 159});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{739, 611, 159});

        theTestList.insertAt(3, 86);
        Tester.verifyListContents(theTestList, new int[]{739, 611, 159, 86});

        assertTrue("Fehler: Element 159 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(159));
        theTestList.append(535);
        Tester.verifyListContents(theTestList, new int[]{739, 611, 159, 86, 535});

        theTestList.insertAt(2, 607);
        Tester.verifyListContents(theTestList, new int[]{739, 611, 607, 159, 86, 535});

        theTestList.append(445);
        Tester.verifyListContents(theTestList, new int[]{739, 611, 607, 159, 86, 535, 445});

        assertFalse("Fehler: Element 794 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(794));
        theTestList.append(153);
        Tester.verifyListContents(theTestList, new int[]{739, 611, 607, 159, 86, 535, 445, 153});

        theTestList.insertAt(1, 866);
        Tester.verifyListContents(theTestList, new int[]{739, 866, 611, 607, 159, 86, 535, 445, 153});

        theTestList.append(574);
        Tester.verifyListContents(theTestList, new int[]{739, 866, 611, 607, 159, 86, 535, 445, 153, 574});

        theTestList.insertAt(7, 169);
        Tester.verifyListContents(theTestList, new int[]{739, 866, 611, 607, 159, 86, 535, 169, 445, 153, 574});

        theTestList.append(707);
        Tester.verifyListContents(theTestList, new int[]{739, 866, 611, 607, 159, 86, 535, 169, 445, 153, 574, 707});

        theTestList.insertAt(0, 911);
        Tester.verifyListContents(theTestList, new int[]{911, 739, 866, 611, 607, 159, 86, 535, 169, 445, 153, 574, 707});

        assertFalse("Fehler: Element 476 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(476));
        theTestList.append(353);
        Tester.verifyListContents(theTestList, new int[]{911, 739, 866, 611, 607, 159, 86, 535, 169, 445, 153, 574, 707, 353});

        theTestList.insertAt(1, 459);
        Tester.verifyListContents(theTestList, new int[]{911, 459, 739, 866, 611, 607, 159, 86, 535, 169, 445, 153, 574, 707, 353});

        theTestList.insertAt(6, 431);
        Tester.verifyListContents(theTestList, new int[]{911, 459, 739, 866, 611, 607, 431, 159, 86, 535, 169, 445, 153, 574, 707, 353});

        assertFalse("Fehler: Element 672 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(672));
        theTestList.removeAt(13);
        Tester.verifyListContents(theTestList, new int[]{911, 459, 739, 866, 611, 607, 431, 159, 86, 535, 169, 445, 153, 707, 353});

        theTestList.insertAt(0, 87);
        Tester.verifyListContents(theTestList, new int[]{87, 911, 459, 739, 866, 611, 607, 431, 159, 86, 535, 169, 445, 153, 707, 353});

        theTestList.removeAt(13);
        Tester.verifyListContents(theTestList, new int[]{87, 911, 459, 739, 866, 611, 607, 431, 159, 86, 535, 169, 445, 707, 353});

    }

    public static void test6Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(83);
        Tester.verifyListContents(theTestList, new int[]{83});

        assertFalse("Fehler: Element 893 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(893));
        theTestList.insertAt(0, 421);
        Tester.verifyListContents(theTestList, new int[]{421, 83});

        assertFalse("Fehler: Element 714 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(714));
        theTestList.insertAt(0, 175);
        Tester.verifyListContents(theTestList, new int[]{175, 421, 83});

        assertFalse("Fehler: Element 736 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(736));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{175, 421});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{421});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 639);
        Tester.verifyListContents(theTestList, new int[]{639});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 311);
        Tester.verifyListContents(theTestList, new int[]{311});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 175);
        Tester.verifyListContents(theTestList, new int[]{175});

        assertFalse("Fehler: Element 352 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(352));
        theTestList.append(759);
        Tester.verifyListContents(theTestList, new int[]{175, 759});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{175});

        assertTrue("Fehler: Element 175 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(175));
        assertFalse("Fehler: Element 203 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(203));
        assertTrue("Fehler: Element 175 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(175));
        theTestList.append(117);
        Tester.verifyListContents(theTestList, new int[]{175, 117});

        theTestList.append(664);
        Tester.verifyListContents(theTestList, new int[]{175, 117, 664});

        theTestList.insertAt(3, 1);
        Tester.verifyListContents(theTestList, new int[]{175, 117, 664, 1});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 661);
        Tester.verifyListContents(theTestList, new int[]{661});

        assertFalse("Fehler: Element 752 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(752));
        theTestList.append(285);
        Tester.verifyListContents(theTestList, new int[]{661, 285});

        theTestList.insertAt(2, 234);
        Tester.verifyListContents(theTestList, new int[]{661, 285, 234});

        assertTrue("Fehler: Element 234 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(234));
        theTestList.append(249);
        Tester.verifyListContents(theTestList, new int[]{661, 285, 234, 249});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{285, 234, 249});

        assertFalse("Fehler: Element 227 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(227));
        assertFalse("Fehler: Element 735 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(735));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{285, 234});

        theTestList.append(769);
        Tester.verifyListContents(theTestList, new int[]{285, 234, 769});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{234, 769});

        theTestList.insertAt(1, 617);
        Tester.verifyListContents(theTestList, new int[]{234, 617, 769});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{234, 617});

        assertTrue("Fehler: Element 234 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(234));
        theTestList.append(998);
        Tester.verifyListContents(theTestList, new int[]{234, 617, 998});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{234, 998});

        assertFalse("Fehler: Element 591 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(591));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{998});

        theTestList.insertAt(1, 976);
        Tester.verifyListContents(theTestList, new int[]{998, 976});

        assertTrue("Fehler: Element 976 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(976));
        theTestList.append(474);
        Tester.verifyListContents(theTestList, new int[]{998, 976, 474});

        assertFalse("Fehler: Element 806 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(806));
        assertTrue("Fehler: Element 976 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(976));
        theTestList.append(666);
        Tester.verifyListContents(theTestList, new int[]{998, 976, 474, 666});

        theTestList.append(983);
        Tester.verifyListContents(theTestList, new int[]{998, 976, 474, 666, 983});

        theTestList.append(428);
        Tester.verifyListContents(theTestList, new int[]{998, 976, 474, 666, 983, 428});

        assertTrue("Fehler: Element 428 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(428));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{976, 474, 666, 983, 428});

        assertFalse("Fehler: Element 517 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(517));
        theTestList.append(778);
        Tester.verifyListContents(theTestList, new int[]{976, 474, 666, 983, 428, 778});

        theTestList.insertAt(2, 424);
        Tester.verifyListContents(theTestList, new int[]{976, 474, 424, 666, 983, 428, 778});

        assertTrue("Fehler: Element 424 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(424));
        assertFalse("Fehler: Element 891 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(891));
        assertFalse("Fehler: Element 81 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(81));
        theTestList.insertAt(1, 208);
        Tester.verifyListContents(theTestList, new int[]{976, 208, 474, 424, 666, 983, 428, 778});

        assertFalse("Fehler: Element 809 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(809));
        theTestList.append(623);
        Tester.verifyListContents(theTestList, new int[]{976, 208, 474, 424, 666, 983, 428, 778, 623});

        theTestList.insertAt(4, 412);
        Tester.verifyListContents(theTestList, new int[]{976, 208, 474, 424, 412, 666, 983, 428, 778, 623});

        theTestList.append(321);
        Tester.verifyListContents(theTestList, new int[]{976, 208, 474, 424, 412, 666, 983, 428, 778, 623, 321});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{976, 208, 474, 424, 412, 666, 983, 428, 623, 321});

        theTestList.insertAt(10, 658);
        Tester.verifyListContents(theTestList, new int[]{976, 208, 474, 424, 412, 666, 983, 428, 623, 321, 658});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{976, 208, 474, 424, 412, 983, 428, 623, 321, 658});

        assertFalse("Fehler: Element 924 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(924));
        theTestList.append(719);
        Tester.verifyListContents(theTestList, new int[]{976, 208, 474, 424, 412, 983, 428, 623, 321, 658, 719});

        theTestList.insertAt(8, 367);
        Tester.verifyListContents(theTestList, new int[]{976, 208, 474, 424, 412, 983, 428, 623, 367, 321, 658, 719});

        theTestList.insertAt(12, 35);
        Tester.verifyListContents(theTestList, new int[]{976, 208, 474, 424, 412, 983, 428, 623, 367, 321, 658, 719, 35});

        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{976, 208, 474, 424, 412, 983, 428, 623, 367, 321, 658, 35});

        theTestList.insertAt(7, 973);
        Tester.verifyListContents(theTestList, new int[]{976, 208, 474, 424, 412, 983, 428, 973, 623, 367, 321, 658, 35});

        assertTrue("Fehler: Element 412 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(412));
        theTestList.append(840);
        Tester.verifyListContents(theTestList, new int[]{976, 208, 474, 424, 412, 983, 428, 973, 623, 367, 321, 658, 35, 840});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{208, 474, 424, 412, 983, 428, 973, 623, 367, 321, 658, 35, 840});

        theTestList.append(95);
        Tester.verifyListContents(theTestList, new int[]{208, 474, 424, 412, 983, 428, 973, 623, 367, 321, 658, 35, 840, 95});

        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{208, 474, 424, 412, 983, 428, 973, 623, 367, 321, 35, 840, 95});

        theTestList.insertAt(7, 499);
        Tester.verifyListContents(theTestList, new int[]{208, 474, 424, 412, 983, 428, 973, 499, 623, 367, 321, 35, 840, 95});

        assertFalse("Fehler: Element 599 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(599));
        theTestList.removeAt(12);
        Tester.verifyListContents(theTestList, new int[]{208, 474, 424, 412, 983, 428, 973, 499, 623, 367, 321, 35, 95});

        theTestList.append(448);
        Tester.verifyListContents(theTestList, new int[]{208, 474, 424, 412, 983, 428, 973, 499, 623, 367, 321, 35, 95, 448});

        assertFalse("Fehler: Element 296 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(296));
        theTestList.append(105);
        Tester.verifyListContents(theTestList, new int[]{208, 474, 424, 412, 983, 428, 973, 499, 623, 367, 321, 35, 95, 448, 105});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{208, 474, 424, 412, 983, 428, 973, 499, 623, 321, 35, 95, 448, 105});

        assertFalse("Fehler: Element 903 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(903));
        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{208, 474, 424, 412, 983, 428, 973, 499, 623, 321, 95, 448, 105});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{208, 474, 424, 412, 983, 428, 973, 499, 623, 95, 448, 105});

        theTestList.insertAt(12, 42);
        Tester.verifyListContents(theTestList, new int[]{208, 474, 424, 412, 983, 428, 973, 499, 623, 95, 448, 105, 42});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{208, 474, 424, 983, 428, 973, 499, 623, 95, 448, 105, 42});

        assertFalse("Fehler: Element 317 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(317));
        assertTrue("Fehler: Element 42 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(42));
        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{208, 474, 424, 983, 428, 973, 499, 623, 448, 105, 42});

        theTestList.append(874);
        Tester.verifyListContents(theTestList, new int[]{208, 474, 424, 983, 428, 973, 499, 623, 448, 105, 42, 874});

        assertFalse("Fehler: Element 95 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(95));
        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{208, 474, 424, 983, 428, 973, 623, 448, 105, 42, 874});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{208, 424, 983, 428, 973, 623, 448, 105, 42, 874});

        theTestList.insertAt(2, 120);
        Tester.verifyListContents(theTestList, new int[]{208, 424, 120, 983, 428, 973, 623, 448, 105, 42, 874});

        theTestList.insertAt(4, 683);
        Tester.verifyListContents(theTestList, new int[]{208, 424, 120, 983, 683, 428, 973, 623, 448, 105, 42, 874});

    }

    public static void test7Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(388);
        Tester.verifyListContents(theTestList, new int[]{388});

// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 417);
        Tester.verifyListContents(theTestList, new int[]{417});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 85);
        Tester.verifyListContents(theTestList, new int[]{85});

        theTestList.insertAt(0, 3);
        Tester.verifyListContents(theTestList, new int[]{3, 85});

        theTestList.append(725);
        Tester.verifyListContents(theTestList, new int[]{3, 85, 725});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{3, 725});

        theTestList.append(707);
        Tester.verifyListContents(theTestList, new int[]{3, 725, 707});

        assertFalse("Fehler: Element 523 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(523));
        assertTrue("Fehler: Element 707 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(707));
        theTestList.append(455);
        Tester.verifyListContents(theTestList, new int[]{3, 725, 707, 455});

        theTestList.append(196);
        Tester.verifyListContents(theTestList, new int[]{3, 725, 707, 455, 196});

        assertFalse("Fehler: Element 290 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(290));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{725, 707, 455, 196});

        theTestList.insertAt(3, 454);
        Tester.verifyListContents(theTestList, new int[]{725, 707, 455, 454, 196});

        theTestList.append(489);
        Tester.verifyListContents(theTestList, new int[]{725, 707, 455, 454, 196, 489});

        assertTrue("Fehler: Element 707 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(707));
        assertTrue("Fehler: Element 196 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(196));
        theTestList.insertAt(3, 772);
        Tester.verifyListContents(theTestList, new int[]{725, 707, 455, 772, 454, 196, 489});

        theTestList.append(672);
        Tester.verifyListContents(theTestList, new int[]{725, 707, 455, 772, 454, 196, 489, 672});

        assertFalse("Fehler: Element 368 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(368));
        assertTrue("Fehler: Element 489 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(489));
        assertTrue("Fehler: Element 672 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(672));
        theTestList.append(79);
        Tester.verifyListContents(theTestList, new int[]{725, 707, 455, 772, 454, 196, 489, 672, 79});

        theTestList.append(219);
        Tester.verifyListContents(theTestList, new int[]{725, 707, 455, 772, 454, 196, 489, 672, 79, 219});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(354);
        Tester.verifyListContents(theTestList, new int[]{354});

        theTestList.insertAt(1, 780);
        Tester.verifyListContents(theTestList, new int[]{354, 780});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{780});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(221);
        Tester.verifyListContents(theTestList, new int[]{221});

        theTestList.insertAt(0, 92);
        Tester.verifyListContents(theTestList, new int[]{92, 221});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{92});

        theTestList.insertAt(0, 799);
        Tester.verifyListContents(theTestList, new int[]{799, 92});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{92});

        theTestList.insertAt(1, 588);
        Tester.verifyListContents(theTestList, new int[]{92, 588});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(879);
        Tester.verifyListContents(theTestList, new int[]{879});

        theTestList.append(409);
        Tester.verifyListContents(theTestList, new int[]{879, 409});

        assertFalse("Fehler: Element 982 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(982));
        theTestList.append(688);
        Tester.verifyListContents(theTestList, new int[]{879, 409, 688});

        theTestList.append(377);
        Tester.verifyListContents(theTestList, new int[]{879, 409, 688, 377});

        theTestList.insertAt(3, 763);
        Tester.verifyListContents(theTestList, new int[]{879, 409, 688, 763, 377});

        assertFalse("Fehler: Element 206 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(206));
        assertFalse("Fehler: Element 461 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(461));
        theTestList.insertAt(4, 306);
        Tester.verifyListContents(theTestList, new int[]{879, 409, 688, 763, 306, 377});

        theTestList.insertAt(1, 175);
        Tester.verifyListContents(theTestList, new int[]{879, 175, 409, 688, 763, 306, 377});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{879, 175, 688, 763, 306, 377});

        theTestList.insertAt(4, 148);
        Tester.verifyListContents(theTestList, new int[]{879, 175, 688, 763, 148, 306, 377});

        theTestList.append(675);
        Tester.verifyListContents(theTestList, new int[]{879, 175, 688, 763, 148, 306, 377, 675});

        theTestList.insertAt(7, 893);
        Tester.verifyListContents(theTestList, new int[]{879, 175, 688, 763, 148, 306, 377, 893, 675});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{175, 688, 763, 148, 306, 377, 893, 675});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{175, 688, 763, 306, 377, 893, 675});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{688, 763, 306, 377, 893, 675});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{688, 763, 306, 893, 675});

        assertFalse("Fehler: Element 68 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(68));
        theTestList.insertAt(4, 97);
        Tester.verifyListContents(theTestList, new int[]{688, 763, 306, 893, 97, 675});

        assertFalse("Fehler: Element 348 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(348));
        theTestList.append(868);
        Tester.verifyListContents(theTestList, new int[]{688, 763, 306, 893, 97, 675, 868});

        theTestList.append(688);
        Tester.verifyListContents(theTestList, new int[]{688, 763, 306, 893, 97, 675, 868, 688});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{688, 306, 893, 97, 675, 868, 688});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 692);
        Tester.verifyListContents(theTestList, new int[]{692});

        theTestList.insertAt(0, 130);
        Tester.verifyListContents(theTestList, new int[]{130, 692});

        theTestList.insertAt(0, 541);
        Tester.verifyListContents(theTestList, new int[]{541, 130, 692});

        theTestList.append(92);
        Tester.verifyListContents(theTestList, new int[]{541, 130, 692, 92});

        theTestList.insertAt(4, 695);
        Tester.verifyListContents(theTestList, new int[]{541, 130, 692, 92, 695});

        theTestList.insertAt(4, 157);
        Tester.verifyListContents(theTestList, new int[]{541, 130, 692, 92, 157, 695});

        assertTrue("Fehler: Element 692 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(692));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{541, 130, 692, 92, 695});

        assertTrue("Fehler: Element 541 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(541));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{130, 692, 92, 695});

        theTestList.append(170);
        Tester.verifyListContents(theTestList, new int[]{130, 692, 92, 695, 170});

        theTestList.insertAt(3, 159);
        Tester.verifyListContents(theTestList, new int[]{130, 692, 92, 159, 695, 170});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{130, 692, 92, 159, 170});

        theTestList.insertAt(1, 7);
        Tester.verifyListContents(theTestList, new int[]{130, 7, 692, 92, 159, 170});

        assertFalse("Fehler: Element 216 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(216));
        theTestList.insertAt(0, 828);
        Tester.verifyListContents(theTestList, new int[]{828, 130, 7, 692, 92, 159, 170});

        assertFalse("Fehler: Element 913 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(913));
        theTestList.append(526);
        Tester.verifyListContents(theTestList, new int[]{828, 130, 7, 692, 92, 159, 170, 526});

        theTestList.append(634);
        Tester.verifyListContents(theTestList, new int[]{828, 130, 7, 692, 92, 159, 170, 526, 634});

        theTestList.append(576);
        Tester.verifyListContents(theTestList, new int[]{828, 130, 7, 692, 92, 159, 170, 526, 634, 576});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{828, 130, 692, 92, 159, 170, 526, 634, 576});

        theTestList.insertAt(2, 967);
        Tester.verifyListContents(theTestList, new int[]{828, 130, 967, 692, 92, 159, 170, 526, 634, 576});

        theTestList.append(530);
        Tester.verifyListContents(theTestList, new int[]{828, 130, 967, 692, 92, 159, 170, 526, 634, 576, 530});

        theTestList.append(612);
        Tester.verifyListContents(theTestList, new int[]{828, 130, 967, 692, 92, 159, 170, 526, 634, 576, 530, 612});

        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{828, 130, 967, 692, 92, 159, 170, 526, 634, 576, 530});

        theTestList.insertAt(1, 941);
        Tester.verifyListContents(theTestList, new int[]{828, 941, 130, 967, 692, 92, 159, 170, 526, 634, 576, 530});

        assertTrue("Fehler: Element 828 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(828));
        assertTrue("Fehler: Element 941 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(941));
        theTestList.append(220);
        Tester.verifyListContents(theTestList, new int[]{828, 941, 130, 967, 692, 92, 159, 170, 526, 634, 576, 530, 220});

        theTestList.append(11);
        Tester.verifyListContents(theTestList, new int[]{828, 941, 130, 967, 692, 92, 159, 170, 526, 634, 576, 530, 220, 11});

        assertFalse("Fehler: Element 734 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(734));
        theTestList.insertAt(5, 335);
        Tester.verifyListContents(theTestList, new int[]{828, 941, 130, 967, 692, 335, 92, 159, 170, 526, 634, 576, 530, 220, 11});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{828, 941, 130, 967, 692, 335, 159, 170, 526, 634, 576, 530, 220, 11});

        assertTrue("Fehler: Element 130 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(130));
        theTestList.insertAt(1, 469);
        Tester.verifyListContents(theTestList, new int[]{828, 469, 941, 130, 967, 692, 335, 159, 170, 526, 634, 576, 530, 220, 11});

        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{828, 469, 941, 130, 967, 692, 335, 159, 170, 526, 634, 530, 220, 11});

    }

    public static void test8Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 151);
        Tester.verifyListContents(theTestList, new int[]{151});

        theTestList.insertAt(0, 709);
        Tester.verifyListContents(theTestList, new int[]{709, 151});

        assertTrue("Fehler: Element 151 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(151));
        theTestList.append(262);
        Tester.verifyListContents(theTestList, new int[]{709, 151, 262});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{709, 262});

        assertTrue("Fehler: Element 262 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(262));
        theTestList.append(21);
        Tester.verifyListContents(theTestList, new int[]{709, 262, 21});

        theTestList.append(502);
        Tester.verifyListContents(theTestList, new int[]{709, 262, 21, 502});

        theTestList.append(527);
        Tester.verifyListContents(theTestList, new int[]{709, 262, 21, 502, 527});

        theTestList.append(850);
        Tester.verifyListContents(theTestList, new int[]{709, 262, 21, 502, 527, 850});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{709, 21, 502, 527, 850});

        assertTrue("Fehler: Element 21 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(21));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{709, 502, 527, 850});

        assertFalse("Fehler: Element 584 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(584));
        theTestList.insertAt(0, 364);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 850});

        theTestList.append(89);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 850, 89});

        theTestList.append(710);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 850, 89, 710});

        assertTrue("Fehler: Element 710 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(710));
        theTestList.insertAt(5, 185);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 850, 185, 89, 710});

        theTestList.insertAt(4, 862);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 862, 850, 185, 89, 710});

        theTestList.append(24);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 862, 850, 185, 89, 710, 24});

        assertTrue("Fehler: Element 364 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(364));
        theTestList.append(391);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 862, 850, 185, 89, 710, 24, 391});

        assertTrue("Fehler: Element 850 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(850));
        assertTrue("Fehler: Element 185 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(185));
        theTestList.append(355);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 862, 850, 185, 89, 710, 24, 391, 355});

        theTestList.append(174);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 862, 850, 185, 89, 710, 24, 391, 355, 174});

        theTestList.insertAt(9, 732);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 862, 850, 185, 89, 710, 732, 24, 391, 355, 174});

        theTestList.append(137);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 862, 850, 185, 89, 710, 732, 24, 391, 355, 174, 137});

        theTestList.removeAt(13);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 862, 850, 185, 89, 710, 732, 24, 391, 355, 137});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 862, 850, 89, 710, 732, 24, 391, 355, 137});

        theTestList.append(326);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 862, 850, 89, 710, 732, 24, 391, 355, 137, 326});

        theTestList.append(883);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 862, 850, 89, 710, 732, 24, 391, 355, 137, 326, 883});

        theTestList.insertAt(4, 60);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 60, 862, 850, 89, 710, 732, 24, 391, 355, 137, 326, 883});

        theTestList.append(140);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 60, 862, 850, 89, 710, 732, 24, 391, 355, 137, 326, 883, 140});

        assertFalse("Fehler: Element 792 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(792));
        theTestList.insertAt(5, 368);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 60, 368, 862, 850, 89, 710, 732, 24, 391, 355, 137, 326, 883, 140});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 60, 862, 850, 89, 710, 732, 24, 391, 355, 137, 326, 883, 140});

        theTestList.insertAt(11, 236);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 60, 862, 850, 89, 710, 732, 24, 236, 391, 355, 137, 326, 883, 140});

        assertTrue("Fehler: Element 326 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(326));
        theTestList.insertAt(8, 78);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 60, 862, 850, 89, 78, 710, 732, 24, 236, 391, 355, 137, 326, 883, 140});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{364, 709, 502, 527, 60, 862, 850, 78, 710, 732, 24, 236, 391, 355, 137, 326, 883, 140});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{364, 502, 527, 60, 862, 850, 78, 710, 732, 24, 236, 391, 355, 137, 326, 883, 140});

        theTestList.insertAt(6, 120);
        Tester.verifyListContents(theTestList, new int[]{364, 502, 527, 60, 862, 850, 120, 78, 710, 732, 24, 236, 391, 355, 137, 326, 883, 140});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{364, 502, 527, 60, 862, 850, 120, 78, 732, 24, 236, 391, 355, 137, 326, 883, 140});

        theTestList.removeAt(13);
        Tester.verifyListContents(theTestList, new int[]{364, 502, 527, 60, 862, 850, 120, 78, 732, 24, 236, 391, 355, 326, 883, 140});

        theTestList.append(234);
        Tester.verifyListContents(theTestList, new int[]{364, 502, 527, 60, 862, 850, 120, 78, 732, 24, 236, 391, 355, 326, 883, 140, 234});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(931);
        Tester.verifyListContents(theTestList, new int[]{931});

        theTestList.insertAt(0, 103);
        Tester.verifyListContents(theTestList, new int[]{103, 931});

        theTestList.append(707);
        Tester.verifyListContents(theTestList, new int[]{103, 931, 707});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{103, 931});

        theTestList.insertAt(0, 77);
        Tester.verifyListContents(theTestList, new int[]{77, 103, 931});

        theTestList.append(428);
        Tester.verifyListContents(theTestList, new int[]{77, 103, 931, 428});

        theTestList.append(463);
        Tester.verifyListContents(theTestList, new int[]{77, 103, 931, 428, 463});

        theTestList.insertAt(0, 128);
        Tester.verifyListContents(theTestList, new int[]{128, 77, 103, 931, 428, 463});

        theTestList.append(689);
        Tester.verifyListContents(theTestList, new int[]{128, 77, 103, 931, 428, 463, 689});

        assertTrue("Fehler: Element 77 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(77));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{128, 77, 931, 428, 463, 689});

        theTestList.append(135);
        Tester.verifyListContents(theTestList, new int[]{128, 77, 931, 428, 463, 689, 135});

        theTestList.append(99);
        Tester.verifyListContents(theTestList, new int[]{128, 77, 931, 428, 463, 689, 135, 99});

        theTestList.append(192);
        Tester.verifyListContents(theTestList, new int[]{128, 77, 931, 428, 463, 689, 135, 99, 192});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{128, 77, 931, 428, 463, 135, 99, 192});

        theTestList.insertAt(7, 905);
        Tester.verifyListContents(theTestList, new int[]{128, 77, 931, 428, 463, 135, 99, 905, 192});

        theTestList.insertAt(1, 44);
        Tester.verifyListContents(theTestList, new int[]{128, 44, 77, 931, 428, 463, 135, 99, 905, 192});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{44, 77, 931, 428, 463, 135, 99, 905, 192});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{44, 77, 931, 428, 463, 135, 905, 192});

        assertFalse("Fehler: Element 580 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(580));
        theTestList.insertAt(8, 815);
        Tester.verifyListContents(theTestList, new int[]{44, 77, 931, 428, 463, 135, 905, 192, 815});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{44, 77, 931, 428, 463, 905, 192, 815});

        theTestList.insertAt(6, 300);
        Tester.verifyListContents(theTestList, new int[]{44, 77, 931, 428, 463, 905, 300, 192, 815});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{44, 77, 931, 428, 905, 300, 192, 815});

        theTestList.insertAt(1, 607);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 905, 300, 192, 815});

        theTestList.append(672);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 905, 300, 192, 815, 672});

        theTestList.append(575);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 905, 300, 192, 815, 672, 575});

        assertFalse("Fehler: Element 670 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(670));
        theTestList.append(133);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 905, 300, 192, 815, 672, 575, 133});

        theTestList.append(967);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 905, 300, 192, 815, 672, 575, 133, 967});

        theTestList.removeAt(12);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 905, 300, 192, 815, 672, 575, 133});

        assertTrue("Fehler: Element 300 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(300));
        assertTrue("Fehler: Element 44 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(44));
        assertFalse("Fehler: Element 985 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(985));
        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 905, 300, 192, 815, 575, 133});

        assertTrue("Fehler: Element 428 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(428));
        theTestList.append(906);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 905, 300, 192, 815, 575, 133, 906});

        assertTrue("Fehler: Element 77 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(77));
        assertFalse("Fehler: Element 793 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(793));
        theTestList.insertAt(5, 789);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 789, 905, 300, 192, 815, 575, 133, 906});

        assertTrue("Fehler: Element 906 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(906));
        theTestList.append(548);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 789, 905, 300, 192, 815, 575, 133, 906, 548});

        theTestList.append(471);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 789, 905, 300, 192, 815, 575, 133, 906, 548, 471});

        assertTrue("Fehler: Element 300 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(300));
        theTestList.insertAt(15, 540);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 789, 905, 300, 192, 815, 575, 133, 906, 548, 471, 540});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 789, 905, 300, 815, 575, 133, 906, 548, 471, 540});

        assertFalse("Fehler: Element 541 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(541));
        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 789, 905, 300, 575, 133, 906, 548, 471, 540});

        theTestList.append(999);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 789, 905, 300, 575, 133, 906, 548, 471, 540, 999});

        assertTrue("Fehler: Element 931 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(931));
        theTestList.append(382);
        Tester.verifyListContents(theTestList, new int[]{44, 607, 77, 931, 428, 789, 905, 300, 575, 133, 906, 548, 471, 540, 999, 382});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{607, 77, 931, 428, 789, 905, 300, 575, 133, 906, 548, 471, 540, 999, 382});

    }

    public static void test9Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(449);
        Tester.verifyListContents(theTestList, new int[]{449});

        assertFalse("Fehler: Element 884 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(884));
// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(948);
        Tester.verifyListContents(theTestList, new int[]{948});

        theTestList.insertAt(1, 728);
        Tester.verifyListContents(theTestList, new int[]{948, 728});

        assertTrue("Fehler: Element 948 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(948));
        theTestList.append(890);
        Tester.verifyListContents(theTestList, new int[]{948, 728, 890});

        theTestList.insertAt(3, 382);
        Tester.verifyListContents(theTestList, new int[]{948, 728, 890, 382});

        theTestList.insertAt(4, 168);
        Tester.verifyListContents(theTestList, new int[]{948, 728, 890, 382, 168});

        assertTrue("Fehler: Element 890 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(890));
        theTestList.append(849);
        Tester.verifyListContents(theTestList, new int[]{948, 728, 890, 382, 168, 849});

        assertTrue("Fehler: Element 948 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(948));
        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{948, 728, 890, 382, 168});

        theTestList.append(225);
        Tester.verifyListContents(theTestList, new int[]{948, 728, 890, 382, 168, 225});

        theTestList.insertAt(6, 56);
        Tester.verifyListContents(theTestList, new int[]{948, 728, 890, 382, 168, 225, 56});

        theTestList.insertAt(6, 256);
        Tester.verifyListContents(theTestList, new int[]{948, 728, 890, 382, 168, 225, 256, 56});

        theTestList.insertAt(0, 47);
        Tester.verifyListContents(theTestList, new int[]{47, 948, 728, 890, 382, 168, 225, 256, 56});

        theTestList.insertAt(5, 723);
        Tester.verifyListContents(theTestList, new int[]{47, 948, 728, 890, 382, 723, 168, 225, 256, 56});

        theTestList.append(867);
        Tester.verifyListContents(theTestList, new int[]{47, 948, 728, 890, 382, 723, 168, 225, 256, 56, 867});

        theTestList.append(497);
        Tester.verifyListContents(theTestList, new int[]{47, 948, 728, 890, 382, 723, 168, 225, 256, 56, 867, 497});

        theTestList.insertAt(8, 780);
        Tester.verifyListContents(theTestList, new int[]{47, 948, 728, 890, 382, 723, 168, 225, 780, 256, 56, 867, 497});

        theTestList.append(11);
        Tester.verifyListContents(theTestList, new int[]{47, 948, 728, 890, 382, 723, 168, 225, 780, 256, 56, 867, 497, 11});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 382, 723, 168, 225, 780, 256, 56, 867, 497, 11});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 382, 723, 168, 225, 256, 56, 867, 497, 11});

        theTestList.append(832);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 382, 723, 168, 225, 256, 56, 867, 497, 11, 832});

        theTestList.insertAt(4, 283);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 382, 283, 723, 168, 225, 256, 56, 867, 497, 11, 832});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 723, 168, 225, 256, 56, 867, 497, 11, 832});

        theTestList.append(453);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 723, 168, 225, 256, 56, 867, 497, 11, 832, 453});

        theTestList.insertAt(5, 598);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 723, 598, 168, 225, 256, 56, 867, 497, 11, 832, 453});

        theTestList.removeAt(12);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 723, 598, 168, 225, 256, 56, 867, 497, 832, 453});

        theTestList.append(859);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 723, 598, 168, 225, 256, 56, 867, 497, 832, 453, 859});

        theTestList.removeAt(13);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 723, 598, 168, 225, 256, 56, 867, 497, 832, 859});

        assertFalse("Fehler: Element 802 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(802));
        theTestList.append(793);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 723, 598, 168, 225, 256, 56, 867, 497, 832, 859, 793});

        assertTrue("Fehler: Element 598 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(598));
        theTestList.append(180);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 723, 598, 168, 225, 256, 56, 867, 497, 832, 859, 793, 180});

        theTestList.insertAt(1, 420);
        Tester.verifyListContents(theTestList, new int[]{47, 420, 728, 890, 283, 723, 598, 168, 225, 256, 56, 867, 497, 832, 859, 793, 180});

        theTestList.append(392);
        Tester.verifyListContents(theTestList, new int[]{47, 420, 728, 890, 283, 723, 598, 168, 225, 256, 56, 867, 497, 832, 859, 793, 180, 392});

        theTestList.removeAt(17);
        Tester.verifyListContents(theTestList, new int[]{47, 420, 728, 890, 283, 723, 598, 168, 225, 256, 56, 867, 497, 832, 859, 793, 180});

        assertFalse("Fehler: Element 239 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(239));
        theTestList.insertAt(7, 593);
        Tester.verifyListContents(theTestList, new int[]{47, 420, 728, 890, 283, 723, 598, 593, 168, 225, 256, 56, 867, 497, 832, 859, 793, 180});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 723, 598, 593, 168, 225, 256, 56, 867, 497, 832, 859, 793, 180});

        theTestList.insertAt(17, 832);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 723, 598, 593, 168, 225, 256, 56, 867, 497, 832, 859, 793, 180, 832});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 598, 593, 168, 225, 256, 56, 867, 497, 832, 859, 793, 180, 832});

        assertTrue("Fehler: Element 832 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(832));
        theTestList.insertAt(4, 505);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 505, 598, 593, 168, 225, 256, 56, 867, 497, 832, 859, 793, 180, 832});

        theTestList.insertAt(14, 19);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 505, 598, 593, 168, 225, 256, 56, 867, 497, 832, 19, 859, 793, 180, 832});

        theTestList.append(631);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 505, 598, 593, 168, 225, 256, 56, 867, 497, 832, 19, 859, 793, 180, 832, 631});

        theTestList.append(534);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 505, 598, 593, 168, 225, 256, 56, 867, 497, 832, 19, 859, 793, 180, 832, 631, 534});

        theTestList.insertAt(5, 395);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 505, 395, 598, 593, 168, 225, 256, 56, 867, 497, 832, 19, 859, 793, 180, 832, 631, 534});

        theTestList.append(718);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 505, 395, 598, 593, 168, 225, 256, 56, 867, 497, 832, 19, 859, 793, 180, 832, 631, 534, 718});

        theTestList.append(144);
        Tester.verifyListContents(theTestList, new int[]{47, 728, 890, 283, 505, 395, 598, 593, 168, 225, 256, 56, 867, 497, 832, 19, 859, 793, 180, 832, 631, 534, 718, 144});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(752);
        Tester.verifyListContents(theTestList, new int[]{752});

// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 885);
        Tester.verifyListContents(theTestList, new int[]{885});

        assertFalse("Fehler: Element 533 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(533));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(868);
        Tester.verifyListContents(theTestList, new int[]{868});

        theTestList.append(490);
        Tester.verifyListContents(theTestList, new int[]{868, 490});

        theTestList.insertAt(1, 276);
        Tester.verifyListContents(theTestList, new int[]{868, 276, 490});

        theTestList.insertAt(2, 339);
        Tester.verifyListContents(theTestList, new int[]{868, 276, 339, 490});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{868, 339, 490});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{868, 339});

        theTestList.insertAt(2, 990);
        Tester.verifyListContents(theTestList, new int[]{868, 339, 990});

        assertTrue("Fehler: Element 868 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(868));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{868, 990});

        theTestList.insertAt(0, 185);
        Tester.verifyListContents(theTestList, new int[]{185, 868, 990});

        assertFalse("Fehler: Element 725 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(725));
        theTestList.insertAt(2, 738);
        Tester.verifyListContents(theTestList, new int[]{185, 868, 738, 990});

        assertFalse("Fehler: Element 615 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(615));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{185, 868, 990});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{185, 868});

        assertTrue("Fehler: Element 868 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(868));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{185});

        assertTrue("Fehler: Element 185 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(185));
        theTestList.insertAt(0, 622);
        Tester.verifyListContents(theTestList, new int[]{622, 185});

        assertFalse("Fehler: Element 668 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(668));
        theTestList.append(637);
        Tester.verifyListContents(theTestList, new int[]{622, 185, 637});

        theTestList.append(718);
        Tester.verifyListContents(theTestList, new int[]{622, 185, 637, 718});

        assertTrue("Fehler: Element 637 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(637));
        assertTrue("Fehler: Element 718 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(718));
        assertFalse("Fehler: Element 442 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(442));
        assertFalse("Fehler: Element 419 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(419));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{185, 637, 718});

        theTestList.insertAt(3, 92);
        Tester.verifyListContents(theTestList, new int[]{185, 637, 718, 92});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{185, 718, 92});

        assertFalse("Fehler: Element 590 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(590));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{718, 92});

        theTestList.append(597);
        Tester.verifyListContents(theTestList, new int[]{718, 92, 597});

        theTestList.insertAt(1, 401);
        Tester.verifyListContents(theTestList, new int[]{718, 401, 92, 597});

        theTestList.insertAt(3, 937);
        Tester.verifyListContents(theTestList, new int[]{718, 401, 92, 937, 597});

        theTestList.insertAt(0, 239);
        Tester.verifyListContents(theTestList, new int[]{239, 718, 401, 92, 937, 597});

        assertTrue("Fehler: Element 597 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(597));
        theTestList.append(251);
        Tester.verifyListContents(theTestList, new int[]{239, 718, 401, 92, 937, 597, 251});

        theTestList.insertAt(2, 492);
        Tester.verifyListContents(theTestList, new int[]{239, 718, 492, 401, 92, 937, 597, 251});

        theTestList.append(704);
        Tester.verifyListContents(theTestList, new int[]{239, 718, 492, 401, 92, 937, 597, 251, 704});

        assertTrue("Fehler: Element 401 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(401));
        assertFalse("Fehler: Element 681 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(681));
        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{239, 718, 492, 401, 92, 937, 597, 251});

    }

    public static void test10Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(799);
        Tester.verifyListContents(theTestList, new int[]{799});

        theTestList.append(137);
        Tester.verifyListContents(theTestList, new int[]{799, 137});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{799});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(262);
        Tester.verifyListContents(theTestList, new int[]{262});

        theTestList.insertAt(1, 403);
        Tester.verifyListContents(theTestList, new int[]{262, 403});

        theTestList.insertAt(2, 118);
        Tester.verifyListContents(theTestList, new int[]{262, 403, 118});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{403, 118});

        theTestList.append(905);
        Tester.verifyListContents(theTestList, new int[]{403, 118, 905});

        theTestList.insertAt(3, 135);
        Tester.verifyListContents(theTestList, new int[]{403, 118, 905, 135});

        assertTrue("Fehler: Element 118 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(118));
        theTestList.append(391);
        Tester.verifyListContents(theTestList, new int[]{403, 118, 905, 135, 391});

        theTestList.append(452);
        Tester.verifyListContents(theTestList, new int[]{403, 118, 905, 135, 391, 452});

        assertTrue("Fehler: Element 118 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(118));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{118, 905, 135, 391, 452});

        theTestList.append(508);
        Tester.verifyListContents(theTestList, new int[]{118, 905, 135, 391, 452, 508});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{118, 135, 391, 452, 508});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{135, 391, 452, 508});

        assertTrue("Fehler: Element 135 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(135));
        theTestList.insertAt(3, 68);
        Tester.verifyListContents(theTestList, new int[]{135, 391, 452, 68, 508});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(108);
        Tester.verifyListContents(theTestList, new int[]{108});

        theTestList.insertAt(1, 989);
        Tester.verifyListContents(theTestList, new int[]{108, 989});

        assertTrue("Fehler: Element 989 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(989));
        theTestList.append(912);
        Tester.verifyListContents(theTestList, new int[]{108, 989, 912});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(81);
        Tester.verifyListContents(theTestList, new int[]{81});

        theTestList.append(928);
        Tester.verifyListContents(theTestList, new int[]{81, 928});

        assertFalse("Fehler: Element 400 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(400));
        assertFalse("Fehler: Element 600 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(600));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{81});

        theTestList.insertAt(0, 700);
        Tester.verifyListContents(theTestList, new int[]{700, 81});

        theTestList.append(991);
        Tester.verifyListContents(theTestList, new int[]{700, 81, 991});

        assertFalse("Fehler: Element 407 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(407));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{700, 81});

        theTestList.insertAt(2, 292);
        Tester.verifyListContents(theTestList, new int[]{700, 81, 292});

        theTestList.insertAt(1, 379);
        Tester.verifyListContents(theTestList, new int[]{700, 379, 81, 292});

        theTestList.append(634);
        Tester.verifyListContents(theTestList, new int[]{700, 379, 81, 292, 634});

        theTestList.insertAt(5, 319);
        Tester.verifyListContents(theTestList, new int[]{700, 379, 81, 292, 634, 319});

        assertTrue("Fehler: Element 700 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(700));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{700, 379, 81, 292, 319});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{700, 379, 81, 292});

        theTestList.append(4);
        Tester.verifyListContents(theTestList, new int[]{700, 379, 81, 292, 4});

        theTestList.append(274);
        Tester.verifyListContents(theTestList, new int[]{700, 379, 81, 292, 4, 274});

        assertTrue("Fehler: Element 274 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(274));
        assertTrue("Fehler: Element 4 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(4));
        theTestList.append(822);
        Tester.verifyListContents(theTestList, new int[]{700, 379, 81, 292, 4, 274, 822});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{700, 379, 292, 4, 274, 822});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{700, 292, 4, 274, 822});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{700, 292, 4, 822});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{700, 292, 822});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{700, 292});

        assertFalse("Fehler: Element 367 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(367));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{292});

        theTestList.insertAt(1, 203);
        Tester.verifyListContents(theTestList, new int[]{292, 203});

        assertFalse("Fehler: Element 847 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(847));
        theTestList.insertAt(0, 366);
        Tester.verifyListContents(theTestList, new int[]{366, 292, 203});

        theTestList.append(400);
        Tester.verifyListContents(theTestList, new int[]{366, 292, 203, 400});

        assertTrue("Fehler: Element 400 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(400));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{366, 203, 400});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{366, 400});

        theTestList.insertAt(0, 963);
        Tester.verifyListContents(theTestList, new int[]{963, 366, 400});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{366, 400});

        theTestList.insertAt(0, 778);
        Tester.verifyListContents(theTestList, new int[]{778, 366, 400});

        theTestList.insertAt(3, 813);
        Tester.verifyListContents(theTestList, new int[]{778, 366, 400, 813});

        theTestList.append(881);
        Tester.verifyListContents(theTestList, new int[]{778, 366, 400, 813, 881});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{778, 366, 813, 881});

        theTestList.append(172);
        Tester.verifyListContents(theTestList, new int[]{778, 366, 813, 881, 172});

        theTestList.insertAt(1, 509);
        Tester.verifyListContents(theTestList, new int[]{778, 509, 366, 813, 881, 172});

        assertFalse("Fehler: Element 772 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(772));
        theTestList.insertAt(6, 355);
        Tester.verifyListContents(theTestList, new int[]{778, 509, 366, 813, 881, 172, 355});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{778, 509, 813, 881, 172, 355});

        theTestList.append(247);
        Tester.verifyListContents(theTestList, new int[]{778, 509, 813, 881, 172, 355, 247});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{778, 509, 881, 172, 355, 247});

        theTestList.insertAt(1, 543);
        Tester.verifyListContents(theTestList, new int[]{778, 543, 509, 881, 172, 355, 247});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{778, 543, 509, 172, 355, 247});

        theTestList.insertAt(6, 59);
        Tester.verifyListContents(theTestList, new int[]{778, 543, 509, 172, 355, 247, 59});

        assertFalse("Fehler: Element 705 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(705));
        theTestList.insertAt(0, 334);
        Tester.verifyListContents(theTestList, new int[]{334, 778, 543, 509, 172, 355, 247, 59});

        theTestList.insertAt(0, 137);
        Tester.verifyListContents(theTestList, new int[]{137, 334, 778, 543, 509, 172, 355, 247, 59});

        theTestList.insertAt(8, 339);
        Tester.verifyListContents(theTestList, new int[]{137, 334, 778, 543, 509, 172, 355, 247, 339, 59});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{137, 334, 778, 543, 172, 355, 247, 339, 59});

        theTestList.insertAt(4, 754);
        Tester.verifyListContents(theTestList, new int[]{137, 334, 778, 543, 754, 172, 355, 247, 339, 59});

        theTestList.append(61);
        Tester.verifyListContents(theTestList, new int[]{137, 334, 778, 543, 754, 172, 355, 247, 339, 59, 61});

        theTestList.append(447);
        Tester.verifyListContents(theTestList, new int[]{137, 334, 778, 543, 754, 172, 355, 247, 339, 59, 61, 447});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{137, 334, 778, 543, 754, 172, 355, 339, 59, 61, 447});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{137, 334, 778, 754, 172, 355, 339, 59, 61, 447});

        assertFalse("Fehler: Element 138 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(138));
        theTestList.insertAt(2, 18);
        Tester.verifyListContents(theTestList, new int[]{137, 334, 18, 778, 754, 172, 355, 339, 59, 61, 447});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{137, 334, 18, 778, 754, 172, 355, 339, 59, 447});

        theTestList.insertAt(7, 355);
        Tester.verifyListContents(theTestList, new int[]{137, 334, 18, 778, 754, 172, 355, 355, 339, 59, 447});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{137, 334, 18, 778, 754, 172, 355, 339, 59, 447});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{137, 334, 18, 778, 754, 355, 339, 59, 447});

        theTestList.insertAt(1, 666);
        Tester.verifyListContents(theTestList, new int[]{137, 666, 334, 18, 778, 754, 355, 339, 59, 447});

        theTestList.insertAt(4, 343);
        Tester.verifyListContents(theTestList, new int[]{137, 666, 334, 18, 343, 778, 754, 355, 339, 59, 447});

        theTestList.insertAt(10, 268);
        Tester.verifyListContents(theTestList, new int[]{137, 666, 334, 18, 343, 778, 754, 355, 339, 59, 268, 447});

        theTestList.insertAt(10, 539);
        Tester.verifyListContents(theTestList, new int[]{137, 666, 334, 18, 343, 778, 754, 355, 339, 59, 539, 268, 447});

        theTestList.append(968);
        Tester.verifyListContents(theTestList, new int[]{137, 666, 334, 18, 343, 778, 754, 355, 339, 59, 539, 268, 447, 968});

        theTestList.append(186);
        Tester.verifyListContents(theTestList, new int[]{137, 666, 334, 18, 343, 778, 754, 355, 339, 59, 539, 268, 447, 968, 186});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

    }
}
