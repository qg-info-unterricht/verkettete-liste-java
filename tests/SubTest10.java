package tests;


import static org.junit.Assert.*;

import liste.*;

/**
 * Die Test-Klasse SubTest10.
 *
 * @author Rainer Helfrich
 * @version 03.10.2020
 */
public class SubTest10 {


    public static void test1Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(184);
        Tester.verifyListContents(theTestList, new int[]{184});

        theTestList.append(90);
        Tester.verifyListContents(theTestList, new int[]{184, 90});

        assertTrue("Fehler: Element 90 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(90));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{184});

        assertFalse("Fehler: Element 332 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(332));
        theTestList.append(911);
        Tester.verifyListContents(theTestList, new int[]{184, 911});

        theTestList.append(418);
        Tester.verifyListContents(theTestList, new int[]{184, 911, 418});

        theTestList.append(522);
        Tester.verifyListContents(theTestList, new int[]{184, 911, 418, 522});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{184, 911, 522});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{184, 522});

        assertFalse("Fehler: Element 952 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(952));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{184});

        assertTrue("Fehler: Element 184 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(184));
        theTestList.insertAt(1, 362);
        Tester.verifyListContents(theTestList, new int[]{184, 362});

        theTestList.append(108);
        Tester.verifyListContents(theTestList, new int[]{184, 362, 108});

        assertFalse("Fehler: Element 265 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(265));
        assertFalse("Fehler: Element 640 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(640));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{184, 362});

        theTestList.append(308);
        Tester.verifyListContents(theTestList, new int[]{184, 362, 308});

        theTestList.insertAt(1, 627);
        Tester.verifyListContents(theTestList, new int[]{184, 627, 362, 308});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(603);
        Tester.verifyListContents(theTestList, new int[]{603});

        assertTrue("Fehler: Element 603 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(603));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(90);
        Tester.verifyListContents(theTestList, new int[]{90});

        assertFalse("Fehler: Element 622 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(622));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(182);
        Tester.verifyListContents(theTestList, new int[]{182});

        theTestList.insertAt(1, 371);
        Tester.verifyListContents(theTestList, new int[]{182, 371});

        theTestList.insertAt(2, 525);
        Tester.verifyListContents(theTestList, new int[]{182, 371, 525});

        theTestList.insertAt(3, 380);
        Tester.verifyListContents(theTestList, new int[]{182, 371, 525, 380});

        assertFalse("Fehler: Element 810 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(810));
        assertTrue("Fehler: Element 371 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(371));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{371, 525, 380});

        assertTrue("Fehler: Element 371 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(371));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{525, 380});

        assertFalse("Fehler: Element 253 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(253));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{380});

        theTestList.insertAt(0, 942);
        Tester.verifyListContents(theTestList, new int[]{942, 380});

        theTestList.append(848);
        Tester.verifyListContents(theTestList, new int[]{942, 380, 848});

        assertTrue("Fehler: Element 380 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(380));
        assertFalse("Fehler: Element 620 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(620));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{942, 848});

        theTestList.insertAt(2, 914);
        Tester.verifyListContents(theTestList, new int[]{942, 848, 914});

        assertTrue("Fehler: Element 914 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(914));
        assertTrue("Fehler: Element 848 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(848));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{942, 848});

        assertTrue("Fehler: Element 848 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(848));
        theTestList.insertAt(2, 311);
        Tester.verifyListContents(theTestList, new int[]{942, 848, 311});

        assertFalse("Fehler: Element 687 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(687));
        theTestList.append(7);
        Tester.verifyListContents(theTestList, new int[]{942, 848, 311, 7});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 863);
        Tester.verifyListContents(theTestList, new int[]{863});

        theTestList.insertAt(1, 206);
        Tester.verifyListContents(theTestList, new int[]{863, 206});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{863});

        theTestList.append(360);
        Tester.verifyListContents(theTestList, new int[]{863, 360});

        theTestList.insertAt(0, 860);
        Tester.verifyListContents(theTestList, new int[]{860, 863, 360});

        assertTrue("Fehler: Element 863 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(863));
        assertTrue("Fehler: Element 863 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(863));
        assertFalse("Fehler: Element 433 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(433));
        theTestList.append(230);
        Tester.verifyListContents(theTestList, new int[]{860, 863, 360, 230});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(264);
        Tester.verifyListContents(theTestList, new int[]{264});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 820);
        Tester.verifyListContents(theTestList, new int[]{820});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 796);
        Tester.verifyListContents(theTestList, new int[]{796});

        theTestList.append(860);
        Tester.verifyListContents(theTestList, new int[]{796, 860});

        assertTrue("Fehler: Element 796 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(796));
        theTestList.insertAt(1, 141);
        Tester.verifyListContents(theTestList, new int[]{796, 141, 860});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{796, 141});

        theTestList.insertAt(2, 703);
        Tester.verifyListContents(theTestList, new int[]{796, 141, 703});

        theTestList.append(376);
        Tester.verifyListContents(theTestList, new int[]{796, 141, 703, 376});

        theTestList.insertAt(2, 536);
        Tester.verifyListContents(theTestList, new int[]{796, 141, 536, 703, 376});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{796, 141, 703, 376});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(587);
        Tester.verifyListContents(theTestList, new int[]{587});

        theTestList.insertAt(1, 240);
        Tester.verifyListContents(theTestList, new int[]{587, 240});

        theTestList.insertAt(0, 449);
        Tester.verifyListContents(theTestList, new int[]{449, 587, 240});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{587, 240});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{587});

// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 826);
        Tester.verifyListContents(theTestList, new int[]{826});

        theTestList.insertAt(1, 361);
        Tester.verifyListContents(theTestList, new int[]{826, 361});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{826});

        assertTrue("Fehler: Element 826 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(826));
        theTestList.append(100);
        Tester.verifyListContents(theTestList, new int[]{826, 100});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(23);
        Tester.verifyListContents(theTestList, new int[]{23});

// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(296);
        Tester.verifyListContents(theTestList, new int[]{296});

        theTestList.append(822);
        Tester.verifyListContents(theTestList, new int[]{296, 822});

        theTestList.insertAt(2, 149);
        Tester.verifyListContents(theTestList, new int[]{296, 822, 149});

        theTestList.append(934);
        Tester.verifyListContents(theTestList, new int[]{296, 822, 149, 934});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{296, 149, 934});

        theTestList.insertAt(2, 281);
        Tester.verifyListContents(theTestList, new int[]{296, 149, 281, 934});

        assertFalse("Fehler: Element 950 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(950));
        theTestList.append(750);
        Tester.verifyListContents(theTestList, new int[]{296, 149, 281, 934, 750});

        theTestList.insertAt(0, 594);
        Tester.verifyListContents(theTestList, new int[]{594, 296, 149, 281, 934, 750});

        theTestList.append(787);
        Tester.verifyListContents(theTestList, new int[]{594, 296, 149, 281, 934, 750, 787});

    }

    public static void test2Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 794);
        Tester.verifyListContents(theTestList, new int[]{794});

        theTestList.insertAt(0, 18);
        Tester.verifyListContents(theTestList, new int[]{18, 794});

        theTestList.insertAt(1, 546);
        Tester.verifyListContents(theTestList, new int[]{18, 546, 794});

        assertTrue("Fehler: Element 546 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(546));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{18, 794});

        theTestList.append(984);
        Tester.verifyListContents(theTestList, new int[]{18, 794, 984});

        assertFalse("Fehler: Element 957 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(957));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{794, 984});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{794});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 572);
        Tester.verifyListContents(theTestList, new int[]{572});

        theTestList.insertAt(1, 599);
        Tester.verifyListContents(theTestList, new int[]{572, 599});

        theTestList.append(284);
        Tester.verifyListContents(theTestList, new int[]{572, 599, 284});

        assertFalse("Fehler: Element 817 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(817));
        assertTrue("Fehler: Element 284 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(284));
        theTestList.insertAt(2, 78);
        Tester.verifyListContents(theTestList, new int[]{572, 599, 78, 284});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{572, 599, 284});

        theTestList.append(529);
        Tester.verifyListContents(theTestList, new int[]{572, 599, 284, 529});

        assertTrue("Fehler: Element 599 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(599));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{572, 599, 284});

        assertFalse("Fehler: Element 540 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(540));
        theTestList.insertAt(1, 9);
        Tester.verifyListContents(theTestList, new int[]{572, 9, 599, 284});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{572, 599, 284});

        theTestList.append(469);
        Tester.verifyListContents(theTestList, new int[]{572, 599, 284, 469});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{572, 599, 469});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{599, 469});

        theTestList.append(39);
        Tester.verifyListContents(theTestList, new int[]{599, 469, 39});

        theTestList.append(880);
        Tester.verifyListContents(theTestList, new int[]{599, 469, 39, 880});

        assertTrue("Fehler: Element 39 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(39));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{599, 469, 880});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{599, 880});

        theTestList.insertAt(1, 264);
        Tester.verifyListContents(theTestList, new int[]{599, 264, 880});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{599, 264});

        assertFalse("Fehler: Element 225 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(225));
        assertFalse("Fehler: Element 335 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(335));
        assertTrue("Fehler: Element 264 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(264));
        theTestList.insertAt(2, 686);
        Tester.verifyListContents(theTestList, new int[]{599, 264, 686});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{599, 264});

        theTestList.insertAt(2, 884);
        Tester.verifyListContents(theTestList, new int[]{599, 264, 884});

        theTestList.append(721);
        Tester.verifyListContents(theTestList, new int[]{599, 264, 884, 721});

        assertTrue("Fehler: Element 884 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(884));
        theTestList.append(307);
        Tester.verifyListContents(theTestList, new int[]{599, 264, 884, 721, 307});

        theTestList.insertAt(0, 26);
        Tester.verifyListContents(theTestList, new int[]{26, 599, 264, 884, 721, 307});

        assertTrue("Fehler: Element 599 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(599));
        assertTrue("Fehler: Element 264 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(264));
        assertTrue("Fehler: Element 599 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(599));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{26, 599, 264, 721, 307});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{26, 599, 721, 307});

        theTestList.insertAt(1, 82);
        Tester.verifyListContents(theTestList, new int[]{26, 82, 599, 721, 307});

        theTestList.append(531);
        Tester.verifyListContents(theTestList, new int[]{26, 82, 599, 721, 307, 531});

        theTestList.insertAt(1, 686);
        Tester.verifyListContents(theTestList, new int[]{26, 686, 82, 599, 721, 307, 531});

        assertTrue("Fehler: Element 307 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(307));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 407);
        Tester.verifyListContents(theTestList, new int[]{407});

        assertFalse("Fehler: Element 254 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(254));
        theTestList.append(25);
        Tester.verifyListContents(theTestList, new int[]{407, 25});

        theTestList.append(262);
        Tester.verifyListContents(theTestList, new int[]{407, 25, 262});

        assertFalse("Fehler: Element 521 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(521));
        theTestList.append(736);
        Tester.verifyListContents(theTestList, new int[]{407, 25, 262, 736});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{407, 262, 736});

        assertFalse("Fehler: Element 346 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(346));
        theTestList.insertAt(1, 149);
        Tester.verifyListContents(theTestList, new int[]{407, 149, 262, 736});

        assertFalse("Fehler: Element 482 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(482));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{149, 262, 736});

        theTestList.append(502);
        Tester.verifyListContents(theTestList, new int[]{149, 262, 736, 502});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{149, 262, 736});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(372);
        Tester.verifyListContents(theTestList, new int[]{372});

        assertTrue("Fehler: Element 372 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(372));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(999);
        Tester.verifyListContents(theTestList, new int[]{999});

        theTestList.append(790);
        Tester.verifyListContents(theTestList, new int[]{999, 790});

        assertFalse("Fehler: Element 905 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(905));
        assertFalse("Fehler: Element 360 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(360));
        theTestList.insertAt(0, 524);
        Tester.verifyListContents(theTestList, new int[]{524, 999, 790});

        assertFalse("Fehler: Element 387 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(387));
        theTestList.insertAt(2, 13);
        Tester.verifyListContents(theTestList, new int[]{524, 999, 13, 790});

        assertFalse("Fehler: Element 853 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(853));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{524, 13, 790});

        theTestList.insertAt(1, 309);
        Tester.verifyListContents(theTestList, new int[]{524, 309, 13, 790});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{309, 13, 790});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{13, 790});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{790});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(245);
        Tester.verifyListContents(theTestList, new int[]{245});

        theTestList.insertAt(0, 517);
        Tester.verifyListContents(theTestList, new int[]{517, 245});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{245});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 907);
        Tester.verifyListContents(theTestList, new int[]{907});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(898);
        Tester.verifyListContents(theTestList, new int[]{898});

        assertTrue("Fehler: Element 898 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(898));
        theTestList.insertAt(0, 296);
        Tester.verifyListContents(theTestList, new int[]{296, 898});

        assertFalse("Fehler: Element 677 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(677));
        assertFalse("Fehler: Element 621 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(621));
        theTestList.append(777);
        Tester.verifyListContents(theTestList, new int[]{296, 898, 777});

        assertFalse("Fehler: Element 208 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(208));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 140);
        Tester.verifyListContents(theTestList, new int[]{140});

    }

    public static void test3Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(62);
        Tester.verifyListContents(theTestList, new int[]{62});

        assertTrue("Fehler: Element 62 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(62));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 247);
        Tester.verifyListContents(theTestList, new int[]{247});

        assertFalse("Fehler: Element 494 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(494));
        theTestList.append(425);
        Tester.verifyListContents(theTestList, new int[]{247, 425});

        assertTrue("Fehler: Element 247 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(247));
        assertTrue("Fehler: Element 425 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(425));
        assertFalse("Fehler: Element 501 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(501));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{247});

        theTestList.append(552);
        Tester.verifyListContents(theTestList, new int[]{247, 552});

        assertFalse("Fehler: Element 295 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(295));
        assertFalse("Fehler: Element 259 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(259));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{247});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 395);
        Tester.verifyListContents(theTestList, new int[]{395});

        theTestList.append(387);
        Tester.verifyListContents(theTestList, new int[]{395, 387});

        assertFalse("Fehler: Element 687 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(687));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{395});

        theTestList.insertAt(1, 403);
        Tester.verifyListContents(theTestList, new int[]{395, 403});

        assertTrue("Fehler: Element 403 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(403));
        assertTrue("Fehler: Element 403 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(403));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{395});

        theTestList.insertAt(0, 223);
        Tester.verifyListContents(theTestList, new int[]{223, 395});

        theTestList.insertAt(2, 830);
        Tester.verifyListContents(theTestList, new int[]{223, 395, 830});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{223, 830});

        assertFalse("Fehler: Element 504 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(504));
        theTestList.append(277);
        Tester.verifyListContents(theTestList, new int[]{223, 830, 277});

        assertTrue("Fehler: Element 277 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(277));
        assertTrue("Fehler: Element 830 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(830));
        theTestList.append(292);
        Tester.verifyListContents(theTestList, new int[]{223, 830, 277, 292});

        theTestList.insertAt(4, 653);
        Tester.verifyListContents(theTestList, new int[]{223, 830, 277, 292, 653});

        theTestList.insertAt(4, 458);
        Tester.verifyListContents(theTestList, new int[]{223, 830, 277, 292, 458, 653});

        theTestList.append(839);
        Tester.verifyListContents(theTestList, new int[]{223, 830, 277, 292, 458, 653, 839});

        theTestList.append(100);
        Tester.verifyListContents(theTestList, new int[]{223, 830, 277, 292, 458, 653, 839, 100});

        theTestList.append(73);
        Tester.verifyListContents(theTestList, new int[]{223, 830, 277, 292, 458, 653, 839, 100, 73});

        assertTrue("Fehler: Element 277 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(277));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{223, 277, 292, 458, 653, 839, 100, 73});

        theTestList.append(110);
        Tester.verifyListContents(theTestList, new int[]{223, 277, 292, 458, 653, 839, 100, 73, 110});

        theTestList.insertAt(9, 116);
        Tester.verifyListContents(theTestList, new int[]{223, 277, 292, 458, 653, 839, 100, 73, 110, 116});

        theTestList.insertAt(9, 332);
        Tester.verifyListContents(theTestList, new int[]{223, 277, 292, 458, 653, 839, 100, 73, 110, 332, 116});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(927);
        Tester.verifyListContents(theTestList, new int[]{927});

        theTestList.append(526);
        Tester.verifyListContents(theTestList, new int[]{927, 526});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{526});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(837);
        Tester.verifyListContents(theTestList, new int[]{837});

        assertFalse("Fehler: Element 311 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(311));
        theTestList.insertAt(0, 135);
        Tester.verifyListContents(theTestList, new int[]{135, 837});

        theTestList.insertAt(1, 472);
        Tester.verifyListContents(theTestList, new int[]{135, 472, 837});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{135, 472});

        theTestList.append(811);
        Tester.verifyListContents(theTestList, new int[]{135, 472, 811});

        assertFalse("Fehler: Element 750 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(750));
        theTestList.append(775);
        Tester.verifyListContents(theTestList, new int[]{135, 472, 811, 775});

        theTestList.insertAt(4, 895);
        Tester.verifyListContents(theTestList, new int[]{135, 472, 811, 775, 895});

        theTestList.insertAt(4, 408);
        Tester.verifyListContents(theTestList, new int[]{135, 472, 811, 775, 408, 895});

        assertFalse("Fehler: Element 325 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(325));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(366);
        Tester.verifyListContents(theTestList, new int[]{366});

        theTestList.append(786);
        Tester.verifyListContents(theTestList, new int[]{366, 786});

        assertFalse("Fehler: Element 734 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(734));
        theTestList.append(598);
        Tester.verifyListContents(theTestList, new int[]{366, 786, 598});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{366, 598});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{598});

        theTestList.append(668);
        Tester.verifyListContents(theTestList, new int[]{598, 668});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{668});

        theTestList.insertAt(1, 344);
        Tester.verifyListContents(theTestList, new int[]{668, 344});

        assertTrue("Fehler: Element 344 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(344));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{668});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(620);
        Tester.verifyListContents(theTestList, new int[]{620});

        assertFalse("Fehler: Element 157 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(157));
        theTestList.append(389);
        Tester.verifyListContents(theTestList, new int[]{620, 389});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{620});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 72);
        Tester.verifyListContents(theTestList, new int[]{72});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(152);
        Tester.verifyListContents(theTestList, new int[]{152});

        theTestList.insertAt(0, 101);
        Tester.verifyListContents(theTestList, new int[]{101, 152});

        theTestList.append(753);
        Tester.verifyListContents(theTestList, new int[]{101, 152, 753});

        theTestList.append(695);
        Tester.verifyListContents(theTestList, new int[]{101, 152, 753, 695});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{101, 152, 753});

        theTestList.append(121);
        Tester.verifyListContents(theTestList, new int[]{101, 152, 753, 121});

        theTestList.insertAt(1, 220);
        Tester.verifyListContents(theTestList, new int[]{101, 220, 152, 753, 121});

        theTestList.insertAt(4, 47);
        Tester.verifyListContents(theTestList, new int[]{101, 220, 152, 753, 47, 121});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{101, 220, 152, 753, 121});

        theTestList.insertAt(4, 453);
        Tester.verifyListContents(theTestList, new int[]{101, 220, 152, 753, 453, 121});

        assertFalse("Fehler: Element 875 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(875));
        theTestList.append(349);
        Tester.verifyListContents(theTestList, new int[]{101, 220, 152, 753, 453, 121, 349});

        theTestList.append(634);
        Tester.verifyListContents(theTestList, new int[]{101, 220, 152, 753, 453, 121, 349, 634});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{101, 220, 753, 453, 121, 349, 634});

        assertFalse("Fehler: Element 161 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(161));
        assertFalse("Fehler: Element 759 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(759));
        theTestList.insertAt(3, 820);
        Tester.verifyListContents(theTestList, new int[]{101, 220, 753, 820, 453, 121, 349, 634});

        theTestList.append(515);
        Tester.verifyListContents(theTestList, new int[]{101, 220, 753, 820, 453, 121, 349, 634, 515});

        theTestList.append(715);
        Tester.verifyListContents(theTestList, new int[]{101, 220, 753, 820, 453, 121, 349, 634, 515, 715});

        theTestList.insertAt(6, 378);
        Tester.verifyListContents(theTestList, new int[]{101, 220, 753, 820, 453, 121, 378, 349, 634, 515, 715});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{101, 220, 820, 453, 121, 378, 349, 634, 515, 715});

        theTestList.insertAt(6, 73);
        Tester.verifyListContents(theTestList, new int[]{101, 220, 820, 453, 121, 378, 73, 349, 634, 515, 715});

        theTestList.insertAt(3, 0);
        Tester.verifyListContents(theTestList, new int[]{101, 220, 820, 0, 453, 121, 378, 73, 349, 634, 515, 715});

    }

    public static void test4Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(434);
        Tester.verifyListContents(theTestList, new int[]{434});

        theTestList.append(157);
        Tester.verifyListContents(theTestList, new int[]{434, 157});

        theTestList.append(387);
        Tester.verifyListContents(theTestList, new int[]{434, 157, 387});

        theTestList.append(306);
        Tester.verifyListContents(theTestList, new int[]{434, 157, 387, 306});

        assertTrue("Fehler: Element 387 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(387));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{157, 387, 306});

        assertFalse("Fehler: Element 922 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(922));
        theTestList.append(625);
        Tester.verifyListContents(theTestList, new int[]{157, 387, 306, 625});

        theTestList.insertAt(3, 751);
        Tester.verifyListContents(theTestList, new int[]{157, 387, 306, 751, 625});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{157, 306, 751, 625});

        assertFalse("Fehler: Element 498 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(498));
        assertFalse("Fehler: Element 782 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(782));
        theTestList.append(438);
        Tester.verifyListContents(theTestList, new int[]{157, 306, 751, 625, 438});

        theTestList.insertAt(3, 758);
        Tester.verifyListContents(theTestList, new int[]{157, 306, 751, 758, 625, 438});

        assertFalse("Fehler: Element 509 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(509));
        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{157, 306, 751, 758, 625});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{157, 306, 758, 625});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{157, 758, 625});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{157, 758});

        theTestList.append(584);
        Tester.verifyListContents(theTestList, new int[]{157, 758, 584});

        theTestList.append(616);
        Tester.verifyListContents(theTestList, new int[]{157, 758, 584, 616});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{758, 584, 616});

        assertFalse("Fehler: Element 965 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(965));
        theTestList.insertAt(3, 652);
        Tester.verifyListContents(theTestList, new int[]{758, 584, 616, 652});

        assertFalse("Fehler: Element 157 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(157));
        assertFalse("Fehler: Element 644 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(644));
        assertFalse("Fehler: Element 301 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(301));
        theTestList.append(660);
        Tester.verifyListContents(theTestList, new int[]{758, 584, 616, 652, 660});

        theTestList.insertAt(5, 880);
        Tester.verifyListContents(theTestList, new int[]{758, 584, 616, 652, 660, 880});

        assertFalse("Fehler: Element 407 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(407));
        theTestList.append(711);
        Tester.verifyListContents(theTestList, new int[]{758, 584, 616, 652, 660, 880, 711});

        theTestList.insertAt(2, 176);
        Tester.verifyListContents(theTestList, new int[]{758, 584, 176, 616, 652, 660, 880, 711});

        theTestList.append(803);
        Tester.verifyListContents(theTestList, new int[]{758, 584, 176, 616, 652, 660, 880, 711, 803});

        assertTrue("Fehler: Element 660 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(660));
        assertFalse("Fehler: Element 573 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(573));
        theTestList.append(63);
        Tester.verifyListContents(theTestList, new int[]{758, 584, 176, 616, 652, 660, 880, 711, 803, 63});

        theTestList.insertAt(4, 525);
        Tester.verifyListContents(theTestList, new int[]{758, 584, 176, 616, 525, 652, 660, 880, 711, 803, 63});

        theTestList.insertAt(0, 50);
        Tester.verifyListContents(theTestList, new int[]{50, 758, 584, 176, 616, 525, 652, 660, 880, 711, 803, 63});

        assertTrue("Fehler: Element 711 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(711));
        theTestList.insertAt(8, 695);
        Tester.verifyListContents(theTestList, new int[]{50, 758, 584, 176, 616, 525, 652, 660, 695, 880, 711, 803, 63});

        theTestList.append(605);
        Tester.verifyListContents(theTestList, new int[]{50, 758, 584, 176, 616, 525, 652, 660, 695, 880, 711, 803, 63, 605});

        theTestList.append(983);
        Tester.verifyListContents(theTestList, new int[]{50, 758, 584, 176, 616, 525, 652, 660, 695, 880, 711, 803, 63, 605, 983});

        assertTrue("Fehler: Element 880 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(880));
        assertFalse("Fehler: Element 224 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(224));
        theTestList.insertAt(0, 535);
        Tester.verifyListContents(theTestList, new int[]{535, 50, 758, 584, 176, 616, 525, 652, 660, 695, 880, 711, 803, 63, 605, 983});

        assertFalse("Fehler: Element 14 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(14));
        assertFalse("Fehler: Element 996 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(996));
        theTestList.append(800);
        Tester.verifyListContents(theTestList, new int[]{535, 50, 758, 584, 176, 616, 525, 652, 660, 695, 880, 711, 803, 63, 605, 983, 800});

        theTestList.append(699);
        Tester.verifyListContents(theTestList, new int[]{535, 50, 758, 584, 176, 616, 525, 652, 660, 695, 880, 711, 803, 63, 605, 983, 800, 699});

        theTestList.removeAt(15);
        Tester.verifyListContents(theTestList, new int[]{535, 50, 758, 584, 176, 616, 525, 652, 660, 695, 880, 711, 803, 63, 605, 800, 699});

        assertTrue("Fehler: Element 758 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(758));
        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{535, 50, 758, 584, 176, 616, 525, 660, 695, 880, 711, 803, 63, 605, 800, 699});

        theTestList.append(964);
        Tester.verifyListContents(theTestList, new int[]{535, 50, 758, 584, 176, 616, 525, 660, 695, 880, 711, 803, 63, 605, 800, 699, 964});

        assertFalse("Fehler: Element 632 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(632));
        theTestList.append(699);
        Tester.verifyListContents(theTestList, new int[]{535, 50, 758, 584, 176, 616, 525, 660, 695, 880, 711, 803, 63, 605, 800, 699, 964, 699});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{535, 50, 758, 584, 176, 525, 660, 695, 880, 711, 803, 63, 605, 800, 699, 964, 699});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{535, 50, 758, 584, 176, 525, 660, 695, 880, 803, 63, 605, 800, 699, 964, 699});

        theTestList.insertAt(12, 210);
        Tester.verifyListContents(theTestList, new int[]{535, 50, 758, 584, 176, 525, 660, 695, 880, 803, 63, 605, 210, 800, 699, 964, 699});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 778);
        Tester.verifyListContents(theTestList, new int[]{778});

        theTestList.insertAt(0, 91);
        Tester.verifyListContents(theTestList, new int[]{91, 778});

        assertTrue("Fehler: Element 778 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(778));
        assertTrue("Fehler: Element 91 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(91));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{91});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(987);
        Tester.verifyListContents(theTestList, new int[]{987});

        assertFalse("Fehler: Element 966 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(966));
        assertTrue("Fehler: Element 987 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(987));
        theTestList.append(955);
        Tester.verifyListContents(theTestList, new int[]{987, 955});

        assertFalse("Fehler: Element 842 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(842));
        assertFalse("Fehler: Element 556 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(556));
        assertTrue("Fehler: Element 955 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(955));
        assertTrue("Fehler: Element 987 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(987));
        assertTrue("Fehler: Element 955 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(955));
        assertTrue("Fehler: Element 955 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(955));
        theTestList.insertAt(2, 337);
        Tester.verifyListContents(theTestList, new int[]{987, 955, 337});

        assertTrue("Fehler: Element 337 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(337));
        theTestList.insertAt(3, 877);
        Tester.verifyListContents(theTestList, new int[]{987, 955, 337, 877});

        theTestList.insertAt(4, 50);
        Tester.verifyListContents(theTestList, new int[]{987, 955, 337, 877, 50});

        theTestList.insertAt(1, 987);
        Tester.verifyListContents(theTestList, new int[]{987, 987, 955, 337, 877, 50});

        assertTrue("Fehler: Element 987 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(987));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{987, 987, 955, 337, 50});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{987, 987, 955, 50});

        theTestList.append(666);
        Tester.verifyListContents(theTestList, new int[]{987, 987, 955, 50, 666});

        assertTrue("Fehler: Element 987 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(987));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{987, 987, 955, 50});

        assertFalse("Fehler: Element 925 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(925));
        theTestList.insertAt(4, 788);
        Tester.verifyListContents(theTestList, new int[]{987, 987, 955, 50, 788});

        theTestList.append(194);
        Tester.verifyListContents(theTestList, new int[]{987, 987, 955, 50, 788, 194});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{987, 987, 955, 50, 194});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{987, 987, 50, 194});

        theTestList.insertAt(4, 451);
        Tester.verifyListContents(theTestList, new int[]{987, 987, 50, 194, 451});

        assertTrue("Fehler: Element 987 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(987));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{987, 50, 194, 451});

        theTestList.insertAt(2, 260);
        Tester.verifyListContents(theTestList, new int[]{987, 50, 260, 194, 451});

        assertFalse("Fehler: Element 360 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(360));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{50, 260, 194, 451});

        theTestList.insertAt(1, 248);
        Tester.verifyListContents(theTestList, new int[]{50, 248, 260, 194, 451});

        theTestList.insertAt(1, 380);
        Tester.verifyListContents(theTestList, new int[]{50, 380, 248, 260, 194, 451});

        theTestList.append(13);
        Tester.verifyListContents(theTestList, new int[]{50, 380, 248, 260, 194, 451, 13});

    }

    public static void test5Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 948);
        Tester.verifyListContents(theTestList, new int[]{948});

        theTestList.insertAt(1, 800);
        Tester.verifyListContents(theTestList, new int[]{948, 800});

        theTestList.insertAt(0, 881);
        Tester.verifyListContents(theTestList, new int[]{881, 948, 800});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{881, 800});

        assertTrue("Fehler: Element 881 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(881));
        theTestList.append(568);
        Tester.verifyListContents(theTestList, new int[]{881, 800, 568});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{881, 800});

        theTestList.insertAt(2, 734);
        Tester.verifyListContents(theTestList, new int[]{881, 800, 734});

        assertTrue("Fehler: Element 800 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(800));
        theTestList.append(166);
        Tester.verifyListContents(theTestList, new int[]{881, 800, 734, 166});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{881, 800, 166});

        theTestList.append(974);
        Tester.verifyListContents(theTestList, new int[]{881, 800, 166, 974});

        theTestList.insertAt(1, 814);
        Tester.verifyListContents(theTestList, new int[]{881, 814, 800, 166, 974});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{881, 814, 800, 166});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{881, 814, 166});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{881, 166});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{166});

        theTestList.insertAt(1, 801);
        Tester.verifyListContents(theTestList, new int[]{166, 801});

        assertFalse("Fehler: Element 564 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(564));
        theTestList.insertAt(1, 727);
        Tester.verifyListContents(theTestList, new int[]{166, 727, 801});

        assertFalse("Fehler: Element 503 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(503));
        theTestList.append(623);
        Tester.verifyListContents(theTestList, new int[]{166, 727, 801, 623});

        theTestList.append(58);
        Tester.verifyListContents(theTestList, new int[]{166, 727, 801, 623, 58});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(453);
        Tester.verifyListContents(theTestList, new int[]{453});

        theTestList.insertAt(0, 517);
        Tester.verifyListContents(theTestList, new int[]{517, 453});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 132);
        Tester.verifyListContents(theTestList, new int[]{132});

        theTestList.insertAt(0, 623);
        Tester.verifyListContents(theTestList, new int[]{623, 132});

        assertTrue("Fehler: Element 623 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(623));
        theTestList.insertAt(1, 387);
        Tester.verifyListContents(theTestList, new int[]{623, 387, 132});

        theTestList.append(518);
        Tester.verifyListContents(theTestList, new int[]{623, 387, 132, 518});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{623, 387, 518});

        theTestList.append(359);
        Tester.verifyListContents(theTestList, new int[]{623, 387, 518, 359});

        theTestList.append(845);
        Tester.verifyListContents(theTestList, new int[]{623, 387, 518, 359, 845});

        theTestList.insertAt(3, 177);
        Tester.verifyListContents(theTestList, new int[]{623, 387, 518, 177, 359, 845});

        assertFalse("Fehler: Element 477 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(477));
        theTestList.insertAt(4, 191);
        Tester.verifyListContents(theTestList, new int[]{623, 387, 518, 177, 191, 359, 845});

        assertTrue("Fehler: Element 177 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(177));
        assertFalse("Fehler: Element 725 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(725));
        theTestList.insertAt(0, 837);
        Tester.verifyListContents(theTestList, new int[]{837, 623, 387, 518, 177, 191, 359, 845});

        assertTrue("Fehler: Element 359 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(359));
        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{837, 623, 387, 518, 177, 359, 845});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(936);
        Tester.verifyListContents(theTestList, new int[]{936});

        theTestList.append(0);
        Tester.verifyListContents(theTestList, new int[]{936, 0});

        theTestList.insertAt(2, 588);
        Tester.verifyListContents(theTestList, new int[]{936, 0, 588});

        assertFalse("Fehler: Element 470 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(470));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{936, 0});

        theTestList.append(87);
        Tester.verifyListContents(theTestList, new int[]{936, 0, 87});

        assertFalse("Fehler: Element 784 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(784));
        theTestList.append(872);
        Tester.verifyListContents(theTestList, new int[]{936, 0, 87, 872});

        assertFalse("Fehler: Element 955 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(955));
        theTestList.insertAt(3, 964);
        Tester.verifyListContents(theTestList, new int[]{936, 0, 87, 964, 872});

        assertTrue("Fehler: Element 0 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(0));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{936, 87, 964, 872});

        theTestList.insertAt(4, 516);
        Tester.verifyListContents(theTestList, new int[]{936, 87, 964, 872, 516});

        theTestList.append(22);
        Tester.verifyListContents(theTestList, new int[]{936, 87, 964, 872, 516, 22});

        theTestList.insertAt(3, 649);
        Tester.verifyListContents(theTestList, new int[]{936, 87, 964, 649, 872, 516, 22});

        theTestList.append(93);
        Tester.verifyListContents(theTestList, new int[]{936, 87, 964, 649, 872, 516, 22, 93});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(828);
        Tester.verifyListContents(theTestList, new int[]{828});

        theTestList.insertAt(1, 113);
        Tester.verifyListContents(theTestList, new int[]{828, 113});

        assertFalse("Fehler: Element 485 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(485));
        assertFalse("Fehler: Element 227 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(227));
        theTestList.append(306);
        Tester.verifyListContents(theTestList, new int[]{828, 113, 306});

        theTestList.insertAt(2, 27);
        Tester.verifyListContents(theTestList, new int[]{828, 113, 27, 306});

        assertFalse("Fehler: Element 449 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(449));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{828, 27, 306});

        theTestList.append(28);
        Tester.verifyListContents(theTestList, new int[]{828, 27, 306, 28});

        assertTrue("Fehler: Element 828 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(828));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{828, 27, 28});

        theTestList.insertAt(3, 511);
        Tester.verifyListContents(theTestList, new int[]{828, 27, 28, 511});

        assertTrue("Fehler: Element 28 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(28));
        theTestList.append(749);
        Tester.verifyListContents(theTestList, new int[]{828, 27, 28, 511, 749});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{828, 28, 511, 749});

        theTestList.append(603);
        Tester.verifyListContents(theTestList, new int[]{828, 28, 511, 749, 603});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{828, 28, 511, 749});

        theTestList.insertAt(2, 539);
        Tester.verifyListContents(theTestList, new int[]{828, 28, 539, 511, 749});

        theTestList.append(331);
        Tester.verifyListContents(theTestList, new int[]{828, 28, 539, 511, 749, 331});

        theTestList.append(837);
        Tester.verifyListContents(theTestList, new int[]{828, 28, 539, 511, 749, 331, 837});

        theTestList.append(353);
        Tester.verifyListContents(theTestList, new int[]{828, 28, 539, 511, 749, 331, 837, 353});

        assertFalse("Fehler: Element 412 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(412));
        theTestList.append(456);
        Tester.verifyListContents(theTestList, new int[]{828, 28, 539, 511, 749, 331, 837, 353, 456});

        assertTrue("Fehler: Element 456 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(456));
        theTestList.insertAt(0, 524);
        Tester.verifyListContents(theTestList, new int[]{524, 828, 28, 539, 511, 749, 331, 837, 353, 456});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{524, 828, 28, 539, 749, 331, 837, 353, 456});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{524, 828, 28, 539, 749, 331, 353, 456});

        assertTrue("Fehler: Element 28 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(28));
        assertTrue("Fehler: Element 353 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(353));
        theTestList.append(707);
        Tester.verifyListContents(theTestList, new int[]{524, 828, 28, 539, 749, 331, 353, 456, 707});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{524, 828, 28, 539, 749, 331, 353, 456});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{524, 828, 28, 539, 331, 353, 456});

        assertTrue("Fehler: Element 524 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(524));
        assertTrue("Fehler: Element 353 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(353));
        theTestList.append(264);
        Tester.verifyListContents(theTestList, new int[]{524, 828, 28, 539, 331, 353, 456, 264});

        theTestList.insertAt(1, 119);
        Tester.verifyListContents(theTestList, new int[]{524, 119, 828, 28, 539, 331, 353, 456, 264});

        assertFalse("Fehler: Element 482 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(482));
        theTestList.append(127);
        Tester.verifyListContents(theTestList, new int[]{524, 119, 828, 28, 539, 331, 353, 456, 264, 127});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{524, 119, 828, 28, 539, 331, 353, 456, 264});

    }

    public static void test6Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(154);
        Tester.verifyListContents(theTestList, new int[]{154});

        theTestList.insertAt(0, 572);
        Tester.verifyListContents(theTestList, new int[]{572, 154});

        theTestList.insertAt(0, 253);
        Tester.verifyListContents(theTestList, new int[]{253, 572, 154});

        assertTrue("Fehler: Element 253 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(253));
        theTestList.append(282);
        Tester.verifyListContents(theTestList, new int[]{253, 572, 154, 282});

        assertFalse("Fehler: Element 704 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(704));
        theTestList.insertAt(3, 692);
        Tester.verifyListContents(theTestList, new int[]{253, 572, 154, 692, 282});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{253, 572, 154, 692});

        assertFalse("Fehler: Element 862 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(862));
        assertTrue("Fehler: Element 253 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(253));
        assertTrue("Fehler: Element 154 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(154));
        theTestList.append(587);
        Tester.verifyListContents(theTestList, new int[]{253, 572, 154, 692, 587});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{253, 572, 154, 587});

        assertFalse("Fehler: Element 818 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(818));
        assertFalse("Fehler: Element 878 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(878));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{253, 572, 587});

        assertFalse("Fehler: Element 293 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(293));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{253, 587});

        assertFalse("Fehler: Element 527 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(527));
        assertTrue("Fehler: Element 587 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(587));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{587});

        theTestList.insertAt(0, 528);
        Tester.verifyListContents(theTestList, new int[]{528, 587});

        assertFalse("Fehler: Element 634 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(634));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 175);
        Tester.verifyListContents(theTestList, new int[]{175});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 268);
        Tester.verifyListContents(theTestList, new int[]{268});

// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(239);
        Tester.verifyListContents(theTestList, new int[]{239});

        assertTrue("Fehler: Element 239 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(239));
        theTestList.insertAt(1, 705);
        Tester.verifyListContents(theTestList, new int[]{239, 705});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{239});

        assertTrue("Fehler: Element 239 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(239));
        theTestList.append(77);
        Tester.verifyListContents(theTestList, new int[]{239, 77});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{239});

        theTestList.append(461);
        Tester.verifyListContents(theTestList, new int[]{239, 461});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{239});

        assertTrue("Fehler: Element 239 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(239));
        assertTrue("Fehler: Element 239 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(239));
        theTestList.insertAt(0, 396);
        Tester.verifyListContents(theTestList, new int[]{396, 239});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{396});

        theTestList.append(749);
        Tester.verifyListContents(theTestList, new int[]{396, 749});

        assertTrue("Fehler: Element 396 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(396));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{396});

        assertTrue("Fehler: Element 396 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(396));
        theTestList.insertAt(1, 220);
        Tester.verifyListContents(theTestList, new int[]{396, 220});

        theTestList.append(969);
        Tester.verifyListContents(theTestList, new int[]{396, 220, 969});

        theTestList.append(739);
        Tester.verifyListContents(theTestList, new int[]{396, 220, 969, 739});

        theTestList.append(846);
        Tester.verifyListContents(theTestList, new int[]{396, 220, 969, 739, 846});

        theTestList.insertAt(2, 784);
        Tester.verifyListContents(theTestList, new int[]{396, 220, 784, 969, 739, 846});

        theTestList.append(696);
        Tester.verifyListContents(theTestList, new int[]{396, 220, 784, 969, 739, 846, 696});

        theTestList.append(509);
        Tester.verifyListContents(theTestList, new int[]{396, 220, 784, 969, 739, 846, 696, 509});

        assertFalse("Fehler: Element 367 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(367));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{396, 220, 784, 739, 846, 696, 509});

        theTestList.append(44);
        Tester.verifyListContents(theTestList, new int[]{396, 220, 784, 739, 846, 696, 509, 44});

        assertFalse("Fehler: Element 718 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(718));
        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{396, 220, 784, 739, 846, 696, 44});

        assertFalse("Fehler: Element 881 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(881));
        theTestList.append(947);
        Tester.verifyListContents(theTestList, new int[]{396, 220, 784, 739, 846, 696, 44, 947});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{396, 784, 739, 846, 696, 44, 947});

        theTestList.insertAt(0, 287);
        Tester.verifyListContents(theTestList, new int[]{287, 396, 784, 739, 846, 696, 44, 947});

        theTestList.insertAt(6, 982);
        Tester.verifyListContents(theTestList, new int[]{287, 396, 784, 739, 846, 696, 982, 44, 947});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{287, 784, 739, 846, 696, 982, 44, 947});

        theTestList.insertAt(3, 226);
        Tester.verifyListContents(theTestList, new int[]{287, 784, 739, 226, 846, 696, 982, 44, 947});

        theTestList.insertAt(2, 980);
        Tester.verifyListContents(theTestList, new int[]{287, 784, 980, 739, 226, 846, 696, 982, 44, 947});

        theTestList.append(390);
        Tester.verifyListContents(theTestList, new int[]{287, 784, 980, 739, 226, 846, 696, 982, 44, 947, 390});

        theTestList.insertAt(6, 850);
        Tester.verifyListContents(theTestList, new int[]{287, 784, 980, 739, 226, 846, 850, 696, 982, 44, 947, 390});

        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{287, 784, 980, 739, 226, 846, 850, 696, 982, 44, 947});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{287, 784, 980, 739, 226, 846, 850, 982, 44, 947});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{287, 784, 739, 226, 846, 850, 982, 44, 947});

        assertFalse("Fehler: Element 269 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(269));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{287, 739, 226, 846, 850, 982, 44, 947});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{287, 739, 226, 850, 982, 44, 947});

        theTestList.insertAt(2, 467);
        Tester.verifyListContents(theTestList, new int[]{287, 739, 467, 226, 850, 982, 44, 947});

        assertFalse("Fehler: Element 512 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(512));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{287, 739, 226, 850, 982, 44, 947});

        theTestList.append(351);
        Tester.verifyListContents(theTestList, new int[]{287, 739, 226, 850, 982, 44, 947, 351});

        theTestList.insertAt(2, 644);
        Tester.verifyListContents(theTestList, new int[]{287, 739, 644, 226, 850, 982, 44, 947, 351});

        assertTrue("Fehler: Element 739 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(739));
        assertTrue("Fehler: Element 287 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(287));
        theTestList.insertAt(1, 594);
        Tester.verifyListContents(theTestList, new int[]{287, 594, 739, 644, 226, 850, 982, 44, 947, 351});

        theTestList.append(940);
        Tester.verifyListContents(theTestList, new int[]{287, 594, 739, 644, 226, 850, 982, 44, 947, 351, 940});

        theTestList.insertAt(2, 531);
        Tester.verifyListContents(theTestList, new int[]{287, 594, 531, 739, 644, 226, 850, 982, 44, 947, 351, 940});

        theTestList.append(544);
        Tester.verifyListContents(theTestList, new int[]{287, 594, 531, 739, 644, 226, 850, 982, 44, 947, 351, 940, 544});

        assertFalse("Fehler: Element 35 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(35));
        theTestList.append(409);
        Tester.verifyListContents(theTestList, new int[]{287, 594, 531, 739, 644, 226, 850, 982, 44, 947, 351, 940, 544, 409});

        assertFalse("Fehler: Element 523 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(523));
        assertTrue("Fehler: Element 940 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(940));
        theTestList.insertAt(13, 257);
        Tester.verifyListContents(theTestList, new int[]{287, 594, 531, 739, 644, 226, 850, 982, 44, 947, 351, 940, 544, 257, 409});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{287, 594, 531, 739, 644, 226, 850, 982, 947, 351, 940, 544, 257, 409});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{287, 594, 531, 739, 644, 226, 982, 947, 351, 940, 544, 257, 409});

        assertTrue("Fehler: Element 226 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(226));
        assertFalse("Fehler: Element 717 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(717));
        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{287, 594, 531, 739, 644, 226, 982, 947, 351, 940, 257, 409});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{287, 594, 531, 739, 644, 226, 947, 351, 940, 257, 409});

        assertTrue("Fehler: Element 739 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(739));
        theTestList.append(37);
        Tester.verifyListContents(theTestList, new int[]{287, 594, 531, 739, 644, 226, 947, 351, 940, 257, 409, 37});

        theTestList.insertAt(3, 28);
        Tester.verifyListContents(theTestList, new int[]{287, 594, 531, 28, 739, 644, 226, 947, 351, 940, 257, 409, 37});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{287, 594, 531, 28, 739, 644, 947, 351, 940, 257, 409, 37});

    }

    public static void test7Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(547);
        Tester.verifyListContents(theTestList, new int[]{547});

        theTestList.insertAt(1, 385);
        Tester.verifyListContents(theTestList, new int[]{547, 385});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{547});

        assertFalse("Fehler: Element 189 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(189));
        assertTrue("Fehler: Element 547 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(547));
        theTestList.insertAt(1, 346);
        Tester.verifyListContents(theTestList, new int[]{547, 346});

        theTestList.append(142);
        Tester.verifyListContents(theTestList, new int[]{547, 346, 142});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{346, 142});

        assertTrue("Fehler: Element 346 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(346));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{142});

        theTestList.append(920);
        Tester.verifyListContents(theTestList, new int[]{142, 920});

        theTestList.append(857);
        Tester.verifyListContents(theTestList, new int[]{142, 920, 857});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{142, 920});

        theTestList.append(715);
        Tester.verifyListContents(theTestList, new int[]{142, 920, 715});

        theTestList.append(964);
        Tester.verifyListContents(theTestList, new int[]{142, 920, 715, 964});

        assertTrue("Fehler: Element 920 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(920));
        theTestList.append(516);
        Tester.verifyListContents(theTestList, new int[]{142, 920, 715, 964, 516});

        assertTrue("Fehler: Element 964 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(964));
        theTestList.insertAt(1, 171);
        Tester.verifyListContents(theTestList, new int[]{142, 171, 920, 715, 964, 516});

        theTestList.insertAt(2, 48);
        Tester.verifyListContents(theTestList, new int[]{142, 171, 48, 920, 715, 964, 516});

        theTestList.insertAt(4, 256);
        Tester.verifyListContents(theTestList, new int[]{142, 171, 48, 920, 256, 715, 964, 516});

        theTestList.insertAt(0, 239);
        Tester.verifyListContents(theTestList, new int[]{239, 142, 171, 48, 920, 256, 715, 964, 516});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{239, 142, 171, 48, 920, 256, 715, 516});

        assertTrue("Fehler: Element 715 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(715));
        theTestList.append(725);
        Tester.verifyListContents(theTestList, new int[]{239, 142, 171, 48, 920, 256, 715, 516, 725});

        theTestList.insertAt(1, 295);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 256, 715, 516, 725});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 256, 715, 516});

        theTestList.append(355);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 256, 715, 516, 355});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 256, 516, 355});

        assertFalse("Fehler: Element 930 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(930));
        assertFalse("Fehler: Element 934 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(934));
        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 256, 355});

        assertFalse("Fehler: Element 179 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(179));
        theTestList.append(412);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 256, 355, 412});

        theTestList.append(823);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 256, 355, 412, 823});

        theTestList.insertAt(6, 150);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 150, 256, 355, 412, 823});

        theTestList.append(13);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 150, 256, 355, 412, 823, 13});

        theTestList.append(484);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 150, 256, 355, 412, 823, 13, 484});

        theTestList.append(493);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 150, 256, 355, 412, 823, 13, 484, 493});

        theTestList.append(74);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 150, 256, 355, 412, 823, 13, 484, 493, 74});

        assertTrue("Fehler: Element 171 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(171));
        assertTrue("Fehler: Element 150 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(150));
        theTestList.append(489);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 150, 256, 355, 412, 823, 13, 484, 493, 74, 489});

        theTestList.insertAt(6, 238);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 238, 150, 256, 355, 412, 823, 13, 484, 493, 74, 489});

        theTestList.append(417);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 238, 150, 256, 355, 412, 823, 13, 484, 493, 74, 489, 417});

        assertFalse("Fehler: Element 456 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(456));
        theTestList.append(702);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 238, 150, 256, 355, 412, 823, 13, 484, 493, 74, 489, 417, 702});

        assertTrue("Fehler: Element 171 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(171));
        theTestList.insertAt(16, 947);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 238, 150, 256, 355, 412, 823, 13, 484, 493, 74, 947, 489, 417, 702});

        theTestList.removeAt(18);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 920, 238, 150, 256, 355, 412, 823, 13, 484, 493, 74, 947, 489, 702});

        theTestList.insertAt(5, 573);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 573, 920, 238, 150, 256, 355, 412, 823, 13, 484, 493, 74, 947, 489, 702});

        theTestList.removeAt(16);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 573, 920, 238, 150, 256, 355, 412, 823, 13, 484, 493, 947, 489, 702});

        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 573, 920, 238, 150, 256, 412, 823, 13, 484, 493, 947, 489, 702});

        theTestList.insertAt(6, 794);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 573, 794, 920, 238, 150, 256, 412, 823, 13, 484, 493, 947, 489, 702});

        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 142, 171, 48, 573, 794, 920, 238, 150, 412, 823, 13, 484, 493, 947, 489, 702});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 171, 48, 573, 794, 920, 238, 150, 412, 823, 13, 484, 493, 947, 489, 702});

        theTestList.append(420);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 171, 48, 573, 794, 920, 238, 150, 412, 823, 13, 484, 493, 947, 489, 702, 420});

        assertFalse("Fehler: Element 723 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(723));
        theTestList.append(207);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 171, 48, 573, 794, 920, 238, 150, 412, 823, 13, 484, 493, 947, 489, 702, 420, 207});

        assertTrue("Fehler: Element 207 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(207));
        assertFalse("Fehler: Element 179 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(179));
        theTestList.append(70);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 171, 48, 573, 794, 920, 238, 150, 412, 823, 13, 484, 493, 947, 489, 702, 420, 207, 70});

        assertTrue("Fehler: Element 412 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(412));
        theTestList.insertAt(13, 373);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 171, 48, 573, 794, 920, 238, 150, 412, 823, 13, 484, 373, 493, 947, 489, 702, 420, 207, 70});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 171, 48, 573, 794, 920, 150, 412, 823, 13, 484, 373, 493, 947, 489, 702, 420, 207, 70});

        theTestList.insertAt(11, 825);
        Tester.verifyListContents(theTestList, new int[]{239, 295, 171, 48, 573, 794, 920, 150, 412, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 207, 70});

        theTestList.insertAt(1, 170);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 295, 171, 48, 573, 794, 920, 150, 412, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 207, 70});

        theTestList.append(450);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 295, 171, 48, 573, 794, 920, 150, 412, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 207, 70, 450});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 295, 171, 48, 573, 920, 150, 412, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 207, 70, 450});

        theTestList.removeAt(19);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 295, 171, 48, 573, 920, 150, 412, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450});

        theTestList.append(196);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 295, 171, 48, 573, 920, 150, 412, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450, 196});

        theTestList.insertAt(9, 574);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 295, 171, 48, 573, 920, 150, 412, 574, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450, 196});

        theTestList.insertAt(5, 335);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 295, 171, 48, 335, 573, 920, 150, 412, 574, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450, 196});

        theTestList.append(348);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 295, 171, 48, 335, 573, 920, 150, 412, 574, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450, 196, 348});

        theTestList.append(935);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 295, 171, 48, 335, 573, 920, 150, 412, 574, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450, 196, 348, 935});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 295, 171, 48, 335, 573, 920, 412, 574, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450, 196, 348, 935});

        assertTrue("Fehler: Element 420 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(420));
        assertTrue("Fehler: Element 295 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(295));
        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 295, 171, 48, 335, 920, 412, 574, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450, 196, 348, 935});

        theTestList.append(441);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 295, 171, 48, 335, 920, 412, 574, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450, 196, 348, 935, 441});

        theTestList.insertAt(2, 688);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 688, 295, 171, 48, 335, 920, 412, 574, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450, 196, 348, 935, 441});

        assertTrue("Fehler: Element 489 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(489));
        theTestList.append(558);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 688, 295, 171, 48, 335, 920, 412, 574, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450, 196, 348, 935, 441, 558});

        theTestList.insertAt(1, 613);
        Tester.verifyListContents(theTestList, new int[]{239, 613, 170, 688, 295, 171, 48, 335, 920, 412, 574, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450, 196, 348, 935, 441, 558});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{239, 613, 170, 688, 295, 171, 335, 920, 412, 574, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450, 196, 348, 935, 441, 558});

        assertFalse("Fehler: Element 303 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(303));
        assertFalse("Fehler: Element 111 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(111));
        theTestList.append(369);
        Tester.verifyListContents(theTestList, new int[]{239, 613, 170, 688, 295, 171, 335, 920, 412, 574, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450, 196, 348, 935, 441, 558, 369});

        theTestList.append(209);
        Tester.verifyListContents(theTestList, new int[]{239, 613, 170, 688, 295, 171, 335, 920, 412, 574, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450, 196, 348, 935, 441, 558, 369, 209});

        theTestList.insertAt(23, 243);
        Tester.verifyListContents(theTestList, new int[]{239, 613, 170, 688, 295, 171, 335, 920, 412, 574, 823, 13, 825, 484, 373, 493, 947, 489, 702, 420, 70, 450, 196, 243, 348, 935, 441, 558, 369, 209});

        theTestList.insertAt(19, 523);
        Tester.verifyListContents(theTestList, new int[]{239, 613, 170, 688, 295, 171, 335, 920, 412, 574, 823, 13, 825, 484, 373, 493, 947, 489, 702, 523, 420, 70, 450, 196, 243, 348, 935, 441, 558, 369, 209});

        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{239, 613, 170, 688, 295, 171, 335, 920, 412, 574, 823, 825, 484, 373, 493, 947, 489, 702, 523, 420, 70, 450, 196, 243, 348, 935, 441, 558, 369, 209});

        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{239, 613, 170, 688, 295, 171, 335, 920, 412, 574, 823, 484, 373, 493, 947, 489, 702, 523, 420, 70, 450, 196, 243, 348, 935, 441, 558, 369, 209});

        theTestList.insertAt(4, 144);
        Tester.verifyListContents(theTestList, new int[]{239, 613, 170, 688, 144, 295, 171, 335, 920, 412, 574, 823, 484, 373, 493, 947, 489, 702, 523, 420, 70, 450, 196, 243, 348, 935, 441, 558, 369, 209});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{239, 613, 170, 688, 144, 295, 171, 335, 920, 574, 823, 484, 373, 493, 947, 489, 702, 523, 420, 70, 450, 196, 243, 348, 935, 441, 558, 369, 209});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 688, 144, 295, 171, 335, 920, 574, 823, 484, 373, 493, 947, 489, 702, 523, 420, 70, 450, 196, 243, 348, 935, 441, 558, 369, 209});

        assertFalse("Fehler: Element 121 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(121));
        assertTrue("Fehler: Element 489 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(489));
        theTestList.append(48);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 688, 144, 295, 171, 335, 920, 574, 823, 484, 373, 493, 947, 489, 702, 523, 420, 70, 450, 196, 243, 348, 935, 441, 558, 369, 209, 48});

        theTestList.insertAt(16, 365);
        Tester.verifyListContents(theTestList, new int[]{239, 170, 688, 144, 295, 171, 335, 920, 574, 823, 484, 373, 493, 947, 489, 702, 365, 523, 420, 70, 450, 196, 243, 348, 935, 441, 558, 369, 209, 48});

    }

    public static void test8Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 842);
        Tester.verifyListContents(theTestList, new int[]{842});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(767);
        Tester.verifyListContents(theTestList, new int[]{767});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 846);
        Tester.verifyListContents(theTestList, new int[]{846});

        theTestList.append(433);
        Tester.verifyListContents(theTestList, new int[]{846, 433});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{433});

        assertFalse("Fehler: Element 820 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(820));
        theTestList.append(466);
        Tester.verifyListContents(theTestList, new int[]{433, 466});

        theTestList.append(701);
        Tester.verifyListContents(theTestList, new int[]{433, 466, 701});

        theTestList.insertAt(1, 40);
        Tester.verifyListContents(theTestList, new int[]{433, 40, 466, 701});

        theTestList.append(465);
        Tester.verifyListContents(theTestList, new int[]{433, 40, 466, 701, 465});

        theTestList.insertAt(0, 121);
        Tester.verifyListContents(theTestList, new int[]{121, 433, 40, 466, 701, 465});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{433, 40, 466, 701, 465});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{433, 466, 701, 465});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{466, 701, 465});

        theTestList.append(94);
        Tester.verifyListContents(theTestList, new int[]{466, 701, 465, 94});

        theTestList.append(660);
        Tester.verifyListContents(theTestList, new int[]{466, 701, 465, 94, 660});

        assertFalse("Fehler: Element 31 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(31));
        theTestList.append(728);
        Tester.verifyListContents(theTestList, new int[]{466, 701, 465, 94, 660, 728});

        theTestList.append(683);
        Tester.verifyListContents(theTestList, new int[]{466, 701, 465, 94, 660, 728, 683});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{466, 701, 465, 94, 728, 683});

        theTestList.append(15);
        Tester.verifyListContents(theTestList, new int[]{466, 701, 465, 94, 728, 683, 15});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{466, 701, 94, 728, 683, 15});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{466, 701, 728, 683, 15});

        theTestList.append(384);
        Tester.verifyListContents(theTestList, new int[]{466, 701, 728, 683, 15, 384});

        assertFalse("Fehler: Element 155 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(155));
        assertTrue("Fehler: Element 683 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(683));
        assertTrue("Fehler: Element 728 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(728));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{466, 701, 728, 683, 384});

        theTestList.insertAt(1, 182);
        Tester.verifyListContents(theTestList, new int[]{466, 182, 701, 728, 683, 384});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{466, 701, 728, 683, 384});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{466, 728, 683, 384});

        theTestList.append(211);
        Tester.verifyListContents(theTestList, new int[]{466, 728, 683, 384, 211});

        theTestList.insertAt(2, 916);
        Tester.verifyListContents(theTestList, new int[]{466, 728, 916, 683, 384, 211});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{466, 916, 683, 384, 211});

        theTestList.append(450);
        Tester.verifyListContents(theTestList, new int[]{466, 916, 683, 384, 211, 450});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{466, 916, 683, 384, 211});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{466, 683, 384, 211});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(916);
        Tester.verifyListContents(theTestList, new int[]{916});

        theTestList.append(598);
        Tester.verifyListContents(theTestList, new int[]{916, 598});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{598});

        assertTrue("Fehler: Element 598 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(598));
        assertFalse("Fehler: Element 112 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(112));
        assertTrue("Fehler: Element 598 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(598));
        theTestList.insertAt(1, 161);
        Tester.verifyListContents(theTestList, new int[]{598, 161});

        assertTrue("Fehler: Element 598 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(598));
        theTestList.insertAt(1, 483);
        Tester.verifyListContents(theTestList, new int[]{598, 483, 161});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{598, 483});

        assertTrue("Fehler: Element 483 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(483));
        theTestList.append(370);
        Tester.verifyListContents(theTestList, new int[]{598, 483, 370});

        assertFalse("Fehler: Element 266 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(266));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{483, 370});

        theTestList.append(497);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 497});

        assertTrue("Fehler: Element 497 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(497));
        theTestList.insertAt(2, 273);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 273, 497});

        theTestList.insertAt(2, 650);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 273, 497});

        theTestList.append(593);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 273, 497, 593});

        theTestList.insertAt(6, 51);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 273, 497, 593, 51});

        theTestList.insertAt(3, 175);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 175, 273, 497, 593, 51});

        theTestList.append(581);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 175, 273, 497, 593, 51, 581});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 175, 273, 593, 51, 581});

        assertTrue("Fehler: Element 581 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(581));
        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 175, 273, 51, 581});

        theTestList.append(514);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 175, 273, 51, 581, 514});

        assertFalse("Fehler: Element 80 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(80));
        theTestList.insertAt(3, 930);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 273, 51, 581, 514});

        theTestList.append(23);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 273, 51, 581, 514, 23});

        theTestList.insertAt(7, 375);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 273, 51, 375, 581, 514, 23});

        theTestList.append(478);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 273, 51, 375, 581, 514, 23, 478});

        theTestList.append(793);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 273, 51, 375, 581, 514, 23, 478, 793});

        assertTrue("Fehler: Element 514 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(514));
        theTestList.insertAt(9, 303);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 273, 51, 375, 581, 303, 514, 23, 478, 793});

        assertFalse("Fehler: Element 747 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(747));
        theTestList.insertAt(11, 379);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 273, 51, 375, 581, 303, 514, 379, 23, 478, 793});

        assertFalse("Fehler: Element 419 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(419));
        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 51, 375, 581, 303, 514, 379, 23, 478, 793});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 51, 375, 303, 514, 379, 23, 478, 793});

        theTestList.append(789);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 51, 375, 303, 514, 379, 23, 478, 793, 789});

        theTestList.append(37);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 51, 375, 303, 514, 379, 23, 478, 793, 789, 37});

        theTestList.append(727);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 51, 375, 303, 514, 379, 23, 478, 793, 789, 37, 727});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 51, 375, 303, 514, 23, 478, 793, 789, 37, 727});

        assertTrue("Fehler: Element 727 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(727));
        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 51, 375, 303, 514, 23, 793, 789, 37, 727});

        theTestList.append(37);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 51, 375, 303, 514, 23, 793, 789, 37, 727, 37});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 51, 303, 514, 23, 793, 789, 37, 727, 37});

        theTestList.append(741);
        Tester.verifyListContents(theTestList, new int[]{483, 370, 650, 930, 175, 51, 303, 514, 23, 793, 789, 37, 727, 37, 741});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(280);
        Tester.verifyListContents(theTestList, new int[]{280});

        theTestList.insertAt(0, 744);
        Tester.verifyListContents(theTestList, new int[]{744, 280});

        theTestList.insertAt(1, 382);
        Tester.verifyListContents(theTestList, new int[]{744, 382, 280});

        assertTrue("Fehler: Element 744 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(744));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{382, 280});

        assertTrue("Fehler: Element 280 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(280));
        theTestList.append(194);
        Tester.verifyListContents(theTestList, new int[]{382, 280, 194});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 932);
        Tester.verifyListContents(theTestList, new int[]{932});

        assertFalse("Fehler: Element 24 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(24));
    }

    public static void test9Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 441);
        Tester.verifyListContents(theTestList, new int[]{441});

        theTestList.append(248);
        Tester.verifyListContents(theTestList, new int[]{441, 248});

        assertTrue("Fehler: Element 441 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(441));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{248});

        assertFalse("Fehler: Element 594 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(594));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(780);
        Tester.verifyListContents(theTestList, new int[]{780});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(676);
        Tester.verifyListContents(theTestList, new int[]{676});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(150);
        Tester.verifyListContents(theTestList, new int[]{150});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(932);
        Tester.verifyListContents(theTestList, new int[]{932});

        theTestList.append(942);
        Tester.verifyListContents(theTestList, new int[]{932, 942});

        theTestList.insertAt(0, 162);
        Tester.verifyListContents(theTestList, new int[]{162, 932, 942});

        theTestList.insertAt(3, 778);
        Tester.verifyListContents(theTestList, new int[]{162, 932, 942, 778});

        assertFalse("Fehler: Element 241 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(241));
        theTestList.insertAt(1, 95);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 932, 942, 778});

        theTestList.append(366);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 932, 942, 778, 366});

        assertFalse("Fehler: Element 76 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(76));
        theTestList.insertAt(6, 134);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 932, 942, 778, 366, 134});

        assertTrue("Fehler: Element 134 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(134));
        assertFalse("Fehler: Element 966 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(966));
        theTestList.insertAt(7, 464);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 932, 942, 778, 366, 134, 464});

        theTestList.append(910);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 932, 942, 778, 366, 134, 464, 910});

        assertFalse("Fehler: Element 822 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(822));
        assertFalse("Fehler: Element 638 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(638));
        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 932, 942, 778, 134, 464, 910});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 942, 778, 134, 464, 910});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 942, 778, 134, 910});

        assertFalse("Fehler: Element 325 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(325));
        theTestList.append(755);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 942, 778, 134, 910, 755});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 942, 778, 134, 910});

        theTestList.insertAt(6, 82);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 942, 778, 134, 910, 82});

        assertTrue("Fehler: Element 910 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(910));
        theTestList.append(164);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 942, 778, 134, 910, 82, 164});

        theTestList.append(916);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 942, 778, 134, 910, 82, 164, 916});

        assertFalse("Fehler: Element 149 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(149));
        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 942, 778, 134, 910, 164, 916});

        theTestList.append(777);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 942, 778, 134, 910, 164, 916, 777});

        theTestList.append(618);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 942, 778, 134, 910, 164, 916, 777, 618});

        assertFalse("Fehler: Element 928 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(928));
        theTestList.insertAt(6, 356);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 942, 778, 134, 910, 356, 164, 916, 777, 618});

        theTestList.insertAt(7, 239);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 942, 778, 134, 910, 356, 239, 164, 916, 777, 618});

        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 942, 778, 134, 910, 356, 239, 164, 916, 777});

        assertTrue("Fehler: Element 95 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(95));
        assertTrue("Fehler: Element 777 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(777));
        theTestList.insertAt(7, 311);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 942, 778, 134, 910, 356, 311, 239, 164, 916, 777});

        assertTrue("Fehler: Element 239 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(239));
        theTestList.append(774);
        Tester.verifyListContents(theTestList, new int[]{162, 95, 942, 778, 134, 910, 356, 311, 239, 164, 916, 777, 774});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(787);
        Tester.verifyListContents(theTestList, new int[]{787});

        assertTrue("Fehler: Element 787 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(787));
        theTestList.append(732);
        Tester.verifyListContents(theTestList, new int[]{787, 732});

        theTestList.append(750);
        Tester.verifyListContents(theTestList, new int[]{787, 732, 750});

        theTestList.insertAt(2, 158);
        Tester.verifyListContents(theTestList, new int[]{787, 732, 158, 750});

        theTestList.append(645);
        Tester.verifyListContents(theTestList, new int[]{787, 732, 158, 750, 645});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(196);
        Tester.verifyListContents(theTestList, new int[]{196});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(117);
        Tester.verifyListContents(theTestList, new int[]{117});

        theTestList.append(937);
        Tester.verifyListContents(theTestList, new int[]{117, 937});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{117});

// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 485);
        Tester.verifyListContents(theTestList, new int[]{485});

        assertTrue("Fehler: Element 485 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(485));
        theTestList.insertAt(0, 481);
        Tester.verifyListContents(theTestList, new int[]{481, 485});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{481});

        assertFalse("Fehler: Element 898 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(898));
        theTestList.insertAt(1, 285);
        Tester.verifyListContents(theTestList, new int[]{481, 285});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{285});

        theTestList.append(133);
        Tester.verifyListContents(theTestList, new int[]{285, 133});

        theTestList.append(444);
        Tester.verifyListContents(theTestList, new int[]{285, 133, 444});

        assertTrue("Fehler: Element 133 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(133));
        assertFalse("Fehler: Element 393 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(393));
        assertFalse("Fehler: Element 777 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(777));
        theTestList.insertAt(3, 276);
        Tester.verifyListContents(theTestList, new int[]{285, 133, 444, 276});

        theTestList.append(213);
        Tester.verifyListContents(theTestList, new int[]{285, 133, 444, 276, 213});

        assertFalse("Fehler: Element 891 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(891));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{285, 133, 276, 213});

        assertFalse("Fehler: Element 764 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(764));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{285, 276, 213});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{276, 213});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{213});

// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 251);
        Tester.verifyListContents(theTestList, new int[]{251});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 745);
        Tester.verifyListContents(theTestList, new int[]{745});

        assertTrue("Fehler: Element 745 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(745));
        assertFalse("Fehler: Element 233 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(233));
        assertTrue("Fehler: Element 745 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(745));
        theTestList.append(562);
        Tester.verifyListContents(theTestList, new int[]{745, 562});

        theTestList.append(447);
        Tester.verifyListContents(theTestList, new int[]{745, 562, 447});

        theTestList.insertAt(2, 274);
        Tester.verifyListContents(theTestList, new int[]{745, 562, 274, 447});

        theTestList.append(620);
        Tester.verifyListContents(theTestList, new int[]{745, 562, 274, 447, 620});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{745, 562, 274, 447});

        theTestList.insertAt(2, 779);
        Tester.verifyListContents(theTestList, new int[]{745, 562, 779, 274, 447});

        assertTrue("Fehler: Element 447 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(447));
        theTestList.insertAt(2, 118);
        Tester.verifyListContents(theTestList, new int[]{745, 562, 118, 779, 274, 447});

        assertFalse("Fehler: Element 494 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(494));
    }

    public static void test10Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 903);
        Tester.verifyListContents(theTestList, new int[]{903});

// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 665);
        Tester.verifyListContents(theTestList, new int[]{665});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(605);
        Tester.verifyListContents(theTestList, new int[]{605});

        theTestList.insertAt(0, 446);
        Tester.verifyListContents(theTestList, new int[]{446, 605});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{605});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(900);
        Tester.verifyListContents(theTestList, new int[]{900});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 59);
        Tester.verifyListContents(theTestList, new int[]{59});

        assertTrue("Fehler: Element 59 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(59));
        theTestList.append(350);
        Tester.verifyListContents(theTestList, new int[]{59, 350});

        assertTrue("Fehler: Element 59 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(59));
        theTestList.append(180);
        Tester.verifyListContents(theTestList, new int[]{59, 350, 180});

        assertTrue("Fehler: Element 59 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(59));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{59, 350});

        theTestList.append(853);
        Tester.verifyListContents(theTestList, new int[]{59, 350, 853});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{350, 853});

        theTestList.append(311);
        Tester.verifyListContents(theTestList, new int[]{350, 853, 311});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{350, 853});

        theTestList.append(634);
        Tester.verifyListContents(theTestList, new int[]{350, 853, 634});

        theTestList.insertAt(2, 243);
        Tester.verifyListContents(theTestList, new int[]{350, 853, 243, 634});

        theTestList.append(740);
        Tester.verifyListContents(theTestList, new int[]{350, 853, 243, 634, 740});

        assertFalse("Fehler: Element 11 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(11));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{350, 243, 634, 740});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{350, 634, 740});

        assertTrue("Fehler: Element 634 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(634));
        assertTrue("Fehler: Element 634 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(634));
        theTestList.append(861);
        Tester.verifyListContents(theTestList, new int[]{350, 634, 740, 861});

        theTestList.append(828);
        Tester.verifyListContents(theTestList, new int[]{350, 634, 740, 861, 828});

        assertFalse("Fehler: Element 837 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(837));
        assertFalse("Fehler: Element 714 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(714));
        theTestList.insertAt(0, 664);
        Tester.verifyListContents(theTestList, new int[]{664, 350, 634, 740, 861, 828});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{664, 634, 740, 861, 828});

        assertTrue("Fehler: Element 861 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(861));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{664, 740, 861, 828});

        theTestList.append(519);
        Tester.verifyListContents(theTestList, new int[]{664, 740, 861, 828, 519});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{664, 740, 828, 519});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{740, 828, 519});

        theTestList.insertAt(0, 627);
        Tester.verifyListContents(theTestList, new int[]{627, 740, 828, 519});

        theTestList.insertAt(2, 518);
        Tester.verifyListContents(theTestList, new int[]{627, 740, 518, 828, 519});

        assertTrue("Fehler: Element 519 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(519));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{627, 740, 518, 828});

        theTestList.insertAt(0, 443);
        Tester.verifyListContents(theTestList, new int[]{443, 627, 740, 518, 828});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{443, 627, 518, 828});

        assertFalse("Fehler: Element 210 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(210));
        theTestList.insertAt(2, 280);
        Tester.verifyListContents(theTestList, new int[]{443, 627, 280, 518, 828});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(425);
        Tester.verifyListContents(theTestList, new int[]{425});

        theTestList.insertAt(1, 436);
        Tester.verifyListContents(theTestList, new int[]{425, 436});

        theTestList.append(637);
        Tester.verifyListContents(theTestList, new int[]{425, 436, 637});

        theTestList.insertAt(0, 410);
        Tester.verifyListContents(theTestList, new int[]{410, 425, 436, 637});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{425, 436, 637});

        theTestList.insertAt(1, 656);
        Tester.verifyListContents(theTestList, new int[]{425, 656, 436, 637});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 61);
        Tester.verifyListContents(theTestList, new int[]{61});

// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(502);
        Tester.verifyListContents(theTestList, new int[]{502});

        theTestList.insertAt(1, 94);
        Tester.verifyListContents(theTestList, new int[]{502, 94});

        theTestList.insertAt(0, 755);
        Tester.verifyListContents(theTestList, new int[]{755, 502, 94});

        theTestList.insertAt(2, 542);
        Tester.verifyListContents(theTestList, new int[]{755, 502, 542, 94});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{502, 542, 94});

        assertFalse("Fehler: Element 675 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(675));
        assertTrue("Fehler: Element 502 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(502));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{502, 94});

        theTestList.append(869);
        Tester.verifyListContents(theTestList, new int[]{502, 94, 869});

        theTestList.insertAt(1, 305);
        Tester.verifyListContents(theTestList, new int[]{502, 305, 94, 869});

        theTestList.append(970);
        Tester.verifyListContents(theTestList, new int[]{502, 305, 94, 869, 970});

        assertTrue("Fehler: Element 94 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(94));
        assertTrue("Fehler: Element 94 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(94));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{305, 94, 869, 970});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{94, 869, 970});

        assertTrue("Fehler: Element 970 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(970));
        theTestList.append(280);
        Tester.verifyListContents(theTestList, new int[]{94, 869, 970, 280});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{94, 869, 280});

        theTestList.append(498);
        Tester.verifyListContents(theTestList, new int[]{94, 869, 280, 498});

        theTestList.insertAt(0, 967);
        Tester.verifyListContents(theTestList, new int[]{967, 94, 869, 280, 498});

        theTestList.insertAt(1, 124);
        Tester.verifyListContents(theTestList, new int[]{967, 124, 94, 869, 280, 498});

        theTestList.insertAt(2, 761);
        Tester.verifyListContents(theTestList, new int[]{967, 124, 761, 94, 869, 280, 498});

        theTestList.append(157);
        Tester.verifyListContents(theTestList, new int[]{967, 124, 761, 94, 869, 280, 498, 157});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(105);
        Tester.verifyListContents(theTestList, new int[]{105});

        theTestList.append(59);
        Tester.verifyListContents(theTestList, new int[]{105, 59});

        assertFalse("Fehler: Element 412 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(412));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{59});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(719);
        Tester.verifyListContents(theTestList, new int[]{719});

        theTestList.insertAt(0, 427);
        Tester.verifyListContents(theTestList, new int[]{427, 719});

        theTestList.append(487);
        Tester.verifyListContents(theTestList, new int[]{427, 719, 487});

        assertTrue("Fehler: Element 427 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(427));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{427, 719});

        theTestList.insertAt(1, 87);
        Tester.verifyListContents(theTestList, new int[]{427, 87, 719});

        theTestList.append(249);
        Tester.verifyListContents(theTestList, new int[]{427, 87, 719, 249});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{427, 719, 249});

        theTestList.append(215);
        Tester.verifyListContents(theTestList, new int[]{427, 719, 249, 215});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{719, 249, 215});

    }
}
