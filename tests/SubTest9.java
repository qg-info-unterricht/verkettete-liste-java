package tests;

import static org.junit.Assert.*;

import liste.*;

/**
 * Die Test-Klasse SubTest9.
 *
 * @author Rainer Helfrich
 * @version 03.10.2020
 */
public class SubTest9 {


    public static void test1Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 21);
        Tester.verifyListContents(theTestList, new int[]{21});

        theTestList.append(515);
        Tester.verifyListContents(theTestList, new int[]{21, 515});

        theTestList.insertAt(2, 49);
        Tester.verifyListContents(theTestList, new int[]{21, 515, 49});

        assertFalse("Fehler: Element 949 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(949));
        assertTrue("Fehler: Element 21 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(21));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{21, 49});

        theTestList.append(476);
        Tester.verifyListContents(theTestList, new int[]{21, 49, 476});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{49, 476});

        theTestList.insertAt(0, 927);
        Tester.verifyListContents(theTestList, new int[]{927, 49, 476});

        theTestList.insertAt(1, 132);
        Tester.verifyListContents(theTestList, new int[]{927, 132, 49, 476});

        theTestList.insertAt(2, 854);
        Tester.verifyListContents(theTestList, new int[]{927, 132, 854, 49, 476});

        theTestList.insertAt(2, 624);
        Tester.verifyListContents(theTestList, new int[]{927, 132, 624, 854, 49, 476});

        theTestList.append(470);
        Tester.verifyListContents(theTestList, new int[]{927, 132, 624, 854, 49, 476, 470});

        theTestList.insertAt(1, 558);
        Tester.verifyListContents(theTestList, new int[]{927, 558, 132, 624, 854, 49, 476, 470});

        assertFalse("Fehler: Element 578 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(578));
        theTestList.insertAt(0, 213);
        Tester.verifyListContents(theTestList, new int[]{213, 927, 558, 132, 624, 854, 49, 476, 470});

        assertFalse("Fehler: Element 701 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(701));
        theTestList.append(257);
        Tester.verifyListContents(theTestList, new int[]{213, 927, 558, 132, 624, 854, 49, 476, 470, 257});

        theTestList.insertAt(4, 742);
        Tester.verifyListContents(theTestList, new int[]{213, 927, 558, 132, 742, 624, 854, 49, 476, 470, 257});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{213, 927, 558, 132, 742, 624, 854, 49, 476, 257});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 984);
        Tester.verifyListContents(theTestList, new int[]{984});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(720);
        Tester.verifyListContents(theTestList, new int[]{720});

        theTestList.append(854);
        Tester.verifyListContents(theTestList, new int[]{720, 854});

        theTestList.append(821);
        Tester.verifyListContents(theTestList, new int[]{720, 854, 821});

        theTestList.append(799);
        Tester.verifyListContents(theTestList, new int[]{720, 854, 821, 799});

        theTestList.insertAt(3, 240);
        Tester.verifyListContents(theTestList, new int[]{720, 854, 821, 240, 799});

        theTestList.insertAt(2, 490);
        Tester.verifyListContents(theTestList, new int[]{720, 854, 490, 821, 240, 799});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{720, 854, 490, 821, 799});

        theTestList.insertAt(0, 880);
        Tester.verifyListContents(theTestList, new int[]{880, 720, 854, 490, 821, 799});

        theTestList.append(975);
        Tester.verifyListContents(theTestList, new int[]{880, 720, 854, 490, 821, 799, 975});

        theTestList.append(458);
        Tester.verifyListContents(theTestList, new int[]{880, 720, 854, 490, 821, 799, 975, 458});

        assertTrue("Fehler: Element 854 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(854));
        assertTrue("Fehler: Element 799 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(799));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{880, 720, 490, 821, 799, 975, 458});

        theTestList.append(44);
        Tester.verifyListContents(theTestList, new int[]{880, 720, 490, 821, 799, 975, 458, 44});

        theTestList.insertAt(8, 748);
        Tester.verifyListContents(theTestList, new int[]{880, 720, 490, 821, 799, 975, 458, 44, 748});

        assertTrue("Fehler: Element 44 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(44));
        theTestList.insertAt(6, 942);
        Tester.verifyListContents(theTestList, new int[]{880, 720, 490, 821, 799, 975, 942, 458, 44, 748});

        theTestList.insertAt(2, 844);
        Tester.verifyListContents(theTestList, new int[]{880, 720, 844, 490, 821, 799, 975, 942, 458, 44, 748});

        theTestList.insertAt(4, 89);
        Tester.verifyListContents(theTestList, new int[]{880, 720, 844, 490, 89, 821, 799, 975, 942, 458, 44, 748});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{720, 844, 490, 89, 821, 799, 975, 942, 458, 44, 748});

        theTestList.append(907);
        Tester.verifyListContents(theTestList, new int[]{720, 844, 490, 89, 821, 799, 975, 942, 458, 44, 748, 907});

        theTestList.insertAt(4, 510);
        Tester.verifyListContents(theTestList, new int[]{720, 844, 490, 89, 510, 821, 799, 975, 942, 458, 44, 748, 907});

        theTestList.append(496);
        Tester.verifyListContents(theTestList, new int[]{720, 844, 490, 89, 510, 821, 799, 975, 942, 458, 44, 748, 907, 496});

        assertTrue("Fehler: Element 844 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(844));
        theTestList.insertAt(11, 780);
        Tester.verifyListContents(theTestList, new int[]{720, 844, 490, 89, 510, 821, 799, 975, 942, 458, 44, 780, 748, 907, 496});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{844, 490, 89, 510, 821, 799, 975, 942, 458, 44, 780, 748, 907, 496});

        assertTrue("Fehler: Element 496 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(496));
        theTestList.append(142);
        Tester.verifyListContents(theTestList, new int[]{844, 490, 89, 510, 821, 799, 975, 942, 458, 44, 780, 748, 907, 496, 142});

        theTestList.insertAt(5, 374);
        Tester.verifyListContents(theTestList, new int[]{844, 490, 89, 510, 821, 374, 799, 975, 942, 458, 44, 780, 748, 907, 496, 142});

        theTestList.insertAt(2, 887);
        Tester.verifyListContents(theTestList, new int[]{844, 490, 887, 89, 510, 821, 374, 799, 975, 942, 458, 44, 780, 748, 907, 496, 142});

        assertFalse("Fehler: Element 704 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(704));
        theTestList.insertAt(0, 534);
        Tester.verifyListContents(theTestList, new int[]{534, 844, 490, 887, 89, 510, 821, 374, 799, 975, 942, 458, 44, 780, 748, 907, 496, 142});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(746);
        Tester.verifyListContents(theTestList, new int[]{746});

        theTestList.append(941);
        Tester.verifyListContents(theTestList, new int[]{746, 941});

        theTestList.append(828);
        Tester.verifyListContents(theTestList, new int[]{746, 941, 828});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{941, 828});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{828});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 590);
        Tester.verifyListContents(theTestList, new int[]{590});

        theTestList.insertAt(1, 562);
        Tester.verifyListContents(theTestList, new int[]{590, 562});

        theTestList.append(895);
        Tester.verifyListContents(theTestList, new int[]{590, 562, 895});

        theTestList.append(165);
        Tester.verifyListContents(theTestList, new int[]{590, 562, 895, 165});

        theTestList.insertAt(4, 574);
        Tester.verifyListContents(theTestList, new int[]{590, 562, 895, 165, 574});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{590, 562, 165, 574});

        theTestList.insertAt(0, 368);
        Tester.verifyListContents(theTestList, new int[]{368, 590, 562, 165, 574});

        theTestList.insertAt(2, 190);
        Tester.verifyListContents(theTestList, new int[]{368, 590, 190, 562, 165, 574});

        assertTrue("Fehler: Element 368 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(368));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{368, 590, 562, 165, 574});

        theTestList.append(624);
        Tester.verifyListContents(theTestList, new int[]{368, 590, 562, 165, 574, 624});

        theTestList.insertAt(2, 494);
        Tester.verifyListContents(theTestList, new int[]{368, 590, 494, 562, 165, 574, 624});

        theTestList.append(330);
        Tester.verifyListContents(theTestList, new int[]{368, 590, 494, 562, 165, 574, 624, 330});

        theTestList.insertAt(2, 409);
        Tester.verifyListContents(theTestList, new int[]{368, 590, 409, 494, 562, 165, 574, 624, 330});

        assertFalse("Fehler: Element 18 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(18));
        assertTrue("Fehler: Element 574 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(574));
        theTestList.insertAt(8, 678);
        Tester.verifyListContents(theTestList, new int[]{368, 590, 409, 494, 562, 165, 574, 624, 678, 330});

        theTestList.append(979);
        Tester.verifyListContents(theTestList, new int[]{368, 590, 409, 494, 562, 165, 574, 624, 678, 330, 979});

        theTestList.append(19);
        Tester.verifyListContents(theTestList, new int[]{368, 590, 409, 494, 562, 165, 574, 624, 678, 330, 979, 19});

        assertFalse("Fehler: Element 380 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(380));
        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{368, 590, 409, 494, 562, 165, 574, 624, 678, 330, 979});

        theTestList.append(21);
        Tester.verifyListContents(theTestList, new int[]{368, 590, 409, 494, 562, 165, 574, 624, 678, 330, 979, 21});

        theTestList.append(499);
        Tester.verifyListContents(theTestList, new int[]{368, 590, 409, 494, 562, 165, 574, 624, 678, 330, 979, 21, 499});

        theTestList.append(387);
        Tester.verifyListContents(theTestList, new int[]{368, 590, 409, 494, 562, 165, 574, 624, 678, 330, 979, 21, 499, 387});

        theTestList.insertAt(1, 42);
        Tester.verifyListContents(theTestList, new int[]{368, 42, 590, 409, 494, 562, 165, 574, 624, 678, 330, 979, 21, 499, 387});

        assertTrue("Fehler: Element 330 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(330));
        theTestList.insertAt(13, 91);
        Tester.verifyListContents(theTestList, new int[]{368, 42, 590, 409, 494, 562, 165, 574, 624, 678, 330, 979, 21, 91, 499, 387});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{368, 42, 590, 409, 494, 165, 574, 624, 678, 330, 979, 21, 91, 499, 387});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{368, 42, 409, 494, 165, 574, 624, 678, 330, 979, 21, 91, 499, 387});

        assertFalse("Fehler: Element 210 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(210));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{368, 42, 409, 165, 574, 624, 678, 330, 979, 21, 91, 499, 387});

        theTestList.append(967);
        Tester.verifyListContents(theTestList, new int[]{368, 42, 409, 165, 574, 624, 678, 330, 979, 21, 91, 499, 387, 967});

        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{368, 42, 409, 165, 574, 624, 678, 330, 979, 21, 91, 387, 967});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(287);
        Tester.verifyListContents(theTestList, new int[]{287});

        theTestList.insertAt(0, 593);
        Tester.verifyListContents(theTestList, new int[]{593, 287});

        theTestList.append(196);
        Tester.verifyListContents(theTestList, new int[]{593, 287, 196});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{593, 196});

    }

    public static void test2Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(765);
        Tester.verifyListContents(theTestList, new int[]{765});

        assertFalse("Fehler: Element 335 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(335));
        theTestList.append(575);
        Tester.verifyListContents(theTestList, new int[]{765, 575});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{575});

        theTestList.insertAt(0, 273);
        Tester.verifyListContents(theTestList, new int[]{273, 575});

        theTestList.insertAt(2, 39);
        Tester.verifyListContents(theTestList, new int[]{273, 575, 39});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{273, 39});

        theTestList.append(989);
        Tester.verifyListContents(theTestList, new int[]{273, 39, 989});

        assertFalse("Fehler: Element 897 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(897));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{273, 989});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{989});

        theTestList.append(23);
        Tester.verifyListContents(theTestList, new int[]{989, 23});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{23});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 792);
        Tester.verifyListContents(theTestList, new int[]{792});

        theTestList.insertAt(1, 615);
        Tester.verifyListContents(theTestList, new int[]{792, 615});

        assertTrue("Fehler: Element 615 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(615));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{792});

        assertFalse("Fehler: Element 220 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(220));
        theTestList.insertAt(0, 803);
        Tester.verifyListContents(theTestList, new int[]{803, 792});

        theTestList.insertAt(2, 398);
        Tester.verifyListContents(theTestList, new int[]{803, 792, 398});

        assertFalse("Fehler: Element 986 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(986));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{803, 792});

        theTestList.insertAt(1, 42);
        Tester.verifyListContents(theTestList, new int[]{803, 42, 792});

        assertTrue("Fehler: Element 42 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(42));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{803, 792});

        theTestList.insertAt(2, 757);
        Tester.verifyListContents(theTestList, new int[]{803, 792, 757});

        theTestList.append(35);
        Tester.verifyListContents(theTestList, new int[]{803, 792, 757, 35});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{803, 757, 35});

        theTestList.append(213);
        Tester.verifyListContents(theTestList, new int[]{803, 757, 35, 213});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{803, 35, 213});

        theTestList.append(632);
        Tester.verifyListContents(theTestList, new int[]{803, 35, 213, 632});

        theTestList.insertAt(3, 559);
        Tester.verifyListContents(theTestList, new int[]{803, 35, 213, 559, 632});

        assertTrue("Fehler: Element 213 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(213));
        theTestList.append(141);
        Tester.verifyListContents(theTestList, new int[]{803, 35, 213, 559, 632, 141});

        theTestList.append(388);
        Tester.verifyListContents(theTestList, new int[]{803, 35, 213, 559, 632, 141, 388});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{803, 35, 213, 632, 141, 388});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{35, 213, 632, 141, 388});

        theTestList.append(961);
        Tester.verifyListContents(theTestList, new int[]{35, 213, 632, 141, 388, 961});

        assertTrue("Fehler: Element 35 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(35));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{35, 213, 632, 388, 961});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{35, 632, 388, 961});

        theTestList.append(349);
        Tester.verifyListContents(theTestList, new int[]{35, 632, 388, 961, 349});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{35, 632, 961, 349});

        theTestList.append(175);
        Tester.verifyListContents(theTestList, new int[]{35, 632, 961, 349, 175});

        theTestList.append(488);
        Tester.verifyListContents(theTestList, new int[]{35, 632, 961, 349, 175, 488});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{35, 632, 961, 175, 488});

        theTestList.append(466);
        Tester.verifyListContents(theTestList, new int[]{35, 632, 961, 175, 488, 466});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 175, 488, 466});

        theTestList.insertAt(2, 689);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 689, 175, 488, 466});

        theTestList.insertAt(2, 482);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 175, 488, 466});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 488, 466});

        theTestList.append(119);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 488, 466, 119});

        assertFalse("Fehler: Element 878 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(878));
        theTestList.append(715);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 488, 466, 119, 715});

        assertTrue("Fehler: Element 961 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(961));
        theTestList.append(375);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 488, 466, 119, 715, 375});

        theTestList.append(152);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 488, 466, 119, 715, 375, 152});

        assertTrue("Fehler: Element 961 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(961));
        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 488, 466, 119, 715, 152});

        theTestList.append(43);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 488, 466, 119, 715, 152, 43});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 488, 466, 715, 152, 43});

        theTestList.insertAt(6, 136);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 488, 466, 136, 715, 152, 43});

        theTestList.insertAt(6, 16);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 488, 466, 16, 136, 715, 152, 43});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 136, 715, 152, 43});

        theTestList.insertAt(6, 956);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 956, 136, 715, 152, 43});

        theTestList.insertAt(10, 160);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 956, 136, 715, 152, 160, 43});

        theTestList.insertAt(6, 482);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 482, 956, 136, 715, 152, 160, 43});

        theTestList.append(632);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 482, 956, 136, 715, 152, 160, 43, 632});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 482, 956, 715, 152, 160, 43, 632});

        theTestList.append(809);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 482, 956, 715, 152, 160, 43, 632, 809});

        theTestList.append(578);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 482, 956, 715, 152, 160, 43, 632, 809, 578});

        theTestList.append(604);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 482, 956, 715, 152, 160, 43, 632, 809, 578, 604});

        theTestList.append(726);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 482, 956, 715, 152, 160, 43, 632, 809, 578, 604, 726});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 482, 715, 152, 160, 43, 632, 809, 578, 604, 726});

        theTestList.append(273);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 482, 715, 152, 160, 43, 632, 809, 578, 604, 726, 273});

        theTestList.insertAt(14, 238);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 482, 715, 152, 160, 43, 632, 809, 578, 238, 604, 726, 273});

        theTestList.insertAt(18, 435);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 482, 715, 152, 160, 43, 632, 809, 578, 238, 604, 726, 273, 435});

        theTestList.removeAt(12);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 482, 715, 152, 160, 43, 632, 578, 238, 604, 726, 273, 435});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 715, 152, 160, 43, 632, 578, 238, 604, 726, 273, 435});

        theTestList.append(359);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 715, 152, 160, 43, 632, 578, 238, 604, 726, 273, 435, 359});

        assertFalse("Fehler: Element 521 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(521));
        theTestList.append(443);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 715, 152, 160, 43, 632, 578, 238, 604, 726, 273, 435, 359, 443});

        theTestList.insertAt(17, 423);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 715, 152, 160, 43, 632, 578, 238, 604, 726, 273, 435, 423, 359, 443});

        theTestList.removeAt(15);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 715, 152, 160, 43, 632, 578, 238, 604, 726, 435, 423, 359, 443});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 152, 160, 43, 632, 578, 238, 604, 726, 435, 423, 359, 443});

        assertTrue("Fehler: Element 632 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(632));
        theTestList.append(769);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 152, 160, 43, 632, 578, 238, 604, 726, 435, 423, 359, 443, 769});

        theTestList.append(482);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 152, 160, 43, 632, 578, 238, 604, 726, 435, 423, 359, 443, 769, 482});

        theTestList.insertAt(11, 239);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 152, 160, 43, 632, 578, 239, 238, 604, 726, 435, 423, 359, 443, 769, 482});

        assertTrue("Fehler: Element 769 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(769));
        theTestList.removeAt(16);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 152, 160, 43, 632, 578, 239, 238, 604, 726, 435, 359, 443, 769, 482});

        theTestList.insertAt(10, 504);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 152, 160, 43, 632, 504, 578, 239, 238, 604, 726, 435, 359, 443, 769, 482});

        theTestList.removeAt(16);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 152, 160, 43, 632, 504, 578, 239, 238, 604, 726, 359, 443, 769, 482});

        assertFalse("Fehler: Element 529 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(529));
        assertTrue("Fehler: Element 466 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(466));
        assertFalse("Fehler: Element 281 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(281));
        theTestList.insertAt(10, 525);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 152, 160, 43, 632, 525, 504, 578, 239, 238, 604, 726, 359, 443, 769, 482});

        theTestList.removeAt(20);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 152, 160, 43, 632, 525, 504, 578, 239, 238, 604, 726, 359, 443, 769});

        theTestList.insertAt(8, 301);
        Tester.verifyListContents(theTestList, new int[]{632, 961, 482, 689, 466, 16, 152, 160, 301, 43, 632, 525, 504, 578, 239, 238, 604, 726, 359, 443, 769});

    }

    public static void test3Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(397);
        Tester.verifyListContents(theTestList, new int[]{397});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 67);
        Tester.verifyListContents(theTestList, new int[]{67});

        assertTrue("Fehler: Element 67 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(67));
        assertFalse("Fehler: Element 748 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(748));
        theTestList.insertAt(0, 304);
        Tester.verifyListContents(theTestList, new int[]{304, 67});

        theTestList.insertAt(2, 69);
        Tester.verifyListContents(theTestList, new int[]{304, 67, 69});

        theTestList.append(577);
        Tester.verifyListContents(theTestList, new int[]{304, 67, 69, 577});

        theTestList.append(436);
        Tester.verifyListContents(theTestList, new int[]{304, 67, 69, 577, 436});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{304, 69, 577, 436});

        theTestList.append(260);
        Tester.verifyListContents(theTestList, new int[]{304, 69, 577, 436, 260});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{304, 69, 436, 260});

        theTestList.append(541);
        Tester.verifyListContents(theTestList, new int[]{304, 69, 436, 260, 541});

        theTestList.append(943);
        Tester.verifyListContents(theTestList, new int[]{304, 69, 436, 260, 541, 943});

        assertTrue("Fehler: Element 943 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(943));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{304, 69, 436, 541, 943});

        theTestList.append(372);
        Tester.verifyListContents(theTestList, new int[]{304, 69, 436, 541, 943, 372});

        theTestList.append(846);
        Tester.verifyListContents(theTestList, new int[]{304, 69, 436, 541, 943, 372, 846});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{304, 69, 436, 943, 372, 846});

        theTestList.append(689);
        Tester.verifyListContents(theTestList, new int[]{304, 69, 436, 943, 372, 846, 689});

        theTestList.insertAt(4, 57);
        Tester.verifyListContents(theTestList, new int[]{304, 69, 436, 943, 57, 372, 846, 689});

        theTestList.insertAt(0, 262);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 69, 436, 943, 57, 372, 846, 689});

        assertTrue("Fehler: Element 943 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(943));
        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 69, 436, 943, 57, 372, 689});

        theTestList.append(129);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 69, 436, 943, 57, 372, 689, 129});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 69, 436, 943, 57, 372, 689});

        theTestList.append(437);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 69, 436, 943, 57, 372, 689, 437});

        theTestList.append(952);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 69, 436, 943, 57, 372, 689, 437, 952});

        theTestList.append(807);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 69, 436, 943, 57, 372, 689, 437, 952, 807});

        theTestList.insertAt(7, 72);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 69, 436, 943, 57, 372, 72, 689, 437, 952, 807});

        assertFalse("Fehler: Element 886 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(886));
        assertFalse("Fehler: Element 772 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(772));
        assertTrue("Fehler: Element 72 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(72));
        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 69, 436, 943, 57, 372, 72, 689, 437, 952});

        theTestList.append(709);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 69, 436, 943, 57, 372, 72, 689, 437, 952, 709});

        assertTrue("Fehler: Element 262 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(262));
        theTestList.insertAt(10, 821);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 69, 436, 943, 57, 372, 72, 689, 437, 821, 952, 709});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 69, 436, 943, 372, 72, 689, 437, 821, 952, 709});

        theTestList.append(898);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 69, 436, 943, 372, 72, 689, 437, 821, 952, 709, 898});

        assertTrue("Fehler: Element 72 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(72));
        theTestList.insertAt(5, 139);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 69, 436, 943, 139, 372, 72, 689, 437, 821, 952, 709, 898});

        theTestList.insertAt(2, 886);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 886, 69, 436, 943, 139, 372, 72, 689, 437, 821, 952, 709, 898});

        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 886, 69, 436, 943, 139, 372, 72, 689, 821, 952, 709, 898});

        theTestList.insertAt(9, 836);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 886, 69, 436, 943, 139, 372, 72, 836, 689, 821, 952, 709, 898});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 886, 436, 943, 139, 372, 72, 836, 689, 821, 952, 709, 898});

        theTestList.removeAt(13);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 886, 436, 943, 139, 372, 72, 836, 689, 821, 952, 709});

        assertTrue("Fehler: Element 943 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(943));
        assertFalse("Fehler: Element 237 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(237));
        theTestList.insertAt(13, 803);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 886, 436, 943, 139, 372, 72, 836, 689, 821, 952, 709, 803});

        theTestList.append(918);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 886, 436, 943, 139, 372, 72, 836, 689, 821, 952, 709, 803, 918});

        assertFalse("Fehler: Element 799 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(799));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 436, 943, 139, 372, 72, 836, 689, 821, 952, 709, 803, 918});

        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 436, 943, 139, 372, 72, 836, 689, 821, 709, 803, 918});

        assertTrue("Fehler: Element 918 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(918));
        assertFalse("Fehler: Element 652 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(652));
        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 436, 943, 139, 372, 72, 836, 689, 709, 803, 918});

        theTestList.append(578);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 436, 943, 139, 372, 72, 836, 689, 709, 803, 918, 578});

        theTestList.append(954);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 436, 943, 139, 372, 72, 836, 689, 709, 803, 918, 578, 954});

        theTestList.insertAt(10, 961);
        Tester.verifyListContents(theTestList, new int[]{262, 304, 436, 943, 139, 372, 72, 836, 689, 709, 961, 803, 918, 578, 954});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{304, 436, 943, 139, 372, 72, 836, 689, 709, 961, 803, 918, 578, 954});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{304, 436, 943, 139, 372, 72, 836, 689, 709, 803, 918, 578, 954});

        theTestList.append(178);
        Tester.verifyListContents(theTestList, new int[]{304, 436, 943, 139, 372, 72, 836, 689, 709, 803, 918, 578, 954, 178});

        theTestList.append(733);
        Tester.verifyListContents(theTestList, new int[]{304, 436, 943, 139, 372, 72, 836, 689, 709, 803, 918, 578, 954, 178, 733});

        assertTrue("Fehler: Element 803 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(803));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{304, 436, 943, 372, 72, 836, 689, 709, 803, 918, 578, 954, 178, 733});

        assertFalse("Fehler: Element 325 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(325));
        assertFalse("Fehler: Element 50 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(50));
        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{304, 436, 943, 372, 72, 836, 689, 709, 803, 918, 954, 178, 733});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 467);
        Tester.verifyListContents(theTestList, new int[]{467});

        theTestList.insertAt(1, 927);
        Tester.verifyListContents(theTestList, new int[]{467, 927});

        theTestList.insertAt(0, 242);
        Tester.verifyListContents(theTestList, new int[]{242, 467, 927});

        theTestList.append(474);
        Tester.verifyListContents(theTestList, new int[]{242, 467, 927, 474});

        assertFalse("Fehler: Element 72 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(72));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{242, 467, 927});

        assertFalse("Fehler: Element 342 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(342));
        assertFalse("Fehler: Element 210 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(210));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{242, 927});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{242});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 111);
        Tester.verifyListContents(theTestList, new int[]{111});

        assertFalse("Fehler: Element 34 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(34));
        theTestList.insertAt(1, 591);
        Tester.verifyListContents(theTestList, new int[]{111, 591});

        theTestList.insertAt(1, 308);
        Tester.verifyListContents(theTestList, new int[]{111, 308, 591});

        theTestList.insertAt(1, 819);
        Tester.verifyListContents(theTestList, new int[]{111, 819, 308, 591});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{111, 819, 308});

        assertTrue("Fehler: Element 819 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(819));
        theTestList.insertAt(1, 225);
        Tester.verifyListContents(theTestList, new int[]{111, 225, 819, 308});

        assertTrue("Fehler: Element 111 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(111));
        theTestList.insertAt(1, 680);
        Tester.verifyListContents(theTestList, new int[]{111, 680, 225, 819, 308});

        assertTrue("Fehler: Element 680 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(680));
        theTestList.insertAt(1, 686);
        Tester.verifyListContents(theTestList, new int[]{111, 686, 680, 225, 819, 308});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{111, 680, 225, 819, 308});

        theTestList.insertAt(4, 816);
        Tester.verifyListContents(theTestList, new int[]{111, 680, 225, 819, 816, 308});

        theTestList.append(124);
        Tester.verifyListContents(theTestList, new int[]{111, 680, 225, 819, 816, 308, 124});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{680, 225, 819, 816, 308, 124});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{680, 819, 816, 308, 124});

        theTestList.insertAt(4, 19);
        Tester.verifyListContents(theTestList, new int[]{680, 819, 816, 308, 19, 124});

        theTestList.insertAt(4, 569);
        Tester.verifyListContents(theTestList, new int[]{680, 819, 816, 308, 569, 19, 124});

    }

    public static void test4Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 298);
        Tester.verifyListContents(theTestList, new int[]{298});

// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(735);
        Tester.verifyListContents(theTestList, new int[]{735});

        theTestList.insertAt(0, 474);
        Tester.verifyListContents(theTestList, new int[]{474, 735});

        theTestList.insertAt(0, 562);
        Tester.verifyListContents(theTestList, new int[]{562, 474, 735});

        theTestList.append(201);
        Tester.verifyListContents(theTestList, new int[]{562, 474, 735, 201});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{562, 474, 735});

        theTestList.insertAt(0, 103);
        Tester.verifyListContents(theTestList, new int[]{103, 562, 474, 735});

        theTestList.append(54);
        Tester.verifyListContents(theTestList, new int[]{103, 562, 474, 735, 54});

        assertTrue("Fehler: Element 735 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(735));
        assertTrue("Fehler: Element 474 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(474));
        theTestList.append(940);
        Tester.verifyListContents(theTestList, new int[]{103, 562, 474, 735, 54, 940});

        assertTrue("Fehler: Element 562 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(562));
        theTestList.insertAt(5, 631);
        Tester.verifyListContents(theTestList, new int[]{103, 562, 474, 735, 54, 631, 940});

        assertFalse("Fehler: Element 250 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(250));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{103, 562, 735, 54, 631, 940});

        theTestList.insertAt(6, 30);
        Tester.verifyListContents(theTestList, new int[]{103, 562, 735, 54, 631, 940, 30});

        theTestList.append(966);
        Tester.verifyListContents(theTestList, new int[]{103, 562, 735, 54, 631, 940, 30, 966});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{103, 562, 735, 54, 631, 940, 966});

        theTestList.append(931);
        Tester.verifyListContents(theTestList, new int[]{103, 562, 735, 54, 631, 940, 966, 931});

        theTestList.append(748);
        Tester.verifyListContents(theTestList, new int[]{103, 562, 735, 54, 631, 940, 966, 931, 748});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{103, 562, 735, 54, 940, 966, 931, 748});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{562, 735, 54, 940, 966, 931, 748});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{562, 54, 940, 966, 931, 748});

        theTestList.insertAt(5, 214);
        Tester.verifyListContents(theTestList, new int[]{562, 54, 940, 966, 931, 214, 748});

        theTestList.insertAt(2, 28);
        Tester.verifyListContents(theTestList, new int[]{562, 54, 28, 940, 966, 931, 214, 748});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{562, 54, 28, 940, 966, 214, 748});

        theTestList.append(397);
        Tester.verifyListContents(theTestList, new int[]{562, 54, 28, 940, 966, 214, 748, 397});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(413);
        Tester.verifyListContents(theTestList, new int[]{413});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(950);
        Tester.verifyListContents(theTestList, new int[]{950});

        theTestList.insertAt(0, 480);
        Tester.verifyListContents(theTestList, new int[]{480, 950});

        theTestList.append(962);
        Tester.verifyListContents(theTestList, new int[]{480, 950, 962});

        assertFalse("Fehler: Element 287 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(287));
        assertTrue("Fehler: Element 962 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(962));
        theTestList.append(581);
        Tester.verifyListContents(theTestList, new int[]{480, 950, 962, 581});

        theTestList.insertAt(2, 163);
        Tester.verifyListContents(theTestList, new int[]{480, 950, 163, 962, 581});

        theTestList.insertAt(1, 678);
        Tester.verifyListContents(theTestList, new int[]{480, 678, 950, 163, 962, 581});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 267);
        Tester.verifyListContents(theTestList, new int[]{267});

        theTestList.insertAt(1, 595);
        Tester.verifyListContents(theTestList, new int[]{267, 595});

        assertTrue("Fehler: Element 595 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(595));
        theTestList.append(812);
        Tester.verifyListContents(theTestList, new int[]{267, 595, 812});

        assertTrue("Fehler: Element 267 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(267));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{267, 812});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{267});

        assertFalse("Fehler: Element 82 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(82));
        assertFalse("Fehler: Element 256 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(256));
        theTestList.insertAt(0, 968);
        Tester.verifyListContents(theTestList, new int[]{968, 267});

        assertTrue("Fehler: Element 968 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(968));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 715);
        Tester.verifyListContents(theTestList, new int[]{715});

        theTestList.insertAt(0, 56);
        Tester.verifyListContents(theTestList, new int[]{56, 715});

        theTestList.insertAt(0, 332);
        Tester.verifyListContents(theTestList, new int[]{332, 56, 715});

        theTestList.append(757);
        Tester.verifyListContents(theTestList, new int[]{332, 56, 715, 757});

        theTestList.append(844);
        Tester.verifyListContents(theTestList, new int[]{332, 56, 715, 757, 844});

        theTestList.insertAt(1, 771);
        Tester.verifyListContents(theTestList, new int[]{332, 771, 56, 715, 757, 844});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{332, 56, 715, 757, 844});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{332, 56, 757, 844});

        assertTrue("Fehler: Element 332 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(332));
        assertFalse("Fehler: Element 905 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(905));
        theTestList.insertAt(4, 175);
        Tester.verifyListContents(theTestList, new int[]{332, 56, 757, 844, 175});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{332, 56, 757, 175});

        theTestList.insertAt(2, 774);
        Tester.verifyListContents(theTestList, new int[]{332, 56, 774, 757, 175});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{332, 56, 757, 175});

        assertTrue("Fehler: Element 56 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(56));
        theTestList.append(711);
        Tester.verifyListContents(theTestList, new int[]{332, 56, 757, 175, 711});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{332, 56, 757, 711});

        theTestList.append(980);
        Tester.verifyListContents(theTestList, new int[]{332, 56, 757, 711, 980});

        theTestList.insertAt(1, 238);
        Tester.verifyListContents(theTestList, new int[]{332, 238, 56, 757, 711, 980});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{332, 238, 757, 711, 980});

        theTestList.append(778);
        Tester.verifyListContents(theTestList, new int[]{332, 238, 757, 711, 980, 778});

        assertTrue("Fehler: Element 980 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(980));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{332, 238, 757, 711, 778});

        theTestList.append(241);
        Tester.verifyListContents(theTestList, new int[]{332, 238, 757, 711, 778, 241});

        theTestList.insertAt(4, 142);
        Tester.verifyListContents(theTestList, new int[]{332, 238, 757, 711, 142, 778, 241});

        assertFalse("Fehler: Element 727 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(727));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{332, 238, 711, 142, 778, 241});

        theTestList.append(689);
        Tester.verifyListContents(theTestList, new int[]{332, 238, 711, 142, 778, 241, 689});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{332, 238, 711, 778, 241, 689});

        assertTrue("Fehler: Element 238 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(238));
        theTestList.insertAt(2, 843);
        Tester.verifyListContents(theTestList, new int[]{332, 238, 843, 711, 778, 241, 689});

        theTestList.append(267);
        Tester.verifyListContents(theTestList, new int[]{332, 238, 843, 711, 778, 241, 689, 267});

        assertTrue("Fehler: Element 241 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(241));
        assertFalse("Fehler: Element 412 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(412));
        assertFalse("Fehler: Element 925 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(925));
        theTestList.append(316);
        Tester.verifyListContents(theTestList, new int[]{332, 238, 843, 711, 778, 241, 689, 267, 316});

        assertTrue("Fehler: Element 241 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(241));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{238, 843, 711, 778, 241, 689, 267, 316});

        assertFalse("Fehler: Element 460 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(460));
        assertFalse("Fehler: Element 950 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(950));
        assertTrue("Fehler: Element 241 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(241));
        theTestList.insertAt(5, 368);
        Tester.verifyListContents(theTestList, new int[]{238, 843, 711, 778, 241, 368, 689, 267, 316});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{238, 843, 711, 241, 368, 689, 267, 316});

        assertFalse("Fehler: Element 335 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(335));
        theTestList.insertAt(2, 902);
        Tester.verifyListContents(theTestList, new int[]{238, 843, 902, 711, 241, 368, 689, 267, 316});

        theTestList.append(905);
        Tester.verifyListContents(theTestList, new int[]{238, 843, 902, 711, 241, 368, 689, 267, 316, 905});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 321);
        Tester.verifyListContents(theTestList, new int[]{321});

    }

    public static void test5Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(194);
        Tester.verifyListContents(theTestList, new int[]{194});

        theTestList.insertAt(0, 694);
        Tester.verifyListContents(theTestList, new int[]{694, 194});

        assertFalse("Fehler: Element 927 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(927));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{194});

        assertFalse("Fehler: Element 555 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(555));
// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(411);
        Tester.verifyListContents(theTestList, new int[]{411});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 598);
        Tester.verifyListContents(theTestList, new int[]{598});

        assertFalse("Fehler: Element 360 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(360));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 413);
        Tester.verifyListContents(theTestList, new int[]{413});

        assertFalse("Fehler: Element 340 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(340));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 163);
        Tester.verifyListContents(theTestList, new int[]{163});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 766);
        Tester.verifyListContents(theTestList, new int[]{766});

        assertTrue("Fehler: Element 766 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(766));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 496);
        Tester.verifyListContents(theTestList, new int[]{496});

        theTestList.append(347);
        Tester.verifyListContents(theTestList, new int[]{496, 347});

        assertFalse("Fehler: Element 196 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(196));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{347});

        theTestList.insertAt(1, 247);
        Tester.verifyListContents(theTestList, new int[]{347, 247});

        assertTrue("Fehler: Element 347 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(347));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{347});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(316);
        Tester.verifyListContents(theTestList, new int[]{316});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(210);
        Tester.verifyListContents(theTestList, new int[]{210});

        assertFalse("Fehler: Element 326 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(326));
        theTestList.insertAt(1, 349);
        Tester.verifyListContents(theTestList, new int[]{210, 349});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{210});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(277);
        Tester.verifyListContents(theTestList, new int[]{277});

        assertTrue("Fehler: Element 277 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(277));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(594);
        Tester.verifyListContents(theTestList, new int[]{594});

        theTestList.append(154);
        Tester.verifyListContents(theTestList, new int[]{594, 154});

        theTestList.insertAt(1, 586);
        Tester.verifyListContents(theTestList, new int[]{594, 586, 154});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{594, 154});

        theTestList.insertAt(1, 390);
        Tester.verifyListContents(theTestList, new int[]{594, 390, 154});

        assertFalse("Fehler: Element 637 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(637));
        theTestList.insertAt(0, 179);
        Tester.verifyListContents(theTestList, new int[]{179, 594, 390, 154});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{179, 594, 154});

        theTestList.append(564);
        Tester.verifyListContents(theTestList, new int[]{179, 594, 154, 564});

        theTestList.insertAt(4, 249);
        Tester.verifyListContents(theTestList, new int[]{179, 594, 154, 564, 249});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{179, 594, 154, 564});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{594, 154, 564});

        theTestList.insertAt(2, 9);
        Tester.verifyListContents(theTestList, new int[]{594, 154, 9, 564});

        theTestList.append(772);
        Tester.verifyListContents(theTestList, new int[]{594, 154, 9, 564, 772});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{594, 154, 9, 564});

        theTestList.append(601);
        Tester.verifyListContents(theTestList, new int[]{594, 154, 9, 564, 601});

        assertFalse("Fehler: Element 178 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(178));
        assertTrue("Fehler: Element 594 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(594));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{154, 9, 564, 601});

        theTestList.append(438);
        Tester.verifyListContents(theTestList, new int[]{154, 9, 564, 601, 438});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{154, 9, 564, 438});

        theTestList.append(116);
        Tester.verifyListContents(theTestList, new int[]{154, 9, 564, 438, 116});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{9, 564, 438, 116});

        theTestList.append(732);
        Tester.verifyListContents(theTestList, new int[]{9, 564, 438, 116, 732});

        theTestList.append(748);
        Tester.verifyListContents(theTestList, new int[]{9, 564, 438, 116, 732, 748});

        theTestList.append(412);
        Tester.verifyListContents(theTestList, new int[]{9, 564, 438, 116, 732, 748, 412});

        assertTrue("Fehler: Element 748 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(748));
        theTestList.insertAt(5, 736);
        Tester.verifyListContents(theTestList, new int[]{9, 564, 438, 116, 732, 736, 748, 412});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{9, 438, 116, 732, 736, 748, 412});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 732, 736, 748, 412});

        assertFalse("Fehler: Element 445 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(445));
        assertFalse("Fehler: Element 52 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(52));
        theTestList.append(394);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 732, 736, 748, 412, 394});

        theTestList.insertAt(5, 352);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 732, 736, 748, 352, 412, 394});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 732, 736, 748, 352, 394});

        theTestList.append(421);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 732, 736, 748, 352, 394, 421});

        assertFalse("Fehler: Element 179 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(179));
        assertTrue("Fehler: Element 421 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(421));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 748, 352, 394, 421});

        assertFalse("Fehler: Element 629 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(629));
        theTestList.append(377);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 748, 352, 394, 421, 377});

        theTestList.append(282);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 748, 352, 394, 421, 377, 282});

        theTestList.append(735);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 748, 352, 394, 421, 377, 282, 735});

        assertTrue("Fehler: Element 9 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(9));
        theTestList.insertAt(3, 170);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 170, 748, 352, 394, 421, 377, 282, 735});

        assertTrue("Fehler: Element 116 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(116));
        theTestList.append(737);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 170, 748, 352, 394, 421, 377, 282, 735, 737});

        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 170, 748, 352, 394, 421, 377, 282, 737});

        theTestList.append(78);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 170, 748, 352, 394, 421, 377, 282, 737, 78});

        theTestList.append(71);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 170, 748, 352, 394, 421, 377, 282, 737, 78, 71});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 170, 748, 352, 421, 377, 282, 737, 78, 71});

        theTestList.insertAt(6, 369);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 170, 748, 352, 369, 421, 377, 282, 737, 78, 71});

        theTestList.insertAt(10, 758);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 170, 748, 352, 369, 421, 377, 282, 758, 737, 78, 71});

        theTestList.append(364);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 170, 748, 352, 369, 421, 377, 282, 758, 737, 78, 71, 364});

        assertFalse("Fehler: Element 275 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(275));
        theTestList.insertAt(3, 727);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 727, 170, 748, 352, 369, 421, 377, 282, 758, 737, 78, 71, 364});

        theTestList.append(958);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 727, 170, 748, 352, 369, 421, 377, 282, 758, 737, 78, 71, 364, 958});

        theTestList.insertAt(16, 555);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 727, 170, 748, 352, 369, 421, 377, 282, 758, 737, 78, 71, 364, 555, 958});

        theTestList.append(147);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 727, 170, 748, 352, 369, 421, 377, 282, 758, 737, 78, 71, 364, 555, 958, 147});

        theTestList.append(382);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 727, 170, 748, 352, 369, 421, 377, 282, 758, 737, 78, 71, 364, 555, 958, 147, 382});

        theTestList.removeAt(17);
        Tester.verifyListContents(theTestList, new int[]{9, 116, 736, 727, 170, 748, 352, 369, 421, 377, 282, 758, 737, 78, 71, 364, 555, 147, 382});

    }

    public static void test6Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(981);
        Tester.verifyListContents(theTestList, new int[]{981});

        assertFalse("Fehler: Element 802 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(802));
        theTestList.insertAt(0, 672);
        Tester.verifyListContents(theTestList, new int[]{672, 981});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{981});

        theTestList.append(633);
        Tester.verifyListContents(theTestList, new int[]{981, 633});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{981});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(26);
        Tester.verifyListContents(theTestList, new int[]{26});

        theTestList.insertAt(1, 55);
        Tester.verifyListContents(theTestList, new int[]{26, 55});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 184);
        Tester.verifyListContents(theTestList, new int[]{184});

        theTestList.insertAt(0, 717);
        Tester.verifyListContents(theTestList, new int[]{717, 184});

        theTestList.insertAt(2, 994);
        Tester.verifyListContents(theTestList, new int[]{717, 184, 994});

        assertFalse("Fehler: Element 700 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(700));
        assertFalse("Fehler: Element 574 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(574));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{717, 184});

        theTestList.insertAt(2, 641);
        Tester.verifyListContents(theTestList, new int[]{717, 184, 641});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{717, 184});

        theTestList.append(97);
        Tester.verifyListContents(theTestList, new int[]{717, 184, 97});

        theTestList.append(166);
        Tester.verifyListContents(theTestList, new int[]{717, 184, 97, 166});

        assertFalse("Fehler: Element 178 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(178));
        theTestList.insertAt(1, 362);
        Tester.verifyListContents(theTestList, new int[]{717, 362, 184, 97, 166});

        assertFalse("Fehler: Element 765 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(765));
        theTestList.insertAt(1, 106);
        Tester.verifyListContents(theTestList, new int[]{717, 106, 362, 184, 97, 166});

        theTestList.insertAt(6, 495);
        Tester.verifyListContents(theTestList, new int[]{717, 106, 362, 184, 97, 166, 495});

        theTestList.insertAt(4, 360);
        Tester.verifyListContents(theTestList, new int[]{717, 106, 362, 184, 360, 97, 166, 495});

        theTestList.insertAt(4, 440);
        Tester.verifyListContents(theTestList, new int[]{717, 106, 362, 184, 440, 360, 97, 166, 495});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{717, 362, 184, 440, 360, 97, 166, 495});

        assertFalse("Fehler: Element 195 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(195));
        theTestList.insertAt(2, 652);
        Tester.verifyListContents(theTestList, new int[]{717, 362, 652, 184, 440, 360, 97, 166, 495});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{717, 362, 184, 440, 360, 97, 166, 495});

        theTestList.insertAt(1, 824);
        Tester.verifyListContents(theTestList, new int[]{717, 824, 362, 184, 440, 360, 97, 166, 495});

        assertTrue("Fehler: Element 360 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(360));
        assertFalse("Fehler: Element 315 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(315));
        assertFalse("Fehler: Element 496 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(496));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{717, 824, 362, 440, 360, 97, 166, 495});

        theTestList.append(717);
        Tester.verifyListContents(theTestList, new int[]{717, 824, 362, 440, 360, 97, 166, 495, 717});

        theTestList.append(805);
        Tester.verifyListContents(theTestList, new int[]{717, 824, 362, 440, 360, 97, 166, 495, 717, 805});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{717, 824, 362, 360, 97, 166, 495, 717, 805});

        theTestList.append(968);
        Tester.verifyListContents(theTestList, new int[]{717, 824, 362, 360, 97, 166, 495, 717, 805, 968});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{717, 824, 362, 360, 97, 166, 495, 717, 968});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{717, 824, 362, 360, 97, 495, 717, 968});

        theTestList.insertAt(6, 723);
        Tester.verifyListContents(theTestList, new int[]{717, 824, 362, 360, 97, 495, 723, 717, 968});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{717, 824, 360, 97, 495, 723, 717, 968});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{717, 824, 360, 97, 495, 723, 968});

        theTestList.insertAt(2, 46);
        Tester.verifyListContents(theTestList, new int[]{717, 824, 46, 360, 97, 495, 723, 968});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{717, 824, 46, 360, 97, 723, 968});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{717, 824, 360, 97, 723, 968});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{717, 824, 97, 723, 968});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{717, 97, 723, 968});

        assertTrue("Fehler: Element 723 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(723));
        assertTrue("Fehler: Element 717 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(717));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{97, 723, 968});

        assertTrue("Fehler: Element 97 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(97));
        theTestList.insertAt(2, 236);
        Tester.verifyListContents(theTestList, new int[]{97, 723, 236, 968});

        theTestList.insertAt(0, 103);
        Tester.verifyListContents(theTestList, new int[]{103, 97, 723, 236, 968});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{103, 97, 723, 236});

        theTestList.append(990);
        Tester.verifyListContents(theTestList, new int[]{103, 97, 723, 236, 990});

        theTestList.insertAt(1, 574);
        Tester.verifyListContents(theTestList, new int[]{103, 574, 97, 723, 236, 990});

        theTestList.insertAt(6, 626);
        Tester.verifyListContents(theTestList, new int[]{103, 574, 97, 723, 236, 990, 626});

        theTestList.insertAt(6, 19);
        Tester.verifyListContents(theTestList, new int[]{103, 574, 97, 723, 236, 990, 19, 626});

        assertTrue("Fehler: Element 626 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(626));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(116);
        Tester.verifyListContents(theTestList, new int[]{116});

        assertFalse("Fehler: Element 306 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(306));
        theTestList.insertAt(1, 512);
        Tester.verifyListContents(theTestList, new int[]{116, 512});

        theTestList.insertAt(0, 830);
        Tester.verifyListContents(theTestList, new int[]{830, 116, 512});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{830, 512});

        theTestList.append(455);
        Tester.verifyListContents(theTestList, new int[]{830, 512, 455});

        theTestList.insertAt(3, 682);
        Tester.verifyListContents(theTestList, new int[]{830, 512, 455, 682});

        assertFalse("Fehler: Element 602 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(602));
        theTestList.insertAt(1, 931);
        Tester.verifyListContents(theTestList, new int[]{830, 931, 512, 455, 682});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{830, 931, 512, 682});

        theTestList.insertAt(1, 566);
        Tester.verifyListContents(theTestList, new int[]{830, 566, 931, 512, 682});

        theTestList.append(349);
        Tester.verifyListContents(theTestList, new int[]{830, 566, 931, 512, 682, 349});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{566, 931, 512, 682, 349});

        theTestList.insertAt(3, 388);
        Tester.verifyListContents(theTestList, new int[]{566, 931, 512, 388, 682, 349});

        theTestList.insertAt(0, 618);
        Tester.verifyListContents(theTestList, new int[]{618, 566, 931, 512, 388, 682, 349});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{618, 566, 931, 512, 388, 682});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 104);
        Tester.verifyListContents(theTestList, new int[]{104});

        theTestList.append(189);
        Tester.verifyListContents(theTestList, new int[]{104, 189});

        theTestList.append(922);
        Tester.verifyListContents(theTestList, new int[]{104, 189, 922});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{104, 922});

        assertFalse("Fehler: Element 490 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(490));
        assertFalse("Fehler: Element 507 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(507));
        theTestList.insertAt(1, 910);
        Tester.verifyListContents(theTestList, new int[]{104, 910, 922});

        theTestList.insertAt(2, 662);
        Tester.verifyListContents(theTestList, new int[]{104, 910, 662, 922});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{104, 910, 662});

        theTestList.insertAt(1, 820);
        Tester.verifyListContents(theTestList, new int[]{104, 820, 910, 662});

        assertTrue("Fehler: Element 910 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(910));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{104, 910, 662});

        theTestList.append(358);
        Tester.verifyListContents(theTestList, new int[]{104, 910, 662, 358});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{104, 910, 662});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{104, 662});

        assertFalse("Fehler: Element 846 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(846));
        assertFalse("Fehler: Element 77 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(77));
        theTestList.append(473);
        Tester.verifyListContents(theTestList, new int[]{104, 662, 473});

        theTestList.append(802);
        Tester.verifyListContents(theTestList, new int[]{104, 662, 473, 802});

    }

    public static void test7Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 278);
        Tester.verifyListContents(theTestList, new int[]{278});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(664);
        Tester.verifyListContents(theTestList, new int[]{664});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

// Alles löschen
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 274);
        Tester.verifyListContents(theTestList, new int[]{274});

        theTestList.insertAt(1, 839);
        Tester.verifyListContents(theTestList, new int[]{274, 839});

        theTestList.insertAt(1, 565);
        Tester.verifyListContents(theTestList, new int[]{274, 565, 839});

        theTestList.insertAt(0, 136);
        Tester.verifyListContents(theTestList, new int[]{136, 274, 565, 839});

        theTestList.insertAt(1, 93);
        Tester.verifyListContents(theTestList, new int[]{136, 93, 274, 565, 839});

        theTestList.insertAt(3, 208);
        Tester.verifyListContents(theTestList, new int[]{136, 93, 274, 208, 565, 839});

        theTestList.insertAt(1, 320);
        Tester.verifyListContents(theTestList, new int[]{136, 320, 93, 274, 208, 565, 839});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{136, 320, 93, 274, 208, 565});

        assertTrue("Fehler: Element 320 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(320));
        assertTrue("Fehler: Element 320 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(320));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{136, 93, 274, 208, 565});

        theTestList.insertAt(1, 976);
        Tester.verifyListContents(theTestList, new int[]{136, 976, 93, 274, 208, 565});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(134);
        Tester.verifyListContents(theTestList, new int[]{134});

        theTestList.insertAt(0, 851);
        Tester.verifyListContents(theTestList, new int[]{851, 134});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{134});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(502);
        Tester.verifyListContents(theTestList, new int[]{502});

        theTestList.append(763);
        Tester.verifyListContents(theTestList, new int[]{502, 763});

        theTestList.insertAt(2, 154);
        Tester.verifyListContents(theTestList, new int[]{502, 763, 154});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{502, 154});

        theTestList.append(38);
        Tester.verifyListContents(theTestList, new int[]{502, 154, 38});

        assertFalse("Fehler: Element 975 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(975));
        theTestList.append(608);
        Tester.verifyListContents(theTestList, new int[]{502, 154, 38, 608});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{502, 154, 608});

        theTestList.insertAt(3, 467);
        Tester.verifyListContents(theTestList, new int[]{502, 154, 608, 467});

        assertFalse("Fehler: Element 715 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(715));
        assertTrue("Fehler: Element 154 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(154));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{502, 154, 467});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{502, 154});

        theTestList.append(837);
        Tester.verifyListContents(theTestList, new int[]{502, 154, 837});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{502, 837});

        theTestList.insertAt(1, 144);
        Tester.verifyListContents(theTestList, new int[]{502, 144, 837});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{502, 144});

        theTestList.insertAt(0, 957);
        Tester.verifyListContents(theTestList, new int[]{957, 502, 144});

        assertTrue("Fehler: Element 144 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(144));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{502, 144});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{144});

        assertTrue("Fehler: Element 144 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(144));
        assertTrue("Fehler: Element 144 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(144));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(216);
        Tester.verifyListContents(theTestList, new int[]{216});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(754);
        Tester.verifyListContents(theTestList, new int[]{754});

        theTestList.insertAt(1, 375);
        Tester.verifyListContents(theTestList, new int[]{754, 375});

        assertTrue("Fehler: Element 375 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(375));
        theTestList.append(922);
        Tester.verifyListContents(theTestList, new int[]{754, 375, 922});

        theTestList.append(288);
        Tester.verifyListContents(theTestList, new int[]{754, 375, 922, 288});

        theTestList.append(18);
        Tester.verifyListContents(theTestList, new int[]{754, 375, 922, 288, 18});

        theTestList.append(490);
        Tester.verifyListContents(theTestList, new int[]{754, 375, 922, 288, 18, 490});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{375, 922, 288, 18, 490});

        theTestList.append(841);
        Tester.verifyListContents(theTestList, new int[]{375, 922, 288, 18, 490, 841});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{375, 922, 288, 490, 841});

        theTestList.append(23);
        Tester.verifyListContents(theTestList, new int[]{375, 922, 288, 490, 841, 23});

        theTestList.insertAt(3, 595);
        Tester.verifyListContents(theTestList, new int[]{375, 922, 288, 595, 490, 841, 23});

        assertTrue("Fehler: Element 490 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(490));
        theTestList.append(314);
        Tester.verifyListContents(theTestList, new int[]{375, 922, 288, 595, 490, 841, 23, 314});

        theTestList.insertAt(5, 674);
        Tester.verifyListContents(theTestList, new int[]{375, 922, 288, 595, 490, 674, 841, 23, 314});

        assertTrue("Fehler: Element 490 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(490));
        assertTrue("Fehler: Element 314 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(314));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{375, 288, 595, 490, 674, 841, 23, 314});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{375, 288, 595, 490, 841, 23, 314});

        assertTrue("Fehler: Element 490 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(490));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{375, 288, 595, 490, 23, 314});

        assertFalse("Fehler: Element 107 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(107));
        theTestList.append(858);
        Tester.verifyListContents(theTestList, new int[]{375, 288, 595, 490, 23, 314, 858});

        theTestList.append(892);
        Tester.verifyListContents(theTestList, new int[]{375, 288, 595, 490, 23, 314, 858, 892});

        theTestList.insertAt(1, 539);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 490, 23, 314, 858, 892});

        theTestList.append(875);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 490, 23, 314, 858, 892, 875});

        theTestList.insertAt(4, 64);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 64, 490, 23, 314, 858, 892, 875});

        theTestList.append(588);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 64, 490, 23, 314, 858, 892, 875, 588});

        theTestList.insertAt(12, 567);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 64, 490, 23, 314, 858, 892, 875, 588, 567});

        theTestList.append(261);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 64, 490, 23, 314, 858, 892, 875, 588, 567, 261});

        theTestList.insertAt(10, 75);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 64, 490, 23, 314, 858, 892, 75, 875, 588, 567, 261});

        theTestList.append(528);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 64, 490, 23, 314, 858, 892, 75, 875, 588, 567, 261, 528});

        theTestList.insertAt(12, 88);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 64, 490, 23, 314, 858, 892, 75, 875, 88, 588, 567, 261, 528});

        assertFalse("Fehler: Element 433 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(433));
        theTestList.removeAt(11);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 64, 490, 23, 314, 858, 892, 75, 88, 588, 567, 261, 528});

        theTestList.append(630);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 64, 490, 23, 314, 858, 892, 75, 88, 588, 567, 261, 528, 630});

        assertTrue("Fehler: Element 490 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(490));
        theTestList.append(683);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 64, 490, 23, 314, 858, 892, 75, 88, 588, 567, 261, 528, 630, 683});

        theTestList.append(211);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 64, 490, 23, 314, 858, 892, 75, 88, 588, 567, 261, 528, 630, 683, 211});

        assertTrue("Fehler: Element 375 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(375));
        assertTrue("Fehler: Element 588 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(588));
        assertFalse("Fehler: Element 25 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(25));
        theTestList.insertAt(5, 332);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 64, 332, 490, 23, 314, 858, 892, 75, 88, 588, 567, 261, 528, 630, 683, 211});

        theTestList.append(414);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 64, 332, 490, 23, 314, 858, 892, 75, 88, 588, 567, 261, 528, 630, 683, 211, 414});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 288, 595, 64, 332, 490, 314, 858, 892, 75, 88, 588, 567, 261, 528, 630, 683, 211, 414});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 595, 64, 332, 490, 314, 858, 892, 75, 88, 588, 567, 261, 528, 630, 683, 211, 414});

        assertTrue("Fehler: Element 211 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(211));
        theTestList.append(449);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 595, 64, 332, 490, 314, 858, 892, 75, 88, 588, 567, 261, 528, 630, 683, 211, 414, 449});

        theTestList.insertAt(16, 77);
        Tester.verifyListContents(theTestList, new int[]{375, 539, 595, 64, 332, 490, 314, 858, 892, 75, 88, 588, 567, 261, 528, 630, 77, 683, 211, 414, 449});

        theTestList.insertAt(1, 278);
        Tester.verifyListContents(theTestList, new int[]{375, 278, 539, 595, 64, 332, 490, 314, 858, 892, 75, 88, 588, 567, 261, 528, 630, 77, 683, 211, 414, 449});

        theTestList.removeAt(15);
        Tester.verifyListContents(theTestList, new int[]{375, 278, 539, 595, 64, 332, 490, 314, 858, 892, 75, 88, 588, 567, 261, 630, 77, 683, 211, 414, 449});

        theTestList.append(34);
        Tester.verifyListContents(theTestList, new int[]{375, 278, 539, 595, 64, 332, 490, 314, 858, 892, 75, 88, 588, 567, 261, 630, 77, 683, 211, 414, 449, 34});

    }

    public static void test8Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(491);
        Tester.verifyListContents(theTestList, new int[]{491});

        theTestList.insertAt(1, 958);
        Tester.verifyListContents(theTestList, new int[]{491, 958});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{491});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 73);
        Tester.verifyListContents(theTestList, new int[]{73});

        theTestList.append(235);
        Tester.verifyListContents(theTestList, new int[]{73, 235});

        theTestList.insertAt(0, 723);
        Tester.verifyListContents(theTestList, new int[]{723, 73, 235});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 665);
        Tester.verifyListContents(theTestList, new int[]{665});

        assertFalse("Fehler: Element 695 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(695));
// Alles löschen
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(257);
        Tester.verifyListContents(theTestList, new int[]{257});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(341);
        Tester.verifyListContents(theTestList, new int[]{341});

        theTestList.append(341);
        Tester.verifyListContents(theTestList, new int[]{341, 341});

        assertTrue("Fehler: Element 341 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(341));
// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(566);
        Tester.verifyListContents(theTestList, new int[]{566});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(121);
        Tester.verifyListContents(theTestList, new int[]{121});

        theTestList.append(574);
        Tester.verifyListContents(theTestList, new int[]{121, 574});

        theTestList.append(935);
        Tester.verifyListContents(theTestList, new int[]{121, 574, 935});

        theTestList.insertAt(3, 473);
        Tester.verifyListContents(theTestList, new int[]{121, 574, 935, 473});

        theTestList.append(655);
        Tester.verifyListContents(theTestList, new int[]{121, 574, 935, 473, 655});

        assertTrue("Fehler: Element 655 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(655));
        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{121, 574, 935, 473});

        theTestList.append(646);
        Tester.verifyListContents(theTestList, new int[]{121, 574, 935, 473, 646});

        theTestList.append(337);
        Tester.verifyListContents(theTestList, new int[]{121, 574, 935, 473, 646, 337});

        theTestList.insertAt(5, 490);
        Tester.verifyListContents(theTestList, new int[]{121, 574, 935, 473, 646, 490, 337});

        assertTrue("Fehler: Element 490 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(490));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{121, 574, 935, 646, 490, 337});

        theTestList.insertAt(2, 72);
        Tester.verifyListContents(theTestList, new int[]{121, 574, 72, 935, 646, 490, 337});

        assertTrue("Fehler: Element 490 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(490));
        assertFalse("Fehler: Element 628 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(628));
        theTestList.append(313);
        Tester.verifyListContents(theTestList, new int[]{121, 574, 72, 935, 646, 490, 337, 313});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{121, 574, 72, 646, 490, 337, 313});

        theTestList.append(909);
        Tester.verifyListContents(theTestList, new int[]{121, 574, 72, 646, 490, 337, 313, 909});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{121, 574, 72, 646, 490, 337, 313});

        theTestList.insertAt(5, 331);
        Tester.verifyListContents(theTestList, new int[]{121, 574, 72, 646, 490, 331, 337, 313});

        assertFalse("Fehler: Element 485 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(485));
        theTestList.append(86);
        Tester.verifyListContents(theTestList, new int[]{121, 574, 72, 646, 490, 331, 337, 313, 86});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{121, 72, 646, 490, 331, 337, 313, 86});

        assertTrue("Fehler: Element 121 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(121));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{121, 646, 490, 331, 337, 313, 86});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{121, 646, 331, 337, 313, 86});

        theTestList.append(600);
        Tester.verifyListContents(theTestList, new int[]{121, 646, 331, 337, 313, 86, 600});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{121, 646, 331, 313, 86, 600});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 437);
        Tester.verifyListContents(theTestList, new int[]{437});

        assertFalse("Fehler: Element 412 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(412));
        theTestList.append(788);
        Tester.verifyListContents(theTestList, new int[]{437, 788});

        assertFalse("Fehler: Element 304 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(304));
        theTestList.append(286);
        Tester.verifyListContents(theTestList, new int[]{437, 788, 286});

        assertFalse("Fehler: Element 526 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(526));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{788, 286});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{286});

        theTestList.append(618);
        Tester.verifyListContents(theTestList, new int[]{286, 618});

        theTestList.append(352);
        Tester.verifyListContents(theTestList, new int[]{286, 618, 352});

        assertFalse("Fehler: Element 237 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(237));
        theTestList.append(581);
        Tester.verifyListContents(theTestList, new int[]{286, 618, 352, 581});

        theTestList.append(931);
        Tester.verifyListContents(theTestList, new int[]{286, 618, 352, 581, 931});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{286, 618, 352, 931});

        assertTrue("Fehler: Element 618 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(618));
        theTestList.insertAt(1, 861);
        Tester.verifyListContents(theTestList, new int[]{286, 861, 618, 352, 931});

        assertTrue("Fehler: Element 931 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(931));
        theTestList.insertAt(3, 190);
        Tester.verifyListContents(theTestList, new int[]{286, 861, 618, 190, 352, 931});

        assertTrue("Fehler: Element 861 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(861));
        assertTrue("Fehler: Element 618 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(618));
        theTestList.insertAt(1, 699);
        Tester.verifyListContents(theTestList, new int[]{286, 699, 861, 618, 190, 352, 931});

        theTestList.insertAt(2, 935);
        Tester.verifyListContents(theTestList, new int[]{286, 699, 935, 861, 618, 190, 352, 931});

        theTestList.insertAt(0, 653);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 699, 935, 861, 618, 190, 352, 931});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 699, 935, 861, 618, 352, 931});

        assertTrue("Fehler: Element 286 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(286));
        assertFalse("Fehler: Element 781 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(781));
        theTestList.insertAt(8, 959);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 699, 935, 861, 618, 352, 931, 959});

        theTestList.append(337);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 699, 935, 861, 618, 352, 931, 959, 337});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 935, 861, 618, 352, 931, 959, 337});

        theTestList.insertAt(4, 698);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 935, 861, 698, 618, 352, 931, 959, 337});

        theTestList.append(333);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 935, 861, 698, 618, 352, 931, 959, 337, 333});

        assertFalse("Fehler: Element 180 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(180));
        assertFalse("Fehler: Element 702 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(702));
        assertFalse("Fehler: Element 226 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(226));
        theTestList.append(623);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 935, 861, 698, 618, 352, 931, 959, 337, 333, 623});

        assertFalse("Fehler: Element 939 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(939));
        theTestList.append(25);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 935, 861, 698, 618, 352, 931, 959, 337, 333, 623, 25});

        assertTrue("Fehler: Element 623 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(623));
        theTestList.append(595);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 935, 861, 698, 618, 352, 931, 959, 337, 333, 623, 25, 595});

        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 935, 861, 698, 618, 352, 959, 337, 333, 623, 25, 595});

        theTestList.append(494);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 935, 861, 698, 618, 352, 959, 337, 333, 623, 25, 595, 494});

        assertTrue("Fehler: Element 618 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(618));
        theTestList.append(440);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 935, 861, 698, 618, 352, 959, 337, 333, 623, 25, 595, 494, 440});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 935, 861, 618, 352, 959, 337, 333, 623, 25, 595, 494, 440});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 935, 861, 618, 352, 337, 333, 623, 25, 595, 494, 440});

        assertTrue("Fehler: Element 337 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(337));
        theTestList.append(838);
        Tester.verifyListContents(theTestList, new int[]{653, 286, 935, 861, 618, 352, 337, 333, 623, 25, 595, 494, 440, 838});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{653, 935, 861, 618, 352, 337, 333, 623, 25, 595, 494, 440, 838});

        assertFalse("Fehler: Element 421 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(421));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{935, 861, 618, 352, 337, 333, 623, 25, 595, 494, 440, 838});

        assertFalse("Fehler: Element 153 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(153));
        assertTrue("Fehler: Element 623 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(623));
    }

    public static void test9Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(202);
        Tester.verifyListContents(theTestList, new int[]{202});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 422);
        Tester.verifyListContents(theTestList, new int[]{422});

        assertFalse("Fehler: Element 558 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(558));
        assertTrue("Fehler: Element 422 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(422));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(707);
        Tester.verifyListContents(theTestList, new int[]{707});

        theTestList.insertAt(1, 942);
        Tester.verifyListContents(theTestList, new int[]{707, 942});

        theTestList.insertAt(0, 116);
        Tester.verifyListContents(theTestList, new int[]{116, 707, 942});

        theTestList.insertAt(3, 536);
        Tester.verifyListContents(theTestList, new int[]{116, 707, 942, 536});

        assertTrue("Fehler: Element 942 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(942));
        theTestList.insertAt(2, 339);
        Tester.verifyListContents(theTestList, new int[]{116, 707, 339, 942, 536});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{116, 707, 339, 536});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{116, 707, 339});

        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{116, 707});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{116});

        assertFalse("Fehler: Element 465 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(465));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 134);
        Tester.verifyListContents(theTestList, new int[]{134});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(336);
        Tester.verifyListContents(theTestList, new int[]{336});

        theTestList.append(417);
        Tester.verifyListContents(theTestList, new int[]{336, 417});

        theTestList.append(592);
        Tester.verifyListContents(theTestList, new int[]{336, 417, 592});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{417, 592});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{592});

        theTestList.insertAt(0, 901);
        Tester.verifyListContents(theTestList, new int[]{901, 592});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{592});

        assertFalse("Fehler: Element 351 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(351));
        assertTrue("Fehler: Element 592 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(592));
        assertFalse("Fehler: Element 169 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(169));
        theTestList.insertAt(1, 634);
        Tester.verifyListContents(theTestList, new int[]{592, 634});

        assertTrue("Fehler: Element 592 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(592));
        theTestList.insertAt(2, 862);
        Tester.verifyListContents(theTestList, new int[]{592, 634, 862});

        assertFalse("Fehler: Element 574 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(574));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{592, 634});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{592});

        theTestList.append(866);
        Tester.verifyListContents(theTestList, new int[]{592, 866});

        theTestList.append(785);
        Tester.verifyListContents(theTestList, new int[]{592, 866, 785});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{866, 785});

        theTestList.insertAt(0, 427);
        Tester.verifyListContents(theTestList, new int[]{427, 866, 785});

        theTestList.append(189);
        Tester.verifyListContents(theTestList, new int[]{427, 866, 785, 189});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{427, 866, 785});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{866, 785});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{866});

        theTestList.append(923);
        Tester.verifyListContents(theTestList, new int[]{866, 923});

        theTestList.insertAt(1, 904);
        Tester.verifyListContents(theTestList, new int[]{866, 904, 923});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{904, 923});

        theTestList.insertAt(2, 884);
        Tester.verifyListContents(theTestList, new int[]{904, 923, 884});

        theTestList.append(778);
        Tester.verifyListContents(theTestList, new int[]{904, 923, 884, 778});

        assertTrue("Fehler: Element 778 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(778));
        assertFalse("Fehler: Element 394 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(394));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{904, 923, 884});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{904, 884});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{904});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 956);
        Tester.verifyListContents(theTestList, new int[]{956});

        assertTrue("Fehler: Element 956 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(956));
        theTestList.insertAt(1, 140);
        Tester.verifyListContents(theTestList, new int[]{956, 140});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{140});

        assertFalse("Fehler: Element 797 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(797));
        theTestList.insertAt(1, 174);
        Tester.verifyListContents(theTestList, new int[]{140, 174});

        theTestList.append(893);
        Tester.verifyListContents(theTestList, new int[]{140, 174, 893});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{174, 893});

        theTestList.append(892);
        Tester.verifyListContents(theTestList, new int[]{174, 893, 892});

        theTestList.insertAt(3, 769);
        Tester.verifyListContents(theTestList, new int[]{174, 893, 892, 769});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{893, 892, 769});

        assertFalse("Fehler: Element 429 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(429));
        theTestList.append(193);
        Tester.verifyListContents(theTestList, new int[]{893, 892, 769, 193});

        theTestList.append(676);
        Tester.verifyListContents(theTestList, new int[]{893, 892, 769, 193, 676});

        theTestList.insertAt(0, 723);
        Tester.verifyListContents(theTestList, new int[]{723, 893, 892, 769, 193, 676});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{723, 893, 892, 769, 676});

        theTestList.insertAt(1, 481);
        Tester.verifyListContents(theTestList, new int[]{723, 481, 893, 892, 769, 676});

        assertTrue("Fehler: Element 481 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(481));
        assertTrue("Fehler: Element 892 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(892));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{481, 893, 892, 769, 676});

        theTestList.insertAt(3, 637);
        Tester.verifyListContents(theTestList, new int[]{481, 893, 892, 637, 769, 676});

        assertFalse("Fehler: Element 870 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(870));
        theTestList.append(477);
        Tester.verifyListContents(theTestList, new int[]{481, 893, 892, 637, 769, 676, 477});

        theTestList.insertAt(5, 59);
        Tester.verifyListContents(theTestList, new int[]{481, 893, 892, 637, 769, 59, 676, 477});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{481, 892, 637, 769, 59, 676, 477});

        assertTrue("Fehler: Element 676 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(676));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{892, 637, 769, 59, 676, 477});

        theTestList.insertAt(2, 540);
        Tester.verifyListContents(theTestList, new int[]{892, 637, 540, 769, 59, 676, 477});

        assertFalse("Fehler: Element 861 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(861));
        theTestList.insertAt(7, 857);
        Tester.verifyListContents(theTestList, new int[]{892, 637, 540, 769, 59, 676, 477, 857});

        theTestList.insertAt(3, 674);
        Tester.verifyListContents(theTestList, new int[]{892, 637, 540, 674, 769, 59, 676, 477, 857});

        assertTrue("Fehler: Element 59 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(59));
        theTestList.insertAt(5, 382);
        Tester.verifyListContents(theTestList, new int[]{892, 637, 540, 674, 769, 382, 59, 676, 477, 857});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{892, 637, 540, 674, 769, 382, 59, 676, 477});

        assertTrue("Fehler: Element 674 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(674));
        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{892, 637, 540, 674, 769, 382, 59, 676});

        theTestList.insertAt(3, 921);
        Tester.verifyListContents(theTestList, new int[]{892, 637, 540, 921, 674, 769, 382, 59, 676});

        assertTrue("Fehler: Element 637 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(637));
        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{892, 637, 540, 674, 769, 382, 59, 676});

        theTestList.insertAt(8, 77);
        Tester.verifyListContents(theTestList, new int[]{892, 637, 540, 674, 769, 382, 59, 676, 77});

        assertTrue("Fehler: Element 769 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(769));
        theTestList.append(791);
        Tester.verifyListContents(theTestList, new int[]{892, 637, 540, 674, 769, 382, 59, 676, 77, 791});

        theTestList.insertAt(10, 702);
        Tester.verifyListContents(theTestList, new int[]{892, 637, 540, 674, 769, 382, 59, 676, 77, 791, 702});

        assertFalse("Fehler: Element 553 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(553));
        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{892, 637, 540, 674, 769, 382, 59, 676, 77, 702});

    }

    public static void test10Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(957);
        Tester.verifyListContents(theTestList, new int[]{957});

        theTestList.append(306);
        Tester.verifyListContents(theTestList, new int[]{957, 306});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{306});

        theTestList.append(72);
        Tester.verifyListContents(theTestList, new int[]{306, 72});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{306});

        assertFalse("Fehler: Element 550 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(550));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 149);
        Tester.verifyListContents(theTestList, new int[]{149});

        theTestList.append(61);
        Tester.verifyListContents(theTestList, new int[]{149, 61});

        theTestList.append(671);
        Tester.verifyListContents(theTestList, new int[]{149, 61, 671});

        assertTrue("Fehler: Element 61 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(61));
        assertTrue("Fehler: Element 149 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(149));
        theTestList.append(674);
        Tester.verifyListContents(theTestList, new int[]{149, 61, 671, 674});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{149, 61, 671});

        theTestList.append(510);
        Tester.verifyListContents(theTestList, new int[]{149, 61, 671, 510});

        theTestList.insertAt(1, 519);
        Tester.verifyListContents(theTestList, new int[]{149, 519, 61, 671, 510});

        theTestList.append(460);
        Tester.verifyListContents(theTestList, new int[]{149, 519, 61, 671, 510, 460});

        assertTrue("Fehler: Element 149 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(149));
        theTestList.insertAt(3, 676);
        Tester.verifyListContents(theTestList, new int[]{149, 519, 61, 676, 671, 510, 460});

        theTestList.insertAt(3, 717);
        Tester.verifyListContents(theTestList, new int[]{149, 519, 61, 717, 676, 671, 510, 460});

        theTestList.insertAt(6, 941);
        Tester.verifyListContents(theTestList, new int[]{149, 519, 61, 717, 676, 671, 941, 510, 460});

        assertFalse("Fehler: Element 836 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(836));
        theTestList.append(470);
        Tester.verifyListContents(theTestList, new int[]{149, 519, 61, 717, 676, 671, 941, 510, 460, 470});

        theTestList.append(852);
        Tester.verifyListContents(theTestList, new int[]{149, 519, 61, 717, 676, 671, 941, 510, 460, 470, 852});

        assertFalse("Fehler: Element 894 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(894));
        theTestList.removeAt(10);
        Tester.verifyListContents(theTestList, new int[]{149, 519, 61, 717, 676, 671, 941, 510, 460, 470});

        theTestList.insertAt(4, 126);
        Tester.verifyListContents(theTestList, new int[]{149, 519, 61, 717, 126, 676, 671, 941, 510, 460, 470});

        theTestList.insertAt(7, 396);
        Tester.verifyListContents(theTestList, new int[]{149, 519, 61, 717, 126, 676, 671, 396, 941, 510, 460, 470});

        theTestList.removeAt(6);
        Tester.verifyListContents(theTestList, new int[]{149, 519, 61, 717, 126, 676, 396, 941, 510, 460, 470});

        theTestList.append(659);
        Tester.verifyListContents(theTestList, new int[]{149, 519, 61, 717, 126, 676, 396, 941, 510, 460, 470, 659});

        assertFalse("Fehler: Element 588 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(588));
        theTestList.insertAt(10, 398);
        Tester.verifyListContents(theTestList, new int[]{149, 519, 61, 717, 126, 676, 396, 941, 510, 460, 398, 470, 659});

        assertTrue("Fehler: Element 398 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(398));
        theTestList.insertAt(12, 366);
        Tester.verifyListContents(theTestList, new int[]{149, 519, 61, 717, 126, 676, 396, 941, 510, 460, 398, 470, 366, 659});

// Alles löschen
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        theTestList.removeAt(0);
        assertTrue(theTestList.isEmpty());
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(795);
        Tester.verifyListContents(theTestList, new int[]{795});

        assertTrue("Fehler: Element 795 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(795));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.append(526);
        Tester.verifyListContents(theTestList, new int[]{526});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 661);
        Tester.verifyListContents(theTestList, new int[]{661});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{});

        theTestList.insertAt(0, 893);
        Tester.verifyListContents(theTestList, new int[]{893});

        theTestList.insertAt(0, 502);
        Tester.verifyListContents(theTestList, new int[]{502, 893});

        theTestList.append(854);
        Tester.verifyListContents(theTestList, new int[]{502, 893, 854});

        theTestList.append(99);
        Tester.verifyListContents(theTestList, new int[]{502, 893, 854, 99});

        theTestList.append(808);
        Tester.verifyListContents(theTestList, new int[]{502, 893, 854, 99, 808});

        theTestList.insertAt(5, 21);
        Tester.verifyListContents(theTestList, new int[]{502, 893, 854, 99, 808, 21});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{502, 893, 854, 99, 21});

        theTestList.insertAt(2, 211);
        Tester.verifyListContents(theTestList, new int[]{502, 893, 211, 854, 99, 21});

        theTestList.removeAt(5);
        Tester.verifyListContents(theTestList, new int[]{502, 893, 211, 854, 99});

        theTestList.removeAt(4);
        Tester.verifyListContents(theTestList, new int[]{502, 893, 211, 854});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{893, 211, 854});

        assertTrue("Fehler: Element 854 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(854));
        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{211, 854});

        theTestList.insertAt(2, 922);
        Tester.verifyListContents(theTestList, new int[]{211, 854, 922});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{854, 922});

        assertFalse("Fehler: Element 386 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(386));
        theTestList.insertAt(0, 831);
        Tester.verifyListContents(theTestList, new int[]{831, 854, 922});

        theTestList.insertAt(1, 252);
        Tester.verifyListContents(theTestList, new int[]{831, 252, 854, 922});

        theTestList.insertAt(0, 370);
        Tester.verifyListContents(theTestList, new int[]{370, 831, 252, 854, 922});

        assertFalse("Fehler: Element 492 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(492));
        assertTrue("Fehler: Element 831 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(831));
        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{370, 252, 854, 922});

        assertFalse("Fehler: Element 718 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(718));
        theTestList.append(395);
        Tester.verifyListContents(theTestList, new int[]{370, 252, 854, 922, 395});

        theTestList.removeAt(0);
        Tester.verifyListContents(theTestList, new int[]{252, 854, 922, 395});

        theTestList.insertAt(4, 207);
        Tester.verifyListContents(theTestList, new int[]{252, 854, 922, 395, 207});

        theTestList.append(974);
        Tester.verifyListContents(theTestList, new int[]{252, 854, 922, 395, 207, 974});

        theTestList.append(290);
        Tester.verifyListContents(theTestList, new int[]{252, 854, 922, 395, 207, 974, 290});

        theTestList.insertAt(3, 915);
        Tester.verifyListContents(theTestList, new int[]{252, 854, 922, 915, 395, 207, 974, 290});

        theTestList.insertAt(7, 784);
        Tester.verifyListContents(theTestList, new int[]{252, 854, 922, 915, 395, 207, 974, 784, 290});

        assertFalse("Fehler: Element 870 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(870));
        assertTrue("Fehler: Element 290 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(290));
        theTestList.append(708);
        Tester.verifyListContents(theTestList, new int[]{252, 854, 922, 915, 395, 207, 974, 784, 290, 708});

        assertTrue("Fehler: Element 854 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(854));
        theTestList.removeAt(7);
        Tester.verifyListContents(theTestList, new int[]{252, 854, 922, 915, 395, 207, 974, 290, 708});

        theTestList.removeAt(8);
        Tester.verifyListContents(theTestList, new int[]{252, 854, 922, 915, 395, 207, 974, 290});

        theTestList.removeAt(3);
        Tester.verifyListContents(theTestList, new int[]{252, 854, 922, 395, 207, 974, 290});

        theTestList.append(722);
        Tester.verifyListContents(theTestList, new int[]{252, 854, 922, 395, 207, 974, 290, 722});

        theTestList.append(663);
        Tester.verifyListContents(theTestList, new int[]{252, 854, 922, 395, 207, 974, 290, 722, 663});

        theTestList.removeAt(1);
        Tester.verifyListContents(theTestList, new int[]{252, 922, 395, 207, 974, 290, 722, 663});

        theTestList.append(746);
        Tester.verifyListContents(theTestList, new int[]{252, 922, 395, 207, 974, 290, 722, 663, 746});

        theTestList.insertAt(6, 377);
        Tester.verifyListContents(theTestList, new int[]{252, 922, 395, 207, 974, 290, 377, 722, 663, 746});

        assertTrue("Fehler: Element 974 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(974));
        theTestList.append(271);
        Tester.verifyListContents(theTestList, new int[]{252, 922, 395, 207, 974, 290, 377, 722, 663, 746, 271});

        theTestList.append(708);
        Tester.verifyListContents(theTestList, new int[]{252, 922, 395, 207, 974, 290, 377, 722, 663, 746, 271, 708});

        assertTrue("Fehler: Element 271 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(271));
        theTestList.insertAt(11, 974);
        Tester.verifyListContents(theTestList, new int[]{252, 922, 395, 207, 974, 290, 377, 722, 663, 746, 271, 974, 708});

        theTestList.insertAt(8, 426);
        Tester.verifyListContents(theTestList, new int[]{252, 922, 395, 207, 974, 290, 377, 722, 426, 663, 746, 271, 974, 708});

        assertTrue("Fehler: Element 395 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(395));
        theTestList.removeAt(2);
        Tester.verifyListContents(theTestList, new int[]{252, 922, 207, 974, 290, 377, 722, 426, 663, 746, 271, 974, 708});

        theTestList.removeAt(9);
        Tester.verifyListContents(theTestList, new int[]{252, 922, 207, 974, 290, 377, 722, 426, 663, 271, 974, 708});

        theTestList.insertAt(9, 132);
        Tester.verifyListContents(theTestList, new int[]{252, 922, 207, 974, 290, 377, 722, 426, 663, 132, 271, 974, 708});

        theTestList.insertAt(13, 666);
        Tester.verifyListContents(theTestList, new int[]{252, 922, 207, 974, 290, 377, 722, 426, 663, 132, 271, 974, 708, 666});

        assertTrue("Fehler: Element 252 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(252));
        theTestList.insertAt(0, 717);
        Tester.verifyListContents(theTestList, new int[]{717, 252, 922, 207, 974, 290, 377, 722, 426, 663, 132, 271, 974, 708, 666});

        assertTrue("Fehler: Element 426 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(426));
        assertTrue("Fehler: Element 974 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(974));
        theTestList.append(659);
        Tester.verifyListContents(theTestList, new int[]{717, 252, 922, 207, 974, 290, 377, 722, 426, 663, 132, 271, 974, 708, 666, 659});

    }
}
