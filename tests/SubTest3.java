package tests;

import static org.junit.Assert.*;

import liste.*;

/**
 * Die Test-Klasse SubTest3.
 *
 * @author Rainer Helfrich
 * @version 03.10.2020
 */
public class SubTest3 {


    public static void test1Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(100);
        Tester.verifyListContents(theTestList, new int[]{100});

        theTestList.append(544);
        Tester.verifyListContents(theTestList, new int[]{100, 544});

        assertFalse("Fehler: Element 588 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(588));
        assertFalse("Fehler: Element 553 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(553));
        assertTrue("Fehler: Element 100 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(100));
        assertTrue("Fehler: Element 100 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(100));
        assertTrue("Fehler: Element 100 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(100));
        theTestList.append(162);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162});

        theTestList.append(238);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238});

        theTestList.append(69);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69});

        assertTrue("Fehler: Element 162 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(162));
        assertTrue("Fehler: Element 544 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(544));
        assertTrue("Fehler: Element 238 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(238));
        assertTrue("Fehler: Element 69 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(69));
        theTestList.append(525);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525});

        theTestList.append(146);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146});

        assertFalse("Fehler: Element 24 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(24));
        theTestList.append(565);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565});

        theTestList.append(638);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638});

        assertFalse("Fehler: Element 786 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(786));
        theTestList.append(561);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561});

        theTestList.append(773);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773});

        theTestList.append(872);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872});

        assertTrue("Fehler: Element 146 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(146));
        theTestList.append(802);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802});

        theTestList.append(502);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502});

        theTestList.append(81);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81});

        theTestList.append(78);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78});

        assertTrue("Fehler: Element 638 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(638));
        theTestList.append(934);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934});

        theTestList.append(111);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111});

        theTestList.append(342);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342});

        theTestList.append(413);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413});

        assertFalse("Fehler: Element 451 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(451));
        assertTrue("Fehler: Element 565 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(565));
        theTestList.append(517);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517});

        theTestList.append(178);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178});

        assertFalse("Fehler: Element 890 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(890));
        assertFalse("Fehler: Element 289 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(289));
        theTestList.append(506);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506});

        assertTrue("Fehler: Element 802 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(802));
        theTestList.append(884);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884});

        theTestList.append(219);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219});

        theTestList.append(998);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998});

        assertFalse("Fehler: Element 93 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(93));
        theTestList.append(168);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168});

        assertFalse("Fehler: Element 76 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(76));
        theTestList.append(564);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564});

        assertTrue("Fehler: Element 100 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(100));
        assertTrue("Fehler: Element 525 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(525));
        assertTrue("Fehler: Element 506 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(506));
        theTestList.append(411);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411});

        theTestList.append(869);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869});

        assertFalse("Fehler: Element 74 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(74));
        assertFalse("Fehler: Element 381 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(381));
        assertTrue("Fehler: Element 111 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(111));
        assertTrue("Fehler: Element 802 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(802));
        theTestList.append(130);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130});

        theTestList.append(909);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909});

        theTestList.append(620);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909, 620});

        assertFalse("Fehler: Element 769 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(769));
        assertFalse("Fehler: Element 459 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(459));
        assertFalse("Fehler: Element 484 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(484));
        assertFalse("Fehler: Element 950 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(950));
        assertTrue("Fehler: Element 564 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(564));
        theTestList.append(45);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909, 620, 45});

        theTestList.append(695);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909, 620, 45, 695});

        assertFalse("Fehler: Element 617 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(617));
        assertTrue("Fehler: Element 100 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(100));
        assertFalse("Fehler: Element 786 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(786));
        assertFalse("Fehler: Element 71 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(71));
        assertTrue("Fehler: Element 342 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(342));
        assertTrue("Fehler: Element 638 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(638));
        assertTrue("Fehler: Element 81 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(81));
        theTestList.append(122);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909, 620, 45, 695, 122});

        theTestList.append(368);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909, 620, 45, 695, 122, 368});

        assertFalse("Fehler: Element 586 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(586));
        assertTrue("Fehler: Element 100 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(100));
        theTestList.append(146);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909, 620, 45, 695, 122, 368, 146});

        assertFalse("Fehler: Element 509 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(509));
        assertFalse("Fehler: Element 160 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(160));
        assertTrue("Fehler: Element 146 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(146));
        assertTrue("Fehler: Element 146 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(146));
        theTestList.append(828);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909, 620, 45, 695, 122, 368, 146, 828});

        theTestList.append(769);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909, 620, 45, 695, 122, 368, 146, 828, 769});

        theTestList.append(862);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909, 620, 45, 695, 122, 368, 146, 828, 769, 862});

        theTestList.append(810);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909, 620, 45, 695, 122, 368, 146, 828, 769, 862, 810});

        theTestList.append(499);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909, 620, 45, 695, 122, 368, 146, 828, 769, 862, 810, 499});

        assertTrue("Fehler: Element 561 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(561));
        assertFalse("Fehler: Element 563 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(563));
        assertFalse("Fehler: Element 747 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(747));
        theTestList.append(668);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909, 620, 45, 695, 122, 368, 146, 828, 769, 862, 810, 499, 668});

        assertTrue("Fehler: Element 342 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(342));
        theTestList.append(193);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909, 620, 45, 695, 122, 368, 146, 828, 769, 862, 810, 499, 668, 193});

        assertFalse("Fehler: Element 722 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(722));
        assertFalse("Fehler: Element 860 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(860));
        theTestList.append(930);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909, 620, 45, 695, 122, 368, 146, 828, 769, 862, 810, 499, 668, 193, 930});

        assertFalse("Fehler: Element 266 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(266));
        assertTrue("Fehler: Element 773 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(773));
        theTestList.append(110);
        Tester.verifyListContents(theTestList, new int[]{100, 544, 162, 238, 69, 525, 146, 565, 638, 561, 773, 872, 802, 502, 81, 78, 934, 111, 342, 413, 517, 178, 506, 884, 219, 998, 168, 564, 411, 869, 130, 909, 620, 45, 695, 122, 368, 146, 828, 769, 862, 810, 499, 668, 193, 930, 110});

    }

    public static void test2Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(428);
        Tester.verifyListContents(theTestList, new int[]{428});

        theTestList.append(972);
        Tester.verifyListContents(theTestList, new int[]{428, 972});

        theTestList.append(481);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481});

        theTestList.append(783);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783});

        assertFalse("Fehler: Element 735 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(735));
        assertTrue("Fehler: Element 972 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(972));
        theTestList.append(797);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797});

        assertTrue("Fehler: Element 481 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(481));
        assertFalse("Fehler: Element 99 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(99));
        theTestList.append(901);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901});

        assertTrue("Fehler: Element 481 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(481));
        theTestList.append(335);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335});

        assertTrue("Fehler: Element 428 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(428));
        assertFalse("Fehler: Element 398 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(398));
        theTestList.append(583);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583});

        assertTrue("Fehler: Element 783 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(783));
        assertFalse("Fehler: Element 822 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(822));
        theTestList.append(333);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333});

        assertFalse("Fehler: Element 108 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(108));
        assertFalse("Fehler: Element 989 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(989));
        theTestList.append(937);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937});

        theTestList.append(520);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520});

        assertFalse("Fehler: Element 270 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(270));
        assertTrue("Fehler: Element 583 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(583));
        assertFalse("Fehler: Element 453 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(453));
        theTestList.append(599);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599});

        theTestList.append(147);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147});

        assertTrue("Fehler: Element 428 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(428));
        theTestList.append(402);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402});

        assertTrue("Fehler: Element 583 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(583));
        assertTrue("Fehler: Element 783 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(783));
        theTestList.append(618);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618});

        theTestList.append(523);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523});

        theTestList.append(98);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98});

        theTestList.append(766);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766});

        theTestList.append(960);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960});

        theTestList.append(593);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593});

        theTestList.append(985);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985});

        theTestList.append(458);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458});

        assertTrue("Fehler: Element 147 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(147));
        assertTrue("Fehler: Element 402 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(402));
        assertTrue("Fehler: Element 797 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(797));
        theTestList.append(881);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881});

        theTestList.append(813);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813});

        assertTrue("Fehler: Element 458 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(458));
        theTestList.append(42);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42});

        assertFalse("Fehler: Element 990 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(990));
        assertTrue("Fehler: Element 937 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(937));
        theTestList.append(527);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527});

        theTestList.append(382);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382});

        assertFalse("Fehler: Element 306 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(306));
        theTestList.append(66);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66});

        theTestList.append(581);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581});

        theTestList.append(805);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805});

        theTestList.append(453);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453});

        theTestList.append(688);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688});

        assertTrue("Fehler: Element 382 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(382));
        assertFalse("Fehler: Element 931 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(931));
        assertFalse("Fehler: Element 267 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(267));
        theTestList.append(127);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127});

        assertFalse("Fehler: Element 930 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(930));
        assertFalse("Fehler: Element 718 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(718));
        assertFalse("Fehler: Element 981 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(981));
        theTestList.append(809);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809});

        assertTrue("Fehler: Element 813 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(813));
        assertTrue("Fehler: Element 813 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(813));
        theTestList.append(253);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253});

        assertTrue("Fehler: Element 458 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(458));
        theTestList.append(652);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652});

        assertTrue("Fehler: Element 481 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(481));
        assertFalse("Fehler: Element 750 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(750));
        assertFalse("Fehler: Element 381 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(381));
        theTestList.append(689);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689});

        theTestList.append(285);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285});

        theTestList.append(643);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643});

        theTestList.append(283);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643, 283});

        theTestList.append(643);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643, 283, 643});

        theTestList.append(917);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643, 283, 643, 917});

        theTestList.append(293);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643, 283, 643, 917, 293});

        assertTrue("Fehler: Element 813 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(813));
        assertTrue("Fehler: Element 581 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(581));
        theTestList.append(14);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643, 283, 643, 917, 293, 14});

        theTestList.append(318);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643, 283, 643, 917, 293, 14, 318});

        assertTrue("Fehler: Element 583 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(583));
        assertTrue("Fehler: Element 901 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(901));
        theTestList.append(950);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643, 283, 643, 917, 293, 14, 318, 950});

        theTestList.append(392);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643, 283, 643, 917, 293, 14, 318, 950, 392});

        assertFalse("Fehler: Element 169 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(169));
        theTestList.append(734);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643, 283, 643, 917, 293, 14, 318, 950, 392, 734});

        assertTrue("Fehler: Element 402 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(402));
        assertTrue("Fehler: Element 127 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(127));
        theTestList.append(213);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643, 283, 643, 917, 293, 14, 318, 950, 392, 734, 213});

        theTestList.append(992);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643, 283, 643, 917, 293, 14, 318, 950, 392, 734, 213, 992});

        theTestList.append(339);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643, 283, 643, 917, 293, 14, 318, 950, 392, 734, 213, 992, 339});

        theTestList.append(777);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643, 283, 643, 917, 293, 14, 318, 950, 392, 734, 213, 992, 339, 777});

        theTestList.append(314);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643, 283, 643, 917, 293, 14, 318, 950, 392, 734, 213, 992, 339, 777, 314});

        assertTrue("Fehler: Element 643 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(643));
        assertTrue("Fehler: Element 285 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(285));
        theTestList.append(855);
        Tester.verifyListContents(theTestList, new int[]{428, 972, 481, 783, 797, 901, 335, 583, 333, 937, 520, 599, 147, 402, 618, 523, 98, 766, 960, 593, 985, 458, 881, 813, 42, 527, 382, 66, 581, 805, 453, 688, 127, 809, 253, 652, 689, 285, 643, 283, 643, 917, 293, 14, 318, 950, 392, 734, 213, 992, 339, 777, 314, 855});

        assertFalse("Fehler: Element 775 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(775));
    }

    public static void test3Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(589);
        Tester.verifyListContents(theTestList, new int[]{589});

        assertFalse("Fehler: Element 27 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(27));
        theTestList.append(757);
        Tester.verifyListContents(theTestList, new int[]{589, 757});

        assertTrue("Fehler: Element 589 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(589));
        assertTrue("Fehler: Element 757 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(757));
        theTestList.append(576);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576});

        assertFalse("Fehler: Element 464 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(464));
        theTestList.append(42);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42});

        theTestList.append(526);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526});

        theTestList.append(419);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419});

        theTestList.append(302);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302});

        assertFalse("Fehler: Element 232 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(232));
        theTestList.append(581);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581});

        theTestList.append(736);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736});

        assertTrue("Fehler: Element 589 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(589));
        theTestList.append(731);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731});

        assertTrue("Fehler: Element 736 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(736));
        assertFalse("Fehler: Element 515 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(515));
        theTestList.append(618);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618});

        assertTrue("Fehler: Element 581 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(581));
        theTestList.append(945);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945});

        theTestList.append(714);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714});

        assertTrue("Fehler: Element 736 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(736));
        theTestList.append(897);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897});

        theTestList.append(305);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305});

        assertFalse("Fehler: Element 55 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(55));
        assertTrue("Fehler: Element 589 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(589));
        assertFalse("Fehler: Element 976 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(976));
        theTestList.append(5);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5});

        assertTrue("Fehler: Element 731 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(731));
        theTestList.append(452);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452});

        assertTrue("Fehler: Element 581 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(581));
        theTestList.append(462);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462});

        assertFalse("Fehler: Element 900 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(900));
        theTestList.append(518);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518});

        theTestList.append(83);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83});

        theTestList.append(460);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460});

        assertFalse("Fehler: Element 737 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(737));
        assertTrue("Fehler: Element 589 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(589));
        assertTrue("Fehler: Element 576 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(576));
        theTestList.append(547);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547});

        theTestList.append(843);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843});

        theTestList.append(164);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164});

        assertTrue("Fehler: Element 419 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(419));
        theTestList.append(479);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479});

        theTestList.append(590);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590});

        assertFalse("Fehler: Element 358 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(358));
        theTestList.append(954);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954});

        theTestList.append(325);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325});

        theTestList.append(839);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839});

        assertTrue("Fehler: Element 479 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(479));
        assertFalse("Fehler: Element 112 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(112));
        theTestList.append(794);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794});

        assertTrue("Fehler: Element 589 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(589));
        theTestList.append(363);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363});

        assertTrue("Fehler: Element 325 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(325));
        assertFalse("Fehler: Element 62 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(62));
        assertTrue("Fehler: Element 518 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(518));
        assertFalse("Fehler: Element 444 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(444));
        theTestList.append(688);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688});

        assertTrue("Fehler: Element 302 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(302));
        assertFalse("Fehler: Element 777 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(777));
        theTestList.append(329);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329});

        theTestList.append(124);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124});

        assertFalse("Fehler: Element 571 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(571));
        assertFalse("Fehler: Element 351 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(351));
        assertFalse("Fehler: Element 698 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(698));
        theTestList.append(338);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338});

        theTestList.append(947);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947});

        assertTrue("Fehler: Element 714 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(714));
        theTestList.append(751);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947, 751});

        theTestList.append(247);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947, 751, 247});

        assertTrue("Fehler: Element 460 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(460));
        assertFalse("Fehler: Element 31 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(31));
        theTestList.append(637);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947, 751, 247, 637});

        theTestList.append(360);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947, 751, 247, 637, 360});

        assertTrue("Fehler: Element 325 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(325));
        assertFalse("Fehler: Element 905 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(905));
        theTestList.append(403);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947, 751, 247, 637, 360, 403});

        theTestList.append(302);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947, 751, 247, 637, 360, 403, 302});

        theTestList.append(727);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947, 751, 247, 637, 360, 403, 302, 727});

        assertTrue("Fehler: Element 462 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(462));
        assertTrue("Fehler: Element 5 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(5));
        theTestList.append(108);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947, 751, 247, 637, 360, 403, 302, 727, 108});

        theTestList.append(981);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947, 751, 247, 637, 360, 403, 302, 727, 108, 981});

        assertTrue("Fehler: Element 305 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(305));
        assertTrue("Fehler: Element 714 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(714));
        assertFalse("Fehler: Element 106 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(106));
        theTestList.append(176);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947, 751, 247, 637, 360, 403, 302, 727, 108, 981, 176});

        theTestList.append(747);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947, 751, 247, 637, 360, 403, 302, 727, 108, 981, 176, 747});

        theTestList.append(63);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947, 751, 247, 637, 360, 403, 302, 727, 108, 981, 176, 747, 63});

        assertFalse("Fehler: Element 635 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(635));
        assertTrue("Fehler: Element 479 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(479));
        assertTrue("Fehler: Element 981 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(981));
        theTestList.append(174);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947, 751, 247, 637, 360, 403, 302, 727, 108, 981, 176, 747, 63, 174});

        assertFalse("Fehler: Element 38 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(38));
        assertTrue("Fehler: Element 42 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(42));
        assertTrue("Fehler: Element 526 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(526));
        theTestList.append(435);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947, 751, 247, 637, 360, 403, 302, 727, 108, 981, 176, 747, 63, 174, 435});

        theTestList.append(704);
        Tester.verifyListContents(theTestList, new int[]{589, 757, 576, 42, 526, 419, 302, 581, 736, 731, 618, 945, 714, 897, 305, 5, 452, 462, 518, 83, 460, 547, 843, 164, 479, 590, 954, 325, 839, 794, 363, 688, 329, 124, 338, 947, 751, 247, 637, 360, 403, 302, 727, 108, 981, 176, 747, 63, 174, 435, 704});

    }

    public static void test4Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(440);
        Tester.verifyListContents(theTestList, new int[]{440});

        theTestList.append(392);
        Tester.verifyListContents(theTestList, new int[]{440, 392});

        assertTrue("Fehler: Element 440 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(440));
        theTestList.append(394);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394});

        assertTrue("Fehler: Element 394 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(394));
        assertTrue("Fehler: Element 440 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(440));
        assertFalse("Fehler: Element 859 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(859));
        assertFalse("Fehler: Element 190 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(190));
        assertFalse("Fehler: Element 351 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(351));
        theTestList.append(583);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583});

        assertFalse("Fehler: Element 7 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(7));
        assertFalse("Fehler: Element 31 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(31));
        theTestList.append(64);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64});

        theTestList.append(17);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17});

        assertTrue("Fehler: Element 583 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(583));
        assertTrue("Fehler: Element 583 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(583));
        theTestList.append(180);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180});

        assertFalse("Fehler: Element 907 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(907));
        theTestList.append(921);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921});

        assertTrue("Fehler: Element 921 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(921));
        assertFalse("Fehler: Element 765 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(765));
        assertTrue("Fehler: Element 394 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(394));
        assertTrue("Fehler: Element 394 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(394));
        theTestList.append(537);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537});

        assertTrue("Fehler: Element 64 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(64));
        theTestList.append(705);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705});

        assertFalse("Fehler: Element 356 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(356));
        assertTrue("Fehler: Element 705 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(705));
        assertFalse("Fehler: Element 679 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(679));
        theTestList.append(40);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40});

        theTestList.append(887);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887});

        theTestList.append(887);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887});

        assertFalse("Fehler: Element 978 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(978));
        theTestList.append(846);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846});

        assertTrue("Fehler: Element 17 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(17));
        theTestList.append(675);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675});

        theTestList.append(173);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173});

        theTestList.append(528);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528});

        theTestList.append(431);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431});

        assertTrue("Fehler: Element 64 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(64));
        theTestList.append(600);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600});

        assertTrue("Fehler: Element 180 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(180));
        assertTrue("Fehler: Element 600 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(600));
        theTestList.append(207);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207});

        theTestList.append(712);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712});

        assertTrue("Fehler: Element 64 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(64));
        theTestList.append(732);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732});

        assertFalse("Fehler: Element 853 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(853));
        assertTrue("Fehler: Element 600 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(600));
        theTestList.append(24);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24});

        assertFalse("Fehler: Element 397 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(397));
        theTestList.append(704);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704});

        assertFalse("Fehler: Element 324 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(324));
        assertFalse("Fehler: Element 354 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(354));
        assertFalse("Fehler: Element 251 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(251));
        theTestList.append(357);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357});

        assertFalse("Fehler: Element 532 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(532));
        assertFalse("Fehler: Element 598 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(598));
        assertTrue("Fehler: Element 583 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(583));
        assertTrue("Fehler: Element 537 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(537));
        assertFalse("Fehler: Element 145 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(145));
        theTestList.append(316);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316});

        theTestList.append(112);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112});

        assertFalse("Fehler: Element 981 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(981));
        assertTrue("Fehler: Element 921 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(921));
        theTestList.append(617);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112, 617});

        assertTrue("Fehler: Element 675 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(675));
        assertFalse("Fehler: Element 340 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(340));
        assertTrue("Fehler: Element 357 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(357));
        assertFalse("Fehler: Element 419 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(419));
        theTestList.append(955);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112, 617, 955});

        assertFalse("Fehler: Element 877 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(877));
        theTestList.append(535);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112, 617, 955, 535});

        theTestList.append(864);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112, 617, 955, 535, 864});

        theTestList.append(627);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112, 617, 955, 535, 864, 627});

        theTestList.append(670);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112, 617, 955, 535, 864, 627, 670});

        assertFalse("Fehler: Element 753 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(753));
        assertTrue("Fehler: Element 40 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(40));
        assertTrue("Fehler: Element 40 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(40));
        assertFalse("Fehler: Element 746 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(746));
        assertFalse("Fehler: Element 684 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(684));
        theTestList.append(834);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112, 617, 955, 535, 864, 627, 670, 834});

        theTestList.append(123);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112, 617, 955, 535, 864, 627, 670, 834, 123});

        theTestList.append(573);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112, 617, 955, 535, 864, 627, 670, 834, 123, 573});

        assertTrue("Fehler: Element 207 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(207));
        theTestList.append(852);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112, 617, 955, 535, 864, 627, 670, 834, 123, 573, 852});

        assertTrue("Fehler: Element 394 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(394));
        theTestList.append(23);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112, 617, 955, 535, 864, 627, 670, 834, 123, 573, 852, 23});

        theTestList.append(16);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112, 617, 955, 535, 864, 627, 670, 834, 123, 573, 852, 23, 16});

        assertTrue("Fehler: Element 173 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(173));
        assertFalse("Fehler: Element 721 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(721));
        theTestList.append(61);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112, 617, 955, 535, 864, 627, 670, 834, 123, 573, 852, 23, 16, 61});

        assertFalse("Fehler: Element 51 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(51));
        theTestList.append(965);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112, 617, 955, 535, 864, 627, 670, 834, 123, 573, 852, 23, 16, 61, 965});

        assertFalse("Fehler: Element 115 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(115));
        assertFalse("Fehler: Element 666 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(666));
        assertTrue("Fehler: Element 834 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(834));
        assertFalse("Fehler: Element 636 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(636));
        theTestList.append(661);
        Tester.verifyListContents(theTestList, new int[]{440, 392, 394, 583, 64, 17, 180, 921, 537, 705, 40, 887, 887, 846, 675, 173, 528, 431, 600, 207, 712, 732, 24, 704, 357, 316, 112, 617, 955, 535, 864, 627, 670, 834, 123, 573, 852, 23, 16, 61, 965, 661});

        assertFalse("Fehler: Element 839 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(839));
    }

    public static void test5Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(340);
        Tester.verifyListContents(theTestList, new int[]{340});

        assertTrue("Fehler: Element 340 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(340));
        theTestList.append(210);
        Tester.verifyListContents(theTestList, new int[]{340, 210});

        theTestList.append(118);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118});

        theTestList.append(521);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521});

        assertTrue("Fehler: Element 340 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(340));
        theTestList.append(905);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905});

        theTestList.append(793);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793});

        theTestList.append(989);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989});

        assertFalse("Fehler: Element 654 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(654));
        assertTrue("Fehler: Element 989 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(989));
        theTestList.append(930);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930});

        theTestList.append(591);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591});

        theTestList.append(874);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874});

        assertFalse("Fehler: Element 853 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(853));
        assertTrue("Fehler: Element 521 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(521));
        assertTrue("Fehler: Element 989 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(989));
        theTestList.append(45);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45});

        assertFalse("Fehler: Element 90 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(90));
        theTestList.append(3);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3});

        theTestList.append(877);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877});

        theTestList.append(645);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645});

        assertTrue("Fehler: Element 340 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(340));
        assertFalse("Fehler: Element 855 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(855));
        assertFalse("Fehler: Element 251 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(251));
        theTestList.append(981);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981});

        theTestList.append(395);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395});

        assertTrue("Fehler: Element 45 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(45));
        assertFalse("Fehler: Element 312 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(312));
        assertFalse("Fehler: Element 850 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(850));
        assertFalse("Fehler: Element 659 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(659));
        assertFalse("Fehler: Element 664 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(664));
        theTestList.append(348);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348});

        assertTrue("Fehler: Element 395 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(395));
        theTestList.append(730);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730});

        theTestList.append(990);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990});

        assertTrue("Fehler: Element 591 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(591));
        theTestList.append(499);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499});

        assertFalse("Fehler: Element 542 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(542));
        assertTrue("Fehler: Element 989 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(989));
        theTestList.append(300);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300});

        theTestList.append(758);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758});

        assertFalse("Fehler: Element 487 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(487));
        theTestList.append(126);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126});

        theTestList.append(417);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417});

        theTestList.append(136);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136});

        assertTrue("Fehler: Element 930 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(930));
        assertFalse("Fehler: Element 888 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(888));
        theTestList.append(159);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159});

        theTestList.append(594);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594});

        theTestList.append(626);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626});

        theTestList.append(867);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867});

        theTestList.append(135);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135});

        theTestList.append(768);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768});

        assertFalse("Fehler: Element 480 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(480));
        theTestList.append(451);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451});

        theTestList.append(225);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225});

        assertFalse("Fehler: Element 185 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(185));
        assertTrue("Fehler: Element 499 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(499));
        assertTrue("Fehler: Element 417 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(417));
        assertFalse("Fehler: Element 609 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(609));
        theTestList.append(1);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1});

        assertFalse("Fehler: Element 942 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(942));
        theTestList.append(576);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576});

        theTestList.append(54);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54});

        theTestList.append(463);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463});

        theTestList.append(526);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526});

        assertTrue("Fehler: Element 340 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(340));
        assertTrue("Fehler: Element 417 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(417));
        assertFalse("Fehler: Element 100 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(100));
        theTestList.append(615);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615});

        theTestList.append(822);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822});

        theTestList.append(200);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200});

        theTestList.append(696);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200, 696});

        assertTrue("Fehler: Element 576 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(576));
        theTestList.append(781);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200, 696, 781});

        theTestList.append(57);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200, 696, 781, 57});

        theTestList.append(413);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200, 696, 781, 57, 413});

        assertFalse("Fehler: Element 175 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(175));
        assertTrue("Fehler: Element 874 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(874));
        theTestList.append(439);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200, 696, 781, 57, 413, 439});

        assertFalse("Fehler: Element 509 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(509));
        theTestList.append(837);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200, 696, 781, 57, 413, 439, 837});

        assertTrue("Fehler: Element 135 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(135));
        assertTrue("Fehler: Element 417 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(417));
        theTestList.append(832);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200, 696, 781, 57, 413, 439, 837, 832});

        assertFalse("Fehler: Element 5 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(5));
        theTestList.append(698);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200, 696, 781, 57, 413, 439, 837, 832, 698});

        assertTrue("Fehler: Element 905 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(905));
        assertTrue("Fehler: Element 990 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(990));
        theTestList.append(970);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200, 696, 781, 57, 413, 439, 837, 832, 698, 970});

        theTestList.append(877);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200, 696, 781, 57, 413, 439, 837, 832, 698, 970, 877});

        theTestList.append(830);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200, 696, 781, 57, 413, 439, 837, 832, 698, 970, 877, 830});

        theTestList.append(562);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200, 696, 781, 57, 413, 439, 837, 832, 698, 970, 877, 830, 562});

        theTestList.append(217);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200, 696, 781, 57, 413, 439, 837, 832, 698, 970, 877, 830, 562, 217});

        assertTrue("Fehler: Element 626 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(626));
        theTestList.append(79);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200, 696, 781, 57, 413, 439, 837, 832, 698, 970, 877, 830, 562, 217, 79});

        assertTrue("Fehler: Element 340 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(340));
        theTestList.append(678);
        Tester.verifyListContents(theTestList, new int[]{340, 210, 118, 521, 905, 793, 989, 930, 591, 874, 45, 3, 877, 645, 981, 395, 348, 730, 990, 499, 300, 758, 126, 417, 136, 159, 594, 626, 867, 135, 768, 451, 225, 1, 576, 54, 463, 526, 615, 822, 200, 696, 781, 57, 413, 439, 837, 832, 698, 970, 877, 830, 562, 217, 79, 678});

        assertFalse("Fehler: Element 779 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(779));
    }

    public static void test6Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(983);
        Tester.verifyListContents(theTestList, new int[]{983});

        theTestList.append(675);
        Tester.verifyListContents(theTestList, new int[]{983, 675});

        theTestList.append(165);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165});

        theTestList.append(820);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820});

        assertFalse("Fehler: Element 314 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(314));
        assertFalse("Fehler: Element 624 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(624));
        theTestList.append(792);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792});

        theTestList.append(312);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312});

        assertFalse("Fehler: Element 4 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(4));
        theTestList.append(783);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783});

        assertTrue("Fehler: Element 820 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(820));
        assertFalse("Fehler: Element 406 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(406));
        theTestList.append(916);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916});

        assertTrue("Fehler: Element 916 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(916));
        theTestList.append(170);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170});

        theTestList.append(420);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420});

        theTestList.append(157);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157});

        theTestList.append(484);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484});

        assertTrue("Fehler: Element 792 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(792));
        theTestList.append(736);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736});

        assertTrue("Fehler: Element 165 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(165));
        assertFalse("Fehler: Element 13 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(13));
        theTestList.append(328);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328});

        assertTrue("Fehler: Element 484 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(484));
        theTestList.append(881);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881});

        theTestList.append(728);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728});

        theTestList.append(750);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750});

        assertTrue("Fehler: Element 675 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(675));
        theTestList.append(122);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122});

        assertTrue("Fehler: Element 728 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(728));
        theTestList.append(296);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296});

        theTestList.append(21);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21});

        theTestList.append(369);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369});

        theTestList.append(626);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626});

        theTestList.append(903);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903});

        theTestList.append(641);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641});

        theTestList.append(57);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57});

        assertTrue("Fehler: Element 626 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(626));
        theTestList.append(410);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410});

        theTestList.append(908);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908});

        theTestList.append(51);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51});

        theTestList.append(238);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238});

        theTestList.append(564);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564});

        assertTrue("Fehler: Element 484 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(484));
        theTestList.append(467);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467});

        theTestList.append(856);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856});

        assertTrue("Fehler: Element 165 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(165));
        assertFalse("Fehler: Element 201 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(201));
        assertTrue("Fehler: Element 312 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(312));
        assertTrue("Fehler: Element 165 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(165));
        assertFalse("Fehler: Element 965 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(965));
        theTestList.append(723);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723});

        theTestList.append(978);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978});

        assertTrue("Fehler: Element 165 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(165));
        theTestList.append(923);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923});

        theTestList.append(949);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949});

        assertTrue("Fehler: Element 296 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(296));
        theTestList.append(362);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362});

        assertFalse("Fehler: Element 58 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(58));
        theTestList.append(286);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286});

        assertTrue("Fehler: Element 750 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(750));
        assertTrue("Fehler: Element 978 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(978));
        assertFalse("Fehler: Element 207 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(207));
        theTestList.append(35);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35});

        assertTrue("Fehler: Element 238 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(238));
        theTestList.append(941);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941});

        theTestList.append(366);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366});

        assertTrue("Fehler: Element 57 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(57));
        assertFalse("Fehler: Element 381 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(381));
        theTestList.append(481);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481});

        assertTrue("Fehler: Element 35 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(35));
        theTestList.append(183);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183});

        assertTrue("Fehler: Element 312 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(312));
        theTestList.append(712);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183, 712});

        theTestList.append(664);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183, 712, 664});

        assertFalse("Fehler: Element 853 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(853));
        theTestList.append(575);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183, 712, 664, 575});

        assertFalse("Fehler: Element 85 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(85));
        assertFalse("Fehler: Element 739 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(739));
        assertTrue("Fehler: Element 51 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(51));
        theTestList.append(237);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183, 712, 664, 575, 237});

        theTestList.append(831);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183, 712, 664, 575, 237, 831});

        theTestList.append(371);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183, 712, 664, 575, 237, 831, 371});

        assertFalse("Fehler: Element 777 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(777));
        assertFalse("Fehler: Element 613 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(613));
        theTestList.append(143);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183, 712, 664, 575, 237, 831, 371, 143});

        assertTrue("Fehler: Element 51 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(51));
        assertTrue("Fehler: Element 983 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(983));
        theTestList.append(937);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183, 712, 664, 575, 237, 831, 371, 143, 937});

        assertFalse("Fehler: Element 929 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(929));
        assertTrue("Fehler: Element 420 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(420));
        theTestList.append(655);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183, 712, 664, 575, 237, 831, 371, 143, 937, 655});

        theTestList.append(492);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183, 712, 664, 575, 237, 831, 371, 143, 937, 655, 492});

        theTestList.append(134);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183, 712, 664, 575, 237, 831, 371, 143, 937, 655, 492, 134});

        assertTrue("Fehler: Element 923 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(923));
        theTestList.append(309);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183, 712, 664, 575, 237, 831, 371, 143, 937, 655, 492, 134, 309});

        theTestList.append(292);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183, 712, 664, 575, 237, 831, 371, 143, 937, 655, 492, 134, 309, 292});

        theTestList.append(442);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183, 712, 664, 575, 237, 831, 371, 143, 937, 655, 492, 134, 309, 292, 442});

        assertFalse("Fehler: Element 992 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(992));
        theTestList.append(53);
        Tester.verifyListContents(theTestList, new int[]{983, 675, 165, 820, 792, 312, 783, 916, 170, 420, 157, 484, 736, 328, 881, 728, 750, 122, 296, 21, 369, 626, 903, 641, 57, 410, 908, 51, 238, 564, 467, 856, 723, 978, 923, 949, 362, 286, 35, 941, 366, 481, 183, 712, 664, 575, 237, 831, 371, 143, 937, 655, 492, 134, 309, 292, 442, 53});

    }

    public static void test7Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(320);
        Tester.verifyListContents(theTestList, new int[]{320});

        theTestList.append(971);
        Tester.verifyListContents(theTestList, new int[]{320, 971});

        assertFalse("Fehler: Element 566 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(566));
        theTestList.append(281);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281});

        theTestList.append(714);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714});

        assertFalse("Fehler: Element 960 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(960));
        theTestList.append(334);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334});

        theTestList.append(122);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122});

        assertTrue("Fehler: Element 320 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(320));
        theTestList.append(108);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108});

        theTestList.append(484);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484});

        assertTrue("Fehler: Element 320 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(320));
        assertTrue("Fehler: Element 320 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(320));
        assertFalse("Fehler: Element 112 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(112));
        theTestList.append(588);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588});

        theTestList.append(453);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453});

        assertTrue("Fehler: Element 484 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(484));
        theTestList.append(982);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982});

        assertFalse("Fehler: Element 868 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(868));
        assertFalse("Fehler: Element 482 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(482));
        theTestList.append(200);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200});

        theTestList.append(211);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211});

        assertTrue("Fehler: Element 588 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(588));
        assertTrue("Fehler: Element 971 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(971));
        theTestList.append(537);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537});

        theTestList.append(409);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409});

        theTestList.append(896);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896});

        theTestList.append(157);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157});

        theTestList.append(215);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215});

        assertTrue("Fehler: Element 211 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(211));
        assertTrue("Fehler: Element 896 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(896));
        theTestList.append(540);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540});

        theTestList.append(50);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50});

        assertTrue("Fehler: Element 982 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(982));
        assertFalse("Fehler: Element 159 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(159));
        assertFalse("Fehler: Element 848 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(848));
        theTestList.append(631);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631});

        assertFalse("Fehler: Element 777 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(777));
        theTestList.append(823);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823});

        theTestList.append(30);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30});

        theTestList.append(414);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414});

        assertFalse("Fehler: Element 548 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(548));
        theTestList.append(633);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633});

        theTestList.append(596);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596});

        theTestList.append(281);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281});

        theTestList.append(784);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784});

        theTestList.append(964);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964});

        theTestList.append(191);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191});

        assertTrue("Fehler: Element 537 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(537));
        theTestList.append(530);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530});

        assertTrue("Fehler: Element 281 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(281));
        theTestList.append(301);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301});

        theTestList.append(384);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384});

        theTestList.append(937);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937});

        theTestList.append(845);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845});

        assertTrue("Fehler: Element 484 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(484));
        assertFalse("Fehler: Element 574 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(574));
        assertFalse("Fehler: Element 119 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(119));
        theTestList.append(948);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948});

        theTestList.append(466);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466});

        theTestList.append(774);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774});

        assertTrue("Fehler: Element 823 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(823));
        assertTrue("Fehler: Element 50 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(50));
        theTestList.append(436);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436});

        theTestList.append(120);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120});

        theTestList.append(493);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493});

        theTestList.append(377);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377});

        theTestList.append(507);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507});

        assertTrue("Fehler: Element 120 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(120));
        theTestList.append(744);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507, 744});

        assertTrue("Fehler: Element 215 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(215));
        assertTrue("Fehler: Element 845 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(845));
        theTestList.append(702);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507, 744, 702});

        theTestList.append(329);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507, 744, 702, 329});

        theTestList.append(473);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507, 744, 702, 329, 473});

        assertTrue("Fehler: Element 484 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(484));
        assertTrue("Fehler: Element 120 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(120));
        theTestList.append(328);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507, 744, 702, 329, 473, 328});

        theTestList.append(175);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507, 744, 702, 329, 473, 328, 175});

        theTestList.append(690);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507, 744, 702, 329, 473, 328, 175, 690});

        theTestList.append(596);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507, 744, 702, 329, 473, 328, 175, 690, 596});

        assertFalse("Fehler: Element 799 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(799));
        assertTrue("Fehler: Element 537 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(537));
        theTestList.append(682);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507, 744, 702, 329, 473, 328, 175, 690, 596, 682});

        assertFalse("Fehler: Element 683 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(683));
        theTestList.append(238);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507, 744, 702, 329, 473, 328, 175, 690, 596, 682, 238});

        theTestList.append(851);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507, 744, 702, 329, 473, 328, 175, 690, 596, 682, 238, 851});

        assertFalse("Fehler: Element 831 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(831));
        assertFalse("Fehler: Element 307 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(307));
        assertFalse("Fehler: Element 965 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(965));
        assertFalse("Fehler: Element 127 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(127));
        assertTrue("Fehler: Element 414 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(414));
        theTestList.append(109);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507, 744, 702, 329, 473, 328, 175, 690, 596, 682, 238, 851, 109});

        assertTrue("Fehler: Element 964 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(964));
        theTestList.append(491);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507, 744, 702, 329, 473, 328, 175, 690, 596, 682, 238, 851, 109, 491});

        assertFalse("Fehler: Element 809 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(809));
        assertTrue("Fehler: Element 823 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(823));
        assertFalse("Fehler: Element 383 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(383));
        theTestList.append(134);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507, 744, 702, 329, 473, 328, 175, 690, 596, 682, 238, 851, 109, 491, 134});

        theTestList.append(558);
        Tester.verifyListContents(theTestList, new int[]{320, 971, 281, 714, 334, 122, 108, 484, 588, 453, 982, 200, 211, 537, 409, 896, 157, 215, 540, 50, 631, 823, 30, 414, 633, 596, 281, 784, 964, 191, 530, 301, 384, 937, 845, 948, 466, 774, 436, 120, 493, 377, 507, 744, 702, 329, 473, 328, 175, 690, 596, 682, 238, 851, 109, 491, 134, 558});

    }

    public static void test8Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(804);
        Tester.verifyListContents(theTestList, new int[]{804});

        assertTrue("Fehler: Element 804 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(804));
        theTestList.append(93);
        Tester.verifyListContents(theTestList, new int[]{804, 93});

        theTestList.append(451);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451});

        theTestList.append(123);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123});

        theTestList.append(529);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529});

        assertFalse("Fehler: Element 298 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(298));
        assertTrue("Fehler: Element 93 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(93));
        theTestList.append(124);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124});

        theTestList.append(257);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257});

        assertTrue("Fehler: Element 123 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(123));
        theTestList.append(928);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928});

        theTestList.append(629);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629});

        assertTrue("Fehler: Element 124 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(124));
        assertTrue("Fehler: Element 804 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(804));
        assertFalse("Fehler: Element 512 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(512));
        assertFalse("Fehler: Element 750 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(750));
        assertTrue("Fehler: Element 123 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(123));
        theTestList.append(504);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504});

        theTestList.append(841);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841});

        assertTrue("Fehler: Element 804 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(804));
        assertTrue("Fehler: Element 504 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(504));
        assertTrue("Fehler: Element 841 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(841));
        theTestList.append(788);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788});

        assertTrue("Fehler: Element 928 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(928));
        assertTrue("Fehler: Element 257 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(257));
        assertTrue("Fehler: Element 841 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(841));
        assertTrue("Fehler: Element 123 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(123));
        assertTrue("Fehler: Element 928 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(928));
        assertTrue("Fehler: Element 788 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(788));
        theTestList.append(589);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589});

        assertTrue("Fehler: Element 928 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(928));
        theTestList.append(836);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836});

        assertFalse("Fehler: Element 904 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(904));
        theTestList.append(336);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336});

        theTestList.append(315);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315});

        theTestList.append(957);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957});

        assertFalse("Fehler: Element 732 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(732));
        theTestList.append(350);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350});

        assertFalse("Fehler: Element 581 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(581));
        theTestList.append(789);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789});

        theTestList.append(279);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279});

        assertFalse("Fehler: Element 392 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(392));
        assertTrue("Fehler: Element 529 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(529));
        theTestList.append(288);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288});

        assertTrue("Fehler: Element 804 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(804));
        theTestList.append(158);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158});

        assertTrue("Fehler: Element 279 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(279));
        assertTrue("Fehler: Element 928 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(928));
        assertFalse("Fehler: Element 136 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(136));
        assertTrue("Fehler: Element 336 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(336));
        theTestList.append(255);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255});

        theTestList.append(669);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669});

        theTestList.append(877);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877});

        theTestList.append(359);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359});

        theTestList.append(145);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145});

        assertFalse("Fehler: Element 996 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(996));
        theTestList.append(392);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392});

        assertTrue("Fehler: Element 957 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(957));
        assertTrue("Fehler: Element 279 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(279));
        theTestList.append(569);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569});

        theTestList.append(250);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250});

        theTestList.append(413);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413});

        assertFalse("Fehler: Element 998 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(998));
        theTestList.append(670);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670});

        theTestList.append(925);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925});

        theTestList.append(650);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925, 650});

        assertFalse("Fehler: Element 640 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(640));
        assertFalse("Fehler: Element 690 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(690));
        theTestList.append(383);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925, 650, 383});

        theTestList.append(789);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925, 650, 383, 789});

        assertTrue("Fehler: Element 670 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(670));
        assertTrue("Fehler: Element 669 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(669));
        assertTrue("Fehler: Element 255 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(255));
        theTestList.append(291);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925, 650, 383, 789, 291});

        theTestList.append(927);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925, 650, 383, 789, 291, 927});

        theTestList.append(844);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925, 650, 383, 789, 291, 927, 844});

        assertTrue("Fehler: Element 413 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(413));
        theTestList.append(259);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925, 650, 383, 789, 291, 927, 844, 259});

        assertTrue("Fehler: Element 93 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(93));
        assertTrue("Fehler: Element 877 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(877));
        theTestList.append(194);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925, 650, 383, 789, 291, 927, 844, 259, 194});

        assertFalse("Fehler: Element 303 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(303));
        assertFalse("Fehler: Element 508 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(508));
        assertTrue("Fehler: Element 925 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(925));
        theTestList.append(713);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925, 650, 383, 789, 291, 927, 844, 259, 194, 713});

        assertTrue("Fehler: Element 257 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(257));
        assertFalse("Fehler: Element 244 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(244));
        assertTrue("Fehler: Element 569 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(569));
        theTestList.append(651);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925, 650, 383, 789, 291, 927, 844, 259, 194, 713, 651});

        theTestList.append(843);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925, 650, 383, 789, 291, 927, 844, 259, 194, 713, 651, 843});

        assertTrue("Fehler: Element 145 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(145));
        assertFalse("Fehler: Element 818 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(818));
        assertTrue("Fehler: Element 158 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(158));
        assertFalse("Fehler: Element 619 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(619));
        theTestList.append(914);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925, 650, 383, 789, 291, 927, 844, 259, 194, 713, 651, 843, 914});

        theTestList.append(643);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925, 650, 383, 789, 291, 927, 844, 259, 194, 713, 651, 843, 914, 643});

        theTestList.append(732);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925, 650, 383, 789, 291, 927, 844, 259, 194, 713, 651, 843, 914, 643, 732});

        theTestList.append(243);
        Tester.verifyListContents(theTestList, new int[]{804, 93, 451, 123, 529, 124, 257, 928, 629, 504, 841, 788, 589, 836, 336, 315, 957, 350, 789, 279, 288, 158, 255, 669, 877, 359, 145, 392, 569, 250, 413, 670, 925, 650, 383, 789, 291, 927, 844, 259, 194, 713, 651, 843, 914, 643, 732, 243});

        assertTrue("Fehler: Element 650 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(650));
    }

    public static void test9Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(845);
        Tester.verifyListContents(theTestList, new int[]{845});

        theTestList.append(166);
        Tester.verifyListContents(theTestList, new int[]{845, 166});

        assertTrue("Fehler: Element 845 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(845));
        theTestList.append(961);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961});

        assertTrue("Fehler: Element 961 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(961));
        theTestList.append(772);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772});

        theTestList.append(113);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113});

        assertFalse("Fehler: Element 321 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(321));
        theTestList.append(5);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5});

        theTestList.append(270);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270});

        assertFalse("Fehler: Element 651 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(651));
        theTestList.append(91);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91});

        theTestList.append(326);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326});

        theTestList.append(110);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110});

        assertTrue("Fehler: Element 270 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(270));
        theTestList.append(314);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314});

        theTestList.append(972);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972});

        assertTrue("Fehler: Element 972 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(972));
        theTestList.append(174);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174});

        theTestList.append(743);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743});

        assertTrue("Fehler: Element 166 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(166));
        assertTrue("Fehler: Element 743 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(743));
        theTestList.append(434);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434});

        theTestList.append(255);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255});

        theTestList.append(69);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69});

        assertFalse("Fehler: Element 124 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(124));
        theTestList.append(419);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419});

        assertTrue("Fehler: Element 110 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(110));
        theTestList.append(241);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241});

        theTestList.append(226);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226});

        assertFalse("Fehler: Element 905 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(905));
        assertFalse("Fehler: Element 288 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(288));
        theTestList.append(847);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847});

        theTestList.append(567);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567});

        assertFalse("Fehler: Element 269 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(269));
        theTestList.append(952);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952});

        assertFalse("Fehler: Element 513 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(513));
        theTestList.append(395);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395});

        assertFalse("Fehler: Element 744 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(744));
        theTestList.append(255);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255});

        assertFalse("Fehler: Element 714 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(714));
        assertTrue("Fehler: Element 226 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(226));
        theTestList.append(773);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773});

        assertTrue("Fehler: Element 255 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(255));
        assertFalse("Fehler: Element 135 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(135));
        assertTrue("Fehler: Element 952 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(952));
        assertFalse("Fehler: Element 903 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(903));
        assertTrue("Fehler: Element 255 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(255));
        theTestList.append(139);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139});

        assertFalse("Fehler: Element 245 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(245));
        assertFalse("Fehler: Element 858 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(858));
        theTestList.append(907);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907});

        assertTrue("Fehler: Element 961 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(961));
        theTestList.append(453);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453});

        assertTrue("Fehler: Element 255 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(255));
        assertFalse("Fehler: Element 222 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(222));
        assertTrue("Fehler: Element 419 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(419));
        assertTrue("Fehler: Element 226 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(226));
        theTestList.append(480);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480});

        theTestList.append(557);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557});

        theTestList.append(675);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675});

        theTestList.append(362);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362});

        assertFalse("Fehler: Element 331 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(331));
        assertFalse("Fehler: Element 475 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(475));
        theTestList.append(7);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7});

        assertTrue("Fehler: Element 91 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(91));
        assertTrue("Fehler: Element 743 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(743));
        theTestList.append(975);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975});

        theTestList.append(915);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915});

        assertTrue("Fehler: Element 743 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(743));
        assertFalse("Fehler: Element 384 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(384));
        theTestList.append(947);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915, 947});

        theTestList.append(983);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915, 947, 983});

        theTestList.append(99);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915, 947, 983, 99});

        assertTrue("Fehler: Element 255 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(255));
        assertFalse("Fehler: Element 875 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(875));
        assertTrue("Fehler: Element 743 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(743));
        theTestList.append(26);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915, 947, 983, 99, 26});

        theTestList.append(877);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915, 947, 983, 99, 26, 877});

        assertTrue("Fehler: Element 255 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(255));
        assertFalse("Fehler: Element 820 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(820));
        theTestList.append(391);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915, 947, 983, 99, 26, 877, 391});

        theTestList.append(441);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915, 947, 983, 99, 26, 877, 391, 441});

        assertFalse("Fehler: Element 65 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(65));
        assertFalse("Fehler: Element 155 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(155));
        theTestList.append(958);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915, 947, 983, 99, 26, 877, 391, 441, 958});

        theTestList.append(671);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915, 947, 983, 99, 26, 877, 391, 441, 958, 671});

        assertTrue("Fehler: Element 391 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(391));
        theTestList.append(989);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915, 947, 983, 99, 26, 877, 391, 441, 958, 671, 989});

        theTestList.append(849);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915, 947, 983, 99, 26, 877, 391, 441, 958, 671, 989, 849});

        assertFalse("Fehler: Element 51 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(51));
        theTestList.append(519);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915, 947, 983, 99, 26, 877, 391, 441, 958, 671, 989, 849, 519});

        theTestList.append(219);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915, 947, 983, 99, 26, 877, 391, 441, 958, 671, 989, 849, 519, 219});

        assertFalse("Fehler: Element 210 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(210));
        assertTrue("Fehler: Element 907 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(907));
        theTestList.append(622);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915, 947, 983, 99, 26, 877, 391, 441, 958, 671, 989, 849, 519, 219, 622});

        assertTrue("Fehler: Element 983 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(983));
        assertFalse("Fehler: Element 902 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(902));
        assertTrue("Fehler: Element 622 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(622));
        theTestList.append(822);
        Tester.verifyListContents(theTestList, new int[]{845, 166, 961, 772, 113, 5, 270, 91, 326, 110, 314, 972, 174, 743, 434, 255, 69, 419, 241, 226, 847, 567, 952, 395, 255, 773, 139, 907, 453, 480, 557, 675, 362, 7, 975, 915, 947, 983, 99, 26, 877, 391, 441, 958, 671, 989, 849, 519, 219, 622, 822});

    }

    public static void test10Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(368);
        Tester.verifyListContents(theTestList, new int[]{368});

        theTestList.append(495);
        Tester.verifyListContents(theTestList, new int[]{368, 495});

        assertTrue("Fehler: Element 368 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(368));
        theTestList.append(317);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317});

        assertFalse("Fehler: Element 300 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(300));
        theTestList.append(796);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796});

        assertFalse("Fehler: Element 126 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(126));
        assertFalse("Fehler: Element 360 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(360));
        theTestList.append(401);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401});

        assertFalse("Fehler: Element 20 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(20));
        assertTrue("Fehler: Element 401 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(401));
        theTestList.append(870);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870});

        assertTrue("Fehler: Element 401 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(401));
        theTestList.append(632);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632});

        theTestList.append(962);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962});

        theTestList.append(374);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374});

        assertTrue("Fehler: Element 368 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(368));
        assertFalse("Fehler: Element 550 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(550));
        theTestList.append(736);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736});

        theTestList.append(541);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541});

        theTestList.append(432);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432});

        theTestList.append(211);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211});

        theTestList.append(386);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386});

        assertTrue("Fehler: Element 736 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(736));
        theTestList.append(12);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12});

        theTestList.append(168);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168});

        assertFalse("Fehler: Element 959 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(959));
        theTestList.append(863);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863});

        theTestList.append(22);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22});

        assertFalse("Fehler: Element 899 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(899));
        assertFalse("Fehler: Element 940 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(940));
        assertTrue("Fehler: Element 401 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(401));
        theTestList.append(341);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341});

        theTestList.append(254);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254});

        theTestList.append(492);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492});

        theTestList.append(172);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172});

        theTestList.append(465);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465});

        assertFalse("Fehler: Element 611 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(611));
        assertFalse("Fehler: Element 991 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(991));
        theTestList.append(612);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612});

        assertTrue("Fehler: Element 12 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(12));
        assertTrue("Fehler: Element 12 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(12));
        assertFalse("Fehler: Element 694 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(694));
        assertFalse("Fehler: Element 485 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(485));
        theTestList.append(92);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92});

        theTestList.append(186);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186});

        theTestList.append(57);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57});

        theTestList.append(520);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520});

        assertFalse("Fehler: Element 890 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(890));
        assertFalse("Fehler: Element 411 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(411));
        assertTrue("Fehler: Element 92 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(92));
        theTestList.append(978);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978});

        theTestList.append(899);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899});

        theTestList.append(539);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539});

        assertFalse("Fehler: Element 260 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(260));
        theTestList.append(359);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359});

        assertFalse("Fehler: Element 731 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(731));
        theTestList.append(533);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533});

        assertFalse("Fehler: Element 76 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(76));
        theTestList.append(942);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942});

        assertFalse("Fehler: Element 336 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(336));
        theTestList.append(743);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743});

        assertFalse("Fehler: Element 140 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(140));
        assertFalse("Fehler: Element 346 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(346));
        theTestList.append(740);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740});

        theTestList.append(292);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292});

        theTestList.append(691);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691});

        theTestList.append(384);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384});

        theTestList.append(337);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337});

        theTestList.append(363);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337, 363});

        assertTrue("Fehler: Element 363 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(363));
        assertTrue("Fehler: Element 254 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(254));
        assertTrue("Fehler: Element 22 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(22));
        assertTrue("Fehler: Element 186 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(186));
        theTestList.append(391);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337, 363, 391});

        assertTrue("Fehler: Element 386 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(386));
        assertFalse("Fehler: Element 428 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(428));
        theTestList.append(562);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337, 363, 391, 562});

        assertFalse("Fehler: Element 442 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(442));
        theTestList.append(293);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337, 363, 391, 562, 293});

        assertFalse("Fehler: Element 128 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(128));
        assertTrue("Fehler: Element 368 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(368));
        assertFalse("Fehler: Element 505 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(505));
        theTestList.append(940);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337, 363, 391, 562, 293, 940});

        theTestList.append(887);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337, 363, 391, 562, 293, 940, 887});

        assertTrue("Fehler: Element 612 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(612));
        assertFalse("Fehler: Element 112 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(112));
        assertFalse("Fehler: Element 830 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(830));
        theTestList.append(369);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337, 363, 391, 562, 293, 940, 887, 369});

        assertFalse("Fehler: Element 772 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(772));
        theTestList.append(283);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337, 363, 391, 562, 293, 940, 887, 369, 283});

        theTestList.append(784);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337, 363, 391, 562, 293, 940, 887, 369, 283, 784});

        theTestList.append(706);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337, 363, 391, 562, 293, 940, 887, 369, 283, 784, 706});

        assertTrue("Fehler: Element 292 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(292));
        theTestList.append(425);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337, 363, 391, 562, 293, 940, 887, 369, 283, 784, 706, 425});

        theTestList.append(533);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337, 363, 391, 562, 293, 940, 887, 369, 283, 784, 706, 425, 533});

        theTestList.append(378);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337, 363, 391, 562, 293, 940, 887, 369, 283, 784, 706, 425, 533, 378});

        theTestList.append(563);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337, 363, 391, 562, 293, 940, 887, 369, 283, 784, 706, 425, 533, 378, 563});

        theTestList.append(322);
        Tester.verifyListContents(theTestList, new int[]{368, 495, 317, 796, 401, 870, 632, 962, 374, 736, 541, 432, 211, 386, 12, 168, 863, 22, 341, 254, 492, 172, 465, 612, 92, 186, 57, 520, 978, 899, 539, 359, 533, 942, 743, 740, 292, 691, 384, 337, 363, 391, 562, 293, 940, 887, 369, 283, 784, 706, 425, 533, 378, 563, 322});

        assertFalse("Fehler: Element 851 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(851));
    }
}
