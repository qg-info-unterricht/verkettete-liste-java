package tests;

import static org.junit.Assert.*;

import liste.*;

/**
 * Die Test-Klasse SubTest4.
 *
 * @author Rainer Helfrich
 * @version 03.10.2020
 */
public class SubTest4 {


    public static void test1Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 245);
        Tester.verifyListContents(theTestList, new int[]{245});

        theTestList.append(907);
        Tester.verifyListContents(theTestList, new int[]{245, 907});

        theTestList.append(837);
        Tester.verifyListContents(theTestList, new int[]{245, 907, 837});

        assertFalse("Fehler: Element 897 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(897));
        theTestList.insertAt(3, 307);
        Tester.verifyListContents(theTestList, new int[]{245, 907, 837, 307});

        theTestList.insertAt(1, 934);
        Tester.verifyListContents(theTestList, new int[]{245, 934, 907, 837, 307});

        theTestList.append(498);
        Tester.verifyListContents(theTestList, new int[]{245, 934, 907, 837, 307, 498});

        theTestList.append(683);
        Tester.verifyListContents(theTestList, new int[]{245, 934, 907, 837, 307, 498, 683});

        theTestList.insertAt(7, 882);
        Tester.verifyListContents(theTestList, new int[]{245, 934, 907, 837, 307, 498, 683, 882});

        assertFalse("Fehler: Element 435 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(435));
        assertFalse("Fehler: Element 483 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(483));
        theTestList.append(906);
        Tester.verifyListContents(theTestList, new int[]{245, 934, 907, 837, 307, 498, 683, 882, 906});

        theTestList.append(908);
        Tester.verifyListContents(theTestList, new int[]{245, 934, 907, 837, 307, 498, 683, 882, 906, 908});

        theTestList.append(820);
        Tester.verifyListContents(theTestList, new int[]{245, 934, 907, 837, 307, 498, 683, 882, 906, 908, 820});

        theTestList.append(730);
        Tester.verifyListContents(theTestList, new int[]{245, 934, 907, 837, 307, 498, 683, 882, 906, 908, 820, 730});

        theTestList.insertAt(6, 688);
        Tester.verifyListContents(theTestList, new int[]{245, 934, 907, 837, 307, 498, 688, 683, 882, 906, 908, 820, 730});

        theTestList.append(441);
        Tester.verifyListContents(theTestList, new int[]{245, 934, 907, 837, 307, 498, 688, 683, 882, 906, 908, 820, 730, 441});

        theTestList.append(256);
        Tester.verifyListContents(theTestList, new int[]{245, 934, 907, 837, 307, 498, 688, 683, 882, 906, 908, 820, 730, 441, 256});

        theTestList.append(848);
        Tester.verifyListContents(theTestList, new int[]{245, 934, 907, 837, 307, 498, 688, 683, 882, 906, 908, 820, 730, 441, 256, 848});

        theTestList.insertAt(6, 743);
        Tester.verifyListContents(theTestList, new int[]{245, 934, 907, 837, 307, 498, 743, 688, 683, 882, 906, 908, 820, 730, 441, 256, 848});

        theTestList.append(43);
        Tester.verifyListContents(theTestList, new int[]{245, 934, 907, 837, 307, 498, 743, 688, 683, 882, 906, 908, 820, 730, 441, 256, 848, 43});

        theTestList.insertAt(0, 676);
        Tester.verifyListContents(theTestList, new int[]{676, 245, 934, 907, 837, 307, 498, 743, 688, 683, 882, 906, 908, 820, 730, 441, 256, 848, 43});

        theTestList.append(166);
        Tester.verifyListContents(theTestList, new int[]{676, 245, 934, 907, 837, 307, 498, 743, 688, 683, 882, 906, 908, 820, 730, 441, 256, 848, 43, 166});

        assertFalse("Fehler: Element 605 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(605));
        assertFalse("Fehler: Element 854 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(854));
        theTestList.insertAt(3, 679);
        Tester.verifyListContents(theTestList, new int[]{676, 245, 934, 679, 907, 837, 307, 498, 743, 688, 683, 882, 906, 908, 820, 730, 441, 256, 848, 43, 166});

        theTestList.insertAt(21, 975);
        Tester.verifyListContents(theTestList, new int[]{676, 245, 934, 679, 907, 837, 307, 498, 743, 688, 683, 882, 906, 908, 820, 730, 441, 256, 848, 43, 166, 975});

        assertFalse("Fehler: Element 914 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(914));
        theTestList.append(71);
        Tester.verifyListContents(theTestList, new int[]{676, 245, 934, 679, 907, 837, 307, 498, 743, 688, 683, 882, 906, 908, 820, 730, 441, 256, 848, 43, 166, 975, 71});

        theTestList.insertAt(15, 177);
        Tester.verifyListContents(theTestList, new int[]{676, 245, 934, 679, 907, 837, 307, 498, 743, 688, 683, 882, 906, 908, 820, 177, 730, 441, 256, 848, 43, 166, 975, 71});

        theTestList.append(860);
        Tester.verifyListContents(theTestList, new int[]{676, 245, 934, 679, 907, 837, 307, 498, 743, 688, 683, 882, 906, 908, 820, 177, 730, 441, 256, 848, 43, 166, 975, 71, 860});

        theTestList.insertAt(16, 545);
        Tester.verifyListContents(theTestList, new int[]{676, 245, 934, 679, 907, 837, 307, 498, 743, 688, 683, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 166, 975, 71, 860});

        theTestList.append(906);
        Tester.verifyListContents(theTestList, new int[]{676, 245, 934, 679, 907, 837, 307, 498, 743, 688, 683, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 166, 975, 71, 860, 906});

        assertFalse("Fehler: Element 368 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(368));
        assertTrue("Fehler: Element 71 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(71));
        assertTrue("Fehler: Element 975 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(975));
        theTestList.append(488);
        Tester.verifyListContents(theTestList, new int[]{676, 245, 934, 679, 907, 837, 307, 498, 743, 688, 683, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 166, 975, 71, 860, 906, 488});

        theTestList.insertAt(11, 126);
        Tester.verifyListContents(theTestList, new int[]{676, 245, 934, 679, 907, 837, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 166, 975, 71, 860, 906, 488});

        theTestList.append(77);
        Tester.verifyListContents(theTestList, new int[]{676, 245, 934, 679, 907, 837, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 166, 975, 71, 860, 906, 488, 77});

        theTestList.insertAt(6, 958);
        Tester.verifyListContents(theTestList, new int[]{676, 245, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 166, 975, 71, 860, 906, 488, 77});

        theTestList.append(356);
        Tester.verifyListContents(theTestList, new int[]{676, 245, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 166, 975, 71, 860, 906, 488, 77, 356});

        assertFalse("Fehler: Element 49 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(49));
        theTestList.insertAt(0, 14);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 166, 975, 71, 860, 906, 488, 77, 356});

        theTestList.insertAt(33, 607);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 166, 975, 71, 860, 906, 488, 77, 356, 607});

        theTestList.insertAt(3, 553);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 166, 975, 71, 860, 906, 488, 77, 356, 607});

        theTestList.append(97);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 166, 975, 71, 860, 906, 488, 77, 356, 607, 97});

        theTestList.insertAt(34, 795);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 166, 975, 71, 860, 906, 488, 77, 356, 795, 607, 97});

        assertFalse("Fehler: Element 223 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(223));
        theTestList.insertAt(4, 777);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 166, 975, 71, 860, 906, 488, 77, 356, 795, 607, 97});

        theTestList.insertAt(37, 920);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 166, 975, 71, 860, 906, 488, 77, 356, 795, 607, 920, 97});

        assertFalse("Fehler: Element 251 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(251));
        theTestList.insertAt(27, 742);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 742, 166, 975, 71, 860, 906, 488, 77, 356, 795, 607, 920, 97});

        assertFalse("Fehler: Element 543 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(543));
        theTestList.insertAt(28, 656);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 356, 795, 607, 920, 97});

        assertTrue("Fehler: Element 920 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(920));
        theTestList.insertAt(26, 478);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 177, 545, 730, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 356, 795, 607, 920, 97});

        assertTrue("Fehler: Element 307 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(307));
        theTestList.insertAt(20, 462);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 356, 795, 607, 920, 97});

        theTestList.append(217);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 356, 795, 607, 920, 97, 217});

        assertTrue("Fehler: Element 256 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(256));
        assertTrue("Fehler: Element 462 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(462));
        theTestList.insertAt(39, 591);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 356, 591, 795, 607, 920, 97, 217});

        theTestList.append(710);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 356, 591, 795, 607, 920, 97, 217, 710});

        theTestList.insertAt(24, 360);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 356, 591, 795, 607, 920, 97, 217, 710});

        assertFalse("Fehler: Element 146 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(146));
        theTestList.append(861);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 356, 591, 795, 607, 920, 97, 217, 710, 861});

        assertFalse("Fehler: Element 86 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(86));
        assertFalse("Fehler: Element 804 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(804));
        assertFalse("Fehler: Element 394 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(394));
        assertTrue("Fehler: Element 97 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(97));
        theTestList.append(165);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165});

        assertTrue("Fehler: Element 860 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(860));
        assertFalse("Fehler: Element 470 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(470));
        theTestList.insertAt(39, 501);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165});

        theTestList.append(383);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165, 383});

        theTestList.append(254);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165, 383, 254});

        theTestList.append(166);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165, 383, 254, 166});

        theTestList.insertAt(4, 504);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 504, 777, 934, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165, 383, 254, 166});

        assertFalse("Fehler: Element 415 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(415));
        theTestList.insertAt(7, 748);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 504, 777, 934, 748, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165, 383, 254, 166});

        assertFalse("Fehler: Element 222 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(222));
        theTestList.insertAt(5, 442);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 504, 442, 777, 934, 748, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165, 383, 254, 166});

        assertFalse("Fehler: Element 994 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(994));
        assertTrue("Fehler: Element 934 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(934));
        theTestList.append(970);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 504, 442, 777, 934, 748, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165, 383, 254, 166, 970});

        assertTrue("Fehler: Element 383 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(383));
        theTestList.append(589);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 504, 442, 777, 934, 748, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165, 383, 254, 166, 970, 589});

        assertFalse("Fehler: Element 537 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(537));
        theTestList.append(124);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 504, 442, 777, 934, 748, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165, 383, 254, 166, 970, 589, 124});

        theTestList.append(853);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 504, 442, 777, 934, 748, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165, 383, 254, 166, 970, 589, 124, 853});

        assertTrue("Fehler: Element 908 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(908));
        theTestList.insertAt(21, 489);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 504, 442, 777, 934, 748, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 489, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165, 383, 254, 166, 970, 589, 124, 853});

        theTestList.insertAt(38, 234);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 504, 442, 777, 934, 748, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 489, 908, 820, 462, 177, 545, 730, 360, 441, 256, 848, 478, 43, 742, 656, 166, 975, 234, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165, 383, 254, 166, 970, 589, 124, 853});

        assertFalse("Fehler: Element 875 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(875));
        assertTrue("Fehler: Element 837 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(837));
        theTestList.insertAt(30, 133);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 504, 442, 777, 934, 748, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 489, 908, 820, 462, 177, 545, 730, 360, 441, 133, 256, 848, 478, 43, 742, 656, 166, 975, 234, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165, 383, 254, 166, 970, 589, 124, 853});

        assertTrue("Fehler: Element 478 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(478));
        assertTrue("Fehler: Element 489 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(489));
        theTestList.insertAt(59, 990);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 504, 442, 777, 934, 748, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 489, 908, 820, 462, 177, 545, 730, 360, 441, 133, 256, 848, 478, 43, 742, 656, 166, 975, 234, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165, 383, 254, 166, 990, 970, 589, 124, 853});

        theTestList.append(625);
        Tester.verifyListContents(theTestList, new int[]{14, 676, 245, 553, 504, 442, 777, 934, 748, 679, 907, 837, 958, 307, 498, 743, 688, 683, 126, 882, 906, 489, 908, 820, 462, 177, 545, 730, 360, 441, 133, 256, 848, 478, 43, 742, 656, 166, 975, 234, 71, 860, 906, 488, 77, 501, 356, 591, 795, 607, 920, 97, 217, 710, 861, 165, 383, 254, 166, 990, 970, 589, 124, 853, 625});

    }

    public static void test2Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(549);
        Tester.verifyListContents(theTestList, new int[]{549});

        theTestList.insertAt(1, 581);
        Tester.verifyListContents(theTestList, new int[]{549, 581});

        theTestList.insertAt(1, 368);
        Tester.verifyListContents(theTestList, new int[]{549, 368, 581});

        assertTrue("Fehler: Element 368 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(368));
        theTestList.insertAt(1, 748);
        Tester.verifyListContents(theTestList, new int[]{549, 748, 368, 581});

        theTestList.insertAt(0, 102);
        Tester.verifyListContents(theTestList, new int[]{102, 549, 748, 368, 581});

        assertFalse("Fehler: Element 821 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(821));
        theTestList.append(189);
        Tester.verifyListContents(theTestList, new int[]{102, 549, 748, 368, 581, 189});

        assertFalse("Fehler: Element 56 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(56));
        theTestList.append(657);
        Tester.verifyListContents(theTestList, new int[]{102, 549, 748, 368, 581, 189, 657});

        theTestList.append(358);
        Tester.verifyListContents(theTestList, new int[]{102, 549, 748, 368, 581, 189, 657, 358});

        theTestList.append(706);
        Tester.verifyListContents(theTestList, new int[]{102, 549, 748, 368, 581, 189, 657, 358, 706});

        assertTrue("Fehler: Element 368 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(368));
        theTestList.append(417);
        Tester.verifyListContents(theTestList, new int[]{102, 549, 748, 368, 581, 189, 657, 358, 706, 417});

        theTestList.append(824);
        Tester.verifyListContents(theTestList, new int[]{102, 549, 748, 368, 581, 189, 657, 358, 706, 417, 824});

        theTestList.append(506);
        Tester.verifyListContents(theTestList, new int[]{102, 549, 748, 368, 581, 189, 657, 358, 706, 417, 824, 506});

        theTestList.append(260);
        Tester.verifyListContents(theTestList, new int[]{102, 549, 748, 368, 581, 189, 657, 358, 706, 417, 824, 506, 260});

        assertTrue("Fehler: Element 549 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(549));
        assertTrue("Fehler: Element 189 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(189));
        theTestList.append(66);
        Tester.verifyListContents(theTestList, new int[]{102, 549, 748, 368, 581, 189, 657, 358, 706, 417, 824, 506, 260, 66});

        theTestList.insertAt(2, 855);
        Tester.verifyListContents(theTestList, new int[]{102, 549, 855, 748, 368, 581, 189, 657, 358, 706, 417, 824, 506, 260, 66});

        assertFalse("Fehler: Element 723 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(723));
        theTestList.insertAt(13, 8);
        Tester.verifyListContents(theTestList, new int[]{102, 549, 855, 748, 368, 581, 189, 657, 358, 706, 417, 824, 506, 8, 260, 66});

        theTestList.insertAt(15, 226);
        Tester.verifyListContents(theTestList, new int[]{102, 549, 855, 748, 368, 581, 189, 657, 358, 706, 417, 824, 506, 8, 260, 226, 66});

        assertFalse("Fehler: Element 44 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(44));
        theTestList.append(689);
        Tester.verifyListContents(theTestList, new int[]{102, 549, 855, 748, 368, 581, 189, 657, 358, 706, 417, 824, 506, 8, 260, 226, 66, 689});

        assertTrue("Fehler: Element 358 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(358));
        theTestList.insertAt(7, 2);
        Tester.verifyListContents(theTestList, new int[]{102, 549, 855, 748, 368, 581, 189, 2, 657, 358, 706, 417, 824, 506, 8, 260, 226, 66, 689});

        theTestList.insertAt(1, 267);
        Tester.verifyListContents(theTestList, new int[]{102, 267, 549, 855, 748, 368, 581, 189, 2, 657, 358, 706, 417, 824, 506, 8, 260, 226, 66, 689});

        theTestList.insertAt(16, 861);
        Tester.verifyListContents(theTestList, new int[]{102, 267, 549, 855, 748, 368, 581, 189, 2, 657, 358, 706, 417, 824, 506, 8, 861, 260, 226, 66, 689});

        assertTrue("Fehler: Element 824 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(824));
        theTestList.append(953);
        Tester.verifyListContents(theTestList, new int[]{102, 267, 549, 855, 748, 368, 581, 189, 2, 657, 358, 706, 417, 824, 506, 8, 861, 260, 226, 66, 689, 953});

        theTestList.insertAt(1, 677);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 368, 581, 189, 2, 657, 358, 706, 417, 824, 506, 8, 861, 260, 226, 66, 689, 953});

        theTestList.insertAt(22, 950);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 368, 581, 189, 2, 657, 358, 706, 417, 824, 506, 8, 861, 260, 226, 66, 689, 950, 953});

        theTestList.insertAt(8, 163);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 368, 581, 163, 189, 2, 657, 358, 706, 417, 824, 506, 8, 861, 260, 226, 66, 689, 950, 953});

        theTestList.insertAt(20, 21);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 368, 581, 163, 189, 2, 657, 358, 706, 417, 824, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953});

        assertFalse("Fehler: Element 449 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(449));
        assertTrue("Fehler: Element 861 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(861));
        theTestList.append(972);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 368, 581, 163, 189, 2, 657, 358, 706, 417, 824, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972});

        theTestList.insertAt(16, 62);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 368, 581, 163, 189, 2, 657, 358, 706, 417, 824, 62, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972});

        assertFalse("Fehler: Element 171 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(171));
        theTestList.insertAt(7, 637);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 368, 637, 581, 163, 189, 2, 657, 358, 706, 417, 824, 62, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972});

        theTestList.insertAt(7, 788);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 368, 788, 637, 581, 163, 189, 2, 657, 358, 706, 417, 824, 62, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972});

        theTestList.insertAt(19, 625);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 368, 788, 637, 581, 163, 189, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972});

        theTestList.append(524);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 368, 788, 637, 581, 163, 189, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524});

        assertTrue("Fehler: Element 102 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(102));
        assertFalse("Fehler: Element 16 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(16));
        theTestList.append(113);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 368, 788, 637, 581, 163, 189, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113});

        theTestList.append(899);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 368, 788, 637, 581, 163, 189, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113, 899});

        theTestList.append(860);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 368, 788, 637, 581, 163, 189, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113, 899, 860});

        theTestList.insertAt(6, 360);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113, 899, 860});

        theTestList.append(312);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113, 899, 860, 312});

        theTestList.insertAt(13, 949);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113, 899, 860, 312});

        theTestList.append(445);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113, 899, 860, 312, 445});

        theTestList.append(156);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113, 899, 860, 312, 445, 156});

        assertTrue("Fehler: Element 824 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(824));
        assertTrue("Fehler: Element 445 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(445));
        theTestList.insertAt(38, 261);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113, 899, 860, 312, 261, 445, 156});

        assertFalse("Fehler: Element 907 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(907));
        theTestList.insertAt(41, 718);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113, 899, 860, 312, 261, 445, 156, 718});

        assertFalse("Fehler: Element 230 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(230));
        assertTrue("Fehler: Element 62 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(62));
        theTestList.insertAt(40, 288);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113, 899, 860, 312, 261, 445, 288, 156, 718});

        theTestList.append(992);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113, 899, 860, 312, 261, 445, 288, 156, 718, 992});

        assertFalse("Fehler: Element 415 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(415));
        theTestList.append(30);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113, 899, 860, 312, 261, 445, 288, 156, 718, 992, 30});

        theTestList.append(157);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113, 899, 860, 312, 261, 445, 288, 156, 718, 992, 30, 157});

        theTestList.append(469);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113, 899, 860, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469});

        theTestList.insertAt(2, 239);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 66, 689, 950, 953, 972, 524, 113, 899, 860, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469});

        theTestList.insertAt(29, 713);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469});

        assertTrue("Fehler: Element 713 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(713));
        assertFalse("Fehler: Element 542 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(542));
        theTestList.insertAt(39, 850);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469});

        theTestList.append(329);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329});

        theTestList.append(776);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776});

        theTestList.insertAt(7, 328);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776});

        theTestList.append(907);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907});

        assertFalse("Fehler: Element 4 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(4));
        theTestList.append(689);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689});

        assertFalse("Fehler: Element 866 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(866));
        theTestList.append(621);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689, 621});

        theTestList.append(442);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689, 621, 442});

        assertTrue("Fehler: Element 62 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(62));
        theTestList.append(800);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689, 621, 442, 800});

        assertFalse("Fehler: Element 480 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(480));
        theTestList.insertAt(30, 864);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 864, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689, 621, 442, 800});

        theTestList.insertAt(9, 440);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 440, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 864, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689, 621, 442, 800});

        assertTrue("Fehler: Element 288 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(288));
        theTestList.append(422);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 440, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 864, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689, 621, 442, 800, 422});

        assertFalse("Fehler: Element 854 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(854));
        theTestList.append(645);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 440, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 864, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689, 621, 442, 800, 422, 645});

        theTestList.append(582);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 440, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 864, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689, 621, 442, 800, 422, 645, 582});

        theTestList.insertAt(10, 362);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 440, 362, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 864, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689, 621, 442, 800, 422, 645, 582});

        theTestList.append(404);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 440, 362, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 226, 864, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689, 621, 442, 800, 422, 645, 582, 404});

        assertFalse("Fehler: Element 719 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(719));
        theTestList.insertAt(31, 904);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 440, 362, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 904, 226, 864, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689, 621, 442, 800, 422, 645, 582, 404});

        theTestList.append(787);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 440, 362, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 904, 226, 864, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689, 621, 442, 800, 422, 645, 582, 404, 787});

        theTestList.append(244);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 440, 362, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 904, 226, 864, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689, 621, 442, 800, 422, 645, 582, 404, 787, 244});

        theTestList.append(570);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 328, 360, 440, 362, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 904, 226, 864, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689, 621, 442, 800, 422, 645, 582, 404, 787, 244, 570});

        theTestList.insertAt(7, 835);
        Tester.verifyListContents(theTestList, new int[]{102, 677, 239, 267, 549, 855, 748, 835, 328, 360, 440, 362, 368, 788, 637, 581, 163, 189, 949, 2, 657, 358, 706, 417, 824, 62, 625, 506, 8, 861, 260, 21, 904, 226, 864, 713, 66, 689, 950, 953, 972, 524, 113, 899, 860, 850, 312, 261, 445, 288, 156, 718, 992, 30, 157, 469, 329, 776, 907, 689, 621, 442, 800, 422, 645, 582, 404, 787, 244, 570});

    }

    public static void test3Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 588);
        Tester.verifyListContents(theTestList, new int[]{588});

        theTestList.append(889);
        Tester.verifyListContents(theTestList, new int[]{588, 889});

        assertTrue("Fehler: Element 889 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(889));
        theTestList.append(884);
        Tester.verifyListContents(theTestList, new int[]{588, 889, 884});

        assertFalse("Fehler: Element 467 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(467));
        theTestList.append(151);
        Tester.verifyListContents(theTestList, new int[]{588, 889, 884, 151});

        theTestList.append(57);
        Tester.verifyListContents(theTestList, new int[]{588, 889, 884, 151, 57});

        theTestList.insertAt(2, 960);
        Tester.verifyListContents(theTestList, new int[]{588, 889, 960, 884, 151, 57});

        assertFalse("Fehler: Element 612 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(612));
        theTestList.append(385);
        Tester.verifyListContents(theTestList, new int[]{588, 889, 960, 884, 151, 57, 385});

        theTestList.append(530);
        Tester.verifyListContents(theTestList, new int[]{588, 889, 960, 884, 151, 57, 385, 530});

        theTestList.insertAt(3, 689);
        Tester.verifyListContents(theTestList, new int[]{588, 889, 960, 689, 884, 151, 57, 385, 530});

        theTestList.append(637);
        Tester.verifyListContents(theTestList, new int[]{588, 889, 960, 689, 884, 151, 57, 385, 530, 637});

        assertTrue("Fehler: Element 960 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(960));
        theTestList.insertAt(8, 647);
        Tester.verifyListContents(theTestList, new int[]{588, 889, 960, 689, 884, 151, 57, 385, 647, 530, 637});

        theTestList.insertAt(4, 391);
        Tester.verifyListContents(theTestList, new int[]{588, 889, 960, 689, 391, 884, 151, 57, 385, 647, 530, 637});

        theTestList.insertAt(9, 196);
        Tester.verifyListContents(theTestList, new int[]{588, 889, 960, 689, 391, 884, 151, 57, 385, 196, 647, 530, 637});

        theTestList.append(271);
        Tester.verifyListContents(theTestList, new int[]{588, 889, 960, 689, 391, 884, 151, 57, 385, 196, 647, 530, 637, 271});

        assertFalse("Fehler: Element 435 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(435));
        assertFalse("Fehler: Element 802 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(802));
        theTestList.append(650);
        Tester.verifyListContents(theTestList, new int[]{588, 889, 960, 689, 391, 884, 151, 57, 385, 196, 647, 530, 637, 271, 650});

        theTestList.insertAt(0, 854);
        Tester.verifyListContents(theTestList, new int[]{854, 588, 889, 960, 689, 391, 884, 151, 57, 385, 196, 647, 530, 637, 271, 650});

        theTestList.insertAt(4, 185);
        Tester.verifyListContents(theTestList, new int[]{854, 588, 889, 960, 185, 689, 391, 884, 151, 57, 385, 196, 647, 530, 637, 271, 650});

        assertTrue("Fehler: Element 588 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(588));
        assertFalse("Fehler: Element 9 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(9));
        theTestList.append(141);
        Tester.verifyListContents(theTestList, new int[]{854, 588, 889, 960, 185, 689, 391, 884, 151, 57, 385, 196, 647, 530, 637, 271, 650, 141});

        theTestList.append(613);
        Tester.verifyListContents(theTestList, new int[]{854, 588, 889, 960, 185, 689, 391, 884, 151, 57, 385, 196, 647, 530, 637, 271, 650, 141, 613});

        theTestList.insertAt(8, 720);
        Tester.verifyListContents(theTestList, new int[]{854, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 385, 196, 647, 530, 637, 271, 650, 141, 613});

        theTestList.insertAt(12, 78);
        Tester.verifyListContents(theTestList, new int[]{854, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 385, 78, 196, 647, 530, 637, 271, 650, 141, 613});

        theTestList.insertAt(20, 432);
        Tester.verifyListContents(theTestList, new int[]{854, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 385, 78, 196, 647, 530, 637, 271, 650, 141, 432, 613});

        theTestList.append(326);
        Tester.verifyListContents(theTestList, new int[]{854, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 385, 78, 196, 647, 530, 637, 271, 650, 141, 432, 613, 326});

        assertTrue("Fehler: Element 78 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(78));
        assertTrue("Fehler: Element 637 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(637));
        theTestList.append(92);
        Tester.verifyListContents(theTestList, new int[]{854, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 385, 78, 196, 647, 530, 637, 271, 650, 141, 432, 613, 326, 92});

        theTestList.insertAt(17, 91);
        Tester.verifyListContents(theTestList, new int[]{854, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 385, 78, 196, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92});

        theTestList.insertAt(1, 743);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 385, 78, 196, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92});

        assertTrue("Fehler: Element 185 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(185));
        theTestList.insertAt(12, 425);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 425, 385, 78, 196, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92});

        assertTrue("Fehler: Element 613 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(613));
        assertTrue("Fehler: Element 196 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(196));
        theTestList.append(598);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 425, 385, 78, 196, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598});

        theTestList.insertAt(13, 481);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 425, 481, 385, 78, 196, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598});

        theTestList.append(195);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 425, 481, 385, 78, 196, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 195});

        assertFalse("Fehler: Element 551 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(551));
        assertFalse("Fehler: Element 550 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(550));
        theTestList.append(582);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 425, 481, 385, 78, 196, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 195, 582});

        theTestList.append(487);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 425, 481, 385, 78, 196, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 195, 582, 487});

        theTestList.insertAt(17, 773);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 425, 481, 385, 78, 196, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 195, 582, 487});

        assertFalse("Fehler: Element 769 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(769));
        theTestList.append(170);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 425, 481, 385, 78, 196, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 195, 582, 487, 170});

        theTestList.insertAt(17, 838);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 425, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 195, 582, 487, 170});

        theTestList.append(883);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 425, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 195, 582, 487, 170, 883});

        theTestList.append(296);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 425, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 195, 582, 487, 170, 883, 296});

        theTestList.append(621);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 425, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 195, 582, 487, 170, 883, 296, 621});

        theTestList.insertAt(31, 46);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 884, 720, 151, 57, 425, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621});

        assertFalse("Fehler: Element 822 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(822));
        theTestList.insertAt(8, 874);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 874, 884, 720, 151, 57, 425, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621});

        assertTrue("Fehler: Element 196 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(196));
        assertFalse("Fehler: Element 20 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(20));
        theTestList.append(179);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 874, 884, 720, 151, 57, 425, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621, 179});

        theTestList.insertAt(8, 744);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 57, 425, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621, 179});

        theTestList.append(293);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 57, 425, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621, 179, 293});

        theTestList.insertAt(15, 871);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 57, 425, 871, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621, 179, 293});

        theTestList.append(361);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 57, 425, 871, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621, 179, 293, 361});

        theTestList.append(301);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 57, 425, 871, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621, 179, 293, 361, 301});

        theTestList.insertAt(44, 974);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 57, 425, 871, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 301});

        theTestList.append(583);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 57, 425, 871, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 301, 583});

        theTestList.append(324);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 57, 425, 871, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 301, 583, 324});

        theTestList.insertAt(13, 446);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 446, 57, 425, 871, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 301, 583, 324});

        assertFalse("Fehler: Element 23 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(23));
        assertTrue("Fehler: Element 324 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(324));
        theTestList.insertAt(47, 505);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 446, 57, 425, 871, 481, 385, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 583, 324});

        theTestList.insertAt(19, 321);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 583, 324});

        assertTrue("Fehler: Element 195 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(195));
        theTestList.insertAt(50, 815);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 815, 583, 324});

        theTestList.append(649);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 815, 583, 324, 649});

        assertFalse("Fehler: Element 451 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(451));
        theTestList.insertAt(38, 36);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 432, 613, 326, 92, 598, 46, 195, 36, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 815, 583, 324, 649});

        theTestList.insertAt(31, 405);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 405, 432, 613, 326, 92, 598, 46, 195, 36, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 815, 583, 324, 649});

        assertTrue("Fehler: Element 588 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(588));
        theTestList.append(692);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 530, 637, 91, 271, 650, 141, 405, 432, 613, 326, 92, 598, 46, 195, 36, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 815, 583, 324, 649, 692});

        theTestList.insertAt(26, 558);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 530, 558, 637, 91, 271, 650, 141, 405, 432, 613, 326, 92, 598, 46, 195, 36, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 815, 583, 324, 649, 692});

        theTestList.append(932);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 530, 558, 637, 91, 271, 650, 141, 405, 432, 613, 326, 92, 598, 46, 195, 36, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 815, 583, 324, 649, 692, 932});

        assertFalse("Fehler: Element 229 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(229));
        theTestList.append(583);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 530, 558, 637, 91, 271, 650, 141, 405, 432, 613, 326, 92, 598, 46, 195, 36, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 815, 583, 324, 649, 692, 932, 583});

        theTestList.append(69);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 530, 558, 637, 91, 271, 650, 141, 405, 432, 613, 326, 92, 598, 46, 195, 36, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 815, 583, 324, 649, 692, 932, 583, 69});

        assertTrue("Fehler: Element 613 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(613));
        assertFalse("Fehler: Element 615 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(615));
        theTestList.insertAt(11, 480);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 480, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 530, 558, 637, 91, 271, 650, 141, 405, 432, 613, 326, 92, 598, 46, 195, 36, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 815, 583, 324, 649, 692, 932, 583, 69});

        assertFalse("Fehler: Element 313 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(313));
        theTestList.append(667);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 480, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 530, 558, 637, 91, 271, 650, 141, 405, 432, 613, 326, 92, 598, 46, 195, 36, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 815, 583, 324, 649, 692, 932, 583, 69, 667});

        theTestList.append(393);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 480, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 530, 558, 637, 91, 271, 650, 141, 405, 432, 613, 326, 92, 598, 46, 195, 36, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 815, 583, 324, 649, 692, 932, 583, 69, 667, 393});

        theTestList.append(830);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 480, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 530, 558, 637, 91, 271, 650, 141, 405, 432, 613, 326, 92, 598, 46, 195, 36, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 815, 583, 324, 649, 692, 932, 583, 69, 667, 393, 830});

        theTestList.insertAt(54, 978);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 480, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 530, 558, 637, 91, 271, 650, 141, 405, 432, 613, 326, 92, 598, 46, 195, 36, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 978, 815, 583, 324, 649, 692, 932, 583, 69, 667, 393, 830});

        theTestList.insertAt(26, 901);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 480, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 901, 530, 558, 637, 91, 271, 650, 141, 405, 432, 613, 326, 92, 598, 46, 195, 36, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 978, 815, 583, 324, 649, 692, 932, 583, 69, 667, 393, 830});

        theTestList.insertAt(64, 207);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 480, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 901, 530, 558, 637, 91, 271, 650, 141, 405, 432, 613, 326, 92, 598, 46, 195, 36, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 978, 815, 583, 324, 649, 692, 932, 583, 69, 207, 667, 393, 830});

        assertFalse("Fehler: Element 851 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(851));
        assertFalse("Fehler: Element 251 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(251));
        assertFalse("Fehler: Element 653 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(653));
        theTestList.append(51);
        Tester.verifyListContents(theTestList, new int[]{854, 743, 588, 889, 960, 185, 689, 391, 744, 874, 884, 480, 720, 151, 446, 57, 425, 871, 481, 385, 321, 78, 196, 838, 773, 647, 901, 530, 558, 637, 91, 271, 650, 141, 405, 432, 613, 326, 92, 598, 46, 195, 36, 582, 487, 170, 883, 296, 621, 179, 293, 974, 361, 505, 301, 978, 815, 583, 324, 649, 692, 932, 583, 69, 207, 667, 393, 830, 51});

    }

    public static void test4Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 390);
        Tester.verifyListContents(theTestList, new int[]{390});

        theTestList.insertAt(1, 75);
        Tester.verifyListContents(theTestList, new int[]{390, 75});

        theTestList.insertAt(2, 282);
        Tester.verifyListContents(theTestList, new int[]{390, 75, 282});

        theTestList.append(653);
        Tester.verifyListContents(theTestList, new int[]{390, 75, 282, 653});

        assertFalse("Fehler: Element 195 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(195));
        theTestList.append(596);
        Tester.verifyListContents(theTestList, new int[]{390, 75, 282, 653, 596});

        theTestList.insertAt(1, 398);
        Tester.verifyListContents(theTestList, new int[]{390, 398, 75, 282, 653, 596});

        assertFalse("Fehler: Element 636 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(636));
        assertTrue("Fehler: Element 282 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(282));
        theTestList.insertAt(0, 461);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 282, 653, 596});

        theTestList.insertAt(4, 586);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 586, 282, 653, 596});

        assertTrue("Fehler: Element 282 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(282));
        theTestList.append(144);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 586, 282, 653, 596, 144});

        theTestList.append(104);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 586, 282, 653, 596, 144, 104});

        theTestList.append(619);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 586, 282, 653, 596, 144, 104, 619});

        assertFalse("Fehler: Element 834 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(834));
        theTestList.insertAt(4, 736);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 736, 586, 282, 653, 596, 144, 104, 619});

        theTestList.append(299);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 736, 586, 282, 653, 596, 144, 104, 619, 299});

        assertTrue("Fehler: Element 461 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(461));
        theTestList.append(617);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 736, 586, 282, 653, 596, 144, 104, 619, 299, 617});

        theTestList.append(233);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 736, 586, 282, 653, 596, 144, 104, 619, 299, 617, 233});

        theTestList.append(92);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 736, 586, 282, 653, 596, 144, 104, 619, 299, 617, 233, 92});

        theTestList.append(306);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 736, 586, 282, 653, 596, 144, 104, 619, 299, 617, 233, 92, 306});

        theTestList.append(225);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 736, 586, 282, 653, 596, 144, 104, 619, 299, 617, 233, 92, 306, 225});

        theTestList.insertAt(11, 157);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 736, 586, 282, 653, 596, 144, 104, 157, 619, 299, 617, 233, 92, 306, 225});

        theTestList.append(678);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 736, 586, 282, 653, 596, 144, 104, 157, 619, 299, 617, 233, 92, 306, 225, 678});

        assertFalse("Fehler: Element 332 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(332));
        theTestList.insertAt(5, 129);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 736, 129, 586, 282, 653, 596, 144, 104, 157, 619, 299, 617, 233, 92, 306, 225, 678});

        theTestList.append(312);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 736, 129, 586, 282, 653, 596, 144, 104, 157, 619, 299, 617, 233, 92, 306, 225, 678, 312});

        theTestList.append(202);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 736, 129, 586, 282, 653, 596, 144, 104, 157, 619, 299, 617, 233, 92, 306, 225, 678, 312, 202});

        assertFalse("Fehler: Element 500 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(500));
        assertFalse("Fehler: Element 264 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(264));
        theTestList.append(965);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 736, 129, 586, 282, 653, 596, 144, 104, 157, 619, 299, 617, 233, 92, 306, 225, 678, 312, 202, 965});

        theTestList.insertAt(7, 312);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 75, 736, 129, 586, 312, 282, 653, 596, 144, 104, 157, 619, 299, 617, 233, 92, 306, 225, 678, 312, 202, 965});

        assertFalse("Fehler: Element 571 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(571));
        theTestList.insertAt(3, 299);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 129, 586, 312, 282, 653, 596, 144, 104, 157, 619, 299, 617, 233, 92, 306, 225, 678, 312, 202, 965});

        assertTrue("Fehler: Element 596 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(596));
        theTestList.insertAt(11, 335);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 129, 586, 312, 282, 653, 335, 596, 144, 104, 157, 619, 299, 617, 233, 92, 306, 225, 678, 312, 202, 965});

        theTestList.append(353);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 129, 586, 312, 282, 653, 335, 596, 144, 104, 157, 619, 299, 617, 233, 92, 306, 225, 678, 312, 202, 965, 353});

        theTestList.append(94);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 129, 586, 312, 282, 653, 335, 596, 144, 104, 157, 619, 299, 617, 233, 92, 306, 225, 678, 312, 202, 965, 353, 94});

        assertFalse("Fehler: Element 928 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(928));
        assertFalse("Fehler: Element 799 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(799));
        assertFalse("Fehler: Element 976 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(976));
        theTestList.append(913);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 129, 586, 312, 282, 653, 335, 596, 144, 104, 157, 619, 299, 617, 233, 92, 306, 225, 678, 312, 202, 965, 353, 94, 913});

        assertFalse("Fehler: Element 473 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(473));
        theTestList.append(264);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 129, 586, 312, 282, 653, 335, 596, 144, 104, 157, 619, 299, 617, 233, 92, 306, 225, 678, 312, 202, 965, 353, 94, 913, 264});

        theTestList.append(468);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 129, 586, 312, 282, 653, 335, 596, 144, 104, 157, 619, 299, 617, 233, 92, 306, 225, 678, 312, 202, 965, 353, 94, 913, 264, 468});

        theTestList.insertAt(17, 868);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 129, 586, 312, 282, 653, 335, 596, 144, 104, 157, 619, 868, 299, 617, 233, 92, 306, 225, 678, 312, 202, 965, 353, 94, 913, 264, 468});

        theTestList.insertAt(20, 707);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 129, 586, 312, 282, 653, 335, 596, 144, 104, 157, 619, 868, 299, 617, 707, 233, 92, 306, 225, 678, 312, 202, 965, 353, 94, 913, 264, 468});

        theTestList.insertAt(33, 781);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 129, 586, 312, 282, 653, 335, 596, 144, 104, 157, 619, 868, 299, 617, 707, 233, 92, 306, 225, 678, 312, 202, 965, 353, 94, 913, 264, 781, 468});

        theTestList.insertAt(35, 786);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 129, 586, 312, 282, 653, 335, 596, 144, 104, 157, 619, 868, 299, 617, 707, 233, 92, 306, 225, 678, 312, 202, 965, 353, 94, 913, 264, 781, 468, 786});

        theTestList.insertAt(33, 481);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 129, 586, 312, 282, 653, 335, 596, 144, 104, 157, 619, 868, 299, 617, 707, 233, 92, 306, 225, 678, 312, 202, 965, 353, 94, 913, 264, 481, 781, 468, 786});

        theTestList.insertAt(6, 25);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 25, 129, 586, 312, 282, 653, 335, 596, 144, 104, 157, 619, 868, 299, 617, 707, 233, 92, 306, 225, 678, 312, 202, 965, 353, 94, 913, 264, 481, 781, 468, 786});

        theTestList.append(451);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 25, 129, 586, 312, 282, 653, 335, 596, 144, 104, 157, 619, 868, 299, 617, 707, 233, 92, 306, 225, 678, 312, 202, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451});

        assertTrue("Fehler: Element 736 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(736));
        theTestList.insertAt(20, 337);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 25, 129, 586, 312, 282, 653, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 233, 92, 306, 225, 678, 312, 202, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451});

        assertFalse("Fehler: Element 381 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(381));
        theTestList.insertAt(12, 980);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 25, 129, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 233, 92, 306, 225, 678, 312, 202, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451});

        assertTrue("Fehler: Element 335 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(335));
        theTestList.insertAt(31, 389);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 25, 129, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451});

        theTestList.append(863);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 25, 129, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863});

        assertTrue("Fehler: Element 282 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(282));
        theTestList.append(287);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 25, 129, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287});

        theTestList.append(110);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 25, 129, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110});

        theTestList.append(989);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 25, 129, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 989});

        theTestList.append(850);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 25, 129, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 989, 850});

        theTestList.append(227);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 25, 129, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 989, 850, 227});

        assertFalse("Fehler: Element 539 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(539));
        assertFalse("Fehler: Element 773 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(773));
        assertTrue("Fehler: Element 312 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(312));
        assertFalse("Fehler: Element 500 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(500));
        theTestList.append(106);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 25, 129, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 989, 850, 227, 106});

        theTestList.insertAt(8, 894);
        Tester.verifyListContents(theTestList, new int[]{461, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 989, 850, 227, 106});

        assertFalse("Fehler: Element 541 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(541));
        theTestList.insertAt(1, 792);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 989, 850, 227, 106});

        theTestList.append(535);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 989, 850, 227, 106, 535});

        theTestList.append(987);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 989, 850, 227, 106, 535, 987});

        assertTrue("Fehler: Element 92 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(92));
        theTestList.append(543);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 989, 850, 227, 106, 535, 987, 543});

        assertTrue("Fehler: Element 282 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(282));
        theTestList.insertAt(26, 813);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 299, 337, 617, 707, 813, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 989, 850, 227, 106, 535, 987, 543});

        theTestList.insertAt(22, 786);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 786, 299, 337, 617, 707, 813, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 989, 850, 227, 106, 535, 987, 543});

        assertTrue("Fehler: Element 264 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(264));
        theTestList.append(400);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 786, 299, 337, 617, 707, 813, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 989, 850, 227, 106, 535, 987, 543, 400});

        assertFalse("Fehler: Element 582 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(582));
        theTestList.insertAt(49, 463);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 786, 299, 337, 617, 707, 813, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 463, 989, 850, 227, 106, 535, 987, 543, 400});

        assertTrue("Fehler: Element 25 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(25));
        assertFalse("Fehler: Element 964 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(964));
        theTestList.append(89);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 786, 299, 337, 617, 707, 813, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 463, 989, 850, 227, 106, 535, 987, 543, 400, 89});

        theTestList.append(190);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 104, 157, 619, 868, 786, 299, 337, 617, 707, 813, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 463, 989, 850, 227, 106, 535, 987, 543, 400, 89, 190});

        theTestList.insertAt(18, 317);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 317, 104, 157, 619, 868, 786, 299, 337, 617, 707, 813, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 463, 989, 850, 227, 106, 535, 987, 543, 400, 89, 190});

        theTestList.insertAt(29, 519);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 317, 104, 157, 619, 868, 786, 299, 337, 617, 707, 813, 519, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 463, 989, 850, 227, 106, 535, 987, 543, 400, 89, 190});

        theTestList.insertAt(53, 144);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 317, 104, 157, 619, 868, 786, 299, 337, 617, 707, 813, 519, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 463, 989, 144, 850, 227, 106, 535, 987, 543, 400, 89, 190});

        theTestList.insertAt(23, 115);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 317, 104, 157, 619, 868, 115, 786, 299, 337, 617, 707, 813, 519, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 463, 989, 144, 850, 227, 106, 535, 987, 543, 400, 89, 190});

        assertTrue("Fehler: Element 144 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(144));
        theTestList.insertAt(41, 991);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 317, 104, 157, 619, 868, 115, 786, 299, 337, 617, 707, 813, 519, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 991, 94, 913, 264, 481, 781, 468, 786, 451, 863, 287, 110, 463, 989, 144, 850, 227, 106, 535, 987, 543, 400, 89, 190});

        theTestList.insertAt(49, 555);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 317, 104, 157, 619, 868, 115, 786, 299, 337, 617, 707, 813, 519, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 991, 94, 913, 264, 481, 781, 468, 786, 555, 451, 863, 287, 110, 463, 989, 144, 850, 227, 106, 535, 987, 543, 400, 89, 190});

        theTestList.insertAt(7, 546);
        Tester.verifyListContents(theTestList, new int[]{461, 792, 390, 398, 299, 75, 736, 546, 25, 129, 894, 586, 312, 282, 653, 980, 335, 596, 144, 317, 104, 157, 619, 868, 115, 786, 299, 337, 617, 707, 813, 519, 233, 92, 306, 225, 678, 312, 202, 389, 965, 353, 991, 94, 913, 264, 481, 781, 468, 786, 555, 451, 863, 287, 110, 463, 989, 144, 850, 227, 106, 535, 987, 543, 400, 89, 190});

        assertFalse("Fehler: Element 691 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(691));
        assertFalse("Fehler: Element 791 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(791));
    }

    public static void test5Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 805);
        Tester.verifyListContents(theTestList, new int[]{805});

        theTestList.append(610);
        Tester.verifyListContents(theTestList, new int[]{805, 610});

        theTestList.append(467);
        Tester.verifyListContents(theTestList, new int[]{805, 610, 467});

        theTestList.insertAt(1, 890);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 467});

        theTestList.append(552);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 467, 552});

        theTestList.append(935);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 467, 552, 935});

        theTestList.append(46);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 467, 552, 935, 46});

        theTestList.insertAt(7, 460);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 467, 552, 935, 46, 460});

        theTestList.append(994);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 467, 552, 935, 46, 460, 994});

        assertTrue("Fehler: Element 552 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(552));
        assertTrue("Fehler: Element 552 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(552));
        theTestList.insertAt(3, 787);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 787, 467, 552, 935, 46, 460, 994});

        assertFalse("Fehler: Element 385 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(385));
        theTestList.append(646);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 787, 467, 552, 935, 46, 460, 994, 646});

        assertFalse("Fehler: Element 934 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(934));
        theTestList.insertAt(3, 234);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 994, 646});

        theTestList.append(297);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 994, 646, 297});

        theTestList.insertAt(11, 99);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 994, 99, 646, 297});

        assertTrue("Fehler: Element 805 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(805));
        assertFalse("Fehler: Element 565 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(565));
        theTestList.insertAt(10, 144);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 144, 994, 99, 646, 297});

        assertFalse("Fehler: Element 247 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(247));
        theTestList.append(417);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 144, 994, 99, 646, 297, 417});

        theTestList.insertAt(14, 433);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 144, 994, 99, 646, 433, 297, 417});

        assertTrue("Fehler: Element 890 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(890));
        theTestList.insertAt(10, 693);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 693, 144, 994, 99, 646, 433, 297, 417});

        theTestList.insertAt(14, 680);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 693, 144, 994, 99, 680, 646, 433, 297, 417});

        theTestList.insertAt(10, 114);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 114, 693, 144, 994, 99, 680, 646, 433, 297, 417});

        theTestList.append(469);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 114, 693, 144, 994, 99, 680, 646, 433, 297, 417, 469});

        theTestList.insertAt(14, 310);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 114, 693, 144, 994, 310, 99, 680, 646, 433, 297, 417, 469});

        theTestList.append(351);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 114, 693, 144, 994, 310, 99, 680, 646, 433, 297, 417, 469, 351});

        theTestList.insertAt(16, 791);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 114, 693, 144, 994, 310, 99, 791, 680, 646, 433, 297, 417, 469, 351});

        theTestList.append(982);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 114, 693, 144, 994, 310, 99, 791, 680, 646, 433, 297, 417, 469, 351, 982});

        theTestList.append(404);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 114, 693, 144, 994, 310, 99, 791, 680, 646, 433, 297, 417, 469, 351, 982, 404});

        theTestList.insertAt(17, 184);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 114, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404});

        theTestList.append(765);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 467, 552, 935, 46, 460, 114, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765});

        theTestList.insertAt(5, 285);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 467, 552, 935, 46, 460, 114, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765});

        theTestList.insertAt(10, 95);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 467, 552, 935, 46, 95, 460, 114, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765});

        theTestList.insertAt(13, 922);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 467, 552, 935, 46, 95, 460, 114, 922, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765});

        theTestList.append(620);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 467, 552, 935, 46, 95, 460, 114, 922, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620});

        theTestList.append(743);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 467, 552, 935, 46, 95, 460, 114, 922, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743});

        assertTrue("Fehler: Element 890 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(890));
        theTestList.append(816);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 467, 552, 935, 46, 95, 460, 114, 922, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816});

        assertFalse("Fehler: Element 269 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(269));
        assertFalse("Fehler: Element 833 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(833));
        theTestList.append(908);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 467, 552, 935, 46, 95, 460, 114, 922, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908});

        assertTrue("Fehler: Element 765 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(765));
        theTestList.insertAt(6, 66);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 66, 467, 552, 935, 46, 95, 460, 114, 922, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908});

        theTestList.insertAt(10, 519);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908});

        theTestList.append(513);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908, 513});

        theTestList.append(427);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908, 513, 427});

        assertFalse("Fehler: Element 528 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(528));
        theTestList.append(961);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908, 513, 427, 961});

        theTestList.append(315);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908, 513, 427, 961, 315});

        theTestList.append(68);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908, 513, 427, 961, 315, 68});

        assertTrue("Fehler: Element 184 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(184));
        assertTrue("Fehler: Element 310 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(310));
        assertFalse("Fehler: Element 418 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(418));
        theTestList.append(170);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908, 513, 427, 961, 315, 68, 170});

        theTestList.insertAt(19, 635);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908, 513, 427, 961, 315, 68, 170});

        theTestList.insertAt(6, 385);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908, 513, 427, 961, 315, 68, 170});

        assertTrue("Fehler: Element 310 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(310));
        theTestList.append(304);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908, 513, 427, 961, 315, 68, 170, 304});

        theTestList.append(89);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908, 513, 427, 961, 315, 68, 170, 304, 89});

        theTestList.append(475);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475});

        theTestList.append(328);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475, 328});

        assertTrue("Fehler: Element 89 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(89));
        assertTrue("Fehler: Element 328 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(328));
        theTestList.append(943);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475, 328, 943});

        theTestList.insertAt(38, 338);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475, 328, 943});

        assertFalse("Fehler: Element 534 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(534));
        theTestList.append(763);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 467, 552, 935, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475, 328, 943, 763});

        theTestList.insertAt(11, 506);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 467, 552, 935, 506, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475, 328, 943, 763});

        theTestList.append(318);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 467, 552, 935, 506, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475, 328, 943, 763, 318});

        theTestList.append(463);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 467, 552, 935, 506, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475, 328, 943, 763, 318, 463});

        theTestList.append(416);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 467, 552, 935, 506, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475, 328, 943, 763, 318, 463, 416});

        theTestList.insertAt(12, 88);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 467, 552, 935, 506, 88, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475, 328, 943, 763, 318, 463, 416});

        theTestList.insertAt(8, 469);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 469, 467, 552, 935, 506, 88, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475, 328, 943, 763, 318, 463, 416});

        assertFalse("Fehler: Element 493 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(493));
        theTestList.append(487);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 469, 467, 552, 935, 506, 88, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 351, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475, 328, 943, 763, 318, 463, 416, 487});

        assertTrue("Fehler: Element 304 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(304));
        theTestList.insertAt(34, 410);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 469, 467, 552, 935, 506, 88, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 410, 351, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475, 328, 943, 763, 318, 463, 416, 487});

        assertTrue("Fehler: Element 805 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(805));
        theTestList.append(867);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 469, 467, 552, 935, 506, 88, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 410, 351, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475, 328, 943, 763, 318, 463, 416, 487, 867});

        theTestList.append(861);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 469, 467, 552, 935, 506, 88, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 410, 351, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475, 328, 943, 763, 318, 463, 416, 487, 867, 861});

        theTestList.insertAt(36, 286);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 469, 467, 552, 935, 506, 88, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 410, 351, 286, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 475, 328, 943, 763, 318, 463, 416, 487, 867, 861});

        assertFalse("Fehler: Element 618 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(618));
        assertFalse("Fehler: Element 383 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(383));
        theTestList.insertAt(53, 741);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 469, 467, 552, 935, 506, 88, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 410, 351, 286, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 741, 475, 328, 943, 763, 318, 463, 416, 487, 867, 861});

        theTestList.insertAt(36, 800);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 469, 467, 552, 935, 506, 88, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 410, 351, 800, 286, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 741, 475, 328, 943, 763, 318, 463, 416, 487, 867, 861});

        assertTrue("Fehler: Element 404 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(404));
        theTestList.insertAt(35, 232);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 469, 467, 552, 935, 506, 88, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 410, 232, 351, 800, 286, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 741, 475, 328, 943, 763, 318, 463, 416, 487, 867, 861});

        theTestList.append(860);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 469, 467, 552, 935, 506, 88, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 410, 232, 351, 800, 286, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 741, 475, 328, 943, 763, 318, 463, 416, 487, 867, 861, 860});

        theTestList.insertAt(10, 976);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 469, 467, 976, 552, 935, 506, 88, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 410, 232, 351, 800, 286, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 741, 475, 328, 943, 763, 318, 463, 416, 487, 867, 861, 860});

        assertTrue("Fehler: Element 610 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(610));
        theTestList.insertAt(10, 686);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 469, 467, 686, 976, 552, 935, 506, 88, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 410, 232, 351, 800, 286, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 741, 475, 328, 943, 763, 318, 463, 416, 487, 867, 861, 860});

        theTestList.insertAt(58, 867);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 469, 467, 686, 976, 552, 935, 506, 88, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 410, 232, 351, 800, 286, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 741, 867, 475, 328, 943, 763, 318, 463, 416, 487, 867, 861, 860});

        assertTrue("Fehler: Element 88 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(88));
        theTestList.insertAt(61, 230);
        Tester.verifyListContents(theTestList, new int[]{805, 890, 610, 234, 787, 285, 385, 66, 469, 467, 686, 976, 552, 935, 506, 88, 519, 46, 95, 460, 114, 922, 693, 144, 994, 635, 310, 99, 791, 184, 680, 646, 433, 297, 417, 469, 410, 232, 351, 800, 286, 982, 404, 765, 620, 743, 816, 338, 908, 513, 427, 961, 315, 68, 170, 304, 89, 741, 867, 475, 328, 230, 943, 763, 318, 463, 416, 487, 867, 861, 860});

        assertTrue("Fehler: Element 994 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(994));
    }

    public static void test6Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 903);
        Tester.verifyListContents(theTestList, new int[]{903});

        assertFalse("Fehler: Element 335 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(335));
        theTestList.append(723);
        Tester.verifyListContents(theTestList, new int[]{903, 723});

        theTestList.append(593);
        Tester.verifyListContents(theTestList, new int[]{903, 723, 593});

        theTestList.append(546);
        Tester.verifyListContents(theTestList, new int[]{903, 723, 593, 546});

        theTestList.append(300);
        Tester.verifyListContents(theTestList, new int[]{903, 723, 593, 546, 300});

        assertFalse("Fehler: Element 507 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(507));
        theTestList.insertAt(1, 529);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 723, 593, 546, 300});

        theTestList.insertAt(3, 914);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 723, 914, 593, 546, 300});

        assertFalse("Fehler: Element 873 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(873));
        theTestList.append(334);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 723, 914, 593, 546, 300, 334});

        theTestList.insertAt(5, 696);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 723, 914, 593, 696, 546, 300, 334});

        theTestList.append(443);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 723, 914, 593, 696, 546, 300, 334, 443});

        theTestList.insertAt(6, 92);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 723, 914, 593, 696, 92, 546, 300, 334, 443});

        theTestList.insertAt(3, 688);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 723, 688, 914, 593, 696, 92, 546, 300, 334, 443});

        theTestList.append(514);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 723, 688, 914, 593, 696, 92, 546, 300, 334, 443, 514});

        assertFalse("Fehler: Element 882 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(882));
        assertTrue("Fehler: Element 514 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(514));
        theTestList.insertAt(2, 17);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 17, 723, 688, 914, 593, 696, 92, 546, 300, 334, 443, 514});

        theTestList.insertAt(7, 326);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 514});

        theTestList.append(47);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 514, 47});

        theTestList.append(752);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 514, 47, 752});

        theTestList.append(836);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 514, 47, 752, 836});

        assertFalse("Fehler: Element 730 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(730));
        assertTrue("Fehler: Element 443 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(443));
        assertTrue("Fehler: Element 514 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(514));
        assertTrue("Fehler: Element 443 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(443));
        assertTrue("Fehler: Element 723 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(723));
        theTestList.insertAt(2, 217);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 217, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 514, 47, 752, 836});

        theTestList.append(624);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 217, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 514, 47, 752, 836, 624});

        theTestList.insertAt(15, 109);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 217, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 514, 47, 752, 836, 624});

        theTestList.append(463);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 217, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 514, 47, 752, 836, 624, 463});

        theTestList.append(559);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 217, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 514, 47, 752, 836, 624, 463, 559});

        assertTrue("Fehler: Element 463 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(463));
        theTestList.append(322);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 217, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 514, 47, 752, 836, 624, 463, 559, 322});

        theTestList.append(890);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 217, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 514, 47, 752, 836, 624, 463, 559, 322, 890});

        assertFalse("Fehler: Element 14 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(14));
        assertTrue("Fehler: Element 17 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(17));
        theTestList.insertAt(2, 342);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 514, 47, 752, 836, 624, 463, 559, 322, 890});

        theTestList.append(228);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 514, 47, 752, 836, 624, 463, 559, 322, 890, 228});

        assertTrue("Fehler: Element 688 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(688));
        assertTrue("Fehler: Element 47 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(47));
        theTestList.insertAt(17, 433);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 433, 514, 47, 752, 836, 624, 463, 559, 322, 890, 228});

        assertTrue("Fehler: Element 47 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(47));
        theTestList.append(731);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 433, 514, 47, 752, 836, 624, 463, 559, 322, 890, 228, 731});

        theTestList.append(583);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 433, 514, 47, 752, 836, 624, 463, 559, 322, 890, 228, 731, 583});

        assertTrue("Fehler: Element 752 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(752));
        theTestList.insertAt(19, 358);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 228, 731, 583});

        assertFalse("Fehler: Element 754 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(754));
        theTestList.insertAt(5, 541);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 228, 731, 583});

        theTestList.insertAt(18, 749);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 228, 731, 583});

        theTestList.append(382);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 228, 731, 583, 382});

        assertFalse("Fehler: Element 974 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(974));
        theTestList.append(783);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 228, 731, 583, 382, 783});

        theTestList.append(335);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 228, 731, 583, 382, 783, 335});

        theTestList.insertAt(31, 12);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 228, 12, 731, 583, 382, 783, 335});

        theTestList.insertAt(18, 752);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 752, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 228, 12, 731, 583, 382, 783, 335});

        assertFalse("Fehler: Element 892 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(892));
        theTestList.append(572);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 752, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 228, 12, 731, 583, 382, 783, 335, 572});

        assertFalse("Fehler: Element 600 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(600));
        theTestList.insertAt(31, 301);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 752, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 731, 583, 382, 783, 335, 572});

        theTestList.insertAt(18, 343);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 343, 752, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 731, 583, 382, 783, 335, 572});

        theTestList.append(622);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 343, 752, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 731, 583, 382, 783, 335, 572, 622});

        theTestList.insertAt(35, 75);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 443, 109, 343, 752, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 75, 731, 583, 382, 783, 335, 572, 622});

        assertTrue("Fehler: Element 343 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(343));
        theTestList.insertAt(16, 689);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 75, 731, 583, 382, 783, 335, 572, 622});

        assertTrue("Fehler: Element 731 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(731));
        theTestList.append(17);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 75, 731, 583, 382, 783, 335, 572, 622, 17});

        assertFalse("Fehler: Element 508 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(508));
        theTestList.append(189);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 75, 731, 583, 382, 783, 335, 572, 622, 17, 189});

        assertFalse("Fehler: Element 242 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(242));
        assertFalse("Fehler: Element 86 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(86));
        assertFalse("Fehler: Element 175 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(175));
        theTestList.append(817);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 75, 731, 583, 382, 783, 335, 572, 622, 17, 189, 817});

        theTestList.insertAt(8, 767);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 75, 731, 583, 382, 783, 335, 572, 622, 17, 189, 817});

        theTestList.insertAt(25, 126);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 75, 731, 583, 382, 783, 335, 572, 622, 17, 189, 817});

        theTestList.insertAt(38, 990);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 990, 75, 731, 583, 382, 783, 335, 572, 622, 17, 189, 817});

        theTestList.append(557);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 990, 75, 731, 583, 382, 783, 335, 572, 622, 17, 189, 817, 557});

        assertTrue("Fehler: Element 572 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(572));
        assertTrue("Fehler: Element 767 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(767));
        assertTrue("Fehler: Element 433 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(433));
        theTestList.append(534);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 990, 75, 731, 583, 382, 783, 335, 572, 622, 17, 189, 817, 557, 534});

        assertFalse("Fehler: Element 501 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(501));
        theTestList.insertAt(45, 665);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 990, 75, 731, 583, 382, 783, 335, 665, 572, 622, 17, 189, 817, 557, 534});

        theTestList.insertAt(52, 868);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 990, 75, 731, 583, 382, 783, 335, 665, 572, 622, 17, 189, 817, 557, 868, 534});

        theTestList.append(567);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 990, 75, 731, 583, 382, 783, 335, 665, 572, 622, 17, 189, 817, 557, 868, 534, 567});

        theTestList.insertAt(38, 263);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 301, 228, 12, 263, 990, 75, 731, 583, 382, 783, 335, 665, 572, 622, 17, 189, 817, 557, 868, 534, 567});

        theTestList.insertAt(35, 376);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 376, 301, 228, 12, 263, 990, 75, 731, 583, 382, 783, 335, 665, 572, 622, 17, 189, 817, 557, 868, 534, 567});

        assertFalse("Fehler: Element 93 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(93));
        theTestList.append(659);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 376, 301, 228, 12, 263, 990, 75, 731, 583, 382, 783, 335, 665, 572, 622, 17, 189, 817, 557, 868, 534, 567, 659});

        theTestList.insertAt(55, 445);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 376, 301, 228, 12, 263, 990, 75, 731, 583, 382, 783, 335, 665, 572, 622, 17, 189, 817, 557, 868, 445, 534, 567, 659});

        theTestList.insertAt(39, 218);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 376, 301, 228, 12, 218, 263, 990, 75, 731, 583, 382, 783, 335, 665, 572, 622, 17, 189, 817, 557, 868, 445, 534, 567, 659});

        theTestList.insertAt(48, 542);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 376, 301, 228, 12, 218, 263, 990, 75, 731, 583, 382, 783, 335, 542, 665, 572, 622, 17, 189, 817, 557, 868, 445, 534, 567, 659});

        theTestList.append(125);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 376, 301, 228, 12, 218, 263, 990, 75, 731, 583, 382, 783, 335, 542, 665, 572, 622, 17, 189, 817, 557, 868, 445, 534, 567, 659, 125});

        theTestList.insertAt(17, 105);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 105, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 376, 301, 228, 12, 218, 263, 990, 75, 731, 583, 382, 783, 335, 542, 665, 572, 622, 17, 189, 817, 557, 868, 445, 534, 567, 659, 125});

        theTestList.append(727);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 105, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 376, 301, 228, 12, 218, 263, 990, 75, 731, 583, 382, 783, 335, 542, 665, 572, 622, 17, 189, 817, 557, 868, 445, 534, 567, 659, 125, 727});

        theTestList.insertAt(52, 208);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 105, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 376, 301, 228, 12, 218, 263, 990, 75, 731, 583, 382, 783, 335, 542, 665, 572, 208, 622, 17, 189, 817, 557, 868, 445, 534, 567, 659, 125, 727});

        assertTrue("Fehler: Element 688 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(688));
        assertFalse("Fehler: Element 27 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(27));
        theTestList.append(605);
        Tester.verifyListContents(theTestList, new int[]{903, 529, 342, 217, 17, 541, 723, 688, 767, 914, 593, 326, 696, 92, 546, 300, 334, 105, 689, 443, 109, 343, 752, 749, 433, 514, 126, 358, 47, 752, 836, 624, 463, 559, 322, 890, 376, 301, 228, 12, 218, 263, 990, 75, 731, 583, 382, 783, 335, 542, 665, 572, 208, 622, 17, 189, 817, 557, 868, 445, 534, 567, 659, 125, 727, 605});

    }

    public static void test7Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(627);
        Tester.verifyListContents(theTestList, new int[]{627});

        theTestList.insertAt(0, 602);
        Tester.verifyListContents(theTestList, new int[]{602, 627});

        theTestList.insertAt(0, 208);
        Tester.verifyListContents(theTestList, new int[]{208, 602, 627});

        theTestList.append(623);
        Tester.verifyListContents(theTestList, new int[]{208, 602, 627, 623});

        theTestList.insertAt(0, 793);
        Tester.verifyListContents(theTestList, new int[]{793, 208, 602, 627, 623});

        assertFalse("Fehler: Element 752 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(752));
        theTestList.insertAt(1, 378);
        Tester.verifyListContents(theTestList, new int[]{793, 378, 208, 602, 627, 623});

        theTestList.insertAt(0, 687);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 602, 627, 623});

        theTestList.insertAt(4, 993);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623});

        theTestList.append(699);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 699});

        theTestList.append(725);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 699, 725});

        theTestList.insertAt(10, 527);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 699, 725, 527});

        theTestList.insertAt(8, 429);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 725, 527});

        assertFalse("Fehler: Element 280 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(280));
        theTestList.append(861);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 725, 527, 861});

        theTestList.append(782);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 725, 527, 861, 782});

        theTestList.insertAt(11, 516);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 725, 516, 527, 861, 782});

        theTestList.append(401);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 725, 516, 527, 861, 782, 401});

        assertFalse("Fehler: Element 79 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(79));
        theTestList.insertAt(12, 42);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 725, 516, 42, 527, 861, 782, 401});

        assertFalse("Fehler: Element 410 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(410));
        assertTrue("Fehler: Element 623 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(623));
        theTestList.append(595);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 725, 516, 42, 527, 861, 782, 401, 595});

        theTestList.insertAt(10, 733);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 733, 725, 516, 42, 527, 861, 782, 401, 595});

        theTestList.append(832);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 733, 725, 516, 42, 527, 861, 782, 401, 595, 832});

        theTestList.insertAt(20, 413);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 733, 725, 516, 42, 527, 861, 782, 401, 595, 832, 413});

        assertTrue("Fehler: Element 782 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(782));
        assertFalse("Fehler: Element 423 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(423));
        assertFalse("Fehler: Element 920 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(920));
        theTestList.append(356);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 733, 725, 516, 42, 527, 861, 782, 401, 595, 832, 413, 356});

        theTestList.insertAt(11, 938);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 733, 938, 725, 516, 42, 527, 861, 782, 401, 595, 832, 413, 356});

        theTestList.insertAt(10, 744);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 744, 733, 938, 725, 516, 42, 527, 861, 782, 401, 595, 832, 413, 356});

        assertTrue("Fehler: Element 413 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(413));
        theTestList.append(700);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 744, 733, 938, 725, 516, 42, 527, 861, 782, 401, 595, 832, 413, 356, 700});

        theTestList.insertAt(23, 766);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 744, 733, 938, 725, 516, 42, 527, 861, 782, 401, 595, 832, 413, 766, 356, 700});

        theTestList.insertAt(16, 855);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 699, 744, 733, 938, 725, 516, 42, 855, 527, 861, 782, 401, 595, 832, 413, 766, 356, 700});

        theTestList.insertAt(9, 978);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 978, 699, 744, 733, 938, 725, 516, 42, 855, 527, 861, 782, 401, 595, 832, 413, 766, 356, 700});

        assertTrue("Fehler: Element 938 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(938));
        theTestList.insertAt(26, 508);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 978, 699, 744, 733, 938, 725, 516, 42, 855, 527, 861, 782, 401, 595, 832, 413, 766, 508, 356, 700});

        assertFalse("Fehler: Element 775 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(775));
        assertTrue("Fehler: Element 700 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(700));
        theTestList.insertAt(18, 619);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 978, 699, 744, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 766, 508, 356, 700});

        theTestList.append(718);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 978, 699, 744, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 766, 508, 356, 700, 718});

        theTestList.append(576);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 978, 699, 744, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 766, 508, 356, 700, 718, 576});

        theTestList.append(561);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 978, 699, 744, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 766, 508, 356, 700, 718, 576, 561});

        theTestList.append(132);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 602, 627, 623, 429, 978, 699, 744, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 766, 508, 356, 700, 718, 576, 561, 132});

        assertTrue("Fehler: Element 855 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(855));
        assertTrue("Fehler: Element 208 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(208));
        assertTrue("Fehler: Element 855 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(855));
        assertFalse("Fehler: Element 986 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(986));
        assertFalse("Fehler: Element 945 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(945));
        theTestList.insertAt(5, 469);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 469, 602, 627, 623, 429, 978, 699, 744, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 766, 508, 356, 700, 718, 576, 561, 132});

        theTestList.append(110);
        Tester.verifyListContents(theTestList, new int[]{687, 793, 378, 208, 993, 469, 602, 627, 623, 429, 978, 699, 744, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 766, 508, 356, 700, 718, 576, 561, 132, 110});

        theTestList.insertAt(0, 954);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 208, 993, 469, 602, 627, 623, 429, 978, 699, 744, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 766, 508, 356, 700, 718, 576, 561, 132, 110});

        theTestList.append(546);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 208, 993, 469, 602, 627, 623, 429, 978, 699, 744, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 766, 508, 356, 700, 718, 576, 561, 132, 110, 546});

        theTestList.insertAt(14, 327);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 208, 993, 469, 602, 627, 623, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 766, 508, 356, 700, 718, 576, 561, 132, 110, 546});

        assertFalse("Fehler: Element 358 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(358));
        theTestList.insertAt(4, 378);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 766, 508, 356, 700, 718, 576, 561, 132, 110, 546});

        theTestList.insertAt(30, 620);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 546});

        assertFalse("Fehler: Element 495 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(495));
        assertTrue("Fehler: Element 401 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(401));
        theTestList.append(277);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 546, 277});

        assertFalse("Fehler: Element 43 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(43));
        theTestList.append(589);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 546, 277, 589});

        theTestList.insertAt(11, 436);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 436, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 546, 277, 589});

        theTestList.insertAt(44, 290);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 436, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 546, 277, 589, 290});

        theTestList.insertAt(41, 819);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 436, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290});

        assertFalse("Fehler: Element 421 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(421));
        assertFalse("Fehler: Element 392 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(392));
        theTestList.append(460);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 436, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 460});

        assertTrue("Fehler: Element 602 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(602));
        assertTrue("Fehler: Element 516 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(516));
        assertTrue("Fehler: Element 687 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(687));
        assertFalse("Fehler: Element 297 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(297));
        theTestList.insertAt(29, 65);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 436, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 460});

        theTestList.append(783);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 436, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 460, 783});

        assertFalse("Fehler: Element 705 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(705));
        theTestList.insertAt(47, 399);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 436, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 399, 460, 783});

        theTestList.append(977);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 436, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 399, 460, 783, 977});

        theTestList.append(442);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 436, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 399, 460, 783, 977, 442});

        theTestList.insertAt(11, 27);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 399, 460, 783, 977, 442});

        theTestList.insertAt(49, 32);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 399, 32, 460, 783, 977, 442});

        theTestList.append(244);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 399, 32, 460, 783, 977, 442, 244});

        theTestList.append(909);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 399, 32, 460, 783, 977, 442, 244, 909});

        theTestList.append(180);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 399, 32, 460, 783, 977, 442, 244, 909, 180});

        theTestList.insertAt(51, 121);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 399, 32, 460, 121, 783, 977, 442, 244, 909, 180});

        theTestList.insertAt(21, 588);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 588, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 399, 32, 460, 121, 783, 977, 442, 244, 909, 180});

        theTestList.append(698);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 588, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 399, 32, 460, 121, 783, 977, 442, 244, 909, 180, 698});

        theTestList.insertAt(5, 440);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 440, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 588, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 399, 32, 460, 121, 783, 977, 442, 244, 909, 180, 698});

        theTestList.append(777);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 440, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 588, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 399, 32, 460, 121, 783, 977, 442, 244, 909, 180, 698, 777});

        theTestList.append(82);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 440, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 588, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 132, 110, 819, 546, 277, 589, 290, 399, 32, 460, 121, 783, 977, 442, 244, 909, 180, 698, 777, 82});

        theTestList.insertAt(43, 764);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 440, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 588, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 764, 132, 110, 819, 546, 277, 589, 290, 399, 32, 460, 121, 783, 977, 442, 244, 909, 180, 698, 777, 82});

        theTestList.append(321);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 440, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 588, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 764, 132, 110, 819, 546, 277, 589, 290, 399, 32, 460, 121, 783, 977, 442, 244, 909, 180, 698, 777, 82, 321});

        theTestList.append(545);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 440, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 588, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 764, 132, 110, 819, 546, 277, 589, 290, 399, 32, 460, 121, 783, 977, 442, 244, 909, 180, 698, 777, 82, 321, 545});

        theTestList.append(549);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 440, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 588, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 764, 132, 110, 819, 546, 277, 589, 290, 399, 32, 460, 121, 783, 977, 442, 244, 909, 180, 698, 777, 82, 321, 545, 549});

        theTestList.insertAt(50, 175);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 440, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 588, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 764, 132, 110, 819, 546, 277, 589, 175, 290, 399, 32, 460, 121, 783, 977, 442, 244, 909, 180, 698, 777, 82, 321, 545, 549});

        assertTrue("Fehler: Element 954 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(954));
        theTestList.insertAt(52, 572);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 440, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 588, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 764, 132, 110, 819, 546, 277, 589, 175, 290, 572, 399, 32, 460, 121, 783, 977, 442, 244, 909, 180, 698, 777, 82, 321, 545, 549});

        theTestList.append(264);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 378, 378, 440, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 588, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 764, 132, 110, 819, 546, 277, 589, 175, 290, 572, 399, 32, 460, 121, 783, 977, 442, 244, 909, 180, 698, 777, 82, 321, 545, 549, 264});

        theTestList.insertAt(3, 337);
        Tester.verifyListContents(theTestList, new int[]{954, 687, 793, 337, 378, 378, 440, 208, 993, 469, 602, 627, 623, 27, 436, 429, 978, 699, 744, 327, 733, 938, 725, 588, 516, 42, 855, 619, 527, 861, 782, 401, 595, 65, 832, 413, 620, 766, 508, 356, 700, 718, 576, 561, 764, 132, 110, 819, 546, 277, 589, 175, 290, 572, 399, 32, 460, 121, 783, 977, 442, 244, 909, 180, 698, 777, 82, 321, 545, 549, 264});

    }

    public static void test8Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 9);
        Tester.verifyListContents(theTestList, new int[]{9});

        theTestList.append(388);
        Tester.verifyListContents(theTestList, new int[]{9, 388});

        theTestList.append(990);
        Tester.verifyListContents(theTestList, new int[]{9, 388, 990});

        theTestList.insertAt(0, 553);
        Tester.verifyListContents(theTestList, new int[]{553, 9, 388, 990});

        assertFalse("Fehler: Element 474 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(474));
        theTestList.insertAt(0, 621);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 990});

        theTestList.insertAt(5, 741);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 990, 741});

        assertFalse("Fehler: Element 30 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(30));
        theTestList.append(122);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 990, 741, 122});

        theTestList.append(831);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 990, 741, 122, 831});

        theTestList.insertAt(8, 197);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 990, 741, 122, 831, 197});

        assertTrue("Fehler: Element 990 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(990));
        theTestList.append(257);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 990, 741, 122, 831, 197, 257});

        assertTrue("Fehler: Element 122 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(122));
        assertTrue("Fehler: Element 122 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(122));
        theTestList.append(882);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 990, 741, 122, 831, 197, 257, 882});

        assertTrue("Fehler: Element 741 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(741));
        theTestList.insertAt(4, 618);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 741, 122, 831, 197, 257, 882});

        assertTrue("Fehler: Element 882 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(882));
        theTestList.insertAt(10, 964);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 741, 122, 831, 197, 964, 257, 882});

        theTestList.insertAt(6, 188);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 188, 741, 122, 831, 197, 964, 257, 882});

        theTestList.append(357);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 188, 741, 122, 831, 197, 964, 257, 882, 357});

        assertFalse("Fehler: Element 667 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(667));
        theTestList.insertAt(6, 566);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 188, 741, 122, 831, 197, 964, 257, 882, 357});

        theTestList.append(207);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 188, 741, 122, 831, 197, 964, 257, 882, 357, 207});

        assertFalse("Fehler: Element 824 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(824));
        theTestList.append(786);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 188, 741, 122, 831, 197, 964, 257, 882, 357, 207, 786});

        assertTrue("Fehler: Element 741 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(741));
        theTestList.insertAt(8, 591);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 188, 591, 741, 122, 831, 197, 964, 257, 882, 357, 207, 786});

        theTestList.append(390);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 188, 591, 741, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390});

        theTestList.append(413);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 188, 591, 741, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 413});

        theTestList.insertAt(20, 864);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 188, 591, 741, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 864, 413});

        theTestList.append(297);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 188, 591, 741, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 864, 413, 297});

        assertFalse("Fehler: Element 858 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(858));
        theTestList.append(368);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 188, 591, 741, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 864, 413, 297, 368});

        assertTrue("Fehler: Element 990 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(990));
        theTestList.append(439);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 188, 591, 741, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 864, 413, 297, 368, 439});

        theTestList.append(921);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 188, 591, 741, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 864, 413, 297, 368, 439, 921});

        assertFalse("Fehler: Element 982 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(982));
        assertTrue("Fehler: Element 188 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(188));
        theTestList.insertAt(7, 484);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 484, 188, 591, 741, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 864, 413, 297, 368, 439, 921});

        theTestList.insertAt(7, 707);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 707, 484, 188, 591, 741, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 864, 413, 297, 368, 439, 921});

        theTestList.append(937);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 707, 484, 188, 591, 741, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 864, 413, 297, 368, 439, 921, 937});

        theTestList.insertAt(12, 291);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 707, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 864, 413, 297, 368, 439, 921, 937});

        theTestList.append(829);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 707, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 864, 413, 297, 368, 439, 921, 937, 829});

        assertFalse("Fehler: Element 600 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(600));
        assertFalse("Fehler: Element 453 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(453));
        assertTrue("Fehler: Element 937 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(937));
        theTestList.append(634);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 707, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 864, 413, 297, 368, 439, 921, 937, 829, 634});

        assertFalse("Fehler: Element 988 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(988));
        theTestList.insertAt(23, 905);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 707, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 905, 864, 413, 297, 368, 439, 921, 937, 829, 634});

        theTestList.insertAt(8, 66);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 707, 66, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 905, 864, 413, 297, 368, 439, 921, 937, 829, 634});

        theTestList.insertAt(9, 196);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 9, 388, 618, 990, 566, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 905, 864, 413, 297, 368, 439, 921, 937, 829, 634});

        theTestList.insertAt(2, 655);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 655, 9, 388, 618, 990, 566, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 905, 864, 413, 297, 368, 439, 921, 937, 829, 634});

        theTestList.insertAt(28, 360);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 655, 9, 388, 618, 990, 566, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 905, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634});

        theTestList.append(855);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 655, 9, 388, 618, 990, 566, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 905, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855});

        assertFalse("Fehler: Element 164 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(164));
        theTestList.append(813);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 655, 9, 388, 618, 990, 566, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 905, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813});

        assertTrue("Fehler: Element 707 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(707));
        theTestList.insertAt(7, 815);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 655, 9, 388, 618, 990, 815, 566, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 905, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813});

        assertTrue("Fehler: Element 921 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(921));
        assertTrue("Fehler: Element 741 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(741));
        theTestList.insertAt(8, 391);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 655, 9, 388, 618, 990, 815, 391, 566, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 905, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813});

        assertTrue("Fehler: Element 831 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(831));
        assertTrue("Fehler: Element 634 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(634));
        theTestList.append(901);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 655, 9, 388, 618, 990, 815, 391, 566, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 905, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901});

        assertFalse("Fehler: Element 876 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(876));
        theTestList.insertAt(29, 383);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 655, 9, 388, 618, 990, 815, 391, 566, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901});

        assertTrue("Fehler: Element 413 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(413));
        theTestList.insertAt(26, 43);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 655, 9, 388, 618, 990, 815, 391, 566, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901});

        assertFalse("Fehler: Element 504 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(504));
        theTestList.append(543);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 655, 9, 388, 618, 990, 815, 391, 566, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543});

        assertFalse("Fehler: Element 156 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(156));
        assertTrue("Fehler: Element 964 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(964));
        theTestList.append(395);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 655, 9, 388, 618, 990, 815, 391, 566, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395});

        theTestList.insertAt(10, 800);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395});

        theTestList.append(437);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395, 437});

        assertFalse("Fehler: Element 293 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(293));
        theTestList.insertAt(2, 437);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395, 437});

        theTestList.append(894);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395, 437, 894});

        theTestList.append(298);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395, 437, 894, 298});

        theTestList.insertAt(28, 52);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 52, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395, 437, 894, 298});

        theTestList.append(625);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 52, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395, 437, 894, 298, 625});

        theTestList.append(548);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 52, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395, 437, 894, 298, 625, 548});

        theTestList.append(883);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 831, 197, 964, 257, 882, 357, 207, 52, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395, 437, 894, 298, 625, 548, 883});

        theTestList.insertAt(21, 714);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 714, 831, 197, 964, 257, 882, 357, 207, 52, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395, 437, 894, 298, 625, 548, 883});

        theTestList.append(894);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 714, 831, 197, 964, 257, 882, 357, 207, 52, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395, 437, 894, 298, 625, 548, 883, 894});

        theTestList.append(660);
        Tester.verifyListContents(theTestList, new int[]{621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 714, 831, 197, 964, 257, 882, 357, 207, 52, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395, 437, 894, 298, 625, 548, 883, 894, 660});

        theTestList.insertAt(0, 539);
        Tester.verifyListContents(theTestList, new int[]{539, 621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 714, 831, 197, 964, 257, 882, 357, 207, 52, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395, 437, 894, 298, 625, 548, 883, 894, 660});

        assertTrue("Fehler: Element 634 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(634));
        assertTrue("Fehler: Element 122 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(122));
        theTestList.append(184);
        Tester.verifyListContents(theTestList, new int[]{539, 621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 714, 831, 197, 964, 257, 882, 357, 207, 52, 43, 786, 390, 905, 383, 864, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395, 437, 894, 298, 625, 548, 883, 894, 660, 184});

        assertTrue("Fehler: Element 188 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(188));
        theTestList.insertAt(37, 411);
        Tester.verifyListContents(theTestList, new int[]{539, 621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 714, 831, 197, 964, 257, 882, 357, 207, 52, 43, 786, 390, 905, 383, 864, 411, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395, 437, 894, 298, 625, 548, 883, 894, 660, 184});

        assertTrue("Fehler: Element 894 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(894));
        theTestList.append(154);
        Tester.verifyListContents(theTestList, new int[]{539, 621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 714, 831, 197, 964, 257, 882, 357, 207, 52, 43, 786, 390, 905, 383, 864, 411, 360, 413, 297, 368, 439, 921, 937, 829, 634, 855, 813, 901, 543, 395, 437, 894, 298, 625, 548, 883, 894, 660, 184, 154});

        assertTrue("Fehler: Element 864 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(864));
        theTestList.insertAt(45, 381);
        Tester.verifyListContents(theTestList, new int[]{539, 621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 714, 831, 197, 964, 257, 882, 357, 207, 52, 43, 786, 390, 905, 383, 864, 411, 360, 413, 297, 368, 439, 921, 937, 381, 829, 634, 855, 813, 901, 543, 395, 437, 894, 298, 625, 548, 883, 894, 660, 184, 154});

        theTestList.append(269);
        Tester.verifyListContents(theTestList, new int[]{539, 621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 714, 831, 197, 964, 257, 882, 357, 207, 52, 43, 786, 390, 905, 383, 864, 411, 360, 413, 297, 368, 439, 921, 937, 381, 829, 634, 855, 813, 901, 543, 395, 437, 894, 298, 625, 548, 883, 894, 660, 184, 154, 269});

        theTestList.append(769);
        Tester.verifyListContents(theTestList, new int[]{539, 621, 553, 437, 655, 9, 388, 618, 990, 815, 391, 566, 800, 707, 66, 196, 484, 188, 591, 741, 291, 122, 714, 831, 197, 964, 257, 882, 357, 207, 52, 43, 786, 390, 905, 383, 864, 411, 360, 413, 297, 368, 439, 921, 937, 381, 829, 634, 855, 813, 901, 543, 395, 437, 894, 298, 625, 548, 883, 894, 660, 184, 154, 269, 769});

    }

    public static void test9Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.append(866);
        Tester.verifyListContents(theTestList, new int[]{866});

        assertTrue("Fehler: Element 866 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(866));
        theTestList.insertAt(1, 555);
        Tester.verifyListContents(theTestList, new int[]{866, 555});

        assertFalse("Fehler: Element 687 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(687));
        theTestList.insertAt(1, 303);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 555});

        assertFalse("Fehler: Element 834 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(834));
        theTestList.insertAt(2, 804);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555});

        theTestList.append(711);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555, 711});

        assertFalse("Fehler: Element 542 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(542));
        theTestList.append(995);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555, 711, 995});

        theTestList.insertAt(5, 295);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555, 711, 295, 995});

        assertFalse("Fehler: Element 376 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(376));
        theTestList.append(585);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555, 711, 295, 995, 585});

        theTestList.append(113);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555, 711, 295, 995, 585, 113});

        theTestList.append(683);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555, 711, 295, 995, 585, 113, 683});

        assertTrue("Fehler: Element 804 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(804));
        theTestList.append(704);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555, 711, 295, 995, 585, 113, 683, 704});

        assertTrue("Fehler: Element 585 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(585));
        theTestList.append(80);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555, 711, 295, 995, 585, 113, 683, 704, 80});

        theTestList.insertAt(9, 853);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555, 711, 295, 995, 585, 113, 853, 683, 704, 80});

        theTestList.append(162);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555, 711, 295, 995, 585, 113, 853, 683, 704, 80, 162});

        assertTrue("Fehler: Element 295 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(295));
        theTestList.insertAt(11, 407);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162});

        theTestList.insertAt(4, 166);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162});

        theTestList.append(450);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450});

        assertTrue("Fehler: Element 995 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(995));
        assertFalse("Fehler: Element 240 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(240));
        theTestList.append(859);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859});

        theTestList.append(233);
        Tester.verifyListContents(theTestList, new int[]{866, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859, 233});

        theTestList.insertAt(1, 775);
        Tester.verifyListContents(theTestList, new int[]{866, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859, 233});

        theTestList.append(283);
        Tester.verifyListContents(theTestList, new int[]{866, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859, 233, 283});

        theTestList.append(746);
        Tester.verifyListContents(theTestList, new int[]{866, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859, 233, 283, 746});

        theTestList.append(481);
        Tester.verifyListContents(theTestList, new int[]{866, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859, 233, 283, 746, 481});

        theTestList.append(974);
        Tester.verifyListContents(theTestList, new int[]{866, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859, 233, 283, 746, 481, 974});

        assertFalse("Fehler: Element 933 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(933));
        theTestList.append(64);
        Tester.verifyListContents(theTestList, new int[]{866, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859, 233, 283, 746, 481, 974, 64});

        theTestList.insertAt(19, 721);
        Tester.verifyListContents(theTestList, new int[]{866, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859, 721, 233, 283, 746, 481, 974, 64});

        theTestList.append(171);
        Tester.verifyListContents(theTestList, new int[]{866, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859, 721, 233, 283, 746, 481, 974, 64, 171});

        theTestList.insertAt(1, 417);
        Tester.verifyListContents(theTestList, new int[]{866, 417, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859, 721, 233, 283, 746, 481, 974, 64, 171});

        theTestList.append(51);
        Tester.verifyListContents(theTestList, new int[]{866, 417, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859, 721, 233, 283, 746, 481, 974, 64, 171, 51});

        assertTrue("Fehler: Element 804 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(804));
        assertFalse("Fehler: Element 997 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(997));
        theTestList.insertAt(22, 928);
        Tester.verifyListContents(theTestList, new int[]{866, 417, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 746, 481, 974, 64, 171, 51});

        theTestList.append(533);
        Tester.verifyListContents(theTestList, new int[]{866, 417, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 746, 481, 974, 64, 171, 51, 533});

        assertFalse("Fehler: Element 744 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(744));
        theTestList.insertAt(31, 423);
        Tester.verifyListContents(theTestList, new int[]{866, 417, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 746, 481, 974, 64, 171, 51, 533, 423});

        assertFalse("Fehler: Element 874 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(874));
        theTestList.append(461);
        Tester.verifyListContents(theTestList, new int[]{866, 417, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 746, 481, 974, 64, 171, 51, 533, 423, 461});

        theTestList.insertAt(14, 702);
        Tester.verifyListContents(theTestList, new int[]{866, 417, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 746, 481, 974, 64, 171, 51, 533, 423, 461});

        theTestList.insertAt(0, 907);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 746, 481, 974, 64, 171, 51, 533, 423, 461});

        assertTrue("Fehler: Element 704 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(704));
        theTestList.append(872);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 746, 481, 974, 64, 171, 51, 533, 423, 461, 872});

        theTestList.append(225);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 746, 481, 974, 64, 171, 51, 533, 423, 461, 872, 225});

        theTestList.insertAt(16, 306);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 746, 481, 974, 64, 171, 51, 533, 423, 461, 872, 225});

        assertTrue("Fehler: Element 721 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(721));
        assertTrue("Fehler: Element 859 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(859));
        theTestList.insertAt(6, 837);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 837, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 746, 481, 974, 64, 171, 51, 533, 423, 461, 872, 225});

        theTestList.append(861);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 837, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 746, 481, 974, 64, 171, 51, 533, 423, 461, 872, 225, 861});

        theTestList.append(608);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 837, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 746, 481, 974, 64, 171, 51, 533, 423, 461, 872, 225, 861, 608});

        theTestList.insertAt(36, 536);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 837, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 746, 481, 974, 64, 171, 51, 533, 423, 536, 461, 872, 225, 861, 608});

        theTestList.append(127);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 837, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 746, 481, 974, 64, 171, 51, 533, 423, 536, 461, 872, 225, 861, 608, 127});

        theTestList.insertAt(41, 109);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 837, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 746, 481, 974, 64, 171, 51, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127});

        theTestList.insertAt(28, 399);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 837, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 399, 746, 481, 974, 64, 171, 51, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127});

        theTestList.insertAt(6, 889);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 889, 837, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 399, 746, 481, 974, 64, 171, 51, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127});

        theTestList.append(712);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 889, 837, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 399, 746, 481, 974, 64, 171, 51, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712});

        assertTrue("Fehler: Element 866 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(866));
        theTestList.insertAt(29, 314);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 889, 837, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712});

        theTestList.insertAt(7, 253);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 889, 253, 837, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712});

        assertFalse("Fehler: Element 935 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(935));
        assertFalse("Fehler: Element 221 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(221));
        theTestList.append(515);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 889, 253, 837, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515});

        theTestList.insertAt(6, 680);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 680, 889, 253, 837, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515});

        theTestList.insertAt(39, 238);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 680, 889, 253, 837, 555, 166, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515});

        theTestList.insertAt(12, 613);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 680, 889, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515});

        theTestList.append(589);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 680, 889, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515, 589});

        theTestList.append(509);
        Tester.verifyListContents(theTestList, new int[]{907, 866, 417, 775, 303, 804, 680, 889, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515, 589, 509});

        theTestList.insertAt(1, 929);
        Tester.verifyListContents(theTestList, new int[]{907, 929, 866, 417, 775, 303, 804, 680, 889, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515, 589, 509});

        assertTrue("Fehler: Element 775 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(775));
        theTestList.insertAt(21, 336);
        Tester.verifyListContents(theTestList, new int[]{907, 929, 866, 417, 775, 303, 804, 680, 889, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515, 589, 509});

        assertTrue("Fehler: Element 303 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(303));
        theTestList.insertAt(9, 951);
        Tester.verifyListContents(theTestList, new int[]{907, 929, 866, 417, 775, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515, 589, 509});

        theTestList.insertAt(58, 628);
        Tester.verifyListContents(theTestList, new int[]{907, 929, 866, 417, 775, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515, 589, 509, 628});

        theTestList.append(713);
        Tester.verifyListContents(theTestList, new int[]{907, 929, 866, 417, 775, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713});

        theTestList.append(990);
        Tester.verifyListContents(theTestList, new int[]{907, 929, 866, 417, 775, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713, 990});

        assertFalse("Fehler: Element 230 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(230));
        theTestList.append(223);
        Tester.verifyListContents(theTestList, new int[]{907, 929, 866, 417, 775, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713, 990, 223});

        theTestList.append(45);
        Tester.verifyListContents(theTestList, new int[]{907, 929, 866, 417, 775, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713, 990, 223, 45});

        theTestList.insertAt(1, 439);
        Tester.verifyListContents(theTestList, new int[]{907, 439, 929, 866, 417, 775, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713, 990, 223, 45});

        theTestList.insertAt(6, 693);
        Tester.verifyListContents(theTestList, new int[]{907, 439, 929, 866, 417, 775, 693, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 225, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713, 990, 223, 45});

        theTestList.insertAt(52, 219);
        Tester.verifyListContents(theTestList, new int[]{907, 439, 929, 866, 417, 775, 693, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 225, 219, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713, 990, 223, 45});

        theTestList.insertAt(51, 444);
        Tester.verifyListContents(theTestList, new int[]{907, 439, 929, 866, 417, 775, 693, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 444, 225, 219, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713, 990, 223, 45});

        theTestList.insertAt(52, 908);
        Tester.verifyListContents(theTestList, new int[]{907, 439, 929, 866, 417, 775, 693, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 444, 908, 225, 219, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713, 990, 223, 45});

        theTestList.append(263);
        Tester.verifyListContents(theTestList, new int[]{907, 439, 929, 866, 417, 775, 693, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 444, 908, 225, 219, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713, 990, 223, 45, 263});

        assertFalse("Fehler: Element 665 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(665));
        theTestList.append(226);
        Tester.verifyListContents(theTestList, new int[]{907, 439, 929, 866, 417, 775, 693, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 444, 908, 225, 219, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713, 990, 223, 45, 263, 226});

        theTestList.append(48);
        Tester.verifyListContents(theTestList, new int[]{907, 439, 929, 866, 417, 775, 693, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 444, 908, 225, 219, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713, 990, 223, 45, 263, 226, 48});

        theTestList.append(251);
        Tester.verifyListContents(theTestList, new int[]{907, 439, 929, 866, 417, 775, 693, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 444, 908, 225, 219, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713, 990, 223, 45, 263, 226, 48, 251});

        theTestList.insertAt(0, 462);
        Tester.verifyListContents(theTestList, new int[]{462, 907, 439, 929, 866, 417, 775, 693, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 444, 908, 225, 219, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713, 990, 223, 45, 263, 226, 48, 251});

        theTestList.append(350);
        Tester.verifyListContents(theTestList, new int[]{462, 907, 439, 929, 866, 417, 775, 693, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 444, 908, 225, 219, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713, 990, 223, 45, 263, 226, 48, 251, 350});

        theTestList.insertAt(1, 773);
        Tester.verifyListContents(theTestList, new int[]{462, 773, 907, 439, 929, 866, 417, 775, 693, 303, 804, 680, 889, 951, 253, 837, 555, 166, 613, 711, 295, 995, 585, 113, 853, 683, 336, 702, 306, 407, 704, 80, 162, 450, 859, 721, 233, 928, 283, 314, 399, 746, 481, 974, 64, 171, 51, 238, 533, 423, 536, 461, 872, 444, 908, 225, 219, 861, 109, 608, 127, 712, 515, 589, 509, 628, 713, 990, 223, 45, 263, 226, 48, 251, 350});

    }

    public static void test10Durchfuehren(List<Integer> theTestList) {
        assertTrue(theTestList.isEmpty());
        theTestList.insertAt(0, 619);
        Tester.verifyListContents(theTestList, new int[]{619});

        theTestList.append(168);
        Tester.verifyListContents(theTestList, new int[]{619, 168});

        assertTrue("Fehler: Element 168 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(168));
        theTestList.insertAt(2, 199);
        Tester.verifyListContents(theTestList, new int[]{619, 168, 199});

        assertTrue("Fehler: Element 168 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(168));
        theTestList.append(202);
        Tester.verifyListContents(theTestList, new int[]{619, 168, 199, 202});

        theTestList.insertAt(0, 326);
        Tester.verifyListContents(theTestList, new int[]{326, 619, 168, 199, 202});

        theTestList.insertAt(5, 747);
        Tester.verifyListContents(theTestList, new int[]{326, 619, 168, 199, 202, 747});

        theTestList.append(194);
        Tester.verifyListContents(theTestList, new int[]{326, 619, 168, 199, 202, 747, 194});

        assertTrue("Fehler: Element 202 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(202));
        assertTrue("Fehler: Element 194 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(194));
        assertFalse("Fehler: Element 228 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(228));
        assertTrue("Fehler: Element 326 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(326));
        assertFalse("Fehler: Element 431 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(431));
        assertTrue("Fehler: Element 326 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(326));
        theTestList.append(243);
        Tester.verifyListContents(theTestList, new int[]{326, 619, 168, 199, 202, 747, 194, 243});

        theTestList.append(798);
        Tester.verifyListContents(theTestList, new int[]{326, 619, 168, 199, 202, 747, 194, 243, 798});

        theTestList.insertAt(0, 76);
        Tester.verifyListContents(theTestList, new int[]{76, 326, 619, 168, 199, 202, 747, 194, 243, 798});

        theTestList.append(67);
        Tester.verifyListContents(theTestList, new int[]{76, 326, 619, 168, 199, 202, 747, 194, 243, 798, 67});

        theTestList.insertAt(6, 455);
        Tester.verifyListContents(theTestList, new int[]{76, 326, 619, 168, 199, 202, 455, 747, 194, 243, 798, 67});

        theTestList.append(895);
        Tester.verifyListContents(theTestList, new int[]{76, 326, 619, 168, 199, 202, 455, 747, 194, 243, 798, 67, 895});

        assertFalse("Fehler: Element 524 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(524));
        assertFalse("Fehler: Element 966 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(966));
        assertFalse("Fehler: Element 450 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(450));
        assertTrue("Fehler: Element 194 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(194));
        theTestList.append(289);
        Tester.verifyListContents(theTestList, new int[]{76, 326, 619, 168, 199, 202, 455, 747, 194, 243, 798, 67, 895, 289});

        assertFalse("Fehler: Element 392 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(392));
        assertTrue("Fehler: Element 798 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(798));
        theTestList.insertAt(6, 160);
        Tester.verifyListContents(theTestList, new int[]{76, 326, 619, 168, 199, 202, 160, 455, 747, 194, 243, 798, 67, 895, 289});

        theTestList.append(852);
        Tester.verifyListContents(theTestList, new int[]{76, 326, 619, 168, 199, 202, 160, 455, 747, 194, 243, 798, 67, 895, 289, 852});

        theTestList.append(644);
        Tester.verifyListContents(theTestList, new int[]{76, 326, 619, 168, 199, 202, 160, 455, 747, 194, 243, 798, 67, 895, 289, 852, 644});

        theTestList.insertAt(1, 807);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 160, 455, 747, 194, 243, 798, 67, 895, 289, 852, 644});

        assertFalse("Fehler: Element 856 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(856));
        theTestList.insertAt(13, 808);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 160, 455, 747, 194, 243, 798, 808, 67, 895, 289, 852, 644});

        assertFalse("Fehler: Element 233 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(233));
        theTestList.insertAt(8, 284);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 160, 284, 455, 747, 194, 243, 798, 808, 67, 895, 289, 852, 644});

        theTestList.insertAt(7, 96);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 194, 243, 798, 808, 67, 895, 289, 852, 644});

        theTestList.append(482);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 194, 243, 798, 808, 67, 895, 289, 852, 644, 482});

        theTestList.append(892);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 194, 243, 798, 808, 67, 895, 289, 852, 644, 482, 892});

        theTestList.insertAt(21, 287);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 194, 243, 798, 808, 67, 895, 289, 852, 644, 287, 482, 892});

        theTestList.append(383);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 194, 243, 798, 808, 67, 895, 289, 852, 644, 287, 482, 892, 383});

        theTestList.insertAt(14, 501);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 287, 482, 892, 383});

        theTestList.append(627);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 287, 482, 892, 383, 627});

        theTestList.append(251);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 287, 482, 892, 383, 627, 251});

        theTestList.append(442);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 287, 482, 892, 383, 627, 251, 442});

        theTestList.append(803);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 287, 482, 892, 383, 627, 251, 442, 803});

        assertFalse("Fehler: Element 696 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(696));
        theTestList.append(820);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 287, 482, 892, 383, 627, 251, 442, 803, 820});

        theTestList.insertAt(22, 541);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 541, 287, 482, 892, 383, 627, 251, 442, 803, 820});

        theTestList.append(188);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 541, 287, 482, 892, 383, 627, 251, 442, 803, 820, 188});

        theTestList.insertAt(25, 447);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188});

        theTestList.insertAt(12, 242);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188});

        assertFalse("Fehler: Element 987 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(987));
        assertTrue("Fehler: Element 202 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(202));
        assertFalse("Fehler: Element 731 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(731));
        assertFalse("Fehler: Element 57 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(57));
        theTestList.append(89);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 89});

        theTestList.insertAt(2, 568);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 568, 326, 619, 168, 199, 202, 96, 160, 284, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 89});

        assertFalse("Fehler: Element 788 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(788));
        theTestList.insertAt(11, 667);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 568, 326, 619, 168, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 89});

        theTestList.append(755);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 568, 326, 619, 168, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 89, 755});

        assertFalse("Fehler: Element 812 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(812));
        assertTrue("Fehler: Element 96 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(96));
        theTestList.insertAt(25, 824);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 568, 326, 619, 168, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 89, 755});

        theTestList.insertAt(2, 550);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 550, 568, 326, 619, 168, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 89, 755});

        theTestList.append(524);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 550, 568, 326, 619, 168, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 89, 755, 524});

        assertTrue("Fehler: Element 242 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(242));
        theTestList.append(219);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 550, 568, 326, 619, 168, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 89, 755, 524, 219});

        assertFalse("Fehler: Element 8 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(8));
        theTestList.insertAt(2, 77);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 77, 550, 568, 326, 619, 168, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 89, 755, 524, 219});

        theTestList.append(298);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 77, 550, 568, 326, 619, 168, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 89, 755, 524, 219, 298});

        theTestList.append(211);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 77, 550, 568, 326, 619, 168, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 89, 755, 524, 219, 298, 211});

        theTestList.insertAt(8, 416);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 77, 550, 568, 326, 619, 168, 416, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 89, 755, 524, 219, 298, 211});

        theTestList.append(587);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 77, 550, 568, 326, 619, 168, 416, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 89, 755, 524, 219, 298, 211, 587});

        theTestList.append(615);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 77, 550, 568, 326, 619, 168, 416, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 89, 755, 524, 219, 298, 211, 587, 615});

        theTestList.insertAt(41, 642);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 77, 550, 568, 326, 619, 168, 416, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 642, 89, 755, 524, 219, 298, 211, 587, 615});

        theTestList.append(996);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 77, 550, 568, 326, 619, 168, 416, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 642, 89, 755, 524, 219, 298, 211, 587, 615, 996});

        assertTrue("Fehler: Element 67 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(67));
        theTestList.append(39);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 77, 550, 568, 326, 619, 168, 416, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 642, 89, 755, 524, 219, 298, 211, 587, 615, 996, 39});

        theTestList.append(7);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 77, 550, 568, 326, 619, 168, 416, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 642, 89, 755, 524, 219, 298, 211, 587, 615, 996, 39, 7});

        assertTrue("Fehler: Element 892 nicht gefunden, sollte gefunden werden.", theTestList.hasValue(892));
        theTestList.append(275);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 77, 550, 568, 326, 619, 168, 416, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 642, 89, 755, 524, 219, 298, 211, 587, 615, 996, 39, 7, 275});

        theTestList.insertAt(44, 569);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 77, 550, 568, 326, 619, 168, 416, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 642, 89, 755, 569, 524, 219, 298, 211, 587, 615, 996, 39, 7, 275});

        theTestList.insertAt(9, 544);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 77, 550, 568, 326, 619, 168, 416, 544, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 627, 251, 442, 803, 820, 188, 642, 89, 755, 569, 524, 219, 298, 211, 587, 615, 996, 39, 7, 275});

        theTestList.insertAt(36, 461);
        Tester.verifyListContents(theTestList, new int[]{76, 807, 77, 550, 568, 326, 619, 168, 416, 544, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 461, 627, 251, 442, 803, 820, 188, 642, 89, 755, 569, 524, 219, 298, 211, 587, 615, 996, 39, 7, 275});

        theTestList.insertAt(1, 997);
        Tester.verifyListContents(theTestList, new int[]{76, 997, 807, 77, 550, 568, 326, 619, 168, 416, 544, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 461, 627, 251, 442, 803, 820, 188, 642, 89, 755, 569, 524, 219, 298, 211, 587, 615, 996, 39, 7, 275});

        assertFalse("Fehler: Element 310 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(310));
        theTestList.insertAt(11, 437);
        Tester.verifyListContents(theTestList, new int[]{76, 997, 807, 77, 550, 568, 326, 619, 168, 416, 544, 437, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 461, 627, 251, 442, 803, 820, 188, 642, 89, 755, 569, 524, 219, 298, 211, 587, 615, 996, 39, 7, 275});

        theTestList.append(708);
        Tester.verifyListContents(theTestList, new int[]{76, 997, 807, 77, 550, 568, 326, 619, 168, 416, 544, 437, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 461, 627, 251, 442, 803, 820, 188, 642, 89, 755, 569, 524, 219, 298, 211, 587, 615, 996, 39, 7, 275, 708});

        assertFalse("Fehler: Element 428 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(428));
        theTestList.insertAt(28, 548);
        Tester.verifyListContents(theTestList, new int[]{76, 997, 807, 77, 550, 568, 326, 619, 168, 416, 544, 437, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 548, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 461, 627, 251, 442, 803, 820, 188, 642, 89, 755, 569, 524, 219, 298, 211, 587, 615, 996, 39, 7, 275, 708});

        theTestList.append(467);
        Tester.verifyListContents(theTestList, new int[]{76, 997, 807, 77, 550, 568, 326, 619, 168, 416, 544, 437, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 548, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 461, 627, 251, 442, 803, 820, 188, 642, 89, 755, 569, 524, 219, 298, 211, 587, 615, 996, 39, 7, 275, 708, 467});

        theTestList.insertAt(48, 157);
        Tester.verifyListContents(theTestList, new int[]{76, 997, 807, 77, 550, 568, 326, 619, 168, 416, 544, 437, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 548, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 461, 627, 251, 442, 803, 820, 188, 642, 89, 157, 755, 569, 524, 219, 298, 211, 587, 615, 996, 39, 7, 275, 708, 467});

        assertFalse("Fehler: Element 628 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(628));
        theTestList.insertAt(12, 645);
        Tester.verifyListContents(theTestList, new int[]{76, 997, 807, 77, 550, 568, 326, 619, 168, 416, 544, 437, 645, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 548, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 461, 627, 251, 442, 803, 820, 188, 642, 89, 157, 755, 569, 524, 219, 298, 211, 587, 615, 996, 39, 7, 275, 708, 467});

        assertFalse("Fehler: Element 247 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(247));
        theTestList.append(654);
        Tester.verifyListContents(theTestList, new int[]{76, 997, 807, 77, 550, 568, 326, 619, 168, 416, 544, 437, 645, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 548, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 461, 627, 251, 442, 803, 820, 188, 642, 89, 157, 755, 569, 524, 219, 298, 211, 587, 615, 996, 39, 7, 275, 708, 467, 654});

        theTestList.insertAt(43, 309);
        Tester.verifyListContents(theTestList, new int[]{76, 997, 807, 77, 550, 568, 326, 619, 168, 416, 544, 437, 645, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 548, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 461, 627, 251, 309, 442, 803, 820, 188, 642, 89, 157, 755, 569, 524, 219, 298, 211, 587, 615, 996, 39, 7, 275, 708, 467, 654});

        theTestList.append(240);
        Tester.verifyListContents(theTestList, new int[]{76, 997, 807, 77, 550, 568, 326, 619, 168, 416, 544, 437, 645, 199, 202, 96, 160, 284, 667, 455, 747, 242, 194, 243, 501, 798, 808, 67, 895, 548, 289, 852, 644, 824, 541, 287, 482, 447, 892, 383, 461, 627, 251, 309, 442, 803, 820, 188, 642, 89, 157, 755, 569, 524, 219, 298, 211, 587, 615, 996, 39, 7, 275, 708, 467, 654, 240});

        assertFalse("Fehler: Element 198 gefunden, sollte nicht gefunden werden.", theTestList.hasValue(198));
    }
}
